classdef Create_QuenchHeater < hgsetget
    % this is a very simple class to indicate the properties of a quench heater
    % --> can still convert to struct if needed
    properties
        location = [];      % coil.nrad x coil.nax matrix filled with boolians to indicate which elements of the coil are influenced by this quench heater
        response_t = [];    % time between initiation and heating [s]
        P = [];             % power [W]
        duration = [];      % duration of heating
        working = true;     % boolean to indicate if it's working
        
    end
    
    %% Calculate power per element of coil
    
    % this is called in Creat_Coil in the Quench Heater Power funcion.
    
    methods
        function Pele = Pele(self)
            % output zeros if not working
            Pele = 0;
            if self.working
                Pele = self.P / sum(sum( self.location ));
            end
            
        end
    end
    
   %% Add axial neighbours of existing elements
%     methods
%         function addAx(self,n)
%             % go through layers
%             for i = 1:size(self.location,1)
%                 % go through elements of i'th layer
%                 for j = 1:size(self.location,2)
%                     %
%                     
%                 end
%                  
%                 
%             end
%             
%         end
%     end
end

