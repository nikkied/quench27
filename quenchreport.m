classdef quenchreport < hgsetget
    %QUENCHREPORT is used to write reports for solved mysystem classes.
    % ms.scenario and ms.shortscenario are used to distinguish
    
    properties
        
        % classes
        %mslist = {};        % list of ms'
        tex = [];           % texwriter used to create report, path, name and fid are in here
        qpl = [];           % quenchplot
        
        % other
        %peak_grouping = {}; % grouping of ms' for peak values
        zerolevel = 0;
        
    end
    
    %% Write input of ms
    methods
        function input( self, ms ) %nummeren?
            
            % set figmode to jpg
            self.tex.figmode = 'jpg';
            
            
            % write chapter
            self.tex.head('Input Overview',self.zerolevel + 0);
            
            
            % head to introduce the conductors part
            self.tex.head('Conductors',self.zerolevel + 1);
            
            %%%%%%%%%%%%%%%%%%%%%%%%%% MAKE COIL SPECIFIC --> RRR!
            % go through different types of conductor 
            condlist = ms.cond;
            for i = 1:length(condlist)
                % write head
                self.tex.head(condlist{i}.name, self.zerolevel + 2);
                
                % plot geometry of conductor
                [~,f] = condlist{i}.plotgeo(self.qpl.visible);
                self.tex.fig( f, [condlist{i}.name ' geometry'],'usetitle' )
                
                % plot critical surface of conductor
                if condlist{i}.ScFactor ~= 0;
                    [~,f] = condlist{i}.critsurf(self.qpl.visible);
                    self.tex.fig( f, [condlist{i}.name ' critsurf'],'usetitle' )
                end
                
                % place floats
                self.tex.floatbar;
                
            end
            
            
            % head to introduce the coils part
            self.tex.head('Coils',self.zerolevel + 1);
            
            % REMOVE 1 ELEMENT COILS --> UGLY HARDCODE
            coils = [];
            for i = 1:ms.ncoils
                if ms.coils{i}.nele > 1
                    coils = [coils i];
                end
            end
            
            % go through coils
            for i = coils
                self.tex.head(ms.coils{i}.name,self.zerolevel + 2)
                
                % give information about the coil
                rowl = {'Conductor type';'Radial insulation material';'Axial insulation material'};
                data = {ms.coils{i}.cond.name; [ms.coils{i}.axinsulationmat{:}]; [ms.coils{i}.radinsulationmat{:}]};
                
                % combine
                matrix = [rowl,data];
                
                % fonts
                bold = ones(size(matrix))*false;
                bold(:,1) = true;
                
                % write table
                self.tex.table(matrix,'bold',bold,'caption',['Properties of ' ms.coils{i}.name]);
                
                % plot geometry of coil
                [~,f] = ms.coils{i}.plotgeo(self.qpl.visible);
                %                 self.tex.fig( f, [ms.coils{i}.name ' geometry'],'If the red rectangle aligns with the edge of the coil, the conductor and coil insulation dimensions match with the coil dimensions.' )
                self.tex.fig( f, [ms.coils{i}.name ' geometry'],'usetitle' );
                
                % quench heaters of i'th coil
                if ~isempty(ms.coils{i}.qh);
                    self.tex.head('Quench heaters',self.zerolevel + 3);
                    
                    % create data for table
                    props = {'Response time [s]','Power [W]','Duration [s]','QH working?'};
                    data = cell(length(ms.coils{i}.qh),length(props));
                    QHnames = cell(length(ms.coils{i}.qh),1);
                    for j = 1:length(ms.coils{i}.qh)
                        qh = ms.coils{i}.qh{j};
                        data{j,1} = qh.response_t;
                        data{j,2} = qh.P;
                        data{j,3} = qh.duration;
                        if qh.working; data{j,4} = 'yes'; else data{j,4} = 'no'; end;
                        QHnames{j} = ['QH ' num2str(j)];
                    end
                    
                    % combine
                    matrix = [{''},props;QHnames,data];
                    
                    % fonts
                    bold = ones(size(matrix))*false;
                    bold(1,:) = true; bold(:,1) = true;
                    
                    % write table
                    self.tex.table(matrix,'bold',bold,'caption',['Quench heater properties of ' ms.coils{i}.name]);
                    
                end
                
                % place floats
                self.tex.floatbar;
                
            end
            
            
            % head to introduce diode types
            self.tex.head('Diodes',self.zerolevel + 1);
            
            % find different diode types
            diodenames = cell(1,ms.ncirc);
            for i = 1:ms.ncirc
                diodenames{i} = ms.circ{i}.diodetype;
            end
            [~,idx] = unique(diodenames);
            dn = diodenames(idx);
            
            % go through diodes
            for i = 1:length(dn)
                [~,f] = self.qpl.IV_diode(dn{i});
                self.tex.fig( f, ['IV_' dn{i} '_diode'],'usetitle');
            end
            
            % place floats
            self.tex.floatbar;
            
            
            % head to introduce the circuit part
            self.tex.head('Circuits',self.zerolevel + 1);
            
            % table to indicate correspondance between coils and circuits
            circnames = cell(ms.ncirc,1);
            for i = 1:ms.ncirc;
                % denote circuit names
                circnames{i} = ms.circ{i}.name;
                
                % denote coilnames
                for j = 1:ms.circ{i}.ncoils
                    coilnames{i,j} = ms.circ{i}.coils{j}.name;
                end
                
            end
            
            % combine
            matrix = [circnames,coilnames];
            
            % fonts
            bold = ones(size(matrix))*false;
            bold(:,1) = true;
            
            % write table
            self.tex.table(matrix,'bold',bold,'caption','Correspondance of circuits with their respective coils.');
            
            % table to display circuit information
            props = {'Initial current Iinitial [A]','Initial external resistance Rdi [$\Omega$]','Final external resistance Rdf [$\Omega$]','Rd delay [s]','Rd working?','nr of diodes [-]','Diode type','Circuit breaker delay [s]'};
            
            data = cell(ms.ncirc,length(props));
            for i = 1:ms.ncirc
                data{i,1} = ms.circ{i}.Iinitial;
                data{i,2} = ms.circ{i}.Rdi;
                data{i,3} = ms.circ{i}.Rdf;
                data{i,4} = ms.circ{i}.dt_Rd;
                if ms.circ{i}.Rdworking; data{i,5} = 'yes'; else data{i,5} = 'no'; end;
                data{i,6} = ms.circ{i}.ndiodes;
                data{i,7} = ms.circ{i}.diodetype;
                data{i,8} = ms.circ{i}.dt_CB;
            end
            
            % combine
            matrix = [{''},props;circnames,data];
            
            % fonts
            m = ones(size(matrix))*false;
            m(1,:) = true; bold = m; bold(:,1) = true;
            
            % write table
            self.tex.table(matrix,'bold',bold,'rotate',m*90,'caption','Circuit properties');
            
            % place floats
            self.tex.floatbar;
            
            
            % head to initiate system part
            self.tex.head('System',self.zerolevel + 1);
            
            % plotgeo
            self.tex.head('Geometry',self.zerolevel + 3);
            [~,f] = ms.plotgeo;
            self.tex.fig( f, [ms.name '_plotgeo'],'usetitle');
            
            % table for system properties
            self.tex.head('Properties',self.zerolevel + 3);
            props = {'tstart [s]'; 'tmax [s]'; 'QD voltage threshold [V]'; 'QD time delay [s]'; 'QD time [s]'};
            data = {ms.tstart; ms.tmax; ms.VQD_threshold; ms.dt_QD; ms.tQD};
            
            % combine
            matrix = [props,data];
            
            % fonts
            bold = ones(size(matrix))*false;
            bold(:,1) = true;
            
            % write table
            self.tex.table(matrix,'bold',bold,'caption',['System properties of ' ms.name ': ' ms.scenario]);
            
            % place floats
            self.tex.floatbar;
            
            %             % inductance matrix
            %             data = num2cell(ms.indMcirc);
            %             for i = 1:size(data,1)
            %                 for j = 1:size(data,2)
            %                     data{i,j} = sprintf('%1.2s',data{i,j});
            %                 end
            %             end
            %
            %             % combine
            %             matrix = [{''},circnames';circnames,data];
            %
            %             % fonts
            %             m = ones(size(matrix))*false;
            %             m(1,:) = true; bold = m; bold(:,1) = true;
            %
            %             % write table
            %             self.tex.table(matrix,'size','tiny','bold',bold,'rottable',true,'rotate',m*90,'caption','Mutual inductances of circuits with circuits [H]');
            %
            %             % place floats
            %             self.tex.floatbar;
            
            
        end
    end
    
    %% Write time dependent output of ms
    methods
        function time_output( self, ms )
            
            % set figmode to pdf
            self.tex.figmode = 'pdf';
            
%             % quench detection signal
%             %self.tex.head('Quench detection signal',self.zerolevel + 3);
%             [~,f] = self.qpl.VQD(ms);
%             figname = [ms.shortscenario '_VQD'];
%             self.tex.fig(f,figname,'usetitle');
%             self.tex.floatbar;
            
            % plot current
            %self.tex.head('Current through circuits',self.zerolevel + 3);
            [~,f] = self.qpl.I(ms);
            figname = [ms.shortscenario '_I'];
            self.tex.fig(f,figname,'usetitle');
            self.tex.floatbar;
            
            % plot hotspot temperature
            %self.tex.head('Maximum temperature of coils',self.zerolevel + 3);
            [~,f] = self.qpl.T(ms);
            figname = [ms.shortscenario '_T'];
            self.tex.fig(f,figname,'usetitle');
            self.tex.floatbar;
            
            % plot turn to turn voltage
            %self.tex.head('Maximum turn to turn voltage of coils',self.zerolevel + 3);
            [~,f] = self.qpl.Vt2t(ms);
            figname = [ms.shortscenario '_Vt2t'];
            self.tex.fig(f,figname,'usetitle');
            self.tex.floatbar;
            
            % plot layer to layer voltage
            %self.tex.head('Maximum layer to layer voltage of coils',self.zerolevel + 3);
            [~,f] = self.qpl.Vl2l(ms);
            figname = [ms.shortscenario '_Vl2l'];
            self.tex.fig(f,figname,'usetitle');
            self.tex.floatbar;
            
            % plot voltage tap voltages
            %self.tex.head('Voltages between voltage taps',self.zerolevel + 3);
            [~,f] = self.qpl.Vtaps(ms);
            figname = [ms.shortscenario '_Vtaps'];
            self.tex.fig(f,figname,'usetitle');
            self.tex.floatbar(false);
            
            % plot energy as function of time
            %self.tex.head('Energy distribution of system',self.zerolevel + 3);
            [~,f] = self.qpl.E(ms);
            figname = [ms.shortscenario '_E'];
            self.tex.fig(f,figname,'usetitle');
            self.tex.floatbar;
            
        end
    end
    
    %% Write standalone inductance matrix
    methods
        function indM(self,ms,varargin)
            % analyse varargin and define mode
            if ischar(varargin{1}) && strcmp(varargin,'excel');
                mode = 'excel';
            else
                mode = 'tex';
            end
            
            % define path
            path = [ms.syspath filesep 'InductanceM'];
            name = ['InductanceM_' ms.name];
            
            % switch for excel or tex mode
            switch mode
                case 'excel'
                    % create directory
                    if ~exist(path,'dir')
                        mkdir(path);
                    end
                    
                    % inductance matrix
                    data = num2cell(ms.indMcirc);
                    
                    % get circuitnames
                    circnames = cell(1,ms.ncirc);
                    for i = 1:ms.ncirc
                        circnames{i} = ms.circ{i}.name;
                    end
                    
                    % combine
                    matrix = [{''},circnames;circnames',data];
                    
                    % write to excel
                    filename = [path filesep name];
                    xlswrite(filename,matrix);
                    
                case 'tex'
                    % configure texwriter class
                    self.tex = texwriter;
                    self.tex.path = path;
                    self.tex.name = name;
                    self.tex.documentclass = 'standalone';
                    
                    % setup report
                    self.tex.header;
                    
                    % inductance matrix
                    data = num2cell(ms.indMcirc);
                    for i = 1:size(data,1)
                        for j = 1:size(data,2)
                            % convert to text
                            data{i,j} = sprintf('%2.2E',data{i,j});
                            
                            % clean up zeros
                            data{i,j}(end-2:end-1) = [];
                        end
                    end
                    

                    
                    % get circuitnames
                    circnames = cell(1,ms.ncirc);
                    for i = 1:ms.ncirc
                        circnames{i} = ms.circ{i}.name;
                    end
                    
                    % combine
                    matrix = [{''},circnames;circnames',data];
                    
                    % fonts
                    m = ones(size(matrix))*false;
                    m(1,:) = true; bold = m; bold(:,1) = true;
                    
                    % write table
                    self.tex.table(matrix,'size','small','bold',bold,'rotate',m*90,'caption','Mutual inductances of circuits with circuits [H]','standalone',true);
                    
                    % finish up report
                    self.tex.footer;
                    self.tex.compile;
                    
            end
            
        end
    end
    %% Write peak output of mslist
    methods
        function peak_table( self, mslist )
            % configure texwriter class
            self.tex = texwriter;
            self.tex.path = [mslist{1}.syspath filesep 'PeakReport'];
            self.tex.name = ['PeakReport_' mslist{1}.name];
            self.tex.documentclass = 'standalone';
            
            % setup report
            self.tex.header;
            
            % get a short ms (rather than mslist{1}.)
            ms = mslist{1};
            
            % get circuitnames
            circnames = cell(1,ms.ncirc);
            for i = 1:ms.ncirc
                circnames{i} = ms.circ{i}.name;
            end
            
            % get coilnames
            coilnames = cell(1,ms.ncoils);
            for i = 1:ms.ncoils
                coilnames{i} = ms.coils{i}.name;
            end
            
            % get coilnames which are relevant for Vt2t
            j = 1;
            for i = 1:ms.ncoils
                if ms.coils{i}.nax > 1
                    coilnamesVt2t{j} = ms.coils{i}.name;
                    j = j+1;
                end
            end
            
            % get coilnames which are relevant for Vt2t
            j = 1;
            for i = 1:ms.ncoils
                if ms.coils{i}.nrad > 1
                    coilnamesVl2l{j} = ms.coils{i}.name;
                    j = j+1;
                end
            end
            
            % preallocate data
            h = ms.ncirc + length(coilnames) + length(coilnamesVt2t) + length(coilnamesVl2l); w = length(mslist);
            data = [[circnames, coilnames, coilnamesVt2t, coilnamesVl2l]' cell(h,w)];
            
            % rowlabels
            idx = [1, ms.ncirc, ms.ncoils, length(coilnamesVt2t), length(coilnamesVl2l)];
            idx = cumsum(idx);
            
            rowl = cell(h,1);
            rowl{idx(1)} = '$I_{peak} [A]$';
            rowl{idx(2)} = '$T_{peak} [K]$';
            rowl{idx(3)} = '$V_{t2t, peak} [V]$';
            rowl{idx(4)} = '$V_{l2l, peak} [V]$';
            
            % columnlabels
            columnl = cell(1,w+1);
            columnl{1} = 'Coil/Circuit Name';
            for i = 1:w
                columnl{i+1} = strrep(mslist{i}.shortscenario,'_','$\_$');
            end
            
            % go through quenches and gather data
            for i = 1:w
                % get Ipeak
                Ipeak = max(cell2mat(mslist{i}.Istore));
                
                % get Tpeak
                Tpeak = max(cell2mat(mslist{i}.Tmaxstore));
                
                % get Vt2t
                Vt2t = zeros(1,length(coilnamesVt2t)); k = 1;
                for j = 1:ms.ncoils
                    a = max(cell2mat(mslist{i}.Vt2tmaxstore(:,j)));
                    if ~isempty(a)
                        Vt2t(k) = a;
                        k = k+1;
                    end
                end
                
                % get Vl2l
                Vl2l = zeros(1,length(coilnamesVl2l)); k = 1;
                for j = 1:ms.ncoils
                    a = max(cell2mat(mslist{i}.Vl2lmaxstore(:,j)));
                    if ~isempty(a)
                        Vl2l(k) = a;
                        k = k+1;
                    end
                end
                
                % combine
                data(:,1+i) = num2cell( [Ipeak, Tpeak, Vt2t, Vl2l]' );
                
            end
            
            % fix decimals
            for i = 1:size(data,1)
                for j = 2:size(data,2)
                    % format depending on value
                    if data{i,j} == 0
                        data{i,j} = '0';
                    elseif data{i,j} >= 1000 || data{i,j} < 0.1
                        data{i,j} = sprintf('%1.1E',data{i,j});
                    elseif data{i,j} < 10
                        data{i,j} = sprintf('%.1f',data{i,j});
                    else
                        data{i,j} = sprintf('%.0f',data{i,j});
                    end
                    
                    % remove zeros in E-powers (3E+005 --> 3E+5)
                    if sum(ismember(data{i,j},'E'))
                        data{i,j}(end-2:end-1) = [];
                    end
                    
                end
            end
            
            % combine
            matrix = [{''},columnl;rowl,data];
            
            % fonts
            m = false(size(matrix));
            bold = m; bold(1,:) = true;
            italic = m; italic(:,2) = true;
            rotate = bold*90;
            
            % hlines
            hline = zeros(size(matrix,1),1); thinhline = hline;
            hline(idx) = 2;
            for i = 1:length(idx)-1
                a = idx(i);
                b = idx(i+1);
                
                % place grey lines every 5 rows
                j = 5;
                while j<b-a
                    thinhline(a+j) = 1;
                    j = j+5;
                end
            end
            
            
            % vlines
            vline = ones(size(matrix,2));
            vline([2,end]) = 2;
            
            %             % standalone
            %             standalone = strcmp(self.tex.documentclass,'standalone');
            
            % write table
            self.tex.table(matrix,'bold',bold,'italic',italic,'rotate',rotate,'hline',hline,'thinhline',thinhline,'vline',vline,'standalone',true) % varargin?
            
            % finish up report
            self.tex.footer;
            self.tex.compile;
            
        end
    end
    
    %% Final energy breakdown
    methods
        function energy_table( self,mslist )
            % configure texwriter class
            self.tex = texwriter;
            self.tex.path = [mslist{1}.syspath filesep 'EnergyReport'];
            self.tex.name = ['EnergyReport' mslist{1}.name];
            self.tex.documentclass = 'standalone';
            
            % setup report
            self.tex.header;
            
            % get a short ms (rather than mslist{1}.)
            ms = mslist{1};
            
            % get circuitnames
            circnames = cell(1,ms.ncirc);
            for i = 1:ms.ncirc
                circnames{i} = ms.circ{i}.name;
            end
            
            % get coilnames
            coilnames = cell(1,ms.ncoils);
            for i = 1:ms.ncoils
                coilnames{i} = ms.coils{i}.name;
            end
            
            % get circuitnames which are relevant for Emag initial
            j = 1; Emagi_rc = []; circnamesEmagi = {};
            for i = 1:ms.ncirc
                if ms.circ{i}.Iinitial > 0
                    circnamesEmagi{j} = ms.circ{i}.name;
                    Emagi_rc = [Emagi_rc i];
                    j = j+1;
                end
            end
            
            % get circuitnames which are relevant for Edisdr
            j = 1; Edisdr_rc = []; circnamesEdisdr = {};
            for i = 1:ms.ncirc
                if ms.circ{i}.Rdworking
                    circnamesEdisdr{j} = ms.circ{i}.name;
                    Edisdr_rc = [Edisdr_rc i];
                    j = j+1;
                end
            end
            
            % get circuitnames which are relevant for Edisdio
            j = 1; Edisdio_rc = []; circnamesEdisdio = {};
            for i = 1:ms.ncirc
                if ms.circ{i}.ndiodes ~= 0
                    circnamesEdisdio{j} = ms.circ{i}.name;
                    Edisdio_rc = [Edisdio_rc i];
                    j = j+1;
                end
            end
            
            % get coilnames which are relevant for final thermal energy
            j = 1; Eth_rc = []; coilnamesEth = {};
            for i = 1:ms.ncoils
                if ms.Ethstore{end}(i) ~= 0
                    coilnamesEth{j} = ms.coils{i}.name;
                    Eth_rc = [Eth_rc i];
                    j = j+1;
                end
            end
            
            % get coilnames which are relevant for final thermal energy
            j = 1; Edisqh_rc = []; coilnamesEdisqh = {};
            for i = 1:ms.ncoils
                if ms.Edisqhstore{end}(i) ~= 0
                    coilnamesEdisqh{j} = ms.coils{i}.name;
                    Edisqh_rc = [Edisqh_rc i];
                    j = j+1;
                end
            end
            
            
            % rowlabels
            idx = [1, length(Emagi_rc), ms.ncirc, length(Edisdr_rc),  length(Edisdio_rc), length(Eth_rc), length(Edisqh_rc)];
            idx = cumsum(idx);
            
            % preallocate data
            h = idx(end)-1; w = length(mslist);
            data = [[circnamesEmagi, circnames, circnamesEdisdr, circnamesEdisdio, coilnamesEth, coilnamesEdisqh]' cell(h,w)];
            
            rowl = cell(h,1);
            rowl{idx(1)} = '$E_{mag,i} [J]$';
            rowl{idx(2)} = '$E_{mag,f} [J]$';
            rowl{idx(3)} = '$E_{dis,Rd} [J]$';
            rowl{idx(4)} = '$E_{dis,diodes} [J]$';
            rowl{idx(5)} = '$E_{thermal,f} [J]$';
            rowl{idx(6)} = '$E_{dis,QH} [J]$';
            
            % columnlabels
            columnl = cell(1,w+1);
            columnl{1} = 'Coil/Circuit Name';
            for i = 1:w
                columnl{i+1} = strrep(mslist{i}.shortscenario,'_','$\_$');
            end
            
            % go through quenches and gather data
            for i = 1:w
                % get initial magnetic energy
                Emagi = mslist{i}.Emagstore{1}(Emagi_rc);
                
                % get final magnetic energy
                Emagf = mslist{i}.Emagstore{end};
                
                % get energy dissipated in dumpresistors
                Edisdr = mslist{i}.Edisdrstore{end}(Edisdr_rc);
                
                % get energy dissipated in diodes
                Edisdio = mslist{i}.Edisdiostore{end}(Edisdio_rc);
                
                % get thermal energy of coils
                Eth = mslist{i}.Ethstore{end}(Eth_rc);
                
                % get energy dissipated in quench heaters
                Edisqh = mslist{i}.Edisqhstore{end}(Edisqh_rc);
                
                % combine
                data(:,1+i) = num2cell( [Emagi, Emagf, Edisdr, Edisdio, Eth, Edisqh]' );
                
            end
            
            %             % find trivial rows
            %             keep = true(h,1);
            %             A = cell2mat(data(:,2:end));
            %             for i = 1:h
            %                 if nnz(A(i,:)) == 0;
            %                     keep(i) = false;
            %                 end
            %             end
            
            % fix decimals
            for i = 1:size(data,1)
                for j = 2:size(data,2)
                    % format depending on value
                    if data{i,j} == 0
                        data{i,j} = '0';
                    elseif data{i,j} >= 1000 || data{i,j} < 0.1
                        data{i,j} = sprintf('%1.1E',data{i,j}); 
                    elseif data{i,j} < 10
                        data{i,j} = sprintf('%.1f',data{i,j});
                    else
                        data{i,j} = sprintf('%.0f',data{i,j});
                    end
                    
                    % remove zeros in E-powers (3E+005 --> 3E+5)
                    if sum(ismember(data{i,j},'E'))
                        data{i,j}(end-2:end-1) = [];
                    end
                        
                end
            end
            
            % combine
            matrix = [{''},columnl;rowl,data];
            
            %             % remove trivial rows
            %             keep = [true;keep];
            %             matrix = matrix(keep,:);
            %             idx2 = false(h+1,1); idx2(idx) = true;
            %             idx = find(idx2(keep));
            
            % fonts
            m = ones(size(matrix))*false;
            bold = m; bold(1,:) = true;
            italic = m; italic(:,2) = true;
            rotate = bold*90;
            
            % hlines
            hline = zeros(size(matrix,1),1); thinhline = hline;
            hline(idx) = 2;
            for i = 1:length(idx)-1
                a = idx(i);
                b = idx(i+1);
                
                % place grey lines every 5 rows
                j = 5;
                while j<b-a
                    thinhline(a+j) = 1;
                    j = j+5;
                end
            end
            
            
            % vlines
            vline = ones(size(matrix,2));
            vline([2,end]) = 2;
            
            % standalone
            standalone = strcmp(self.tex.documentclass,'standalone');
            
            % write table
            self.tex.table(matrix,'bold',bold,'italic',italic,'rotate',rotate,'hline',hline,'thinhline',thinhline,'vline',vline,'standalone',standalone) % varargin?
            
            % finish up report
            self.tex.footer;
            self.tex.compile;
            
        end
    end
    
%     methods
%         function mutual_inductanceM(self,ms) % of gewoon indM.mat?
%             % inductance matrix
%             data = num2cell(ms.indMcirc);
%             for i = 1:size(data,1)
%                 for j = 1:size(data,2)
%                     data{i,j} = sprintf('%1.2s',data{i,j});
%                 end
%             end
%             
%             % combine
%             matrix = [{''},circnames';circnames,data];
%             
%             % fonts
%             m = ones(size(matrix))*false;
%             m(1,:) = true; bold = m; bold(:,1) = true;
%             
%             % write table
%             self.tex.table(matrix,'size','tiny','bold',bold,'rottable',true,'rotate',m*90,'caption','Mutual inductances of circuits with circuits [H]');
%             
%             % place floats
%             self.tex.floatbar;
%             
%             
%         end
%     end
    
    %% 'One quench' report
    methods
        function one( self, ms )
            % configure texwriter class
            self.tex = texwriter;
            self.tex.path = ms.path;
            self.tex.name = ['QuenchReport_' ms.name '_' ms.shortscenario];
            self.tex.dpi = 300;
            
            % setup report
            self.tex.header;
            self.tex.toc;
            
            % include input-chapter
            self.input(ms);
            
            % include output
            self.tex.head('Simulation Results',self.zerolevel + 0);
            self.time_output(ms);
            
            % finish up
            self.tex.footer;
            self.tex.compile;
            
        end
    end
    
    
    %% 'short' report
    methods
        function short( self, mslist )
            ms = mslist{1};
            
            % configure texwriter class
            self.tex = texwriter;
            self.tex.path = [ms.syspath filesep 'ShortReport'];
            self.tex.name = ['ShortReport' mslist{1}.name];
            self.tex.dpi = 300;
            
            % setup report
            self.tex.header;
            self.tex.toc;
            
            % include output
            for i = 1:length(mslist)
                self.tex.head(mslist{i}.scenario,self.zerolevel + 0);
                self.tex.write(['%' mslist{i}.shortscenario]);
                self.time_output(mslist{i});
            end
            
            % finish up
            self.tex.footer;
            self.tex.compile;
            
        end
    end
    
    %% Create report
    methods
        function create( self, input, t_output, peak_output )
            
            % setup texwriter class
            tex = [];
            
            
            
        end
    end
    
    %     %% standalone peak report (~= A4 ?)
    %     methods
    %         function peak_report( self, mslist )
    %             % configure texwriter class
    %             self.tex = texwriter;
    %             self.tex.path = [mslist{1}.syspath filesep 'PeakReport'];
    %             self.tex.name = ['PeakReport_' mslist{1}.name];
    %             self.tex.documentclass = 'standalone';
    %
    %             % setup report
    %             self.tex.header;
    %
    %             % table
    %             self.peak_table(mslist);
    %
    %             % finish up
    %             self.tex.footer;
    %             self.tex.compile;
    %
    %         end
    %     end
    
end

