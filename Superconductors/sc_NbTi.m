classdef sc_NbTi < hgsetget
    
    %% Description:
    % Calculates the critical current density of NbTi using the Bottura
    % formula. The initial parameters can be set using two standard
    % variants: the commonly used parameters for large hadron collider
    % cables (LHC) and the original values measured at University of Twente
    % (Twente). It must be noted that the formulas used are not accurate in
    % the low field region below 3T.
    
    % References:
    % M.S. Lubell: IEEE Transactions on Magnetics, Vol. Mag-19, No. 3, May 1983:
    % "Empirical scaling formulas for critical current and critical fields for commercial NbTi"
    
    % L. Bottura, CERN, LHC-Division: IEEE Transactions on applied superconductivity,
    % Vol. 10, No. 1, March 2000: "A Practical Fit for the Critical Surface of NbTi"
    
    % Heat capacity from cryocomp.
    
    %% Properties
    properties
        % material
        material = 'NbTi';
        variant = [];
        
        % fitting parameters
        C0 = 0; % [T]
        alfa = 0;
        beta = 0;
        gamma = 0;
        n = 0;
        
        % materialistic parameters
        Bc20 = 0; % [T] upper critical magnetic flux at 0K
        Tc0 = 0; % [K] critical temperature at zero magnetic flux
        Jref = 0; % [A/mm^2]critical current density at 4.2K and 5T
        
        % environmental parameters
        T = 0;
        B = 0;
        
        % niobium titanium
        NbTi_Tinterp = [4:5:299,300];
        NbTi_HeatCapacity = [0.5230,3.0000,8.6800,18.5000,33.7000,53.5000,78.5000,105.0000,126.0000,146.0000,170.0000,195.0000,217.0000,236.0000,253.0000,267.0000,283.0000,298.0000,305.0000,307.0000,311.0000,316.0000,322.0000,328.0000,334.0000,341.0000,348.0000,355.0000,362.0000,369.0000,373.0000,377.0000,381.0000,384.0000,387.0000,390.0000,393.0000,395.0000,397.0000,400.0000,402.0000,405.0000,407.0000,409.0000,411.0000,413.0000,415.0000,417.0000,418.0000,420.0000,421.0000,422.0000,423.0000,423.0000,424.0000,425.0000,425.0000,425.0000,426.0000,426.0000,426.0000];
        density = 6550; %[kg/m^3];
    end
    
    %% Variants
    methods(Static)
        function v = getvariants
            v = {'Twente','LHC'};
        end
    end
    
    %% Set variant
    methods
        function setvariant(self,variant)
            switch variant
                case 'Twente'
                    self.setTwente;
                case 'LHC'
                    self.setLHC;
                otherwise
                    error('Variant not recognized');
            end
        end
    end
    
    %% load parameters as provided by Bottura see reference
    methods
        function setTwente(self)
            % set material variant
            self.variant = 'Twente';
            
            % fitting parameters
            self.C0 = 27.04; % [T]
            self.alfa = 0.57;
            self.beta = 0.9;
            self.gamma = 2.32;
            self.n = 1.7;
            
            % materialistic parameters
            self.Bc20 = 14.5; % [T] upper critical magnetic flux at 0K
            self.Tc0 = 9.2; % [K] critical temperature at zero magnetic flux
            self.Jref = 3000; % [A/mm^2]critical current density at 4.2K and 5T
        end
    end
    
    %% load parameters typically used for LHC wires
    methods
        function setLHC(self)
            % set material variant
            self.variant = 'LHC';
            
            % fitting parameters
            self.C0 = 31.4; % [T]
            self.alfa = 0.63;
            self.beta = 1.0;
            self.gamma = 2.3;
            self.n = 1.7;
            
            % materialistic parameters
            self.Bc20 = 14.5; % [T] upper critical magnetic flux at 0K
            self.Tc0 = 9.2; % [K] critical temperature at zero magnetic flux
            self.Jref = 3000; % [A/mm^2]critical current density at 4.2K and 5T
        end
    end
    
        %% load parameters typically used for LHC wires
    methods
        function setCustomFit(self) % Custom fit, made 28/8/15, to approximate the wire properties used for the Tim+Alexey coil
            % set material variant
            self.variant = 'Custom fit';
            
            % fitting parameters
            self.C0 = 31.4; % [T]
            self.alfa = 0.63;
            self.beta = 1.0;
            self.gamma = 2.3;
            self.n = 1.7;
            
            % materialistic parameters
            self.Bc20 = 14.5; % [T] upper critical magnetic flux at 0K
            self.Tc0 = 9.2; % [K] critical temperature at zero magnetic flux
            self.Jref = 2383; % [A/mm^2]critical current density at 4.2K and 5T
        end
    end
    
    %% calculate non-Cu Jc
    methods
        function Jc = Jcnoncu(self,varargin)
            % check number of input arguments
            if length(varargin)~=2 && ~isempty(varargin)
                error('Number of input arguments must be 0 or 2');
            end
            
            % allow user to input B and T directly into the formula
            if length(varargin)==2;
                self.B = varargin{1};
                self.T = varargin{2};
            end
            
            % calculate scaled values
            t = self.T/self.Tc0;
            Bc2 = self.Bc20 * (1-t.^self.n); % Lubell's equation
            b = self.B./Bc2;
            
            % bottura's equation
            jc = (self.C0./self.B).*(b.^self.alfa).*(1-b).^self.beta.*(1-t.^self.n).^self.gamma;
            
            % calculate current density
            Jc = jc*self.Jref;
  
            % do not allow critical currents lower than zero
            Jc(self.B>Bc2) = 0;
            Jc(self.B==0) = Inf;
    
        end
    end
    
    %% Calculate heat capacity
    methods
        function Cp = calcCp(self,T)
            Cp = interp1(self.NbTi_Tinterp,self.NbTi_HeatCapacity,T,'linear','extrap');
        end
    end
    
    %% Critical field and temperature
    methods
        % bcritical
        function B = Bc2(self)
            B = self.Bc20;
        end
        
        % Tcritical
        function T = Tc2(self)
            T = self.Tc0;
        end
    end
    
end

