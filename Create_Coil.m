classdef Create_Coil < hgsetget
    
    % This class creates the coils for your system. 
    
    
    %% User input settings
    properties
        % name
        name = [];
        dir = 1;        % winding direction
        system = [];    % dependency with system
        circ = [];      % dependency with circuit
                        % @Rosalinde is given a Create_Circuit object in
                        % Create_Circuit
        cond = [];      % dependency with conductor
        qh = {};        % dependency with quench heaters
        tlinks = {};      % dependency with links
        heliumbaths = {}; % dependency with helium baths
        
        Tinitial = 4.2; % initial temperature [K]
         
        % coil properties (with known nax, nrad, conductor dimensions and
        % coil dimensions the system is overdefined > warning is given for bad input)
        axrange = [];   % [Z1,Z2]
        radrange = [];  % [R1,R2]
        
        nax = [];       % number of turns in one layer
        nrad = [];      % number of layers
            
        % voltage taps location
        Vtapspos = [];          % n * 2 matrix with coordinates of n'th voltage tap on n'th row
        Vtapsmap = [];          % nrad * nax matrix with boolians for voltage taps
        
        % insulation (this should be the insulation that is not a property of
        % the conductor, but of the coil. For instance glass fiber epoxy).
        % this is insulation between layers for instance
        axinsulationmat = {};
        axinsulationthickness = 0;
        
        radinsulationmat = {};
        radinsulationthickness = 0; 
        
        radcoilinsthickness = [];% insulation surrounding the coil in rad direction 
        radcoilinsmat = {};     % material of rad insulation
        axcoilinsthickness = [];% insulation surrounding the coil in ax direction
        axcoilinsmat = {};      % material of ax insulation
        
        usecableprops = false;  % flag which, if set to true, allows you to use thermal conductivity for cable as a whole instead of averaged over components 
                                % (useful if internal structure significantly influences thermal conductivity)
                                % used property will be taken from matprops in case 'Cablerad' and 'Cableax'
                                % !! adjusted thermal conductivity is only used for krad and kax!!
                                 
    end
    
    %% Static coil properties
    properties    
        
        coilnr = [];    % this coil can be found here: system.coils{ coilnr }
        
        % areas in radial and axial direction
        Aax = [];       %m^2
        Arad = [];      %m^2
        
        % dimension matrices 
        Rinner = [];    %m
        Router = [];    %m
        Rcenter = [];   %m
        Zlow = [];      %m  
        Zhigh = [];     %m
        Zcenter = [];   %m
        
        azicondL = [];      % conductor length in azimuthal direction (nrad*nax) matrix
        azicondLline = [];  % conductor length in azimuthal direction (array of length nele)
        azicondLmean = [];  % average conductor length in azimuthal direction (array of length nele-1)
        
        % interpolation array for temperature
        Tinterp = [];   %K
                
        % thermal conductivity
        kazi = [];      %W/m.K
        Kazi = [];      %W/m
        krad = [];      %W/m^2.K
        Krad = [];      %W/m
        kax = [];       %W/m^2.K
        Kax = [];       %W/m
        
        % heat capacity
        Cpl = [];       %J/m.K
        
        % magnetic field maps
        Bri = {};       %T/A
        Bzi = {};       %T/A
        
        % deviation of Jc for each turn
        Jc_deviation = 0;

    end
    
    %% Dynamic coil properties
    properties
        Bupdatereq = false; % update B flag
        T = [];         % temperature of each element [K]
        Cp = [];        % heat capacity [J/K]
        ER = [];        % electrical resistivity [Ohm]
        EReff = [];     % effective electrical resistivity [Ohm]
        Isc = [];       % current through SC of element [A]
        Inc = [];       % current through NC of element [A]
        Bmag = [];      % magnetic field magnitude on all elements [T]
        Vnz = [];       % NZvoltage over each individual element [V]
        Vind = [];      % induced voltage due to dIdt over each individual element [V]
        V = [];         % sum of Vnz and Vind [V]
        Vcs = [];       % cumsum of V [V]
        P = [];         % power dissipation in all elements [W]
        Q = [];         % heat transfer in all elements [W]
        dTdt = [];      % temperature rate of change [K/s]
        Eth = [];       % thermal energy of elements [J]
        Qlink = [];     % thermal energy from link [W]
        Qbath = [];     % thermal energy from bath [W] (should be zero or negative!)
        % @Rosalinde 07/09/2015
        Vindcs = [];    % cumsum of Vinf [V]
        Vnzcs = [];     % cumsum of Vnz [V]
        % end @Rosalinde
    end
    
    %% Set the conductor for this coil    
    methods
        function setcond(self,cond)
            % add coil to conductor
            % cond.coil = self;
            
            % set conductor in self.cond
            self.cond = cond;
            
        end
    end
    
    
    %% Calculate field contribution of this coil on given elements per Amp // Reply to circuit when asked for my field on a set of elements
    methods
        function [Br,Bz] = calcB(self,r,z)
            % call soleno to calculate field [Tesla/A]            
            
            [Br,Bz] = soleno_calcB(r,z,self.radrange(1),self.radrange(2),self.axrange(1),self.axrange(2),self.nele*1,5);
            Br = Br * self.dir; % @Rosalinde -> direction of current
            Bz = Bz * self.dir; % @Rosalinde -> direction of current
        end
    end
    
    %% Do everything that has to be done before calculating
    
    % this function is called by the initialize of the Create_System class.
    
    methods
        function initialize(self)
            jprintf(self.system.printwindow,['Initializing ' self.name '\n']);
             
            % create interpolation variables
            self.Tinterp = 3:300;
            
            % initialize conductor if it has not been initiated by another
            % coil
            if isempty(self.cond.A)
                self.cond.initialize
            end
            
            % preallocate matrices
            matrix = zeros(self.nrad,self.nax);
            self.Rinner = matrix;
            self.Router = matrix;
            self.Zlow = matrix;
            self.Zhigh = matrix;
            
            % set initial conditions
            self.T = self.Tinitial * ones(self.nrad,self.nax);
            
            %% Dimensions
            % calculate minimal and maximal values of R and Z
            for i = 1:self.nrad
                self.Rinner(i,:) = self.radrange(1) + (i-1)*(self.cond.radL + sum(self.cond.radinsulationthickness) + sum(self.radinsulationthickness));
                self.Router(i,:) = self.radrange(1) + i*(self.cond.radL + sum(self.cond.radinsulationthickness) + sum(self.radinsulationthickness));
            end
            for i = 1:self.nax
                self.Zlow(:,i) = self.axrange(1) + (i-1)*(self.cond.axL + sum(self.cond.axinsulationthickness) + sum(self.axinsulationthickness));
                self.Zhigh(:,i) = self.axrange(1) + i*(self.cond.axL + sum(self.cond.axinsulationthickness) + sum(self.axinsulationthickness));
            end
            
            % warn user if coil/conductor dimensions are not properly defined
            if abs( self.Router(end,1) - self.radrange(2) ) > 1E-3
                warning(['Radial dimensions for coil "' self.name '" (or its conductor) seem to be impropely defined'])
            end
            if abs( self.Zhigh(1,end) - self.axrange(2) ) > 1E-3
                warning(['Axial dimensions for coil "' self.name '" (or its conductor) seem to be impropely defined'])
            end
            
            % calculate avarage values R and Z
            self.Zcenter = (self.Zlow + self.Zhigh)/2;
            self.Rcenter = (self.Rinner + self.Router)/2;
            
            % calculate length of each turn
            self.azicondL = 2*pi*self.Rcenter;
            
            % calculate mean conductor length for consecutive turns
            self.azicondLline = mat2line(self.azicondL);
            if length(self.azicondLline)==1; % make sure one-element coils are not used in 'diff' function
                self.azicondLmean = self.azicondLline; 
            else
                self.azicondLmean = self.azicondLline(1:end-1) + diff(self.azicondLline)/2; 
            end
            
            %% Areas            
            % preallocate contact areas
            self.Arad = zeros(self.nrad-1,self.nax);
            self.Aax = zeros(self.nrad,self.nax-1);
            
            % calculate contact areas
            for i = 1:(self.nrad-1)
                self.Arad(i,:) = ones(1,self.nax) * 2*pi*self.Router(i,1)*self.cond.axL;
            end
            for i = 1:(self.nrad)
                self.Aax(i,:) = ones(1,self.nax-1) * pi*(self.Router(i,1)^2 - self.Rinner(i,1)^2);
            end
            
            %% Thermal conductivities
            
            % when you only have one element in a coil, krad and kax return
            % NaN as vaues! Why???
            
            % calculate k along length of conductor [W/m.K] --> multiply by area and
            % divide by l for K
            self.kazi = zeros(1,length(self.Tinterp));
            self.kazi = self.kazi + matprops(self.cond.MatrixMaterial,'TC',self.Tinterp) * self.cond.MatrixFactor;
            self.kazi = self.kazi + matprops(self.cond.ScMaterial,'TC',self.Tinterp) * self.cond.ScFactor;
            self.kazi = self.kazi + matprops(self.cond.StabMaterial,'TC',self.Tinterp) * self.cond.StabFactor;
            self.kazi = self.kazi + matprops(self.cond.StabMaterial2,'TC',self.Tinterp) * self.cond.StabFactor2;
            
            % @Rosalinde 05/09/2015
            if self.usecableprops == false;
                kradaverage = self.kazi;
                kaxaverage = self.kazi;
            elseif self.usecableprops == true;
                kradaverage = matprops('Cablerad','TC',self.Tinterp);
                kaxaverage = matprops('Cableax','TC',self.Tinterp);
            end
            % end @Rosalinde
                       
            % calculate k between adjacent conductors in radial direction 
            % (multiply by area for K)
            rrad = zeros(1,length(self.Tinterp));
            
            % calculate and add contribution of conductor
            rrad = rrad + self.cond.radL ./ kradaverage; % @Rosalinde 05/09/2015 -> self.kazi changed into kradaverage
            
            % calculate and add contribution of conductor insulation
            for i = 1:length(self.cond.radinsulationmat);
                if matprops(self.cond.radinsulationmat{i},'TC',self.cond.Tinterp) ~= 0
                    rrad = rrad + self.cond.radinsulationthickness(i) ./ matprops(self.cond.radinsulationmat{i},'TC',self.cond.Tinterp);
                end
            end

            % calculate and add contribution of coil insulation
            for i = 1:length(self.radinsulationmat);
                if matprops(self.radinsulationmat{i},'TC',self.Tinterp)~= 0
                    rrad = rrad + self.radinsulationthickness(i) ./ matprops(self.radinsulationmat{i},'TC',self.Tinterp);
                end
            end
            
            % invert to obtain thermal conductivity, rather than thermal resistivity
            self.krad =  1 ./ rrad; %[W/m^2.K] --> multiply by area for K
            
            % calculate k between adjacent conductors in axial direction 
            rax = zeros(1,length(self.Tinterp));
            
            % calculate and add contribution of conductor
            rax = rax + self.cond.axL ./ kaxaverage; % @Rosalinde 05/09/2015 -> self.kazi changed into kaxaverage
            
            % calculate and add contribution of conductor insulation
            for i = 1:length(self.cond.axinsulationmat);
                if matprops(self.cond.axinsulationmat{i},'TC',self.cond.Tinterp) ~= 0
                    rax = rax + self.cond.axinsulationthickness(i) ./ matprops(self.cond.axinsulationmat{i},'TC',self.cond.Tinterp);
                end
            end
            % calculate and add contribution of coil insulation
            for i = 1:length(self.axinsulationmat);
                if matprops(self.axinsulationmat{i},'TC',self.Tinterp)
                    rax = rax + self.axinsulationthickness(i) ./ matprops(self.axinsulationmat{i},'TC',self.Tinterp);
                end
            end
            
            % invert to obtain thermal conductivity, rather than thermal resistivity
            self.kax =  1 ./ rax; %[W/m^2.K] --> multiply by area for K
                                    
            %% Heat capacities
            % add heat capacity of coil insulation to heat capacity of
            % conductor and store in self
            Cpins = 0;
            if ~isempty(self.axinsulationmat)
                for i = 1:length(self.axinsulationmat)
                    Cpins = Cpins + matprops(self.axinsulationmat{i},'SH',self.Tinterp) .* matprops(self.axinsulationmat{i},'Ro',self.Tinterp) * self.cond.radL * self.axinsulationthickness(i);
                end
            end
            if ~isempty(self.radinsulationmat)
                for i = 1:length(self.radinsulationmat)
                    Cpins = Cpins + matprops(self.radinsulationmat{i},'SH',self.Tinterp) .* matprops(self.radinsulationmat{i},'Ro',self.Tinterp) * (self.cond.axL + sum(self.radinsulationthickness)) * self.radinsulationthickness(i);
                end
            end
            
            % Calculate heat capacity per meter of conductor plus coil insulation
            self.Cpl = self.cond.Cpl + Cpins;
            
            %% Setup Bi matrix [T/A]
            % this produces two cells (length = #circuits) filled with matrices (nrad*nax) 
            % that give the field in radial and axial direction respectively on this coils 
            % elements per Amp�re in the corresponding circuit
            
            % go through circuits
            for i = 1:self.system.ncirc
                % ask circuit for its magnetic field contribution per Amp
                % running through it
                [self.Bri{i},self.Bzi{i}] = self.system.circ{i}.calcB(self.Rcenter,self.Zcenter);
            end
            
            %% Setup Jc deviation matrix
            self.Jc_deviation = 1 + (rand(self.nax, self.nrad) - 0.5)/100; % one percent deviation
            
            %% Setup voltage taps positions
            self.Vtapsmap = false * ones(self.nrad,self.nax);
            for i = 1:size(self.Vtapspos,1)
                self.Vtapsmap(self.Vtapspos(i,1),self.Vtapspos(i,2)) = true;
            end
                    
        end
    end
    
    %% Calculate thermal conductivity [W/dK] array / matrices for given temperature
    
    % uses the kazi, krad, kax calculated in initialize.
    
    methods
        function calcK(self)
            if self.nele > 1
                % K along length of conductor
                Tline = mat2line(self.T); Tmean = Tline(1:end-1) + diff(Tline)/2;
                self.Kazi = lininterp1f_p(self.Tinterp,self.kazi,Tmean,true) .* self.cond.A ./ self.azicondLmean; %W/K
                
                % Krad
                Tmean = self.T(1:end-1,:) + diff(self.T,1,1)/2;
                self.Krad = lininterp1f_p(self.Tinterp,self.krad,Tmean,true) .* self.Arad; %W/K
                
                % Kax
                Tmean = self.T(:,1:end-1) + diff(self.T,1,2)/2;
                self.Kax = lininterp1f_p(self.Tinterp,self.kax,Tmean,true) .* self.Aax; %W/K
                
            else
                % for 'coils' with only 1 turn 
                self.Kazi = lininterp1f_p(self.Tinterp,self.kazi,self.T,true) .* self.cond.A ./ self.azicondLmean; %W/K
                self.Krad = lininterp1f_p(self.Tinterp,self.krad,self.T,true) .* self.Arad; %W/K
                self.Kax = lininterp1f_p(self.Tinterp,self.kax,self.T,true) .* self.Aax; %W/K
                
            end     
%             
%             if isempty(self.tlinks) ~= 1
%                 for i = 1:length(self.tlinks)
%                     self.tlinks{i}.getTemp;
%                     self.tlinks{i}.calcK;
%                 end
%             end
            
        end
    end
    
    %% Calculate heat capacity for known temperatures
    methods
        function calcCp(self)
            self.Cp = lininterp1f_p(self.Tinterp,self.Cpl,self.T,true) .* self.azicondL; %J/K
        end
    end
    
    %% Calculate electrical resistivity for known temperatures (using non-superconducting material properties)
    methods
        function calcER(self)
            self.ER = lininterp1f_p(self.Tinterp,self.cond.ERl,self.T,true) .* self.azicondL; %Ohm
        end
    end
    
    %% Calculate magnetic field B for known circuit currents
    methods
        function [B] = calcBmag(self)
            % go through circuits and add each circuits contribution
            % to the radial and axial field components of B
            Br = zeros(self.nrad,self.nax); Bz = zeros(self.nrad,self.nax);
            for i = 1:self.system.ncirc
                Br = Br + self.system.circ{i}.I * self.Bri{i};
                Bz = Bz + self.system.circ{i}.I * self.Bzi{i};
            end
            
            % calculate magnetic field magnitude
            B = sqrt(Br.^2 + Bz.^2); %T
            self.Bmag = B;
            %self.Bmag = hypot(Br,Bz);
        end
    end
    
    %% Calculate current through SC and NZ zone, electric field through elements and power dissipation in elements for given B, T and Icirc.
    methods
        function calcCS(self)
            % call shareCurrent to calculate current behaviour, E and P
            Itot = self.circ.I;
            if isempty(self.Jc_deviation); self.Jc_deviation = 0; end;
            [self.Isc, self.Inc, E, Pl] = shareCurrent(self.Bmag,self.T,abs(Itot),self.ER./self.azicondL,self.cond.A,self.cond.ScFactor,self.Jc_deviation);

            % make sure signs are preserved
            self.Isc = self.Isc * sign(Itot);
            self.Inc = self.Inc * sign(Itot);
            E = E * sign(Itot);
                        
            % calculate Vnz and P
            self.Vnz = E .* self.azicondL; %V
            self.P = Pl .* self.azicondL; %W
                
            % define effective electrical resistivity (here superconduction is taken into account)
            if Itot < 0.1;
                self.EReff = self.ER; % this is done to avoid dividing by zero in ... 
            else
                self.EReff = E .* self.azicondL ./ Itot;
            end
            
        end
    end
    
    %% Calculate dTdt for updated coil properties
    
    % takes the Krad, Kazi and Kax from calcK. If there are thermal links,
    % use the appropriate methods to add the Q values of the link! See
    % below (from line 413).
    
    methods
        function calcdTdt(self)
            % azimuthal heat transfer
            Qline = self.Kazi .* diff(mat2line(self.T));
            Qazi = zeros(1,self.nele);
            Qazi = line2mat(Qazi+[Qline 0]-[0 Qline],self.nrad);
            
            % radial heat transfer
            Qmat = self.Krad .* diff(self.T,1,1);
            Qrad = zeros(self.nrad, self.nax);
            Qrad = Qrad + [Qmat; zeros(1,self.nax)] - [zeros(1,self.nax); Qmat];
                        
            % axial heat transfer
            Qmat = self.Kax .* diff(self.T,1,2);
            Qax = zeros(self.nrad, self.nax);
            Qax = Qax + [Qmat zeros(self.nrad,1)] - [zeros(self.nrad,1) Qmat];
        
            % sum heat flow
            self.Q = Qazi + Qrad + Qax;          
            
            % add Qlink
            if ~isempty(self.Qlink); self.Q = self.Q + self.Qlink; end
            
            % add Qtransfer from helium bath (0 or negative value)
            if ~isempty(self.Qbath); self.Q = self.Q + self.Qbath; end
            
            % calculate dTdt
            self.dTdt = (self.P + self.Q + self.Pqh) ./ self.Cp;
                      
        end
    end
    
    
    
    %% Update coil properties for newly given self.T and self.system.circ{:}.I's (<-- no real syntax)
    
    % This is called by Create_System() in the update function which in its turn is used by SolveQuench.m. 
    
    methods
        function update(self)
            % calculate properties in correct order
            self.calcK;
            self.calcCp;
            self.calcER;
            if self.Bupdatereq == true || self.system.AlwaysUpdateB == true
                self.calcBmag; self.Bupdatereq = false;
            end
            self.calcCS;
            self.calcdTdt;
                  
        end
    end
    
    %% Calculate thermal energy [ZERO ENERGY CURRENTLY LIES AT 3K --> SET TO INITIAL TEMPERATURE.
    methods
        function [Eth] = calcEth(self)
            % build interpolation array [ALSO, WHY BUILD INTERPOLATION ARRAY FROM STATIC PROPERTIES HERE --> MOVE TO INITIALIZE?]
            Epl = zeros(1,length(self.Tinterp));
            for i=2:length(self.Tinterp)
                Cplav = (self.Cpl(i) + self.Cpl(i-1))/2;
                Epl(i) = Epl(i-1) + Cplav*(self.Tinterp(i) - self.Tinterp(i-1));
            end
            
            % interpolate for all elements of this coil
            Eth = lininterp1f_p(self.Tinterp,Epl,self.T,true).*self.azicondL;
            self.Eth = Eth;
            
        end
    end
    
    %% Initiate quench
    methods
        function quench(self,pos,Tquench)
            % error is no correct position is supplied
            if numel(pos) ~= 2
                error('numel of "pos" must be 2')
            end
            
            % report
            jprintf(self.system.printwindow,['Initiating quench in ' self.name ' at location: (' num2str(pos) ')\n']);
                 
            % elevate temperature of designated element over Tc if T is below Tc
            
            % if you want to simulate a normal RL-circuit, this should be
            % commented out! dTdt can change, but the temperature should
            % not be raised above critical temperature automatically.
            
            % Furthermore, if you can't quench the coil, raise this
            % temperature more to put more energy in the quench!
            
            if self.T(pos(1),pos(2)) < matprops(self.cond.ScMaterial,'Tc',self.Tinterp) + Tquench
                self.T(pos(1),pos(2)) = matprops(self.cond.ScMaterial,'Tc',self.Tinterp) + Tquench;
            end

        end
    end
    
    %% Quench heater power
    methods
        function Pqh = Pqh(self)
            % start with zeros for quench heater powers
            Pqh = zeros(self.nrad,self.nax);
            
            % only calculate if system is passed tQD
            if ~isempty(self.system.tQD)
                % go through quench heaters
                for i = 1:length(self.qh)
                    % only add power if quench heaters are working and are turned on
                    if self.qh{i}.working && (self.system.tQD + self.qh{i}.response_t < self.system.t) && (self.system.t < self.system.tQD + self.qh{i}.response_t + self.qh{i}.duration)
                        Pqh = Pqh + self.qh{i}.location * self.qh{i}.Pele; % self.qh.Pele can be found in the QuenchHeater class
                    end
                end
            end
                
        end
    end
    
    %% Quench heater map
    methods
        function QHmap = QHmap(self)
            % define empty QHmap
            QHmap = zeros(self.nrad,self.nax);
            
            % go through quench heaters
            for i = 1:length(self.qh)
                % add location of i'th quench heater
                QHmap = QHmap + self.qh{i}.location;
            end
            
        end
    end

    %% Plot geometry
    
    % This shows the geometry of a coil. If for instance the numbers of
    % turns do not match the size of the coil, it shows in this geometry!
    
    methods
        function [ax,f] = plotgeo(self,varargin)
            
            % analyse varargin
            visible = true;
            if ~isempty(varargin) && varargin{1} == false
                visible = false;
            end
            
            % MAKE FUNCTION TO PLOT COIL; 
            % dimensions from coil and conductor
            % quench heater positions
            % voltage taps not here --> in circuit
            
            % create figure
            f = figure('Color','w','colormap',HSV(1000),'Position',[100 100 1000 400],'Renderer','OpenGL');
            ax = axes('Parent',f); hold(ax,'on'); grid(ax,'on');
            if visible == false; set(f,'Visible','off'); end
            
            % coordinates
            Z = linspace(self.axrange(1),self.axrange(2),self.nax+1);
            R = linspace(self.radrange(1),self.radrange(2),self.nrad+1);

            % build mesh
            [Zm,Rm] = meshgrid(Z,R);
            Thm = zeros(size(Zm,1),size(Zm,2));
            
            % color to display
            val = max( line2mat(linspace(0,-1,self.nele),self.nrad) , -1 + 1.5*self.QHmap);
            
            % create surface input
            [a,b] = size(val);
            val = [val zeros(a,1)];
            val = [val; zeros(1,b+1)];
            
            % plot surface
            s = surface(Zm,Rm,Thm,val,'Parent',ax,'EdgeColor','None','Tag','CoilMesh');
            custom_legend(s,{'Quench heater','Dimensions from cond'},{[0.5 0 0],'None'},{'None',[1 0 0]});
            
            % plot rectangle to check insulation thickness
            x = self.axrange(1);
            y = self.radrange(1);
            
            condaxinst = sum(max([self.cond.axinsulationthickness,0],0)); % this is to prevent w being empty
            axinst = sum(max([self.axinsulationthickness,0],0));
            w = self.nax * (self.cond.axL + condaxinst + axinst);
            
            condradinst = sum(max([self.cond.radinsulationthickness,0],0)); % this is to prevent w being empty
            radinst = sum(max([self.radinsulationthickness,0],0));
            h = self.nrad * (self.cond.radL + condradinst + radinst);
            
            rectangle('Position',[x,y,w,h],'EdgeColor','r','LineStyle','-');
            
            % finish up
            title([self.name ' geometry']);
            xlabel('Z (m)')
            ylabel('R (m)')
           
        end
    end
    

    
    %% Number of elements
    methods
        function [nele] = nele(self)
            nele = self.nrad * self.nax;
        end
    end
    
    %% new new new new new Nikkie
    
    %% Number of thermalmal links
    methods
        function [ntlinks] = ntlinks(self)
            ntlinks = length(self.tlinks);
        end
    end
    
    %% end new new new new new Nikkie
    
    %% Destructor method
    methods
        function delete(self)
            % destroy dependency with system
            self.system = {};
            
            % destroy dependency with circuit
            self.circ = {};
            
            % destroy dependency with conductor
            self.cond = {};

        end
    end
 
end