function [ms] = SolveQuenchBasic(ms)

% define ode solver functions
OutputFcn = @(t,y,flag) printoutput(t,y,flag,ms);
StopFcn = @(t,Y) events(t,Y,ms);

% setup options for solver
options = odeset('stats','on','Events',StopFcn,'OutputFcn',OutputFcn,'AbsTol',ms.abstol,'RelTol',ms.reltol,'BDF','off','MaxStep',ms.dt);
%OdeType = 'ode23';
OdeType = 'none';

% set B update to true
ms.tlastBupdate = -Inf;
ms.nfcncalls = 0;

%% Solve

% define input for solver
myfun = @(t,Y)foptim(t,Y,ms);
tspan = ms.tstart : ms.dt : ms.tmax;
Yini = [ms.getI ms.getT zeros(1,ms.s2s(end,end)-ms.s2s(3,1)+1)];     %the zeros are used to find the dissipated energy of the dumpresistors, diodes and quench heaters

% call report
jprintf(ms.printwindow,'\n');
jprintf(ms.printwindow,['Running ',OdeType,' to Solve tstart < t < tmax\n'],'O>>');
jprintf(ms.printwindow,sprintf('Time expired: %d s\n',round(toc)));
jprintf(ms.printwindow,'--- pre-iteration ---\n','>>');



numsteps=floor((ms.tmax-ms.tstart)/ms.dt);
numsmallsteps=ms.solverSubSteps;
dtsmall=ms.dt/numsmallsteps;

len = length(Yini);
Y=zeros(numsteps+1,len);
Ysmall=zeros(numsmallsteps+1,len);
Y(1,:)=Yini;

Ycurrent=Yini;
t=0;


for i=1:numsteps
    Ysmall(1,:)=Ycurrent;
    for j=1:numsmallsteps        
        dYdt=foptim(t,Ysmall(j,:),ms);
        Ysmall(j+1,:)=Ysmall(j,:)+dYdt.*dtsmall;
        t=t+dtsmall;
    end
    Ycurrent=Ysmall(numsmallsteps+1,:);
    Y(i+1,:)=Ycurrent(:);
    %jprintf(ms.printwindow,sprintf('Time expired: %d s\n',round(toc)));
    
    printoutput(t,Ycurrent',false,ms);
    
end


% time stepping done
jprintf(ms.printwindow,'\n','<<');

% stop criterion reached
jprintf(ms.printwindow,'--- stop criterion reached ---\n','>>');
jprintf(ms.printwindow,['solver used: ' OdeType ' \n']);
jprintf(ms.printwindow,sprintf('Time expired: %d s\n',round(toc)));
%jprintf(ms.printwindow,['calculation time: ' num2str(time) ' [s]\n']);

% report done
jprintf(ms.printwindow,'\n','<<');

% store data in ms class; event times and values are also stored
ms.tstore = tspan';
ms.Ystore = [Y];

% sort
%[ms.tstore,idx] = sort(ms.tstore);
%ms.Ystore = ms.Ystore(idx,:);

% uniquify
%[ms.tstore,idx,~] = unique(ms.tstore);
%ms.Ystore = ms.Ystore(idx,:);
end

%% Solver function
function dYdt = foptim(t,Y,ms)
% check update B
if t > ms.tlastBupdate + ms.dt;
    % report
    %jprintf(ms.printwindow,'enabling field magnitude update\n');
    
    % set new last t
    ms.tlastBupdate = t;
    
    % update B flag enable
    for i=1:ms.ncoils
        ms.coils{i}.Bupdatereq = true;
    end
end

% apply input to model
ms.t = t;
ms.setI( Y(ms.s2s(1,1):ms.s2s(1,2))' );
ms.setT(max(4,Y(ms.s2s(2,1):ms.s2s(2,2))')); % if the solver takes a too big step, temperatures below 4K could be entered into the system --> the max prevents non-physical solutions from occuring

% calculate relevant quantities
ms.update

% calculate normal zone resistance for quench detection if tQD is not known
if isempty(ms.tQD) || t <= ms.tQD + ms.dt_QD
    ms.qcirc.calcVnz;
end

% return derivatives
%dYdt = [ms.dIdt'; ms.dTdt'; ms.Pdr; ms.Pdio; ms.Pqh];
dYdt = [ms.dIdt ms.dTdt ms.Pdr' ms.Pdio' ms.Pqh'];

% increment
ms.nfcncalls = ms.nfcncalls + 1;
end

%% Output Function
function status = printoutput(t,y,~,ms)
if ~isempty(y)
    % find out which coil is hottest
    [Tmax,idx] = max(y(ms.ncirc+1:ms.ncirc+ms.nele,1));
    idxcoil = idx >= ms.l2m(:,1) & idx < ms.l2m(:,2);
    if sum(idxcoil)==1
        coilname = ms.coils{idxcoil}.name;
    else
        % display multiple coilnames in case of a tie
        coilname = [];
        for i = 1:ms.ncoils
            if idxcoil(i)
                coilname = [coilname ms.coils{i}.name ', '];
            end
        end
        coilname = coilname(1:end-2);
    end
    
    % display system information
    %str = sprintf('t = %02.3f, Iqcirc = %02.3f, Tmax = %02.3f',t(1,1),y(1,ms.qcirc.circnr),max( y(ms.ncirc+1:end,1)) );
    jprintf(ms.printwindow,'\n','O>>');
    jprintf(ms.printwindow,['--- ',sprintf('Time %02.4f [s]',t(1,1)),' ---\n'],'>>');
    jprintf(ms.printwindow,sprintf('quench circuit current %02.3f [A]\n',y(ms.qcirc.circnr,1))); %y(1,circnr) or y(circnr,1)?
    jprintf(ms.printwindow,sprintf(['hotspot temperature %02.3f [K] (' coilname ')\n'],Tmax));
    
    % display quench detection signal if tQP is still to be determined
    if isempty(ms.tQD)
        jprintf(ms.printwindow,sprintf('NZ voltage of quench circuit: %01.2f [V] \n',ms.qcirc.Vnz));
        if ms.qcirc.Vnz >= ms.VQD_threshold
            ms.tQD = ms.t + ms.dt_QD;
           
        end
    end
    
    % function calls
    jprintf(ms.printwindow,['number of function calls ',num2str(ms.nfcncalls),'\n']);
    jprintf(ms.printwindow,sprintf('Time expired: %d s\n',round(toc)));
    ms.nfcncalls = 0;
    
    % allow matlab to exit routine
    % drawnow;
end

status = 0;
end

%% Events function 
% this function causes the solver to 
function [val,isterminal,direction] = events(t,Y,ms)

if ~isempty(Y)
    % prealocate
    val = ones(length(ms.dt_events)+2, 1);
    isterminal = 0*val;
    direction = -1*val;
    
    % total NZ voltage of quench circuit surpasses VQD threshold
    if isempty(ms.tQD) && ~isempty(ms.VQD_threshold) && ~isempty(ms.qcirc.Vnz)
        val(1) = ms.VQD_threshold - ms.qcirc.Vnz;
        
        % define tQD when conditions are met
        if val(1) < 0
            jprintf(ms.printwindow,sprintf('NZ voltage of quench circuit has surpassed VQDthreshold: tQD set to %01.3f [s] \n',ms.tQD));
            ms.tQD = t + ms.dt_QD;
        end
        
    end
    
    % circuit breakers, dumpresistors and quench heater start and stop times (prepared in initialization)
    if ~isempty(ms.tQD) && ~isempty(ms.dt_events)
        val(2:end-1) = ms.tQD + ms.dt_events - t;
    end
    
    % stop event [default /25]
    val(end) = ms.tmax;
    %val(end) = ms.qcirc.I - ms.qcirc.Iinitial/25;
    if val(end) < 0
        val(end) = 0;
    end
    isterminal(end) = 1;
    
end

end




