classdef Create_Circuit < hgsetget
    
    %% Description:
    
    % This class handles the different circuits of a system.
    
    %% User input settings
    properties
        name = [];          % string
        
        coils = {};         % this circuit's coils
        system = [];        % system the circuit is in
        
        Iinitial = [];      % this circuits initial I
        
        Rdi = 0;            % resistance before switch open
        Rdf = 0;            % dumpresistor; only kicks in after switch open
        Rdworking = true;   % boolean to indicate whether the dumpresistor is working
        
        ndiodes = [];       % voltage over external diodes for each circuit
        diodetype = [];     % type of diode
        
        addV0 = [];         % flag to add a voltage tap before first coil
        
        dt_CB = [];         % response time of circuit breakers
        dt_Rd = [];         % response time of dump resistor switch
        
        
    end
    
    %% Static system properties
    properties
        circnr = [];        % this circuit can be found here: system.circ{ circnr }
        l2m = [];           % conversion indices to transform line back to matrix (only for coils of this circuit)
        QDconfig = [];      % N x 3 matrix with three votlage tap numbers for each quench detect triplet on the rows
        Vtapsindices = [];  % list of boolians of length equal to number of elements ???
        Vtapspos = [];      % element numbers of voltage taps ???
        kQD = [];           % constant used to scale up voltage measured between 2nd and 3rd voltage taps ???
        
    end
    
    %% Dynamic system properties
    properties
        I = [];         % current [A]
        Vdrop = [];     % total voltagedrop over to NZ, dumpresistor and diodes [V]
        Vtaps = [];     % voltages of the voltage taps  [V]
        VQD = [];       % signal of the quench detection triplets [V]
        Vnz = [];       % total NZ voltage of all coils in circuit [V]
    end
    
    %% Method for checking user settings
    
    % ???
    
    methods
        function [ch,msg] = checksettings(self)
            % retard check
            
        end
    end
    
    %% Add coils
    methods
        function addcoils(self,varargin) % a circuit can have multiple coils! 
                                           % @Rosalinde argument: varargin provides the added coil ( one or multiple Create_Coil objects)
            % analyse input
            if isempty(varargin)
                warning(['an attempt was made to add a coil to ' self.name ', but no coils were supplied']);
                return;
            end
            
            % add circuit to all coils
            for i = 1:length(varargin)
                varargin{i}.circ = self;    % @Rosalinde copies self (Create_Circuit object) to variable circ (property of Create_Coil)
            end
            
            % add coils to self.coils
            self.coils = {self.coils{:} varargin{:}};
            
            % add coils to system if circuit is in a system
            if ~isempty(self.system)
                for i = 1:length(varargin)
                    self.system.coils = {self.system.coils{:} varargin{:}};
                end
            end
            
            % make sure matlab is displaying newly added coils
            drawnow;
            
        end
    end
    
    %% Calculate field contribution of this circuit on given elements per Amp // Reply to coil when asked for B on its elements
    methods
        function [Br,Bz] = calcB(self,r,z)
            % preallocate
            Br = zeros(size(r,1),size(r,2));
            Bz = zeros(size(r,1),size(r,2));
            
            % cycle through coils of each circuit
            for i = 1:self.ncoils
                % calc field [Tesla/A]
                [Brnew,Bznew] = self.coils{i}.calcB(r,z);
                Br = Br + Brnew; Bz = Bz + Bznew;
                
            end
            
        end
    end
    
    %% Calulate start and end indices for line2mat operations
    methods
        function calcl2m(self)
            n = [];
            for i = 1:self.ncoils
                n = [n self.coils{i}.nele]; %#ok
            end
            self.l2m = [1,n(1)];
            if length(n)>=2
                for i = 2:length(n)
                    a = self.l2m(i-1,2);
                    self.l2m = [self.l2m; [a+1, a+n(i)]];
                end
            end
            
        end
    end
    
    %% Do everything that has to be done before calculating
    
    % This function is called by 'initialize' of the Create_System() class.
    
    methods
        function initialize(self)
            jprintf(self.system.printwindow,['Initializing ' self.name '\n']);
            
            % calulate start and end indices for line2mat operations
            n = [];
            for i = 1:self.ncoils
                n = [n self.coils{i}.nele]; %#ok
            end
            self.l2m = [1,n(1)];
            if length(n)>=2
                for i = 2:length(n)
                    a = self.l2m(i-1,2);
                    self.l2m = [self.l2m; [a+1, a+n(i)]];
                end
            end
            
            % find voltage taps (line of boolians of length sum(nele))
            self.Vtapspos = [];
            for i = 1:self.ncoils
                self.Vtapspos = [self.Vtapspos mat2line(self.coils{i}.Vtapsmap)];
            end
            
            % find indices of voltage taps
            Vtp = self.Vtapspos; self.Vtapsindices = [];
            while ~sum(Vtp)==0
                idx = find(Vtp,1);
                self.Vtapsindices = [self.Vtapsindices idx];
                Vtp(1:idx) = [];
            end
            self.Vtapsindices = cumsum(self.Vtapsindices);
            
            % temporarily add voltage tap index before first element if requested
            if self.addV0 == true
                self.Vtapsindices = [0 self.Vtapsindices];
            end
            
            % calculate QD bridge constants
            for i = 1:size(self.QDconfig,1)
                a = self.QDconfig(i,1); b = self.QDconfig(i,2); c = self.QDconfig(i,3);
                self.kQD(i) = sum(self.system.indMele(self.Vtapsindices(a)+1:self.Vtapsindices(b),self.circnr))...
                    / sum(self.system.indMele(self.Vtapsindices(b)+1:self.Vtapsindices(c),self.circnr));
            end
            
            % remove voltage tap index before first element if requested
            if self.addV0 == true
                self.Vtapsindices = self.Vtapsindices(2:end);
            end
            
        end
    end
    
    %% Calculate total voltage drop over NZ, dumpresistor and diodes (for solving)
    methods
        function [Vdrop] = calcVdrop(self)
            % add NZ resistance of each coil in circuit
            Rnz = 0;
            for i = 1:self.ncoils
                Rnz = Rnz + sum(sum( self.coils{i}.EReff ));
            end
            
            % add NZ, dumpresistor and diode voltages
            Vdrop = self.I.*(Rnz+self.Rextern) + self.Vdio;
            self.Vdrop = Vdrop;
            
        end
    end
    
    %% Calculate voltage relative to CL (for post processing)
    methods
        function calcVcs(self)
            % obtain induced and NZ voltages
            allVind = [];
            allVnz = [];
            for i = 1:self.ncoils
                allVind = [allVind mat2line(self.coils{i}.Vind)]; %#ok
                allVnz = [allVnz mat2line(self.coils{i}.Vnz)]; %#ok
            end
            
            % create cumsum voltage arrays through circuits
            allV = allVind + allVnz;
            allVcs = cumsum(allV);
            % @Rosalinde 07/09/2015
            allVindcs = cumsum(allVind);
            allVnzcs = cumsum(allVnz);
            % end @Rosalinde 07/09/2015
            
            % separate long strings back into matrices in coils
            for i = 1:self.ncoils
                self.coils{i}.V = line2mat( allV(self.l2m(i,1):self.l2m(i,2)), self.coils{i}.nrad);
                self.coils{i}.Vcs = line2mat( allVcs(self.l2m(i,1):self.l2m(i,2)), self.coils{i}.nrad);
                % @Rosalinde 07/09/2015
                self.coils{i}.Vindcs = line2mat( allVindcs(self.l2m(i,1):self.l2m(i,2)), self.coils{i}.nrad);
                self.coils{i}.Vnzcs = line2mat( allVnzcs(self.l2m(i,1):self.l2m(i,2)), self.coils{i}.nrad);
                % end @Rosalinde 07/09/2015
            end
            
        end
    end
    
    %% Calculate voltages used for quench detection signal (note this is not used to trigger quench protection)
    methods
        function calcVQD(self)
            % get allVcs
            allVcs = [];
            for i = 1:self.ncoils
                allVcs = [allVcs mat2line(self.coils{i}.Vcs)];  %#ok
            end
            
            % calculate voltage-tap voltages
            self.Vtaps = zeros(1,length(self.Vtapsindices));
            for i = 1:length(self.Vtapsindices)
                self.Vtaps(i) = allVcs(self.Vtapsindices(i));
            end
            
            % add voltage tap before first element if requested (note that after the last element is the same as the value of the last element, but before the first element is unequal to value of first element)
            if self.addV0 == true
                self.Vtaps = [0 self.Vtaps];
            end
            
            % quench detection bridge (QD) voltages
            self.VQD = zeros(1,size(self.QDconfig,1));
            for i = 1:size(self.QDconfig,1)
                a = self.QDconfig(i,1); b = self.QDconfig(i,2); c = self.QDconfig(i,3);
                self.VQD(i) = (self.Vtaps(b)-self.Vtaps(a)) - (self.kQD(i)*(self.Vtaps(c)-self.Vtaps(b)));
            end
            
        end
    end
    
    %% Calculate total NZ voltage in circuit
    methods
        function calcVnz(self)
            % QD check; the NZ voltage is taken directly and is used to trigger quench protectior
            self.Vnz = 0;
            for i = 1:self.ncoils
                self.Vnz = self.Vnz + sum(sum(self.coils{i}.Vnz));
            end
        end
    end
    
    %% Plot Geometry
    
    % This shows all the coils in the circuit. If for instance the number of turns in the radial
    % direction does not match the size of the coil this will show in the
    % plotgeo.
    
    % pimp to show voltage taps / original quench location / qh / external
    % components / more?
    methods
        function [ax,f] = plotgeo(self,varargin)
            
            valVt = 0.25;
            valQH = 0.5;
            
            % check if user wants the plot to be visible
            visible = true;
            if ~isempty(varargin) && length(varargin) == 1
                visible = varargin{:};
            end
            
             % create figure
            f = figure('Color','w','colormap',jet(1000),'Position',[100,100,1000,400],'Renderer','OpenGL');
            ax = axes('Parent',f); hold(ax,'on'); grid(ax,'on');
            xlabel('Z [m]');
            ylabel('R [m]');
            title(['Geometry of ' self.name ' circuit']);
            
            % visibility toggle
            if visible == false; set(f,'Visible','off'); end
            
            % find out system range
            minS = inf;
            maxS = -inf;
            for i = 1:self.system.ncoils
                minS = min(minS,self.system.coils{i}.axrange(1));
                maxS = max(maxS,self.system.coils{i}.axrange(2));
            end
            sysrange = maxS - minS;
            
            % find smallest axial conductor length of coils
            min_axL = inf;
            for i = 1:self.ncoils
                min_axL = min(min_axL,self.coils{i}.cond.axL);
            end
            
            % find out n_addcolumns
            n_addcolumns = 0;
            if min_axL < sysrange/250
                n_addcolumns = ceil( sysrange/(250*min_axL) );
            end
                        
            % create rainbow through coils of circuit
            circval = linspace(0,-1,self.nele);
            
            for i = 1:self.ncoils
                % coordinates
                Z = linspace(self.coils{i}.axrange(1),self.coils{i}.axrange(2),self.coils{i}.nax+1);
                R = linspace(self.coils{i}.radrange(1),self.coils{i}.radrange(2),self.coils{i}.nrad+1);
                
                % build mesh
                [Zm,Rm] = meshgrid(Z,R);
                Thm = zeros(size(Zm,1),size(Zm,2));
                
                % color to display
                val = max( line2mat( circval(self.l2m(i,1):self.l2m(i,2)), self.coils{i}.nrad) , -1 + (1+valQH)*self.coils{i}.QHmap);
                idx = logical(self.coils{i}.Vtapsmap); 
                %idx = line2mat(idx,self.coils{i}.nrad);
                Vtap = valVt*ones(self.coils{i}.nrad,self.coils{i}.nax);
                val(idx) = Vtap(idx);
                
                % make sure voltage taps and quench heaters on edges are visible
                if ~isempty(n_addcolumns) && n_addcolumns ~= 0;
                    % find m
                    Vtap_l = val(:,1) == valVt;
                    Vtap_r = val(:,end) == valVt;
                    QH_l = val(:,1) == valQH;
                    QH_r = val(:,end) == valQH;
                    
                    % alter adjacent columns left
                    for j = 2:n_addcolumns+1
                        val(Vtap_l,j) = val(Vtap_l,1);
                        val(QH_l,j) = val(QH_l,1);
                    end
                    
                    % and adjacent columns right
                    for j = 1:n_addcolumns
                        val(Vtap_r,end-j) = val(Vtap_r,end);
                        val(QH_r,end-j) = val(QH_r,end);
                    end
                    
                end

                % addrows ?
                
                % create surface input
                [a,b] = size(val);
                val = [val zeros(a,1)];
                val = [val; zeros(1,b+1)];
                
                % color
                J = jet(1000); factor = (1+valVt) / (1+valQH); C = J(round(factor*1000),:);
                % plot surface
                s = surface(Zm,Rm,Thm,val,'Parent',ax,'EdgeColor','None','Tag','CoilMesh');
                custom_legend(s,{'Dimensions from cond','Quench heater','Voltage tap'},{'None',[0.5 0 0],C},{[1 0 0],'None','None'},'Location','North');
                
                % plot rectangle to check insulation thickness
                x = self.coils{i}.axrange(1);
                y = self.coils{i}.radrange(1);
                
                condaxinst = sum(max([self.coils{i}.cond.axinsulationthickness,0],0)); % this is to prevent w being empty
                axinst = sum(max([self.coils{i}.axinsulationthickness,0],0));
                w = self.coils{i}.nax * (self.coils{i}.cond.axL + condaxinst + axinst);
                
                condradinst = sum(max([self.coils{i}.cond.radinsulationthickness,0],0)); % this is to prevent w being empty
                radinst = sum(max([self.coils{i}.radinsulationthickness,0],0));
                h = self.coils{i}.nrad * (self.coils{i}.cond.radL + condradinst + radinst);
                
                rectangle('Position',[x,y,w,h],'EdgeColor','r','LineStyle','-');
                
            end
            
        end
    end
    
    %% Diode voltage
    methods
        function [Vdio] = Vdio(self)
            % distinguish between before and after tQD: circuit breakers are assumed to always work
            if ~isempty(self.system.tQD) && ~isempty(self.dt_CB) && self.system.t > self.system.tQD + self.dt_CB;
                [U,~] = Vdiode(self.diodetype,self.I);
                Vdio = self.ndiodes .* U;
            else
                Vdio = 0;
            end
            
        end
    end
    
    %% Dump resistor resistance
    methods
        function [Rextern] = Rextern(self)
            % only take final dump resistance if working, a tQD and a
            % delay are known and the current time surpasses the sum
            if self.Rdworking && ~isempty(self.system.tQD) && ~isempty(self.dt_Rd) && (self.system.t > self.system.tQD + self.dt_Rd);
                Rextern = self.Rdf;
            else
                Rextern = self.Rdi;
            end
            
        end
    end
    
    %% Number of coils
    methods
        function [ncoils] = ncoils(self)
            ncoils = length(self.coils);
        end
    end
    
    %% Number of elements
    methods
        function [nele] = nele(self)
            nele = 0;
            for i = 1:self.ncoils
                nele = nele + self.coils{i}.nele;
            end
        end
    end
    
    %% Destructor method
    methods
        function delete(self)
            % destroy dependency with coils
            self.coils = {};
            
            % destroy dependency with system
            self.system = {};
            
        end
    end
    
end

