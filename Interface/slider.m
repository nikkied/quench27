%% Construction function
function [ax,f] = slider(varargin)

% analyse input
if numel(varargin) == 1 && iscell(varargin{1})
    mqlist = varargin{1};
else
    mqlist = varargin;
end

% prompt if needed
if isempty(mqlist)
    % get ms
    [files,path,~] = uigetfile('.mat','Select solved Create_System-classes','multiselect','on');
    
    % make sure filename is in a cell (even if only one ms was picked)
    if ~iscell(files)
        files = {files};
    end
    
    % load
    mqlist = cell(1,length(files));
    for i = 1:length(files)
        % load
        load([path,filesep,files{i}],'ms');
        mqlist{i} = ms;
    end
    
end

% settings
proplist = {'Temperature [K]','dT/dt [K/s]','Electrical Resistivity [Ohm]','Effective Electrical Resistivity [Ohm]','Current through SC [A]','Current through NC [A]','Magnetic Field [T]','NZ Voltage [V]','Induced Voltage [V]','Voltage [V]','CumSum Voltage [V]','Vtap','Qheater'}; plval = 1;
        
% get input
if ~iscell(mqlist); mqlist = {mqlist}; end

% --- create figure and panels---
f = figure('Color','w','Position',[100,100,1000,400],'Renderer','OpenGL','Toolbar','none','MenuBar','none','NumberTitle','off','Name','Quench Complex - miniGUI');
b = uiextras.VBox( 'Parent', f );

% settings
h = uiextras.HBox( 'Parent', b );
namestr = cell(1,length(mqlist));

% define names for dropdown menu
for i=1:length(mqlist); 
    namestr{i} = [mqlist{i}.scenario]; 
end

uicontrol('Parent',h,'Style','popup','String',namestr,'BackgroundColor','w','Callback',@onSetMq);
uicontrol('Parent',h,'Style','popup','String',proplist,'BackgroundColor','w','Value',plval,'Callback',@onSetStr);
uiextras.Empty('Parent',h);
%h.Sizes = [300,150,-1];
h.Sizes = [300,150,-1];

% create axes
axpanel = uipanel('Parent',b,'BackgroundColor','w');
%ax = axes('Parent',axpanel,'Units','pixels'); hold(ax,'on'); grid(ax,'on');
ax = axes('Parent',axpanel); hold(ax,'on'); grid(ax,'on'); 
xlabel(ax,'Z [m]');
ylabel(ax,'R [m]');
cbh = colorbar('EastOutside','Peer',ax);
ylabel(cbh,proplist{plval});

% build slider
h = uiextras.HBox('Parent',b);
tb = uicontrol('Parent',h,'Style','edit','String','0','BackgroundColor','w','Callback',@onTbUpdate);
uicontrol('Parent',h,'Style','text','String','[s]','BackgroundColor','w');
sl = uicontrol('Parent',h,'Style','slider','BackgroundColor',[0.9,0.9,0.9],'Min',mqlist{1}.tstore(1),'Max',mqlist{1}.tstore(end-1),'Value',0,'Callback',@onSlUpdate);
h.Sizes = [40,20,-1];

% --- Toolbar ---
% create Toolbar and buttons
toolbar = uitoolbar(f);
setappdata(f,'toolbar',toolbar);

% save button
icon = 'icons/icon_export.png';
uipushtool('Parent',toolbar,'Cdata',iconread(icon),'ToolTipString','Export Figure','ClickedCallback',@onExportFigure,'Interruptible','off');

% resolution button
icon = 'icons/icon_export.png';
rset = getappdata(f,'export_dpi');
uipushtool('Parent',toolbar,'Cdata',iconread(icon),'ToolTipString',['Set Resolution - r',num2str(rset)],'ClickedCallback',@onChooseResolution,'Interruptible','off');

% tap button
%icon = 'icons/icon_tap.png';
%uitoggletool('Parent',toolbar,'CData',iconread(icon),'OnCallback',@onVertical,'offCallback',@onHorizontal,'ToolTipString','Zoom Type');

% pan button
icon = fullfile(matlabroot,'toolbox','matlab','icons','tool_hand.png');
uitoggletool('Parent',toolbar,'CData',iconread(icon),'OnCallback',@onPanEnable,'offCallback',@onPanDisable,'ToolTipString','Pan','Tag','toggleobject');


% --- appdata ---
setappdata(f,'str',proplist{plval});
setappdata(f,'mqlist',mqlist);
setappdata(f,'mqi',1);
setappdata(f,'sl',sl);
setappdata(f,'tb',tb);
setappdata(f,'ax',ax);
setappdata(f,'cbh',cbh);
setappdata(f,'fit','vertical'); % vertical / horizontal
setappdata(f,'mainvbox',b);
setappdata(f,'toolbar',toolbar);
setappdata(f,'export_dpi',200);
setappdata(f,'export_crop',true);

% set sizes
b.Sizes = [20,-1,20];

% update
update();
axis(ax,'equal');
updateAxesSize();

end

%% slider update
function update()
% get data
if ~isempty(gcbf); fig = gcbf; else fig = gcf; end
str = getappdata(fig,'str');
mqi = getappdata(fig,'mqi');
mqlist = getappdata(fig,'mqlist');
mq = mqlist{mqi};
sl = getappdata(fig,'sl');
tb = getappdata(fig,'tb');
ax = getappdata(fig,'ax');

% get slider position
t = get(sl,'Value');
ti = find(mq.tstore<=t,1,'last'); tit1 = mq.tstore(ti); tit2 = mq.tstore(ti+1); % dt = tit2-tit1;
%f1 = ((tit2-t)/dt); f2 = ((t-tit1)/dt);

% snap to calculated time 
if t-tit1 < tit2-t; 
    mq.t = tit1; %ti = ti;
else
    mq.t = tit2; ti = ti+1;
end
set(sl,'Value',mq.t);
set(tb,'String',num2str(mq.t,3));

% set properties and calculate
mq.settime;

% empty axes
h = findobj(ax,'Tag','CoilMesh'); delete(h);

% walk over coils
for i=1:length(mq.coils)
    % get user input
    switch str
        case 'Temperature [K]'; val = mq.coils{i}.T; 
        case 'dT/dt [K/s]'; val = mq.coils{i}.dTdt;    
        case 'Electrical Resistivity [Ohm]'; val = mq.coils{i}.ER; 
        case 'Effective Electrical Resistivity [Ohm]'; val = mq.coils{i}.EReff;   
        case 'Current through SC [A]'; val = mq.coils{i}.Isc;     
        case 'Current through NC [A]'; val = mq.coils{i}.Inc;   
        case 'Magnetic Field [T]'; val = mq.coils{i}.Bmag;   
        case 'NZ Voltage [V]'; val = mq.coils{i}.Vnz;
        case 'Induced Voltage [V]'; val = mq.coils{i}.Vind;  
        case 'Voltage [V]'; val = mq.coils{i}.V;  
        case 'CumSum Voltage [V]'; val = mq.coils{i}.Vcs;   
        case 'Vtap'; val = mq.coils{i}.Vtapsmap;
        case 'Qheater'; val = mq.coils{i}.QHmap; 
        otherwise; error('welke knuppel heeft hier iets doms ingevoerd ... D:');
    end
    
    % create position arays
    Z = linspace(mq.coils{i}.axrange(1),mq.coils{i}.axrange(2),mq.coils{i}.nax+1);
    R = linspace(mq.coils{i}.radrange(1),mq.coils{i}.radrange(2),mq.coils{i}.nrad+1);
    
    % call plot function
    plotFcn(ax,Z,R,val);
    
end

% update size
updateAxesSize();

% drawnow
drawnow;


end

%% update axes size
function updateAxesSize()
% get data
if ~isempty(gcbf); fig = gcbf; else fig = gcf; end
ax = getappdata(fig,'ax');
fit = getappdata(fig,'fit');
mqi = getappdata(fig,'mqi');
mqlist = getappdata(fig,'mqlist');
mq = mqlist{mqi};

% drawnow
drawnow;

% get coil sizes
axmin = Inf; axmax = -Inf; radmin = Inf; radmax = -Inf;
for i=1:mq.ncoils
    axmin = min(axmin,mq.coils{i}.axrange(1));
    axmax = max(axmax,mq.coils{i}.axrange(2));
    radmin = min(radmin,mq.coils{i}.radrange(1));
    radmax = max(radmax,mq.coils{i}.radrange(2));
end

% set fitting
switch fit
    case 'vertical'; ylim(ax,[radmin,radmax]);
    case 'horizontal'; xlim(ax,[axmin,axmax]);
    otherwise; error('Je moeder past niet...');
end

end

%% Plot function
function plotFcn(ax,Z,R,val)
% build mesh
[Zm,Rm] = meshgrid(Z,R);
Thm = zeros(size(Zm,1),size(Zm,2));

% create surface input
[a,b] = size(val); 
val = [val zeros(a,1)]; 
val = [val; zeros(1,b+1)];

% plot surface
surface(Zm,Rm,Thm,val,'Parent',ax,'EdgeColor','w','Tag','CoilMesh');
%alpha(h,0.4);
end

%% String set callback function
function onSetStr(src,~)
cbh = getappdata(gcbf,'cbh');
str = get(src,'String');
val = get(src,'Value');
setappdata(gcbf,'str',str{val});

ylabel(cbh,str{val});
update();
end

%% Set mq callback function
function onSetMq(src,~)
val = get(src,'Value');
setappdata(gcbf,'mqi',val);
update();
end

%% Time set functions
function onSlUpdate(src,~)
tb = getappdata(gcbf,'tb');
val = get(src,'Value');
set(tb,'String',num2str(val,3));
update();
end

function onTbUpdate(src,~)
sl = getappdata(gcbf,'sl');
strval = get(src,'String');
val = eval(strval);
set(sl,'Value',val);
update();

end

%% Pan on
function onPanEnable(src,~)
toolbar = getappdata(gcbf,'toolbar');
h = findobj(toolbar,'Tag','toggleobject');
h = h(h~=src);
set(h,'State','off');
pan(gcbf,'on');
end

%% Pan off
function onPanDisable(~,~)
pan(gcbf,'off');
end

%% Export figure
function onExportFigure(~,~)
% get info
dpi = getappdata(gcbf,'export_dpi');
crop = getappdata(gcbf,'export_crop');
namestr = get(gcbf,'Name');

% get filepath
[FileName, PathName] = uiputfile({'*.tif';'*.jpg';'*.pdf';'*.bmp';'*.png'},'Save Image As',namestr);
    
    % check for cancel
    if FileName~=0
        % path
        fp = fullfile(PathName,FileName);
        
        % report
        if crop==true
             disp('Cropping    - enabled');
        else
             disp('Cropping    - disabled');
        end
                
        % build
        if crop==true
            export_fig(fp,['-',FileName(end-2:end)],['-r',num2str(dpi)],gcbf);
        else
            export_fig(fp,['-',FileName(end-2:end)],['-r',num2str(dpi)],'-nocrop',gcbf);
        end
        
        % done
        disp('Export Complete');
    end
end

%% Set resolution
function onChooseResolution(src,~)
% create dialog
prompt = {'DPI (100=low, 300=high)'};
defaultanswer = {num2str(getappdata(gcbf,'export_dpi'))};
answer = inputdlg(prompt,'Set Resolution',1,defaultanswer);
if ~isempty(answer)
    val = eval(answer{1});
    if isscalar(val)
        if val<1200 && val>1
            setappdata(gcbf,'export_dpi',val); 
            %set(src,'ToolTipString',['Set Resolution - r',answer{1}]);
            set(src,'ToolTipString',['Set Resolution - r',600]);
        end
    end
end

end

