function [toolbar] = export_fig_toolbar(hf)

% create Toolbar and buttons
toolbar = uitoolbar(hf);
setappdata(hf,'toolbar',toolbar);

% set default
setappdata(hf,'export_dpi',200);
setappdata(hf,'export_crop',true);

% save button
icon = 'icons/icon_export.png';
uipushtool('Parent',toolbar,'Cdata',iconread(icon),'ToolTipString','Export Figure','ClickedCallback',@onExportFigure,'Interruptible','off');

% save button
icon = 'icons/icon_export.png';
rset = getappdata(hf,'export_dpi');
uipushtool('Parent',toolbar,'Cdata',iconread(icon),'ToolTipString',['Set Resolution - r',num2str(rset)],'ClickedCallback',@onChooseResolution,'Interruptible','off');

% save button
icon = 'icons/icon_export.png';
h = uitoggletool('Parent',toolbar,'CData',iconread(icon),'OnCallback',@onEnableCrop,'offCallback',@onDisableCrop);
nocrop = getappdata(hf,'export_crop'); 
if nocrop==true; set(h,'State','on'); set(h,'ToolTipString','Set Cropping - on'); else set(h,'State','off'); set(h,'ToolTipString','Set Cropping - off'); end

% projection button
%icon = 'icons/icon_export.png';
%uipushtool('Parent',toolbar,'Cdata',iconread(icon),'ToolTipString','Toggle Projection','ClickedCallback',@onProjectionToggle,'Interruptible','off');


end

function onExportFigure(~,~)
% get info
dpi = getappdata(gcbf,'export_dpi');
crop = getappdata(gcbf,'export_crop');
namestr = get(gcbf,'Name');

% get filepath
[FileName, PathName] = uiputfile({'*.tif';'*.jpg';'*.pdf';'*.bmp';'*.png'},'Save Image As',namestr);
    
    % check for cancel
    if FileName~=0
        % path
        fp = fullfile(PathName,FileName);
        
        % report
        if crop==true
             disp('Cropping    - enabled');
        else
             disp('Cropping    - disabled');
        end
                
        % build
        if crop==true
            export_fig(fp,['-',FileName(end-2:end)],['-r',num2str(dpi)],gcbf);
        else
            export_fig(fp,['-',FileName(end-2:end)],['-r',num2str(dpi)],'-nocrop',gcbf);
        end
        
        % done
        disp('Export Complete');
    end
end

function onChooseResolution(src,~)
% create dialog
prompt = {'DPI (100=low, 300=high)'};
defaultanswer = {num2str(getappdata(gcbf,'export_dpi'))};
answer = inputdlg(prompt,'Set Resolution',1,defaultanswer);
if ~isempty(answer)
    val = eval(answer{1});
    if isscalar(val)
        if val<1200 && val>1
            setappdata(gcbf,'export_dpi',val); 
            set(src,'ToolTipString',['Set Resolution - r',answer{1}]);
        end
    end
end

end

function onEnableCrop(src,~)
setappdata(gcbf,'export_crop',true);
set(src,'ToolTipString','Set Cropping - on');
end

function onDisableCrop(src,~)
setappdata(gcbf,'export_crop',false);
set(src,'ToolTipString','Set Cropping - off');
end

% function onProjectionToggle(~,~)
% ax = findobj(gcbf,'Type','axes');
% p = get(ax,'Projection');
% % disabling labels is necessary because of graphical bug with perspective
% % projection
% hx = get(ax,'XLabel');
% hy = get(ax,'YLabel');
% hz = get(ax,'ZLabel');
% t = get(ax,'Title');
% if strcmp(p,'orthographic')
%     set(ax,'Projection','perspective');
%     set([hx,hy,hz,t],'Visible','off');
% end
% if strcmp(p,'perspective')
%     set(ax,'Projection','orthographic');
%     set([hx,hy,hz,t],'Visible','on');
% end
% end
