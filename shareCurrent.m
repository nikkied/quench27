% This function calculated the current sharing between the normal
% conducting material and the superconducting material.

% it is called by Create_Coil in the calcCS function.

% see Internship Report of Bas, section 3.2.2 Current Sharing for the
% explanation.

function [Isc,Inc,E,Pl] = shareCurrent(Bmag,T,Itot,ERl,Acond,ScFactor,Jc_deviation)

if max(max(Bmag)) > 1000
    error('Bmag > 1000 was supplied')
end

% settings
N = 90;

E0 = 1e5; % V/m --> Is dit een foutje?
% E0 = 1e-5; % V/m --> Test, 25/8/15

usefzero = false;
useinterp = true;
useMatthijsAndRosalinde=true;

% reshape input
[n1,n2] = size(Bmag); Ns = n1*n2;
Bmag = reshape(Bmag,1,[]);
T = reshape(T,1,[]);
ERl = reshape(ERl,1,[]);
Jc_deviation = reshape(Jc_deviation,1,[]);

% calculate critical current (Jc is given per width in A/mm)
sc = sc_NbTi;
sc.setLHC(); % bottura scaling relation
%sc.setCustomFit(); % This was changed, 28/8/15, to accomodate the properties of the wire of the Tim+Alexey coil


if useinterp == true
    % only calculate if not over Tc or at 4.2K
    idx1 = T > sc.Tc0;
    idx2 = T < 4.25;
    idx3 = ~idx1 & ~idx2;
    Jc = zeros(size(Bmag));
    Jc(idx3) = sc.Jcnoncu(Bmag(idx3),T(idx3)); % A/mm^2
    
    % interpolate for the case: T = 4.2K    
    Binterp = 0.1:0.4:max(max( Bmag )) + 0.4;
    Jinterp = sc.Jcnoncu(Binterp,4.2.*ones(1,length(Binterp)));
    Jc(idx2) = lininterp1f_p(Binterp,Jinterp,Bmag(idx2),true);
else
    idx1 = T > sc.Tc0;
    Jc = zeros(size(Bmag)); 
    Jc(~idx1) = sc.Jcnoncu(Bmag(~idx1),T(~idx1));
end


% calculate Ic
Ic = 1e6 .* Acond .* ScFactor .* Jc .* Jc_deviation; % A

% preallocate
Isc = zeros(1,Ns);


if ScFactor ~= 0    %@Rosalinde 31/08/2015  --> else Isc stays zeros
    
    % over Tc (above current sharing)
    overTc = Ic <= 0; %| Itot < 0.1;
    Isc(overTc) = 0;
    
    % Below Ic
    belowIc = Itot < Ic;
    Isc(belowIc) = Itot;
    
    % sharing regime
    inSharing = ~overTc & ~belowIc;
    
    
    
    % advanced current sharing
    if useMatthijsAndRosalinde;
        % check if there is any element in sharing regime
        if any(inSharing)
            % set lowe boundary            
            Isclow = zeros(1,nnz(inSharing));
            Iscmid = zeros(1,nnz(inSharing));
            Ischigh = Itot.*ones(1,nnz(inSharing));       
            ERlcopy=ERl(inSharing);
            
            for i=1:16
                Iscmid=0.5*(Isclow+Ischigh);
                Esc = E0.*(Iscmid./Ic(inSharing)).^N;
                Enorm = ERlcopy.*(Itot-Iscmid);
                
                Ischigh=Iscmid.*(Esc>Enorm)+Ischigh.*(Esc<=Enorm);
                Isclow=Iscmid.*(Esc<Enorm)+Isclow.*(Esc>=Enorm);
            end
            Isc(inSharing)=0.5*(Isclow+Ischigh);                    
        end
    end
    
    % advanced current sharing
    if ~useMatthijsAndRosalinde && usefzero == false;
        % check if there is any element in sharing regime
        if any(inSharing)
            % set lowe boundary
            lb = zeros(1,nnz(inSharing));
            Istart = ones(1,nnz(inSharing))*Itot;
            
            % Newton-Raphson method first run
            f = @(Isc) Itot - (E0./ERl(inSharing)).*(Isc./Ic(inSharing)).^N - Isc;
            Isc(inSharing) = nrfzero(f,Istart,lb,1e-3);
            %       Isc(inSharing) = nrfzero(f,lb,Istart,1e-3);
        end
    end
    
    % build in matlab function (slower)
    if ~useMatthijsAndRosalinde && usefzero == true;
        % check if there is any element in sharing regime
        if any(inSharing)
            % function start point
            %Istart = ones(1,nnz(inSharing))*Itot;
            
            % Newton-Raphson method first run
            f = @(Isc,i)Itot - (E0./ERl(i)).*(Isc./Ic(i)).^N - Isc;
            
            % array solver
            Isc(inSharing) = arrayfun(@(i)fzero(@(x)f(x,i),0),find(inSharing));
        end
    end
end


% finish up
Inc = Itot - Isc;
E = Inc .* ERl;
Pl = E .* Itot;

% reshape back
Isc = reshape(Isc,n1,n2);
Inc = reshape(Inc,n1,n2);
Pl = reshape(Pl,n1,n2);
E = reshape(E,n1,n2);

end