classdef Create_System < hgsetget
    
    %% Description:
    % Calculate quench simulates a quench by coupling a heat transfer model
    % to a magnetic model. Older versions were designed for one coil. This
    % one is designed for multiple thermally isolated coils in one electrical
    % circuit.
    
    %% User input settings
    properties
        name = [];
        printwindow = -1;
        
        
        coils = {};         % dependency with coils 
        circ = {};          % dependency with circuits
        tlinks = {};        % dependency with tlinks
        heliumbaths = {};   % dependency with heliumbaths
        % new Nikkie:
        layers = {};        % dependency with layers, when you want to make the plots per layer, add layer names with this property for the legend in the plots! 
        
        qcoil = [];         % class of quenchcoil
        qcirc = [];         % circuit of quenchcoil
        % new Nikkie:
        Tquench = [];       % Temperature than the coil must be raised to above critical temperature for a quench

        tstart = 0;    
        tmax = 45;          % alternative way of stopping the solver; it should stop based on current
                            % to stop the solver after t = tmax go to
                            % SolveQuench.m, last lines, and change
                            % val(end)
        tstop = [];         % used for alligning plot times. For paralel computing this can't be used
        
        VQD_threshold = []; % alternative way to define tQD based Vnz of quenchcircuit; ms.qcirc.Vnz > ms.VQDthreshold
        dt_QD = [];         % time delay between ms.qcirc.Vnz > ms.VQD_threshold and tQD
        tQD = [];           % time the quench is detected: at this time quench protection systems are initiated
        
        %qhworking = [];     % boolian to indicate if quench heaters are working   (make qh property for each heater?)
        %Rdworking = [];     % boolian to indicate if dump resistors are working   (make circuit property for each dumpresistor?)
        
        scenario = [];      % name of scenario: this name should be unique to distinguish this quench calculation from others.
        shortscenario = []; % short version of ms.scenario
        
        AlwaysUpdateB = false;  % boolian to toggle always updating B or only updating evert self.dt seconds
        forced_dIdt = [];
        
    
    end
    
    %% Static system properties
    properties
        indM = [];          % "real" inductance matrix (nele x nele) [UNUSED because of size]
        indMele = [];       % mutual inductances of all elements with all circuits (nele x ncirc)
        indMcirc = [];      % mutual inductances of all circuits with all circuits (ncirc x ncirc)
        l2m = [];           % conversion indices to transform line back to matrix
        s2s = [];           % indices to find currents, temperatures and energies in solver-array
        dt_events = [];     % collection of event times relative to tQD (unique)
        
%         syspath = [];       % path where one can find system information (like an unsolved ms and indM)
%         path = [];          % path of folder inside syspath --> used for specific quench reports etc
        
        % @Rosalinde 21-08
        totallayers =0;
        totalturns = 0;
        
    end
    
    %% Dynamic system properties
    properties
        t = [];         % time
        dIdt = [];      % array of dIdt's for each circuit
        dTdt = [];      % array of dTdt's for each element
        Emag = [];      % magnetic energy of system
        Eth = [];       % thermal energy of system
        Edisdio = [];   % dissipated energy of diodes
        Edisdr = [];    % dissipated energy of dumpresistor
        Edisqh = [];    % dissipated energy in quench heaters
        tlastBupdate = 0; % the last time B was updated
        nfcncalls = 0;  % counter for function calls
    end
    
    %% Solver properties
    properties
        dt = [];        % timestep (data is stored every dt seconds)
        abstol = 1E-6;  % absolute tolerance used by solver 
        reltol = 1E-3;  % relative tolerance used by solver
        solverSubSteps = 250;
    end
    
    %% Assign arrays / cells to store properties
    properties
        tstore = []; 
        Ystore = [];
        
        Istore = {};
        dIdtstore = {};
        Vtapsstore = {};
        VQDstore = {};
        
        Emagstore = {};
        Ethstore = {};
        Edisdiostore = {};
        Edisdrstore = {};
        Edisqhstore = {};
        
        Tmaxstore = {};
%         % new Nikkie
%         Tmaxlaystore = {};          % these properties are used in the quenchplot class to make plots for each layer!
%         Tmaxlayerstore = {};        % see lines 800 - 850, add a line for every layer to include it in the plots!
%         Tavaragestore = {};
%         Tavaragelaystore = {};
%         Tavaragelayerstore = {};
%         % end new Nikkie
        Vt2tmaxstore = {};
        Vl2lmaxstore = {};
        % @Rosalinde 21/08
        Vl2lstore = {};
        Vl2laltstore = {};
        Vlayerstore = {};
        mapVl2lstore = [];  % gives for every coil in the system the colom of the first layer in Vl2lstore
        % @Rosalinde 07/09/2015
        Vl2lindstore = {};
        Vl2lnzstore = {};
%         % new Nikkie
%         Vt2tmaxlaystore = {};       % same!
%         Vt2tmaxlayerstore = {};
%         Vt2tavaragestore = {};
%         Vt2tavaragelaystore = {};
%         Vt2tavaragelayerstore = {};
%         VtotlayerstoreA = {};
%         VtotlayerstoreB = {};
%         Vtotlayerstore = {};
%         % end new Nikkie
                
    end
    
    %% Method for checking the user settings
    methods
        function [ch,msg] = checksettings(self)
            % retard check
            ch = [];
            msg = [];
            
        end
    end
    
    %% Add circuits
    methods
        function addcirc(self,varargin) % a system can have multiple circuits!
            % analyse input
            if isempty(varargin)
                error(['an attempt was made to add a circuit to ' self.name ', but no circuit was supplied']);
            end
            
            circs = varargin;
           
            % go through newly supplied circuits
            for i = 1:length(circs)
                % add system to all circuits
                circs{i}.system = self;
                
                % add circnumber to circuits
                circs{i}.circnr = length(self.circ) + i;
                
            end
            
            % add circuits to self.circ
            self.circ = {self.circ{:} circs{:}};
            
            % add coils to system and vice versa
            for i = 1:length(circs)

                % system in coils
                for j = 1:length(circs{i}.coils)
                    circs{i}.coils{j}.system = self;
                    self.totallayers = self.totallayers+circs{i}.coils{j}.nrad;
                    self.totalturns = self.totalturns+circs{i}.coils{j}.nax;

                end
                
                % coils in system
                self.coils = {self.coils{:} circs{i}.coils{:}};
                
            end
            
            % Make sure interface is displaying newly added coils
            drawnow;

        end
    end
    
    %% Calculate inductance matrix M (should store in designated folder)
    methods
        function calcM(self)
            % names
            indMstr = [self.syspath,filesep,'indM.mat'];
            
            % load if possible, otherwise calculate and store
            if exist(indMstr,'file')
                jprintf(self.printwindow,'Loading inductance matrix\n');
                data = load(indMstr);
                self.indMele = data.indMele;
                self.indMcirc = data.indMcirc;
                
            else
                
                %% M for circuitcoils
                jprintf(self.printwindow,'Calculating Mutual Inductances Elements-Circuits\n','>>');
                
                % obtain long lines containing all individual wires
                Rinner = [];
                Router = [];
                Zlow = [];
                Zhigh = [];                
                dir = [];
                for i = 1:self.ncoils
                    Rinner = [Rinner mat2line(self.coils{i}.Rinner)]; %#ok
                    Router = [Router mat2line(self.coils{i}.Router)]; %#ok
                    Zlow = [Zlow mat2line(self.coils{i}.Zlow)]; %#ok
                    Zhigh = [Zhigh mat2line(self.coils{i}.Zhigh)]; %#ok
                    dir = [dir ones(1,size(self.coils{i}.Zhigh,1)*size(self.coils{i}.Zhigh,2))*self.coils{i}.dir]; %#ok
                end
                
                % go through circuits
                self.indMele = zeros(self.nele,self.ncirc);
                for i = 1:self.ncirc
                    % get coil information of given circuit
                    coilRinner = []; coilRouter = []; coilZlow = []; coilZhigh = []; Nele = []; dircoil = [];
                    
                    % go through coils of circuit
                    for j = 1:self.circ{i}.ncoils
                        coilRinner = [coilRinner self.circ{i}.coils{j}.radrange(1)]; %#ok
                        coilRouter = [coilRouter self.circ{i}.coils{j}.radrange(2)]; %#ok
                        coilZlow = [coilZlow self.circ{i}.coils{j}.axrange(1)]; %#ok
                        coilZhigh = [coilZhigh self.circ{i}.coils{j}.axrange(2)]; %#ok
                        dircoil = [dircoil,self.circ{i}.coils{j}.dir]; %#ok
                        Nele = [Nele self.circ{i}.coils{j}.nele]; %#ok
                    end
                    
                    Mele = zeros(self.nele,1);
                    for j = 1:length(Rinner)
                        % calculate mutual inductance of j'th element with i'th circuit
                        miniM = soleno_calcM([Rinner(j) coilRinner],[Router(j) coilRouter],[Zlow(j) coilZlow],[Zhigh(j) coilZhigh],[1 Nele],ones(1,1+length(coilRinner))*5);
                        vdir = [dir(j),dircoil];
                        [v1,v2] = meshgrid(vdir,vdir);
                        miniM = v1.*v2.*miniM;
                        Mele(j) = sum(miniM(1,2:end));
                    end
                    
                    % denote mutual inductances of all elements with i'th
                    % circuit in i'th column of self.indMele
                    self.indMele(:,i) = Mele;
                end
                
                % extract mutual inductances of circuits with circuits
                self.indMcirc = []; a = 1;
                for i = 1:self.ncirc
                    b = a + self.circ{i}.nele - 1;
                    row = sum( self.indMele((a:b),:) ,1);
                    self.indMcirc = [self.indMcirc ; row];
                    a = b+1;
                end
                
                % save data to file
                jprintf(self.printwindow,'Saving Mutual Inductance Matrices to File\n');
                indMele = self.indMele; indMcirc = self.indMcirc;
                save(indMstr,'indMele','indMcirc');
                
            end
            
        end
    end
    
    %% Create time independent properties of the magnets and allocate arrays
    methods
        function initialize(self)
            jprintf(self.printwindow,'Initializing System\n','O>>');
            tic
            
%             % define path for this system
%             fname = mfilename('fullpath');
%             [mfilepath,~,~] = fileparts(fname);
%             self.syspath = [mfilepath filesep self.name];
%             if ~exist(self.syspath,'dir'); mkdir(self.syspath); end;
%             
%             % create path specifically for this quench
%             self.path = [self.syspath,filesep,self.shortscenario];
%             if ~exist(self.path,'dir'); mkdir(self.path); end;
            
            % set currents to initial values
            self.setIini;
            
            % initialize coils
            jprintf(self.printwindow,'Initializing coils\n','>>');
            for i = 1:self.ncoils
                self.coils{i}.coilnr = i;
                self.coils{i}.initialize
            end
            jprintf(self.printwindow,'Done\n','<<');
            
            % start and end indices for line2mat operations
            nele = [];
            for i = 1:self.ncoils
                nele = [nele self.coils{i}.nele]; %#ok
            end
            self.l2m = [1,nele(1)];
            if length(nele)>=2
                for i = 2:length(nele)
                    self.l2m = [self.l2m; [self.l2m(i-1,2)+1, self.l2m(i-1,2)+nele(i)]];
                end
            end
            
            % start and end indices to convert data from solver to system (I T Edr Edio Eqh)
            self.s2s = zeros(5,2);
            self.s2s(1,:) = [1,self.ncirc];
            self.s2s(2,:) = [self.s2s(1,2)+1,self.s2s(1,2)+self.nele];
            self.s2s(3,:) = [self.s2s(2,2)+1,self.s2s(2,2)+self.ncirc];
            self.s2s(4,:) = [self.s2s(3,2)+1,self.s2s(3,2)+self.ncirc];
            self.s2s(5,:) = [self.s2s(4,2)+1,self.s2s(4,2)+self.ncoils];
            
            % obtain mutual inductances
            
           
            self.calcM;
            
            % initialize circuits            
            jprintf(self.printwindow,'Initializing circuits\n','>>');
            for i = 1:self.ncirc
                self.circ{i}.circnr = i;
                self.circ{i}.initialize;
            end
            jprintf(self.printwindow,'Done\n','<<');
            
            % collect event times relative to tQD
            % go through circuits
            dt_e = [];
            for i = 1:self.ncirc
                % circuit breakers
                if ~isempty(self.circ{i}.dt_CB)
                    dt_e = [dt_e; self.circ{i}.dt_CB];
                end
                % dumpresistors
                if ~isempty(self.circ{i}.dt_Rd)
                    dt_e = [dt_e; self.circ{i}.dt_Rd];
                end
            end
            
            % quench heaters
            for i = 1:self.ncoils
                dt_qh_start = zeros(length(self.coils{i}.qh),1);
                dt_qh_stop = zeros(length(self.coils{i}.qh),1);
                for j = 1:length(self.coils{i}.qh)
                    dt_qh_start(j) = self.coils{i}.qh{j}.response_t;
                    dt_qh_stop(j) = dt_qh_start(j) + self.coils{i}.qh{j}.duration;
                end
                dt_e = [dt_e; dt_qh_start; dt_qh_stop];
            end
            
            % uniquify event times
            self.dt_events = unique(dt_e);
            
        end
    end
    

    
    %% Initiate quench 
    % this function assumes just one coil to quench. 'pos' can be either a two coordinate position or a string 'Bmax' to find the most likely position for the coil to quench (given its temperature is constant)
    methods
        function quench(self,coilname,pos)
            % find coil to be quenched
            for i = 1:self.ncoils
                if strcmp(self.coils{i}.name,coilname)
                    % store quenchcoil
                    self.qcoil = self.coils{i};
                    
                    % store quenchcircuit
                    self.qcirc = self.coils{i}.circ;
                    
                end
            end
            
            % analyse 'pos';
            if ischar(pos) && strcmp(pos,'Bmax')
                % calculate Bmag for given currents
                B = self.qcoil.calcBmag;
                
                % find maximum
                [val,p1] = max(B);
                [~,p2] = max(val);
                
                % define pos  with two coordinates
                pos = [p1(p2),p2];
                
            end
            
            % quench given coil
            self.qcoil.quench(pos,self.Tquench);
            
            % @Rosalinde 20/09/15
            % new new new new new Nikkie
            %% Initialize the thermal links in the system
            % If there are thermal links in the system, with this function the k
            % values are calculated because this only needs to be performed once.
            for i = 1:length(self.tlinks)
                self.tlinks{i}.system = self;
                self.tlinks{i}.linknr = i;
                self.tlinks{i}.getStatics;
                self.tlinks{i}.calck;
            end
            % end new new new new new Nikkie
            
            % @Rosalinde 02/09/2015
            for i = 1:length(self.heliumbaths)
                self.heliumbaths{i}.initialize;
            end
            % end@Rosalinde 02/09/2015
            
            
        end
    end
    
    %% Get current of circuits
    methods
        function [I] = getI(self)
            % go through circuits and obtain currents
            I = zeros(1,self.ncirc);
            for i = 1:self.ncirc
                I(i) = self.circ{i}.I;
            end
            
        end
    end
    
    %% Get temperature of elements in array form
    methods
        function [T] = getT(self)
            % go through coils and obtain temperatures
            T = [];
            for i = 1:self.ncoils
                T = [T mat2line(self.coils{i}.T)]; %#ok
            end
            
        end
    end
    
    %% Set current of circuits
    methods
        function setI(self,Iset)
            for i = 1:self.ncirc
                self.circ{i}.I = Iset(i);
            end
            
        end
    end
    
    %% Set current of circuits to initial values
    methods        
          
        function setIini(self)
            
            for i = 1:self.ncirc
                    self.circ{i}.I = self.circ{i}.Iinitial;
            end
            
        end
    end
    
    %% Set temperature of elements for given temperature array
    methods
        function setT(self,Tset)
            for i = 1:self.ncoils
                ind1 = self.l2m(i,1);
                ind2 = self.l2m(i,2);
                self.coils{i}.T = line2mat(Tset(ind1:ind2),self.coils{i}.nrad);
            end
            
        end
    end
    
    %% Update coil and system properties
    
    % This function is used in SolveQuench to go from on iteration to the
    % next. 
    % This function calls the update function of the coils, see
    % Create_Coil().
    
    methods
        function update(self)
            % report
            %jprintf(self.printwindow,'updating coil properties\n');
            
            for i = 1:self.ncoils
                self.coils{i}.Qlink = zeros(self.coils{i}.nrad, self.coils{i}.nax);
                % @Rosalinde 14/09/2015
                % Create array of zeros (nrad*nax) to later write transfered heat in
                self.coils{i}.Qbath = zeros(self.coils{i}.nrad, self.coils{i}.nax);
                % end@Rosalinde 14/09/2015
            end
            
            % calculate thermal link energy exchange
            for i = 1:self.ntlinks
                self.tlinks{i}.getTemp();
                self.tlinks{i}.calcK();
                self.tlinks{i}.calcQ();
                self.tlinks{i}.addQ();
            end
            
            % @Rosalinde 02/09/2015      
            for i = 1:length(self.heliumbaths)
                self.heliumbaths{i}.updateBath;
            end
            % end@Rosalinde 02/09/2015
            
            % update coils
            for i = 1:self.ncoils
                self.coils{i}.update;
            end
            
            % update system
            self.calcdIdt;
            self.getdTdt;
        end
    end
    
    %% Calculate dIdt
    methods
        function calcdIdt(self)
            if isempty(self.forced_dIdt)
                % go through circuits to collect (NZ + external)-voltage
                V = zeros(self.ncirc,1);
                for i = 1:self.ncirc
                    V(i) = self.circ{i}.calcVdrop;
                end
                
                % calculate dIdt
                self.dIdt = (-self.indMcirc\V)';
            else
                self.dIdt = self.forced_dIdt;                 
            end
            
        end
    end
    
    %% Get dTdt of elements in array form
    methods
        function getdTdt(self)
            % define dTdt
            self.dTdt = [];
            
            % iterate through coils and obtain temperature derivatives
            for i = 1:self.ncoils
                self.dTdt = [self.dTdt mat2line(self.coils{i}.dTdt)];
            end           
        end
    end
    
    %% Fire all quench heaters
    methods
        function fireallQH(self)
            for i = 1:self.ncoils
                self.coils{i}.fireQH;
            end
        end
    end
    
    %% Calculate induced voltages
    methods
        function calcVind(self)
            % calculate all induced voltages
            allVind = self.indMele * self.dIdt';
            
            % separate long strings back into matrices of coils
            for i = 1:self.ncoils
                self.coils{i}.Vind = line2mat( allVind(self.l2m(i,1):self.l2m(i,2)), self.coils{i}.nrad);

            end
            
        end
    end
    
    %% Calculate voltage relative to CL
    methods
        function calcVcs(self)
            % tell all circuits to calculate Vcs and store values in coils
            for i = 1:self.ncirc
                self.circ{i}.calcVcs;
            end
        end
    end 
    
    %% Calculate magnetic, thermal, dissipated and total energy
    methods
        function calcE(self)
            
            % prep I
            [I1,I2] = meshgrid(self.getI,self.getI);
            
            % calculate magnetic energy
            self.Emag = sum(0.5*self.indMcirc.*I1.*I2, 1);      % or sum(...,2)' ?
            
            % calculate and add up thermal energies for all coils
            self.Eth = zeros(1,self.ncoils);
            for i = 1:self.ncoils
                self.coils{i}.calcEth;
                self.Eth(i) = sum(sum( self.coils{i}.Eth ));
            end
            
        end
    end
    
    %% Dissipation power in all dumpresistors
    methods
        function Pdr = Pdr(self)
            % preallocate
            Pdr = zeros(self.ncirc,1);
            
            % go through circuits
            for i = 1:self.ncirc
                % save calculation time by not calculating if Rd is not
                % working
                if self.circ{i}.Rdworking
                    Pdr(i) = self.circ{i}.I^2 * self.circ{i}.Rextern;
                end
            end
            
        end
    end
    
    %% Dissipation power in all diodes
    methods
        function Pdio = Pdio(self)
            % preallocate
            Pdio = zeros(self.ncirc,1);
            
            % go through circuits
            for i = 1:self.ncirc
                Pdio(i) = self.circ{i}.I * self.circ{i}.Vdio;    
            end
            
        end
    end
    
    %% Quench heater power
    methods
        function Pqh = Pqh(self)
            % start with zero power
            Pqh = zeros(self.ncoils,1);
            
            % go through coils
            for i = 1:self.ncoils
                Pqh(i) = sum(sum( self.coils{i}.Pqh ));
            end
            
        end
    end
    
    %% Reduce solved data based on maximum deviations in temperature
    methods
        function reducedata(self,threshold)
            %report
            jprintf(self.printwindow,'Reducing data to be processed\n');
                        
            % reduce amount of data to be calculated
            j = 1;
            calc(1:length(self.tstore)) = false; calc(1) = true; calc(length(self.tstore)) = true;
            for i = 2:length(self.tstore)
                % compare last Y-array to be stored with i'th array to be stored
                change = diff([self.Ystore(i,self.s2s(2,1):self.s2s(2,2));self.Ystore(j,self.s2s(2,1):self.s2s(2,2))],1,1);
                
                % if one of the changes is greater than given threshold value --> calculate it
                if any(abs(change) > threshold)
                    j = i; calc(j) = true;
                end
                
                % if the time is an event time also keep it
                if ~isempty(self.tQD) && ~isempty(self.dt_events) && any(self.tQD + self.dt_events - self.tstore(i) == 0)
                    j = i; calc(j) = true;
                end
                
            end
            
            % reduce tstore and Y in size
            self.tstore = self.tstore(calc);
            self.Ystore = self.Ystore(calc,:); 
            
        end
    end
      
    %% Reiterate through calculated values and store data
    
    % these properties are used to make the plots!
    
    methods
        function postprocess(self,varargin)
            
            % analyse input
            if numel(varargin) == 1
                self.reducedata(varargin{1});
            end
            
            % report
            jprintf(self.printwindow,'Post processing:\n','O>>');
            
            % denote length tstore
            lts = length(self.tstore);
            
            % preallocate store array cells
            self.Istore = cell(lts,1);
            self.dIdtstore = cell(lts,1);
            self.Vtapsstore = cell(lts,1);
            self.VQDstore = cell(lts,1);
            
            self.Emagstore = cell(lts,1);
            self.Ethstore = cell(lts,1);
            self.Edisdiostore = cell(lts,1);
            self.Edisdrstore = cell(lts,1);
            self.Edisqhstore = cell(lts,1);
            
            % preallocate max values
            self.Tmaxstore = cell(lts,self.ncoils);
%             % new Nikkie
%             self.Tmaxlaystore = cell(lts,self.ncoils);
%             self.Tmaxlayerstore = cell(lts,self.ncoils*2);
%             self.Tavaragestore = cell(lts,self.ncoils);
%             self.Tavaragelaystore = cell(lts,self.ncoils);
%             self.Tavaragelayerstore = cell(lts,self.ncoils);
%             % end new Nikkie
            self.Vt2tmaxstore = cell(lts,self.ncoils);
            self.Vl2lmaxstore = cell(lts,self.ncoils);
            
            % @Rosainde 21/08
            self.Vl2lstore = cell(lts,self.totallayers);
            self.Vl2laltstore = cell(lts,self.totallayers);
            self.Vlayerstore = cell(lts,self.totallayers);
            for i = 1:self.ncoils
                if i==1
                    self.mapVl2lstore(i) = 1;
                else
                    self.mapVl2lstore(i) = self.mapVl2lstore(i-1)+self.coils{i-1}.nrad;
                end
            end
            % @Rosalinde 07/09/2015
            self.Vl2lindstore = cell(lts, self.totallayers);
            self.Vl2lnzstore = cell(lts, self.totallayers);
            
%             % new Nikkie
%             self.Vt2tmaxlaystore = cell(lts,self.ncoils);
%             self.Vt2tmaxlayerstore = cell(lts,self.ncoils*2);
%             self.Vt2tavaragestore = cell(lts,self.ncoils);
%             self.Vt2tavaragelaystore = cell(lts,self.ncoils);
%             self.Vt2tavaragelayerstore = cell(lts,self.ncoils*2);
%             self.VtotlayerstoreA = cell(lts,self.ncoils);
%             self.VtotlayerstoreB = cell(lts,self.ncoils);
%             self.Vtotlayerstore = cell(lts,self.ncoils*2);
%             % end new Nikkie
            
            % preallocate arrays inside cells
            nVtaps = length(self.qcirc.Vtapsindices)+self.qcirc.addV0;
            nVQD = size(self.qcirc.QDconfig,1);
            for i = 1:lts
                self.Istore{i} = zeros(1,self.ncirc);
                self.dIdtstore{i} = zeros(1,self.ncirc);
        
                self.Emagstore{i} = zeros(1,self.ncirc);
                self.Edisdiostore{i} = zeros(1,self.ncirc);
                self.Edisdrstore{i} = zeros(1,self.ncirc);
                
                self.Ethstore{i} = zeros(1,self.ncoils);
                self.Edisqhstore{i} = zeros(1,self.ncoils);
                
                self.Vtapsstore{i} = zeros(1,nVtaps);
                self.VQDstore{i} = zeros(1,nVQD);
            end
            
            % report
            jprintf(self.printwindow,'Re-entering given data -> calculating all values and store them\n');
            
            % iterate through tstore, calculate system properties and store data
            for i = 1:length(self.tstore)
                
                % set time
                if i~=1; self.dt = self.tstore(i) - self.t; end
                self.t = self.tstore(i);
                
                % toggle update B flag if needed
                for j = 1:self.ncoils
                    self.coils{j}.Bupdatereq = true;
                end
                
                % set properties of system time self.tstore(i)
                self.settime(i)
                
%                 % set I, T and energies
%                 self.setI(self.Ystore(i,self.s2s(1,1):self.s2s(1,2)));
%                 self.setT(self.Ystore(i,self.s2s(2,1):self.s2s(2,2)));
                
%                 % calculate properties coils 
%                 self.update;
%                 self.calcVind;
%                 self.calcVcs;
%                 self.calcE;
                
%                 % set energies
%                 self.Edisdr = self.Ystore(i,self.s2s(3,1):self.s2s(3,2))';
%                 self.Edisdio = self.Ystore(i,self.s2s(4,1):self.s2s(4,2))';
%                 self.Edisqh = self.Ystore(i,self.s2s(5,1):self.s2s(5,2))';
                
                % calculate quench detection signal if quench is not detected yet
                %if self.t <= self.tQD; 
                    self.qcirc.calcVQD; 
                    self.qcirc.calcVnz;  
%                 elseif ~isempty(self.qcirc.VQD)
%                     self.qcirc.VQD = [];
%                     self.qcirc.Vnz = [];
%                 end
                
                % store data
                self.storedata;
                             
            end
            
            % save solved system
            ms = self;
            save([self.syspath,filesep,'ms_' self.shortscenario '.mat'],'ms','-v7.3');
            
        end
    end
    
    %% Store data
    methods
        function storedata(self)
            jprintf(self.printwindow,[sprintf('t = %02.3f',self.t) '\n']);
            
            % find length of tstore array
            idx = find(self.t == self.tstore);
            
            % energies (one array per timestep)
            self.Emagstore{idx,1} = self.Emag;
            self.Ethstore{idx,1} = self.Eth;
            self.Edisdiostore{idx,1} = self.Edisdio;
            self.Edisdrstore{idx,1} = self.Edisdr;
            self.Edisqhstore{idx,1} = self.Edisqh;
            
            % system properties (one array per timestep)
            self.Istore{idx,1} = self.getI;
            self.dIdtstore{idx,1} = self.dIdt;
            self.Vtapsstore{idx,1} = self.qcirc.Vtaps;
            self.VQDstore{idx,1} = [ self.qcirc.Vnz, self.qcirc.VQD ];
            
            % coil properties (one value per coil per timestep)
            for i = 1:self.ncoils
                oddlayer = 1; %@Rosalinde if layernumber is odd: oddlayer =1, else oddlayer = -1;
                self.Tmaxstore{idx,i} = max(max( self.coils{i}.T ));           
                self.Vt2tmaxstore{idx,i} = max(max( abs(diff(self.coils{i}.Vcs,1,2)) )); %cell element is empty if coil is 'not relevant'
                self.Vl2lmaxstore{idx,i} = max(max( abs(diff(self.coils{i}.Vcs,1,1)) )); %cell element is empty if coil is 'not relevant'
                
                 % @Rosalinde 21/08
                % Calculating layer to layer voltages
                for j = 1:self.coils{i}.nrad-1; % Loops over the layers, EXCEPT FOR THE LAST ONE!!!!
                    if oddlayer == 1
                        self.Vl2lstore{idx,self.mapVl2lstore(i)+j-1} = self.coils{i}.Vcs(j+1,self.coils{i}.nax)...
                            -self.coils{i}.Vcs(j,1); %comparing first element of row of layer j with last element of row of layer j+1
                        % @Rosalinde 07/09/2015
                        self.Vl2lindstore{idx,self.mapVl2lstore(i)+j-1} = self.coils{i}.Vindcs(j+1,self.coils{i}.nax)...
                            -self.coils{i}.Vindcs(j,1); % for inductive component
                        self.Vl2lnzstore{idx,self.mapVl2lstore(i)+j-1} = self.coils{i}.Vnzcs(j+1,self.coils{i}.nax)...
                            -self.coils{i}.Vnzcs(j,1); % for resistive component
                    elseif oddlayer == -1
                        self.Vl2lstore{idx,self.mapVl2lstore(i)+j-1} = self.coils{i}.Vcs(j+1,1)...
                            -self.coils{i}.Vcs(j,self.coils{i}.nax); %comparing last element of row of layer j with first element of row of layer j+1
                       % @Rosalinde 07/09/2015
                        self.Vl2lindstore{idx,self.mapVl2lstore(i)+j-1} = self.coils{i}.Vindcs(j+1,1)...
                            -self.coils{i}.Vindcs(j,self.coils{i}.nax); % for inductive component
                        self.Vl2lnzstore{idx,self.mapVl2lstore(i)+j-1} = self.coils{i}.Vnzcs(j+1,1)...
                            -self.coils{i}.Vnzcs(j,self.coils{i}.nax); %for resistive component
                    else disp('in the loop something went wrong with keeping track of the parity of the layers')
                    end
                    oddlayer = oddlayer * -1;
                end
                % Calculating last element of Vl2lstore
                if oddlayer == 1
                    self.Vl2lstore{idx,self.mapVl2lstore(i)+self.coils{i}.nrad-1} = self.coils{i}.Vcs(self.coils{i}.nrad,self.coils{i}.nax)...
                        -self.coils{i}.Vcs(self.coils{i}.nrad,1);
                    % @Rosalinde 07/09/2015
                    self.Vl2lindstore{idx,self.mapVl2lstore(i)+self.coils{i}.nrad-1} = self.coils{i}.Vindcs(self.coils{i}.nrad,self.coils{i}.nax)...
                        -self.coils{i}.Vindcs(self.coils{i}.nrad,1);   % for inductive component
                    self.Vl2lnzstore{idx,self.mapVl2lstore(i)+self.coils{i}.nrad-1} = self.coils{i}.Vnzcs(self.coils{i}.nrad,self.coils{i}.nax)...
                        -self.coils{i}.Vnzcs(self.coils{i}.nrad,1);   % for resistive component
                elseif oddlayer == -1
                    self.Vl2lstore{idx,self.mapVl2lstore(i)+self.coils{i}.nrad-1} = self.coils{i}.Vcs(self.coils{i}.nrad,1)...
                        -self.coils{i}.Vcs(self.coils{i}.nrad,self.coils{i}.nax);
                    % @Rosalinde 07/09/2015
                    self.Vl2lindstore{idx,self.mapVl2lstore(i)+self.coils{i}.nrad-1} = self.coils{i}.Vindcs(self.coils{i}.nrad,1)...
                        -self.coils{i}.Vindcs(self.coils{i}.nrad,self.coils{i}.nax);   % for inductive component
                    self.Vl2lnzstore{idx,self.mapVl2lstore(i)+self.coils{i}.nrad-1} = self.coils{i}.Vnzcs(self.coils{i}.nrad,1)...
                        -self.coils{i}.Vnzcs(self.coils{i}.nrad,self.coils{i}.nax);   % for resistive component
                else disp('in the loop something went wrong with keeping track of the parity of the layers')
                end  
                                        
                temp_Vl2l = rot90( max( diff(self.coils{i}.Vcs,1,1), [], 2) );  % calculates an array with the maximum value per layer of the differences of voltages between the layers per turn
                temp_Vlayer = rot90( max( self.coils{i}.Vcs, [], 2) );  % calculates the maximum value of the voltage of the turns per layer
                for j = 1:self.coils{i}.nrad;   % loops over the layers
                    self.Vlayerstore{idx,self.mapVl2lstore(i)+j-1} = temp_Vlayer(j);
                    if j==1
                    temp_Vlayer1 = max(self.coils{i}.Vcs, [], 2);
                    self.Vl2laltstore{idx,self.mapVl2lstore(i)+j-1} = temp_Vlayer1(self.coils{i}.nrad);
                    else
                    self.Vl2laltstore{idx,self.mapVl2lstore(i)+j-1} = temp_Vl2l(j-1);
                    end
                end
                % end @Rosalinde               
                                
                %self.dTdtstore{i,idx} = self.coils{i}.dTdt;
                %self.ERstore{i,idx} = self.coils{i}.ER;
                %self.EReffstore{i,idx} = self.coils{i}.EReff;
                %self.Iscstore{i,idx} = self.coils{i}.Isc;
                %self.Incstore{i,idx} = self.coils{i}.Inc;
                %self.Bmagstore{i,idx} = self.coils{i}.Bmag;
                %self.Vnzstore{i,idx} = self.coils{i}.Vnz;
                %self.Vindstore{i,idx} = self.coils{i}.Vind;
                %self.Vstore{i,idx} = self.coils{i}.Vnz + self.coils{i}.Vind;
                %self.Vcsstore{i,idx} =  self.coils{i}.Vcs;
            end
            
%             % new Nikkie
%             for i = 1:3
%                 self.Tmaxlaystore{idx,i} = rot90( max( self.coils{i}.T, [], 2 ));
%                 self.Tavaragestore{idx,i} = mean(mean( self.coils{i}.T ));
%                 self.Tavaragelaystore{idx,i} = rot90(mean( self.coils{i}.T, 2 ));
%                 self.Vt2tmaxlaystore{idx,i} = rot90(max(abs( diff(self.coils{i}.Vcs,1,2)), [], 2 ));
%                 self.Vt2tavaragestore{idx,i} = mean(mean( abs(diff( self.coils{i}.Vcs,1,2))));
%                 self.Vt2tavaragelaystore{idx,i} = rot90(mean( abs(diff( self.coils{i}.Vcs,1,2))));
%                 self.VtotlayerstoreA{idx,i} = (self.coils{i}.Vcs(1,1) - self.coils{i}.Vcs(1,self.coils{i}.nax)) ;
%                 self.VtotlayerstoreB{idx,i} = -1* (self.coils{i}.Vcs(2,1) - self.coils{i}.Vcs(2,self.coils{i}.nax)) ;
%             end
%                 
%                 
%             % Use these lines to store the values per layer. This is used
%             % in the quenchplot.m file.
%             
%             % Tmaxlayerstore
%             self.Tmaxlayerstore{idx,1} = self.Tmaxlaystore{idx,1}(1);
%             self.Tmaxlayerstore{idx,2} = self.Tmaxlaystore{idx,2}(1);
%             self.Tmaxlayerstore{idx,3} = self.Tmaxlaystore{idx,2}(1);
%             self.Tmaxlayerstore{idx,4} = self.Tmaxlaystore{idx,2}(2);
%             self.Tmaxlayerstore{idx,5} = self.Tmaxlaystore{idx,3}(1);
%             self.Tmaxlayerstore{idx,6} = self.Tmaxlaystore{idx,3}(2);
% %             self.Tmaxlayerstore{idx,7} = self.Tmaxlaystore{idx,4}(1);
% %             self.Tmaxlayerstore{idx,8} = self.Tmaxlaystore{idx,5}(1);
% %             self.Tmaxlayerstore{idx,9} = self.Tmaxlaystore{idx,6}(1);
% %             self.Tmaxlayerstore{idx,10} = self.Tmaxlaystore{idx,7}(1);
% %             self.Tmaxlayerstore{idx,11} = self.Tmaxlaystore{idx,8}(1);
% %             
%             % Tavaragelayerstore
%             self.Tavaragelayerstore{idx,1} = self.Tmaxlaystore{idx,1}(1);
%             self.Tavaragelayerstore{idx,2} = self.Tmaxlaystore{idx,2}(1);
%             self.Tavaragelayerstore{idx,3} = self.Tmaxlaystore{idx,2}(1);
%             self.Tavaragelayerstore{idx,4} = self.Tmaxlaystore{idx,2}(2);
%             self.Tavaragelayerstore{idx,5} = self.Tmaxlaystore{idx,3}(1);
%             self.Tavaragelayerstore{idx,6} = self.Tmaxlaystore{idx,3}(2);
% %             self.Tavaragelayerstore{idx,7} = self.Tmaxlaystore{idx,4}(1);
% %             self.Tavaragelayerstore{idx,8} = self.Tmaxlaystore{idx,5}(1);
% %             self.Tavaragelayerstore{idx,9} = self.Tmaxlaystore{idx,6}(1);
% %             self.Tavaragelayerstore{idx,10} = self.Tmaxlaystore{idx,7}(1);
% %             self.Tavaragelayerstore{idx,11} = self.Tmaxlaystore{idx,8}(1);
% %            
%             % V2t2maxlayerstore
%             self.Vt2tmaxlayerstore{idx,1} = self.Vt2tmaxlaystore{idx,1}(1);
%             self.Vt2tmaxlayerstore{idx,2} = self.Vt2tmaxlaystore{idx,2}(1);
%             self.Vt2tmaxlayerstore{idx,3} = self.Vt2tmaxlaystore{idx,2}(1);
%             self.Vt2tmaxlayerstore{idx,4} = self.Vt2tmaxlaystore{idx,2}(2);
%             self.Vt2tmaxlayerstore{idx,5} = self.Vt2tmaxlaystore{idx,3}(1);
%             self.Vt2tmaxlayerstore{idx,6} = self.Vt2tmaxlaystore{idx,3}(2);
% %             self.Vt2tmaxlayerstore{idx,7} = self.Vt2tmaxlaystore{idx,4}(1);
% %             self.Vt2tmaxlayerstore{idx,8} = self.Vt2tmaxlaystore{idx,5}(1);
% %             self.Vt2tmaxlayerstore{idx,9} = self.Vt2tmaxlaystore{idx,6}(1);
% %             self.Vt2tmaxlayerstore{idx,10} = self.Vt2tmaxlaystore{idx,7}(1);
% %             self.Vt2tmaxlayerstore{idx,11} = self.Vt2tmaxlaystore{idx,8}(1);
%             
%             % V2t2avaragelayerstore
%             self.Vt2tavaragelayerstore{idx,1} = self.Vt2tavaragelaystore{idx,1}(1);
%             self.Vt2tavaragelayerstore{idx,2} = self.Vt2tavaragelaystore{idx,2}(1);
%             self.Vt2tavaragelayerstore{idx,3} = self.Vt2tavaragelaystore{idx,2}(1);
%             self.Vt2tavaragelayerstore{idx,4} = self.Vt2tavaragelaystore{idx,2}(2);
%             self.Vt2tavaragelayerstore{idx,5} = self.Vt2tavaragelaystore{idx,3}(1);
%             self.Vt2tavaragelayerstore{idx,6} = self.Vt2tavaragelaystore{idx,3}(2);
% %             self.Vt2tavaragelayerstore{idx,7} = self.Vt2tavaragelaystore{idx,4}(1);
% %             self.Vt2tavaragelayerstore{idx,8} = self.Vt2tavaragelaystore{idx,5}(1);
% %             self.Vt2tavaragelayerstore{idx,9} = self.Vt2tavaragelaystore{idx,6}(1);
% %             self.Vt2tavaragelayerstore{idx,10} = self.Vt2tavaragelaystore{idx,7}(1);
% %             self.Vt2tavaragelayerstore{idx,11} = self.Vt2tavaragelaystore{idx,8}(1);
%             
%             % Vtotlayerstore
%             self.Vtotlayerstore{idx,1} = self.VtotlayerstoreA{idx,1};
%             self.Vtotlayerstore{idx,2} = self.VtotlayerstoreB{idx,1};
%             self.Vtotlayerstore{idx,3} = self.VtotlayerstoreA{idx,2};
%             self.Vtotlayerstore{idx,4} = self.VtotlayerstoreB{idx,2};
%             self.Vtotlayerstore{idx,5} = self.VtotlayerstoreA{idx,3};
%             self.Vtotlayerstore{idx,6} = self.VtotlayerstoreB{idx,3};
%             % end new Nikkie
            
        end
    end
    
    %% Set the system to the situation at time t (using Y-store)
    
    % with this function you can go back in time after the simulation. At
    % the end of each simulation, the self._X_store matrices contain the
    % information of the last timestep. With this function you can ask for
    % the information at a time before the last time step.
    
    methods
        function settime(self,varargin)
            % analyse input
            if isempty(varargin)
                tidx = find(self.tstore == self.t,1,'first');
            else
                tidx = varargin{1};
            end
            
            % find Y-array
            Y = self.Ystore(tidx,:);
            
            % set properties
            self.setI(Y( self.s2s(1,1):self.s2s(1,2) ));
            self.setT(Y( self.s2s(2,1):self.s2s(2,2) ));
            
            % set energies
            self.Edisdr = self.Ystore(tidx,self.s2s(3,1):self.s2s(3,2));
            self.Edisdio = self.Ystore(tidx,self.s2s(4,1):self.s2s(4,2));
            self.Edisqh = self.Ystore(tidx,self.s2s(5,1):self.s2s(5,2));
            
            % toggle all coils Bupdatereq to true
            for i = 1:length(self.coils)
                self.coils{i}.Bupdatereq = true;
            end
            
            % calculate properties coils
            self.update;
            self.calcVind;
            self.calcVcs;
            self.calcE;
            
        end
    end
    
    %% Clear dynamic properties of system and content
    methods
        function cleardynamicprops(self)
            % of system
            
            % of circuits
            
            % of coils
            
        end
    end
    
    %% Plot Geometry
    
    % This shows all the coils in the system, the quench heaters and the
    % voltage taps. If for instance the number of turns in the radial
    % direction does not match the size of the coil this will show in the
    % plotgeo.
    
    methods
        function [ax,f] = plotgeo(self,varargin)
            
            % check if user wants the plot to be visible
            visible = true;
            if numel(varargin) == 1
                visible = varargin{1};
            end
            
            % create figure
            f = figure('Color','w','colormap',jet(1000),'Position',[100,100,1000,400],'Renderer','OpenGL');
            ax = axes('Parent',f); hold(ax,'on'); grid(ax,'on');
            xlabel('Z [m]')
            ylabel('R [m]')
            title(['Geometry of ' self.name ' magnet system'])
            
            % visibility toggle (useful for exporting large amounts of plots)
            if visible == false; set(f,'Visible','off'); end
    
            % go through circuits and combine circuit plotgeos
            for i = 1:self.ncirc
                [axi,fi] = self.circ{i}.plotgeo(false); 
                obj = get(axi,'Children');
                set(obj,'Parent',ax);
            end
            
            % finish up
            cf = get(fi,'Children'); leg = cf(1);
            set(leg,'Parent',f,'Location','North')
            
            
        end
    end
    
    %% Copy method
    methods
        function new = copy(this)
            % Instantiate new object of the same class.
            new = feval(class(this));
            
            % Copy all non-hidden properties.
            p = properties(this);
            for i = 1:length(p)
                new.(p{i}) = this.(p{i});
            end
        end
    end
    
    %% Number of circuits
    methods
        function [ncirc] = ncirc(self)
            ncirc = length(self.circ);
        end
    end
    
    %% Number of coils
    methods
        function [ncoils] = ncoils(self)
            ncoils = length(self.coils);
        end
    end
        
    %% Number of links
    methods
        function [ntlinks] = ntlinks(self)
            ntlinks = length(self.tlinks);
        end
    end
    
    %% Number of elements
    methods
        function [nele] = nele(self)
            nele = 0;
            for i = 1:self.ncoils
                nele = nele + self.coils{i}.nele;
            end
        end
    end
    
    %% Find conductors
    methods
        function [condlist] = cond(self)
            % go through coils
            names = cell(1,self.ncoils);
            for i = 1:self.ncoils
                names{i} = self.coils{i}.cond.name;
            end
            
            % only take every conductor type once
            [~,idx] = unique(names);
            
            % go through indices
            condlist = cell(1,length(idx));
            for i = 1:length(idx)
                condlist{i} = self.coils{ idx(i) }.cond;
            end
            
        end
    end

    %% Path where one can find system information (like an unsolved ms and indM)
    methods
        function syspath = syspath(self)
                % location of m-file
                fname = mfilename('fullpath');
            
                % reduce to path
                [mainpath,~,~] = fileparts(fname);
                syspath = [mainpath,filesep,self.name];
            
                % create folder if non-existent
                if ~exist(syspath,'dir'); mkdir(mainpath,self.name); end
        
        end
    end

    %% Path of folder inside syspath --> used for specific quench reports etc
    methods
        function path = path(self)
            % define path inside syspath
            path = [self.syspath,filesep,self.shortscenario];
        
            % create folder if non-existent
            if ~exist(path,'dir'); mkdir(self.syspath,self.shortscenario); end

        end
    end
    
%     %% @Rosalinde
%     %% Add coil to total number of coiuls in system
%     methods
%         function add_total_coils(self, coilin)
%             
%              if isempty(coilin)
%                 error(['an attempt was made to add a coil to total_coils in' self.name ', but no coil was supplied']);
%              end
%             
%             %add coil to total_coils
%                 self.total_coils = { self.total_coils{:}, coilin };
% 
%             
%         end
%     end
%     
    %% Destructor method
    methods
        function delete(self)
            % destroy dependency with circuits
            self.qcirc = {};
            self.circ = {};

            
            % destroy dependency with coils
            self.qcoil = {};
            self.coils = {};
            
        end
    end
    
end

