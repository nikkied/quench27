classdef Helium_Bath< hgsetget
    %HELIUM_BATH Summary of this class goes here
    
    
    % !!!!! Under construction !!!!!
    
    %   !! Heliumbaths should be created after the system is created and !!
    %   !! after the concerning coil has already been added to the system !!
    
    %   Only 1 coil can be addded to a thermal bath!!
    
    %   Properties that have to be set by user:
    %   -> name (of helium bath, free to choose)
    %   -> position, can be either 'outside', 'inside', 'left side' or 'right side' (helium bath can only be connected to the outside or inside of the coils)
    %   -> insulationmat, material should be in matprops (in corefcn) (this insulation is extra insulation between coil and bath, apart from existing insulation)
    %   -> insulationthickness [m]
    %   -> coil (the coil to which the helium bath is connected)
    
    %% User input
    properties
        % names and bathnumber
        name = [];
        bathnr = [];
        
        % dependency with coil
        coil = [];
        
        % position with respect to coil, can be 'inside', 'outside', 'left
        % side' or ' right side'
        position = [];
        
        % insulation between bath and coil (apart from existing insulation of coil and conductor)
        insulationmat = {'None'};
        insulationthickness = 0;
        
    end
    
    %% Static properties
    properties
        Abath = [];         % Contact surface between 1 element of the coil and thermal bath
        Tinterp = [];       % Array of temperatures used for interpolation
        k = [];             % [W/(m^2.K)] Array of thermal properties of connection between coil and helium bath 
        
    end
        
    %% Dynamic properties
    properties
        Tsurf = [];         % Array with the temperatures of all elements in contact with bath      
        K = [];             % [W/m]
        Qtransfer = [];     % [W]

    end
    
    
  %% Add coil  
    methods
        function addCoil(self, mycoil)
            % Check input
            if isempty(mycoil)
                error(['an attempt was made to add a coil to ' self.name ', but no coil was supplied']);
            end
            
            % add mycoil to helium bath 
            self.coil = mycoil;
            % add heliumbath to mycoil
            self.coil.heliumbaths = {self.coil.heliumbaths{:} self};
            % add heliumbath to system of mycoil
            self.coil.system.heliumbaths = {self.coil.system.heliumbaths{:} self};
            
            % check how many heliumbaths the coil already has
            self.bathnr = length(self.coil.heliumbaths)+1;

        end
    end
    
    %% Initialize the helium bath (called from Create_System.quench())
    methods
        function initialize(self)
            
            % Calculate contact surface Abath
            switch self.position
                case 'inside'
                    self.Abath = zeros(1, self.coil.nax);
                    self.Abath(:) = ones(1, self.coil.nax) * 2 * pi * self.coil.radrange(1) * (self.coil.cond.axL);
                case 'outside'
                    self.Abath = zeros(1, self.coil.nax);
                    self.Abath(:) = ones(1, self.coil.nax) * 2 * pi * self.coil.radrange(2) * (self.coil.cond.axL);
                case {'left side'}
                    for i = 1:self.coil.nrad;
                        self.Abath(i,1) = pi * (self.coil.Router(i, 1)^2-self.coil.Rinner(i,1)^2);
                    end
                case {'right side'}
                    for i = 1:self.coil.nrad;
                        self.Abath(i,1) = pi * (self.coil.Router(i,self.coil.nax)^2-self.coil.Rinner(i, self.coil.nax)^2);
                    end
                otherwise; error('Position unknown');
            end
            
            % Create interpolation array
            self.Tinterp = 3:300;
            
            % Calculate k
            self.k = self.getk;
            
        end
    end
    
    
    %% Calculate (static) k (called by initialize)
    methods
        function [k] = getk(self)
            
            % calculate thermal resistivity r for interpolation array
            % allocating r
            r = zeros(1,length(self.Tinterp));
            
            % calculate and add contribution coil (factor 1/2 -> only account for heat transport from half of conductor)
            % You use kazi because this gives the thermal properties of the
            % mixture of materials in the cable
            r = r + 1/2 * self.coil.cond.radL * 1./self.coil.kazi;
            
            % calculate and add contribution of conductor insulation
            for i = 1:length(self.coil.cond.radinsulationmat);
                if matprops(self.coil.cond.radinsulationmat{i},'TC',self.Tinterp) ~= 0
                    r = r + (1/2 .* self.coil.cond.radinsulationthickness(i)) ./ matprops(self.coil.cond.radinsulationmat{i},'TC',self.Tinterp);
                end
            end

            % calculate and add contribution of coil insulation
            for i = 1:length(self.coil.radinsulationmat);
                if matprops(self.coil.radinsulationmat{i},'TC',self.Tinterp)~= 0
                    r = r + (1/2 .* self.coil.radinsulationthickness(i)) ./ matprops(self.coil.radinsulationmat{i},'TC',self.Tinterp);
                end
            end
            
            % calculate and add contribution of insulation surrounding the coil
            for i = 1:length(self.coil.radcoilinsmat);
                if matprops(self.coil.radcoilinsmat{i},'TC',self.Tinterp) ~= 0
                    r = r + (1/2 .* self.coil.radcoilinsthickness(i)) ./ matprops(self.coil.radcoilinsmat{i},'TC',self.Tinterp);
                end
            end
            
            % Filmboiling creates 
            r = r + 1E-3 ./ matprops('helium','TC',self.Tinterp);
            
            % Invert in order to obtain thermal conductivity from thermal resistivity
            k = 1 ./ r; % [W/(m^2.K)]
            
            % The heliumbath does not contribute to the thermal
            % resistivity, because it is very low (with respect to insulation)
            
        end
    end
    
    %% Updating the temperature of all elements in contact surface with helium bath, in array
    methods
        function updateTsurf(self)
            
            % allocate Tsurf
            self.Tsurf = zeros(1,self.coil.nax);
            
            % obtain temperatures of elemnts from coil
            switch self.position
                case 'inside'
                    self.Tsurf = self.coil.T(1,:);
                case 'outside'
                    self.Tsurf = self.coil.T(end,:);
                case 'left side'
                    self.Tsurf = self.coil.T(:,1);
                case 'right side'
                    self.Tsurf = self.coil.T(:,end);
                otherwise; error('Position unknown');
            end
            
        end
    end
    
    
    %% Calculating and subtracting heat transferred to helium bath from coil   
    methods
        function addQtransfer(self)
            
            % calulate K at the temperature of the elements
            self.K = lininterp1f_p(self.Tinterp,self.k,self.Tsurf,true) .* self.Abath; % [W/K]
            
            % calculate Qtransfer [W], from bath to coil, so this will give 0 or negative value!!
            self.Qtransfer = self.K .* (4.2 - self.Tsurf); % the helium bath is at initial temperature of coil (4.2 K) 
                        
            % check if single element
            if self.coil.nrad == 1 && self.coil.nax == 1; Q = sum(sum(self.Qtransfer)); else Q = self.Qtransfer; end
            
            % add heat (which is negative or zero) to the coil
            switch self.position
                case 'inside'
                    self.coil.Qbath(1,:) = self.coil.Qbath(1,:) + Q;
                case 'outside'
                    self.coil.Qbath(end,:) = self.coil.Qbath(end,:) + Q;
                case 'left side'
                    self.coil.Qbath(:,1) = self.coil.Qbath(:,1) + Q;
                case 'right side'
                    self.coil.Qbath(:,end) = self.coil.Qbath(:,end) +Q;
                otherwise; error('Position unknown');
            end
            
        end
    end
    
    %% update heat transferfrom helium bath (called by Create_System.update)
    methods
        function updateBath(self)

           self.updateTsurf;
           self.addQtransfer;
           
        end
    end
    
    
end


