%% System for validating this simulation program

function [ system ] = validation_system()

%% create system

%% ________________
% Coil conductor
% _________________

% create conductor class
conductor = Create_Conductor();

% set a name for conductor
conductor.name = 'Conductor';

% dimensions of conductor
conductor.axL = 1.22E-3;
conductor.radL = 0.75E-3;

% composition of the conducting part
conductor.ScFactor = 0.1923;                          % part of conductor that is superconducting on a scale of 0 to 1
conductor.ScMaterial = 'Niobium Titanium';           % type of superconductor
conductor.MatrixFactor = 0.8077;                      % part of conductor that is matrix material on a scale of 0 to 1
conductor.MatrixMaterial = 'OFHC Copper RRR112.3';     % type of matrix material
conductor.StabFactor = 0;                        % part of conductor that is stabilizer material on a scale of 0 to 1
conductor.StabMaterial = 'None';          % type of stabilizer
conductor.StabFactor2 = 0;                        % part of conductor that is stabilizer material on a scale of 0 to 1
conductor.StabMaterial2 = 'None';          % type of stabilizer
conductor.VoidFactor = 0;

% insulating part of conductor
conductor.axinsulationthickness = 0.05E-3;              % thickness of insulation belonging to conductor in axial direction
conductor.radinsulationthickness = 0.25E-3;             % thickness of insulation belonging to conductor in radial direction
conductor.axinsulationmat = {'Fiberglass Epoxy G-10 CR normal'};   % type of material in axial direction (a string in a cell)
conductor.radinsulationmat = {'Fiberglass Epoxy G-10 CR normal'};  % type of material in radial direction (a string in a cell)



%% _____________________________
% creating superconducting coil
% ______________________________
mycoil = Create_Coil();
mycoil.name = 'Coil';
mycoil.setcond(conductor); % set conductor
% set dimensions of the coil
mycoil.axrange = [-0.0995 0.0995];
mycoil.radrange = [0.1183 0.1283];
mycoil.nax = 157;
mycoil.nrad = 10;

% set non-conductor isolation
mycoil.radinsulationmat = {}; % set material of radial insulation which does not belong to conductor, but to the coil (typically fiberglass epoxy in radial direction for added electrical insulation in layer2layer direction).
mycoil.radinsulationthickness = 0; % set thickness of radial insulation
mycoil.axinsulationmat = {}; % similar for axial direction
mycoil.axinsulationthickness = 0;
mycoil.radcoilinsmat = {};
mycoil.radcoilinsthickness = 0;
mycoil.axcoilinsmat = {};
mycoil.axcoilinsthickness = 0;

% %% create quench heater class
% 
% nQH = 3;       % number of quench heaters
% myQH = {};
% 
% 
% for i = 1:nQH;
%     myQH{i} = Create_QuenchHeater();
%     % define location: location is a matrix the size of the elements of the coil with zeros for elements which are not heated and ones for elements which are heated.
%     myQH{i}.location = zeros(mycoil.nrad,mycoil.nax);  
% end
% 
% % define location: location is a matrix the size of the elements of the coil with zeros for elements which are not heated and ones for elements which are heated.
% myQH{1}.location(1,5:9) = 1;
% myQH{2}.location(1,77:81) = 1;
% myQH{3}.location(1,143:152) = 1;
% 
% 
% for i = 1:nQH;
%     % other properties
%     myQH{i}.response_t = 0.05;     % heating will start 0.05s after the quench has been detected
%     myQH{i}.P = 80;                % power of quench heater as a whole (heat is distributed evenly over elements).
%     myQH{i}.duration = 0.1;          % duration of quench heater fire.
% 
%     % add quench heater to coil
%     mycoil.qh{i} = myQH{i};        % put more quench heater classes in the mycoil.qh-cell to add more quench heaters (mycoil.mq{2} = ... would be next).
% end


%% _____________________________________________
% Creating circuit for the superconducting coil
% ______________________________________________
circ = Create_Circuit();
circ.name = 'Magnet';

circ.addcoils( mycoil );

% setup other circuit properties
circ.Iinitial = 500;
circ.Rdi = 0;
circ.Rdf = 0;
circ.ndiodes = 1;
circ.diodetype = 'MDD26_one';
circ.dt_CB = 0.0;
circ.dt_Rd = 0.0;



%%

%% ______________________________________________
% Creating outer 2 layers
% _______________________________________________
% create conductor class
conductor2 = Create_Conductor();

% set a name for conductor
conductor2.name = 'Conductor2';

% dimensions of conductor
conductor2.axL = 1.10E-3;
conductor2.radL = 0.68E-3;

% composition of the conducting part
conductor2.ScFactor = 0;                          % part of conductor that is superconducting on a scale of 0 to 1
conductor2.ScMaterial = 'None';           % type of superconductor
conductor2.MatrixFactor = 0;                      % part of conductor that is matrix material on a scale of 0 to 1
conductor2.MatrixMaterial = 'None';     % type of matrix material
conductor2.StabFactor = 0.1923;                        % part of conductor that is stabilizer material on a scale of 0 to 1
conductor2.StabMaterial = 'Niobium Titanium';          % type of stabilizer
conductor2.StabFactor2 = 0.8077;                        % part of conductor that is stabilizer material on a scale of 0 to 1
conductor2.StabMaterial2 = 'OFHC Copper RRR112.3';          % type of stabilizer
conductor2.VoidFactor = 0;

% insulating part of conductor
conductor2.axinsulationthickness = 0.17E-3;              % thickness of insulation belonging to conductor in axial direction
conductor2.radinsulationthickness = 0.32E-3;             % thickness of insulation belonging to conductor in radial direction
conductor2.axinsulationmat = {'Fiberglass Epoxy G-10 CR normal'};   % type of material in axial direction (a string in a cell)
conductor2.radinsulationmat = {'Fiberglass Epoxy G-10 CR normal'};  % type of material in radial direction (a string in a cell)


% creating coil
mycoil2 = Create_Coil();
mycoil2.name = 'Coil2';
mycoil2.setcond(conductor2); % set conductor
% set dimensions of the coil
mycoil2.axrange = [-0.0995 0.0995];
mycoil2.radrange = [0.1283 0.1303];
mycoil2.nax = 157;
mycoil2.nrad = 2;

% set non-conductor isolation
mycoil2.radinsulationmat = {}; % set material of radial insulation which does not belong to conductor, but to the coil (typically fiberglass epoxy in radial direction for added electrical insulation in layer2layer direction).
mycoil2.radinsulationthickness = 0; % set thickness of radial insulation
mycoil2.axinsulationmat = {}; % similar for axial direction
mycoil2.axinsulationthickness = 0;
mycoil2.radcoilinsmat = {};
mycoil2.radcoilinsthickness = 0;
mycoil2.axcoilinsmat = {};
mycoil2.axcoilinsthickness = 0;

%creating circuit
circ2 = Create_Circuit();
circ2.name = '2 outer layers';

circ2.addcoils( mycoil2 );

% setup other circuit properties
circ2.Iinitial = 0;
circ2.Rdi = 0;
circ2.Rdf = 100;
circ2.ndiodes = 0;
circ2.diodetype = 'MDD26_one';
circ2.dt_CB = 0.0;
circ2.dt_Rd = 0.0;



%%

%% ______________________
% Creating system
% _______________________

system = Create_System();
system.name = 'validation';

% add to system
system.addcirc(circ);
system.addcirc(circ2);


%% _______________________
% Creating thermal links
% ________________________

% Creating thermal link
mylink = Thermal_Link_n();
mylink.name = 'Coil to outer layers';

% Adding coils to thermal links
mylink.addcoils(mycoil, mycoil2);


% %% _____________________
% % Creating helium bath
% % ______________________
% 
% % Names of helium baths
% bathnames = {'Bath on inside','Bath on outside of extra layers'};%, 'Bath on left side coil',...
%     %'Bath on left side extra layers', 'Bath on right side coil', 'Bath on right side extra layers'};
% 
% heliumbaths = cell(1, length(bathnames));
% 
% % Creating heliumbaths
% for i = 1:length(bathnames)
%     
%     mybath = Helium_Bath();
%     mybath.name = bathnames{i};
%     heliumbaths{i} = mybath;
%     
%     % (extra) insulation between bath and coil
%     heliumbaths{i}.insulationmat = {'Fiberglass Epoxy G-10 CR normal'};
%     heliumbaths{i}.insulationthickness = 1.0E-3;
%     
% end
% 
% % Adding coils to heliumbath
% heliumbaths{1}.addCoil(mycoil);
% heliumbaths{2}.addCoil(mycoil2);
% % heliumbaths{3}.addCoil(mycoil);
% % heliumbaths{4}.addCoil(mycoil2);
% % heliumbaths{5}.addCoil(mycoil);
% % heliumbaths{6}.addCoil(mycoil2);
% 
% % Postitions of baths
% heliumbaths{1}.position = 'inside';
% heliumbaths{2}.position = 'outside';
% % heliumbaths{3}.position = 'left side';
% % heliumbaths{4}.position = 'left side';
% % heliumbaths{5}.position = 'right side';
% % heliumbaths{6}.position = 'right side';




end





