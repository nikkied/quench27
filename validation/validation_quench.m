%% quench for the validation of this simulation program 
% profile on
% tic
system = validation_system();

%% detection time
system.VQD_threshold = 0;                % set threshold normal zone voltage
system.dt_QD = 0;                          % and threshold time. NZ voltage is over the threshold value for more than threshold time, the quench is detected (mysystem.t_QD is set to current time)

%% solver properties
system.tmax = 0.6;
system.dt = 0.005;                               % set timestep to STORE data every 5ms (solver decides which times to use for accurate calculations based on tolerances).
system.abstol = 1E-6;                          % set absolute tolerance of the solver
system.reltol = 1E-4;                          % set relative tolerance of the solver

%% Setup type of quench
system.scenario = '29-09 validation quench, T=14.2, element (1,1)';   % give a description of this simulation in a short sentence. The goal is to be able to distinguish this name from other simulation you perform on the system with the same name.
system.shortscenario = '29-09_val_T=14.2_el1,1_cond1';                % acronym of scenario. This is used to create files and folders and possibly in latex, so choose carefully...
system.Tquench = 5.0;                         % Kelvin above critical temperature

%% Initialize system
system.initialize;

% quench a coil
system.quench('Coil',[1,1])            % this command quenches 'Coil' at the bottom left (layer1, turn1)

% Solve
SolveQuench(system);

% Post process
system.postprocess;

%% Analyse data
system.plotgeo;
slider(system);

% Plotting

% plotting critical surface
% system.coils{1}.cond.critsurf();

% setting preferences
qpl = quenchplot();
qpl.subpos = [1,1,1000,400];                    % This sets the size of the subplots (which contain only one plot in case of one coil and one circuit);

qpl.coilgrp = {[1]};%,[4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21]};                  % use arrays in the cell elements to group coils into multiple groups for plotting
qpl.circgrp = {[1]};%,[10,11,12,13,14,15,16,17,18,19]};                        % use arrays in the cell elements to group circuits into multiple groups for plotting
% qpl.laygrp = {[1]};%,2,3,4,5,6]};               % use arrays in the cell elements to group layers into multiple groups for plotting

qpl.coilC = hsv(1);                             % assign colors for each of the coils
qpl.circC = hsv(1);                             % assign colors for each of the circuits
% qpl.layC = HSV(1);                              % assign colors for each of the layers

% plots
qpl.I(system);                                  % plot I of all circuits (the current) as a function of time
qpl.T(system);                                  % plot the peak temperature for the coils
% qpl.TL(system);                                 % plot peak temperature of layers
% qpl.TA(system);                                 % plot avarage temperature of coils
% qpl.TAL(system);                                % plot avarage temperature of layers 
% qpl.E(system);                                  % energy type distribution of the system as a whole as a function of time
qpl.Vt2t(system);                               % plot Vt2t for the coils
qpl.Vl2l(system);                               % plot Vl2l
% qpl.VtotL(system);                              % plot vt2t layers
% qpl.Vt2tA(system);                               % plot vt2t avarage
% qpl.Vt2tAL(system);                             % plot vt2t avarage per layer


qp2 = testplot();
qp2.subpos = [1,1,1000,400];                    % This sets the size of the subplots (which contain only one plot in case of one coil and one circuit);

qp2.coilgrp = {[1]};%,[4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21]};                  % use arrays in the cell elements to group coils into multiple groups for plotting
qp2.circgrp = {[1]};%,[10,11,12,13,14,15,16,17,18,19]};                        % use arrays in the cell elements to group circuits into multiple groups for plotting
qp2.laygrp = {[1,2,3,4,5,6,7,8,9,10]};%,2,3,4,5,6]};               % use arrays in the cell elements to group layers into multiple groups for plotting

qp2.coilC = hsv(1);                             % assign colors for each of the coils
qp2.circC = hsv(1);                             % assign colors for each of the circuits
qp2.layC = hsv(10);                              % assign colors for each of the layers

% plots
qp2.Vl2l(system);
qp2.Vl2lind(system);
qp2.Vl2lnz(system);
qp2.Vl2lcontrole(system);
% qp2.Vl2l_alt(system);
% qp2.Vlayer(system);
% % profile viewer


qp3 = quenchplot();
qp3.subpos = [1,1,1000,400];                    % This sets the size of the subplots (which contain only one plot in case of one coil and one circuit);

qp3.coilgrp = {[1]};%,[4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21]};                  % use arrays in the cell elements to group coils into multiple groups for plotting
qp3.circgrp = {[2]};%,[10,11,12,13,14,15,16,17,18,19]};                        % use arrays in the cell elements to group circuits into multiple groups for plotting
% qpl.laygrp = {[1]};%,2,3,4,5,6]};               % use arrays in the cell elements to group layers into multiple groups for plotting

qp3.coilC = hsv(1);                             % assign colors for each of the coils
qp3.circC = hsv(2);                             % assign colors for each of the circuits
qp3.I(system);                                  % plot I of all circuits (the current) as a function of time

