%% test 3 Former conductors
function [myformcond] = test3_form_conductor2()

    
    myformcond = Create_Conductor();
    myformcond.name = 'conductor former';
    myformcond.axL = 64.619E-3-1e-3;
    myformcond.radL = 50e-3;
    myformcond.ScFactor = 0;                             % part of conductor that is superconducting on a scale of 0 to 1
    myformcond.ScMaterial = 'None';                      % type of superconductor
    myformcond.MatrixFactor = 0;                         % part of conductor that is matrix material on a scale of 0 to 1
    myformcond.MatrixMaterial = 'None';                  % type of matrix material
    myformcond.StabFactor = 1;                           % part of conductor that is stabilizer material on a scale of 0 to 1
    myformcond.StabMaterial = 'Aluminum_RRR_170';         % type of stabilizer
    myformcond.StabFactor2 = 0;                           
    myformcond.StabMaterial2 = 'None';         
    myformcond.VoidFactor = 0;          
                
myformcond.axinsulationthickness = 1E-3;
myformcond.radinsulationthickness = 1E-3;
    
myformcond.axinsulationmat = {'Fiberglass Epoxy G-10 CR normal'};
myformcond.radinsulationmat = {'Fiberglass Epoxy G-10 CR normal'};
        
    
end