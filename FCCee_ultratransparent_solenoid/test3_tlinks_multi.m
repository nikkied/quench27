%% test 3 thermal Links

function [tlinks] = test3_tlinks_multi(numlinks)

tlinks = cell(1,numlinks);

for i = 1:numlinks
    
    mylink = Thermal_Link();
    mylink.name = sprintf('Thermal link #%0.5g', i);
    tlinks{i} = mylink;
    
end


end
