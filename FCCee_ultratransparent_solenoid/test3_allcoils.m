%% coils test 3
function [coils] = test3_allcoils(numformersinner)

coils=cell(1,1+numformersinner);
coilTemp=4.5;

% Inner coil
innercoil=Create_Coil();
innercoil.name = 'Main solenoid';
innercond = test3_inner_conductor();
innercoil.setcond(innercond);
innercoil.nax=595; innercoil.nrad=1;


innerCoilLength=(innercond.axL+innercond.axinsulationthickness)*innercoil.nax;
innercoil.axrange=[-0.5*innerCoilLength 0.5*innerCoilLength];
innerCoilThickness=(innercond.radL+innercond.radinsulationthickness)*innercoil.nrad;
innercoil.radrange=[2.2 2.2+innerCoilThickness];
innercoil.dir=1;
innercoil.radinsulationmat=[];
innercoil.radinsulationthickness = 0; 
innercoil.axinsulationmat = {}; % similar for axial direction
innercoil.axinsulationthickness = 0;
innercoil.radcoilinsmat = {};
innercoil.radcoilinsthickness = 0;
innercoil.axcoilinsmat = {};
innercoil.axcoilinsthickness = 0;
innercoil.Tinitial = coilTemp;       
%innercoil.usecableprops = true;    % adjusted cable props
coils{1}=innercoil;


%innercoil
%outercoil
