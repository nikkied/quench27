%system test 3

function [ system ] = test3_system(extraction)

%% create system

system = Create_System();
system.name = 'test3';

numelementformermain=0;



coil = test3_allcoils(numelementformermain);   % creating superconducting coils

% make circuit for the superconducting coils
circ = Create_Circuit();
circ.name = 'coils_diode';
circ.addcoils( coil{1} );

% setup other circuit properties
circ.Iinitial = 20e3;
circ.Rdi = 0;
circ.Rdf = 0./circ.Iinitial;

if(extraction>0)    
    circ.Rdf = 1000./circ.Iinitial;
end
circ.ndiodes = 0;%1;
%circ.diodetype = '7V100kA';%'None';
circ.diodetype = 'None';
circ.dt_CB = 0.0;
circ.dt_Rd = 0.0;
system.addcirc(circ);



% make former circuits
% properties of the circuits
%formcircs = test3_form_circuits();





end