%% test quench
% profile on
%warning('off','all');
delete('../test3/indM.mat')
%warning('on','all');

extraction=1;

system = test3_system(extraction);

%% detection time
system.VQD_threshold = 0.0;                % set threshold normal zone voltage
system.dt_QD = 0;                          % and threshold time. NZ voltage is over the threshold value for more than threshold time, the quench is detected (mysystem.t_QD is set to current time)

%% solver properties
system.tmax = 40;
system.dt = 1;                           % set timestep to STORE data every 5ms (solver decides which times to use for accurate calculations based on tolerances).
%system.abstol = 1E-6;                      % set absolute tolerance of the solver
%system.reltol = 1E-4;                      % set relative tolerance of the solver

system.abstol = 1E-5;                      % set absolute tolerance of the solver
system.reltol = 1E-3;                      % set relative tolerance of the solver


%% Setup type of quench
system.scenario = 'asdf';   % give a description of this simulation in a short sentence. The goal is to be able to distinguish this name from other simulation you perform on the system with the same name.
system.shortscenario = 'asdf';                % acronym of scenario. This is used to create files and folders and possibly in latex, so choose carefully...
system.Tquench = 5; % dT above Tc

%% Initialize system


system.initialize;

system.quench('Main solenoid',[1,1]);
%system.quench('Forward solenoid +Z',[1,1]);


heaters = 1;
if(heaters>0)
    numHeatersMain=5*3-1;
    mainHeaterSpacing=floor(595/(numHeatersMain+1));
    for i=1:numHeatersMain;
        system.quench('Main solenoid',[1,mainHeaterSpacing*i]);
    end

end

% Solve
%SolveQuenchBasic(system);
SolveQuench(system);

system.plotgeo;

% Post process
system.postprocess;








%% Analyse data

% Plotting

% plotting critical surface
% system.coils{1}.cond.critsurf();




% setting preferences
qpl = quenchplot();
qpl.subpos = [1,1,2000,400];                    % This sets the size of the subplots (which contain only one plot in case of one coil and one circuit);

qpl.coilgrp = {[1],[2,3,4,5,6,7,8,9]};                 % use arrays in the cell elements to group coils into multiple groups for plotting
qpl.circgrp = {[1],[2,3,4,5,6,7,8,9]};                        % use arrays in the cell elements to group circuits into multiple groups for plotting

qpl.coilC = hsv(30);                             % assign colors for each of the coils
qpl.circC = hsv(30);                             % assign colors for each of the circuits

qpl.E(system); 

T = cell2mat(system.Tmaxstore);
Tmax1=T(:,1);
t=system.tstore;
Eth = sum(cell2mat(system.Ethstore),2);
I = cell2mat(system.Istore);
I1=I(:,1);

M=[t Tmax1 Eth I1];

csvwrite('FCC-ee, detector #2, heaters + 1000 V extraction.csv',M)


slider(system);
axis([-19 19 2 6]);


break

% plots
qpl.I(system);                                  % plot I of all circuits (the current) as a function of time
qpl.T(system);                                  % plot the peak temperature for the coils
qpl.E(system);                                  % energy type distribution of the system as a whole as a function of time
qpl.Vt2t(system);                               % plot Vt2t for the coils (turn-to-turm, naast elkaar)
qpl.Vl2l(system);                               % plot Vl2l (layer-to-layer)

%% test
% Plotting

% plotting critical surface
% system.coils{1}.cond.critsurf();

% setting preferences
qp3 = testplot();
qp3.subpos = [1,1,1000,400];                    % This sets the size of the subplots (which contain only one plot in case of one coil and one circuit);

qp3.coilgrp = {[1],[2]};   % use arrays in the cell elements to group coils into multiple groups for plotting
qp3.circgrp = {[1,2]};             % use arrays in the cell elements to group circuits into multiple groups for plotting
qp3.laygrp = {[1,2,3,4,5,6,7,8,9,10],[11,12,13,14,15,16,17,18]};             % use arrays in the cell elements to group layers into multiple groups for plotting

qp3.coilC = hsv(6);                             % assign colors for each of the coils
qp3.circC = hsv(2);                             % assign colors for each of the circuits
qp3.layC = hsv(36);                             % assign colors for each of the layers


qp3.Vl2l(system);
qp3.Vl2lind(system);
qp3.Vl2lnz(system);

% profile viewer
