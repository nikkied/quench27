%% Inner conductor
function [conductor] = test3_forward_conductor()
conductor = Create_Conductor();
conductor.name = 'forward conductor';

conductor.axL = 48.57E-3-1e-3; conductor.radL = 38.333e-3-1e-3;

% composition of the conducting part
conductor.ScFactor = 0.038/2;      
conductor.ScMaterial = 'Niobium Titanium';  
conductor.MatrixFactor = 0.038/2;         
conductor.MatrixMaterial = 'OFHC Copper RRR80';
conductor.StabFactor = 1-0.038-0.1;          
conductor.StabMaterial = 'Aluminum_RRR_400';
conductor.StabFactor2 = 0.10;  
conductor.StabMaterial2 = 'Aluminum_RRR_400';  
conductor.VoidFactor = 0;                      
conductor.axinsulationthickness = 1E-3;
conductor.radinsulationthickness = 1E-3;
conductor.axinsulationmat = {'Fiberglass Epoxy G-10 CR normal'};
conductor.radinsulationmat = {'Fiberglass Epoxy G-10 CR normal'};

end
 