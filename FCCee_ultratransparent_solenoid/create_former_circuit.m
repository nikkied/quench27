%% test 3 Former Cicuit
function [myformcirc] = create_former_circuit(formercoil,numid)

    myformcirc=Create_Circuit;
    myformcirc.name=sprintf('FormerCircuit %0.5g',numid);
    myformcirc.Iinitial = 0;
    myformcirc.Rdi = 0;
    myformcirc.Rdf = 0;
    myformcirc.ndiodes = 0;
    myformcirc.diodetype = 'None';
    myformcirc.dt_CB = 0;
    myformcirc.dt_Rd = 0;
    
    myformcirc.addcoils(formercoil);
    
end