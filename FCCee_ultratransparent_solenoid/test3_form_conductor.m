%% test 3 Former conductors
function [myformcond] = test3_form_conductor()

    
    myformcond = Create_Conductor();
    myformcond.name = 'conductor former';
    myformcond.axL = -1;
    myformcond.radL = -1;
    myformcond.ScFactor = 0;                             % part of conductor that is superconducting on a scale of 0 to 1
    myformcond.ScMaterial = 'None';                      % type of superconductor
    myformcond.MatrixFactor = 0;                         % part of conductor that is matrix material on a scale of 0 to 1
    myformcond.MatrixMaterial = 'None';                  % type of matrix material
    myformcond.StabFactor = 1;                           % part of conductor that is stabilizer material on a scale of 0 to 1
    myformcond.StabMaterial = 'Aluminum 5083-O';         % type of stabilizer
    myformcond.StabFactor2 = 0;                           
    myformcond.StabMaterial2 = 'None';         
    myformcond.VoidFactor = 0;                           % part of conductor that is void on a scale of 0 to 1
    myformcond.axinsulationthickness = 0;                % thickness of insulation belonging to conductor in axial direction
    myformcond.radinsulationthickness = 0;               % thickness of insulation belonging to conductor in radial direction
    myformcond.axinsulationmat = {'None'};               % type of material in axial direction (a string in a cell)
    myformcond.radinsulationmat = {'None'};              % type of material in radial direction (a string in a cell)
        
    
end