%% Setup
if false % if you are new to matlab, make this true. If not, make sure to load required paths with 'loadpaths' function. This is not required every run.
    clear classes;  % clear up the workspace
    fclose all;     % close any opened files (required in case latex files have been opened in matlab)
    loadpaths;      % tell matlab where to find the various functions and classes used below
end

%% User input
withpauses = false;  % flag to inductate whether or not to pause during running
detect = true;       % flag to decide whether to detect quench or set time to t_QD

%% General notes:
% material properties come from the 'matprops.m' file in 'CoreFcn' folder
% if the system changed causing the inductance matrix to be different; make sure to delete indM from the folder with the name of your system

%% Create conductor
% in this part we create a conductor that can later be used in any coil. 
% Note that the conductor includes possible insulation which is inseparable 
% from the conducting part.




% create conductor class
mycond = Create_Conductor();

% set a name for conductor
mycond.name = 'cable_corematerial';

% conducting part dimensions
mycond.axL = 120E-3;                              % length of conductor in axial direction
mycond.radL = 94E-3;                            % length of conductor in radial direction

% conducting part composition
mycond.ScFactor = 4/100;                        % part of conductor that is superconducting on a scale of 0 to 1
mycond.ScMaterial = 'Niobium Titanium';         % type of superconductor
mycond.MatrixFactor = 4/100;                     % part of conductor that is matrix material on a scale of 0 to 1
mycond.MatrixMaterial = 'OFHC Copper RRR150';   % type of matrix material
mycond.StabFactor = 92/100;                          % part of conductor that is stabilizer material on a scale of 0 to 1
mycond.StabMaterial = 'Pure_Al_RRR500';                   % type of stabilizer
mycond.VoidFactor = 0;                          % part of conductor that is void on a scale of 0 to 1

% insulating part of conductor
mycond.axinsulationthickness = 0;            % thickness of insulation belonging to conductor in axial direction
mycond.radinsulationthickness = 0;           % thickness of insulation belonging to conductor in radial direction
mycond.axinsulationmat = {};            % type of material in axial direction (a string in a cell)
mycond.radinsulationmat = {};           % type of material in radial direction (a string in a cell)

% check if user wants to pause
if withpauses
    % report to user
    disp('Conductor has been created; take a look in the workspace and check ''mycond''. Type mycond.plotgeo for a graphical interpretation. Type mycond.critsurf for the critical surface of the conductor.');
    
    % pause to take a look at our conductor
    keyboard;
end
    
%% Create coils
% In this part two coils are created from the given conductor. Note that in 
% one of them additional insulation is used in the radial direction

% create coil class
mycoil = Create_Coil();

% set a name for this coil
mycoil.name = 'inner_central_coil';

% set conductor
mycoil.setcond(mycond);   % this couples the conductor and the coil
% mycoil.cond.MatrixMaterial = 'OFHC Copper RRR107'; % change RRR value of this specific coil to 107 (in case it was measured and varies per coil)

% coil settings
mycoil.nax = 128;   % number of windings in axial direction
mycoil.nrad = 12;   % number of windings in radial direction

% non-conductor insulation settings
mycoil.radinsulationmat = {}; % set material of radial insulation which does not belong to conductor, but to the coil (typically fiberglass epoxy in radial direction for added electrical insulation in layer2layer direction).
mycoil.radinsulationthickness = 0; % set thickness of radial insulation
mycoil.axinsulationmat = {}; % similar for axial direction
mycoil.axinsulationthickness = 0;

% define position (and indirectly doubly define size) of coil. This definition of size should match the sizes given by conductor and insulation specs and number of windings. Check it with mycoil.plotgeo
mycoil.axrange = [-7.68 7.68];
mycoil.radrange = [6.25 7.378];

% % voltage tap positions
mycoil.Vtapspos = [1,1]; % set locations ([layer1,turn1; ...]) of possible voltage taps on coil. In circuits bridges can be formed (along several circuits of the circuit) for bridge signals. Note that for quench detection in a simulation (optional) the normal zone voltage is taken directly.

% %% Quench heaters
% % This part shows how to add a quench heater to a coil.
% 
% % create quench heater class
% myQH = Create_QuenchHeater();
% 
% % define location: location is a matrix the size of the elements of the coil with zeros for elements which are not heated and ones for elements which are heated.
% myQH.location = zeros(mycoil.nrad,mycoil.nax);  
% myQH.location(4:7,30) = 1; 
% 
% % other properties
% myQH.response_t = 0.05;     % heating will start 0.05s after the quench has been detected
% myQH.P = 80;                % power of quench heater as a whole (heat is distributed evenly over elements).
% myQH.duration = 5;          % duration of quench heater fire.
% 
% % add quench heater to coil
% mycoil.qh{1} = myQH;        % put more quench heater classes in the mycoil.qh-cell to add more quench heaters (mycoil.mq{2} = ... would be next).

% check if user wants to pause
if withpauses
    % report to user
    disp('Coil with quench heaters has been created; take a look in the workspace and check ''mycoil'' or type mycoil.plotgeo for a graphical interpretation. Press F5 to continue.');
    
    % pause to take a look at our conductor
    keyboard;
end

%% Create circuit
% this part shows how to create a circuit. a circuit can contain any number of coils greater than zero.

% create circuit class 
mycirc = Create_Circuit();

% choose a name for the circuit
mycirc.name = 'testcircuit';

% add the coils
mycirc.addcoils(mycoil);      %use multiple inputs (separated by kommas) to add mulitple coils. The order of entering determines the order of the coils in the circuit.

% setup other circuit properties
mycirc.Iinitial = 69500;      % initial current through circuit
mycirc.Rdi = 0;             % external resistance before dumpresistor is switched in
mycirc.Rdf = 0.05;             % external resistance after dumpresistor is switched in
mycirc.dt_Rd = 0.0;        % delay of dumpresistor circuit breakers to switch after quench has been detected
mycirc.ndiodes = 1;         % number of diodes
mycirc.diodetype = 'MDD26'; % type of diodes. Edit 'Vdiode.m' to add/edit diode VI curves.
mycirc.dt_CB = 0.0;        % delay of power supply/diode circuit breakers switching after quench has been detected (this 
mycirc.QDconfig = [1];  % 'bridge' setup; add more rows of length three to simulate multiple signals

% check if user wants to pause
if withpauses
    % report to user
    disp('Circuit has been created; take a look in the workspace and check ''mycirc''. Press F5 to continue.');
    
    % pause to take a look at our conductor
    keyboard;
end

%% Create system: this is the class in which general properties of the system will be described class
mysystem = Create_System(); 

% set a name for the system
mysystem.name = 'testsystem';

% add circuit to the system. Multiple circuit can my added by entering multiple inputs.
mysystem.addcirc(mycirc);

% check flag to find out whether to detect quench or just set detection time to a certain value
if detect
    % quench detection
    mysystem.VQD_threshold = 1; % set threshold normal zone voltage
    mysystem.dt_QD = 0.1;       % and threshold time. NZ voltage is over the threshold value for more than threshold time, the quench is detected (mysystem.t_QD is set to current time)
else
    % alternative to quench detection
    mysystem.t_QD = 0.5;        % set the system to 'detect' a quench after half of a second. Note that delays still apply.
end

% solver properties
mysystem.dt = 0.005;        % set timestep to STORE data every 5ms (solver decides which times to use for accurate calculations based on tolerances).
mysystem.abstol = 1E-6;     % set absolute tolerance of the solver
mysystem.reltol = 1E-4;     % set relative tolerance of the solver

% check if user wants to pause
if withpauses
    % report to user
    disp('System has been created; take a look in the workspace and check ''mysystem''. Press F5 to continue.');
    
    % pause to take a look at our conductor
    keyboard;
end

%% Setup type of quench
% change stuff for parameter sweep here
% for instance set the quench heater or dumpresistors 'working' flag(s) to false
% or a differet initial current

% scenario
mysystem.scenario = 'test run, 69500A';     % give a description of this simulation in a short sentence. The goal is to be able to distinguish this name from other simulation you perform on the system with the same name.
mysystem.shortscenario = 'test69500A';                % acronym of scenario. This is used to create files and folders and possibly in latex, so choose carefully...

%% Initialize system
% this part of the code initializes the system. This means that all calculation 
% that only have to be performed once, before timestepping occur here.

% initialize
mysystem.initialize;

% quench a coil
mysystem.quench('testcoil',[1,1])        % this command quenches 'Examplecoil' at the bottom left (layer1, turn1)
% mysystem.quench('Examplecoil','Bmax')     % this command quenches 'Examplecoil' at the point where the magnetic field magnitude is at its maximum.


% check if user wants to pause
if withpauses
    % report to user
    disp('System has been initialized and a quench has been initialized. Press F5 to continue.');
    
    % pause to take a look at our conductor
    keyboard;
end

%% Solve
% this part calls the solver to take the initial values of the described 
% system and start solving. The end criterium of a simulation is based on the current
% of the circuit containing the coil that was quenched (quenchcircuit). 
% This criterium can be altered in SolveQuench.m (stopfunction, near the bottom).

% call solver
SolveQuench(mysystem);

% check if user wants to pause
if withpauses
    % report to user
    disp('System has been solved, but still needs to be post processed. Press F5 to continue.');
    
    % pause to take a look at our conductor
    keyboard;
end

%% Post process
% At this point only currents, temperatures and energies are known as a 
% function of time (mysystem.t and mysystem.Y). this part post processes 
% the simulation. Other variables of interest are calculated afterwards;
% the post processing step. Note that the amount of steps to save can be 
% reduced by enforcing a minimum change in the temperature of the element
% that changed temperature the most...

% reduce data if required (it is also possible to just call postprocess with an input: mysystem.postprocess(1).
%mysystem.reducedata(0.1);

% post process 
mysystem.postprocess;

% check if user wants to pause
if withpauses
    % report to user
    disp('System has been post processed; the quench data is now ready to be analysed. Press F5 to continue.');
    
    % pause to take a look at our conductor
    keyboard;
end

%% Analyse data
% The data of interest is now available in the mysystem.'something'store properties 
% In this step the obtained data is analysed. Several options are available.

% The slider is a simplistic interface which depicts relevant quantities in
% of each element as a color. Note that in case of a parameter sweep also multiple
% system classes can be listen in a cell and used as input for the slider
slider(mysystem);

if withpauses
    pause;
end

%% Several plots can be obtained through a dedicated plotclass:
% this class contains some setings about how to plot and a number of funtions
% that plot the various variables of interest

qpl = quenchplot();
qpl.subpos = [1,1,1000,400];   % tThis sets the size of the subplots (which contain only one plot in case of one coil and one circuit);

qpl.coilgrp = {1};  % use arrays in the cell elements to group coils into multiple groups for plotting
qpl.circgrp = {1};  % use arrays in the cell elements to group circuits into multiple groups for plotting

qpl.coilC = jet(1); % assign colors for each of the coils
qpl.circC = jet(1); % assign colors for each of the circuits

%tplots
qpl.I(mysystem);            % plot I of all circuits (the current) as a function of time
qpl.T(mysystem);            % plot peak temperature of all coils as a function of T
qpl.Vt2t(mysystem);         % max turn to turn voltage as function of time for each coil
qpl.Vl2l(mysystem);         % max layer to layer voltage as function of time for each coil
qpl.E(mysystem);            % energy type distribution of the system as a whole as a function of time

%qpl.Vcheck(ms,ms.qcirc);
qpl.VQD(mysystem);          % NZ voltage (correct) and quench detection signals (may be buggy -> values slightly deviate from expected values)

if withpauses
    pause;
end

%% Latex reports
%a number of tools have been developed to write data directly to latex files.
%However, these are still unfinished. Below is a routine that will at least
%export all plots (and create a latex report).

%create quenchreport class
%report = quenchreport();

%supply quenchreport class with configured quenchplot class
%report.qpl = qpl;

% create short report
% report.one(mysystem);
% 
% The folder 'LatexScripts' contains a number of old scripts used for latexreports.
% These may serve as an example in case the reader is interested.
