classdef Create_Conductor < hgsetget
    % This class creates the conductor. You specify the number of layers
    % and turns, the sizes of the cable in the rad and ax direction and all
    % the other properties of the conductor (for instance the material).
    
    %% User input settings
    properties
        % names
        name = [];
        
        % dimensions (uninsulated)
        axL = [];
        radL = [];
        
        % relative amounts of SC, matrix, stabilizer and void
        ScFactor = 0;
        MatrixFactor = 0;
        StabFactor = 0;
        StabFactor2 = 0;
        VoidFactor = 0;
        
        % SC, matrix, stabilizer and void materials
        ScMaterial = [];
        MatrixMaterial = [];
        StabMaterial = [];
        StabMaterial2 = [];
                
        % insulation (this should be the insulation that is a property of
        % the conductor. For instance enamel layer around most Cu wires)
        axinsulationmat = {'None'}; 
        axinsulationthickness = 0;  % This is defined as the total insulation 
        % thicknes between two turns. So if the wire has an insulation
        % thickness of 1 mm, you should set this variable to 2mm!
        
        radinsulationmat = {'None'};
        radinsulationthickness = 0; % Same here! 
        
        % known critical surface information (B values on first row, Ic
        % values on second row)
        critsurf_data = [];
           
    end
    
    %% Static conductor properties
    properties 
        % interpolation array for temperature [K]
        Tinterp = [];
        
        % Area [m^2]
        A = [];
        
        % heat capacity [J/m*K]
        Cpl = [];
        
        % electrical resistivity [Ohm/m]
        ERl = [];
        
    end
    
    %% Do everything that has to be done before calculating
    methods
        function initialize(self)    
            
            % This function is called by Create_Coil in initialize.
            
            %% Areas
            % area conductor (uninsulated)
            self.A = self.axL * self.radL;
            
            %% Heat Capacities
            % interpolation array for temperature
            self.Tinterp = 3:300;   % hard coded, but should be identical to version in coil

            % calculate Cp per material: the void is assumed to have no
            % significant heat capacity
            Cpsc = matprops(self.ScMaterial,'SH',self.Tinterp) .* matprops(self.ScMaterial,'Ro',self.Tinterp) * self.ScFactor * self.A;
            Cpmat = matprops(self.MatrixMaterial,'SH',self.Tinterp) .* matprops(self.MatrixMaterial,'Ro',self.Tinterp) * self.MatrixFactor * self.A;
            Cpstab = matprops(self.StabMaterial,'SH',self.Tinterp) .* matprops(self.StabMaterial,'Ro',self.Tinterp) * self.StabFactor * self.A;
            Cpstab2 = matprops(self.StabMaterial2,'SH',self.Tinterp) .* matprops(self.StabMaterial2,'Ro',self.Tinterp) * self.StabFactor2 * self.A;
            
            % calculate Cp for the conductor insulation
            Cpins = zeros(1,length(self.Tinterp));
            for i = 1:length(self.axinsulationmat)
                Cpins = Cpins + matprops(self.axinsulationmat{i},'SH',self.Tinterp) .* matprops(self.axinsulationmat{i},'Ro',self.Tinterp) * self.radL * self.axinsulationthickness(i);
            end
            for i = 1:length(self.radinsulationmat)
                Cpins = Cpins + matprops(self.radinsulationmat{i},'SH',self.Tinterp) .* matprops(self.radinsulationmat{i},'Ro',self.Tinterp) * (self.axL + sum(self.radinsulationthickness)) * self.radinsulationthickness(i);
            end
    
            % sum heat capacities [Cp/m]
            self.Cpl = Cpsc + Cpmat + Cpstab + Cpstab2 + Cpins;
            
            %% Electrical resistivity
            % calcute electrical resistivity (the void is assumed to have
            % infinite electrical resistivity)
            ERMatrix = matprops(self.MatrixMaterial,'ER',self.Tinterp) / (self.A * self.MatrixFactor);
            ERSc = matprops(self.ScMaterial,'ER',self.Tinterp) / (self.A * self.ScFactor);
            ERStab = matprops(self.StabMaterial,'ER',self.Tinterp) / (self.A * self.StabFactor);
            ERStab2 = matprops(self.StabMaterial2,'ER',self.Tinterp) / (self.A * self.StabFactor2);
            
            % parallel resistance summation [Ohm/m]
            self.ERl = 1./(1./ERSc + 1./ERMatrix + 1./ERStab + 1./ERStab2);
            
        end
    end
    
    %% Plot geometry
    
    % This shows the conductor that is made with this class. It shows the
    % thickness of the insulation and the sc, matrix, stabilizer and void
    % factors.
    
    methods
        function [ax,f] = plotgeo(self,varargin)
            
            % analyse varargin
            visible = true;
            if ~isempty(varargin) && varargin{1} == false
                visible = false;
            end
            
            % create figure
            f = figure('Color','w','Position',[100 100 800 400],'Renderer','OpenGL');
            ax = axes('Parent',f); hold(ax,'on'); grid(ax,'on');
            if visible == false; set(f,'Visible','off'); end
            
            % colors
            oc = oceanprint(5); j = jet(7);
            condC = j(3,:);
            axC = oc(1,:);
            radC = oc(2,:);
            
            % find out what the conductor is composed of
            % d = cumsum([self.ScFactor,self.MatrixFactor,self.StabFactor,self.VoidFactor]);
            
            % fix empty input and create flags to not plot squares of
            % width/height 0
            axflag = true; radflag = true;
            if isempty(self.axinsulationthickness) || self.axinsulationthickness == 0
                self.axinsulationthickness = eps;
                axflag = false;
            end
            if isempty(self.radinsulationthickness) || self.radinsulationthickness == 0
                self.radinsulationthickness = eps;
                radflag = false;
            end
            
            % create rectangles (modify for multiple insulation types)
            cond = rectangle('Position',[self.axinsulationthickness/2,self.radinsulationthickness/2,self.axL,self.radL],'FaceColor',condC,'EdgeColor','w');
            ax1 = rectangle('Parent',ax,'Position',[0,0,self.axinsulationthickness/2,self.radL + self.radinsulationthickness],'FaceColor',axC,'EdgeColor','w');
            ax2 = rectangle('Position',[self.axL + self.axinsulationthickness/2,0,self.axinsulationthickness/2,self.radL + self.radinsulationthickness],'FaceColor',axC,'EdgeColor','w');
            rad1 = rectangle('Position',[self.axinsulationthickness/2,0,self.axL,self.radinsulationthickness/2],'FaceColor',radC,'EdgeColor','w');
            rad2 = rectangle('Position',[self.axinsulationthickness/2,self.radL + self.radinsulationthickness/2,self.axL,self.radinsulationthickness/2],'FaceColor',radC,'EdgeColor','w');
            
            % legend
            if axflag && radflag
                rect_legend([cond,ax1,rad1],{'Conductor',['Axial insulation (' self.axinsulationmat{:} ')'],['Radial insulation (' self.radinsulationmat{:} ')']},'Location','NorthEastOutside');
            elseif axflag
                rect_legend([cond,ax1],{'Conductor',['Axial insulation (' self.axinsulationmat{:} ')']},'Location','NorthEastOutside');
            elseif radflag
                rect_legend([cond,rad1],{'Conductor',['Radial insulation (' self.radinsulationmat{:} ')']},'Location','NorthEastOutside');
            else
                rect_legend([cond],{'Conductor'},'Location','NorthEastOutside');
            end
            
            axis equal
            axis([0,self.axL + self.axinsulationthickness,0,self.radL + self.radinsulationthickness]);
            
            % labels
            if isempty(self.StabFactor); self.VoidFactor = 0; end;
            if isempty(self.VoidFactor); self.VoidFactor = 0; end;
            
            idx = [self.ScFactor,self.MatrixFactor,self.StabFactor,self.VoidFactor] ~= 0;
            nlabels = sum(idx);
            xlabels = self.axinsulationthickness/2 + self.axL/5;
            ylabels = self.radinsulationthickness/2 + (1:nlabels)/(nlabels + 1)*self.radL;
            
            i = 1; precision = 2;
            if idx(1); text(xlabels,ylabels(i),1,['SC (' self.ScMaterial '): ' num2str(self.ScFactor,precision)]); i = i+1; end;
            if idx(2); text(xlabels,ylabels(i),1,['Matrix (' self.MatrixMaterial '): ' num2str(self.MatrixFactor,precision)]); i = i+1; end;
            %if idx(3); text(xlabels,ylabels(i),1,['Stabilizer (' self.StabMaterial '): ' num2str(self.StabFactor,precision)]); i = i+1; end; %CHEAT
            if idx(3); text(xlabels,ylabels(i),1,['StabFactor: ' num2str(self.StabFactor,precision)]); i = i+1; end;
            if idx(4); text(xlabels,ylabels(i),1,['Void: ' num2str(self.VoidFactor,precision)]); end;
            
            
            
            
            % finish up
            title([self.name ' geometry']);
            xlabel('Z [m]')
            ylabel('R [m]')
            
        end
    end
    
    
    %% Plot critical surface
    
    % with cond.critsurf(); in you script you can plot the critical surface
    % of the superconductor that you use.
    
    methods
        function [ax,f] = critsurf(self,varargin)
            
            % analyse varargin
            visible = true;
            if ~isempty(varargin) && varargin{1} == false
                visible = false;
            end
     
            % grid
            Ba = linspace(0.1,9,1000);
            Ta = 2.2:1:8.2;
            [Bm,Tm] = meshgrid(Ba,Ta);
            
            % create sc
            sc = sc_NbTi;
            sc.setLHC;
            
            % calc Jc
            Jcm = sc.Jcnoncu(Bm,Tm);
            
            % calculate Ic
            Icm = 1e6 * Jcm * self.A * self.ScFactor; % A
            
            % open figure
            f = figure('Color','w','Position',[100,100,1000,500]);
            ax = axes('Parent',f,'YScale','log'); hold(ax,'on'); grid(ax,'on');
            
            % visibility toggle
            if visible == false; set(f,'Visible','off'); end
            
            % setup color
            C = fireprint_mod(size(Icm,1)+2);
            %C = jet(size(Icm,1)+2);
            
            for i = 1:size(Icm,1)
                plot(Ba,Icm(i,:),'-','LineWidth',2,'Color',C(i,:),'DisplayName',sprintf('T = %2.1f K',Ta(i)),'Parent',ax);
            end
            
            % add red squares to indicate known point properties
            if ~isempty(self.critsurf_data)
                B = self.critsurf_data(1,:);
                Ic = self.critsurf_data(2,:);
                plot(B,Ic,'rs','Parent',ax,'LineWidth',2,'DisplayName','Known 4.2K');
            end
            
            % finish up
            ylim(ax,[10,max(max( Icm ))])
            legend(ax,'show')
            xlabel(ax,'Magnetic Field Magnitude [T]')
            ylabel(ax,'Critical Current [A]')
            title(ax,['Critical Surface: ' self.name])
            
        end
    end

end