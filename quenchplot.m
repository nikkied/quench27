classdef quenchplot < hgsetget
    % Description:
    % This class is designed to make time dependent plots of a solved and
    % post precessed Create_System class.
    
    % edit by Nikkie: TL, TA, TAL, Vt2tL, Vt2tA and Vt2tAL and corresponding plotTL and so one. For these
    % plots: see Create_System class, lines 800 - 850. Add a
    % 'self.Vt2tavaragelayerstore{idx,7} =
    % self.Vt2tavaragelaystore{idx,4}(1);' line for every layer that you
    % want to plot!!!
    
    %% User input settings
    properties
        
        % system information
        cases = [];
        onlyone = false;
        
        % figure and axes handles (for interface)
        ax = [];
        f = [];
        
        % subplots
        circgrp = {};                   % grouping of circuits
        coilgrp = {};                   % grouping of coils
%         %new
%         laygrp = {};                    % grouping of layers
%         %new
        
        % figure preferences
        visible = true;                 % boolian to toggle visibility of plots
        titlestr = [];                  % titlestring
        linewidth = 1.4;                % linewidth
        subpos = [1,1,800,960];
        pos = [100,100,1000,400];
        renderer = 'OpenGL';
        legfs =  9;                     % legend font size
        
        % 
        tlegpos = 'NorthEast';          % legend position of time dependent plots
        caselegpos = 'NorthEastOutside'; % legend position of time independent plots (usually barplots)

        % 
        marg = 2;                      % margin used (in percent) in barplots to distinguish out of bounds data from high inbound data
        %tQD = [];                       % quench protection times of length equeal to length(mslist)
        
        
        %         % export
        %         export = false;
        %         extension = []; %'-pdf' '-tikz' ?
        
        % colors
        circC = [];     % color of circuits (ms.ncirc x 3 matrix)
        coilC = [];     % color of coils (ms.ncoils x 3 matrix)
%         %new
%         layC = [];      % color of layers ((ms.coils{i} x nrad) x 3 matrix)
%         %new
        
    end
    
    %% Initialize
    methods
        function initialize(self,input)
            % distinguish between tplots and caseplots
            if iscell(input)
                % define titlestring
                self.titlestr = [input{1}.qcoil.name ' Quench'];
                
                % define case-strings
                self.cases = cell(1,length(input));
                for i = 1:length(input)
                    self.cases{i} = input{i}.shortscenario;
                end
                
                % for showing peak values of only one quench --> put it in a cell
                if length(input) == 1
                    self.onlyone = true;
                else
                    self.onlyone = false;
                end
                           
            else
                % define titlestring
                self.titlestr = [input.scenario];
                
            end

        end
    end
   
    %% Plot current for given circuits
    methods
        function [ax,f] = plotI(self,ms,circs)
            % initialize
            self.initialize(ms);
            
            % create figure
            f = figure('Color','w','Position',self.pos,'Renderer',self.renderer);
            ax = axes('Parent',f); hold(ax,'on'); grid(ax,'on');
            if self.visible == false; set(f,'Visible','off'); end
            
            % extraxt I matrix from ms
            I = cell2mat(ms.Istore);
            
            % go through circuits
            for i = circs
                % iterate through times
                Icirc = I(:,i);
                
                % plot current line
                plot(ms.tstore,Icirc,'Linewidth',self.linewidth,'Color',self.circC(i,:),'DisplayName',ms.circ{i}.name)
                
            end
            
            % finish up
            axis tight
            leg = legend('show','Location',self.tlegpos);
            set(leg,'FontSize',self.legfs);
            title(self.titlestr);
            xlabel('Time [s]')
            ylabel('Circuit Current [A]')
            
        end
    end
    
    %% Plot hotspot temperature for given coils
    methods
        function [ax,f] = plotT(self,ms,coils)
            % initialize
            self.initialize(ms);
            
            % create figure
            f = figure('Color','w','Position',self.pos,'Renderer',self.renderer);
            ax = axes('Parent',f); hold(ax,'on'); grid(ax,'on');
            if self.visible == false; set(f,'Visible','off'); end
            
            % extraxt T matrix from ms
            T = cell2mat(ms.Tmaxstore);
           
            % iterate through coils
            for i = coils
                % iterate through elements, gather Tstore
                Tmax = T(:,i);
                
                % plot
                plot(ms.tstore,Tmax,'Linewidth',self.linewidth,'Color',self.coilC(i,:),'DisplayName',ms.coils{i}.name);
            end
            
            % finish up
            axis tight
            leg = legend('show','Location',self.tlegpos);
            set(leg,'FontSize',self.legfs);
            title(self.titlestr);
            xlabel('Time [s]')
            ylabel('Hotspot Temperature [K]')
            
        end
    end
    
    %% new new new new new new new Nikkie
    
%     %% Plot hotspot temperature per layer
%     methods
%         function [ax,f] = plotTL(self,ms,layers)
%             % initialize
%             self.initialize(ms);
%             
%             % create figure
%             f = figure('Color','w','Position',self.pos,'Renderer',self.renderer);
%             ax = axes('Parent',f); hold(ax,'on'); grid(ax,'on');
%             if self.visible == false; set(f,'Visible','off'); end
%             
%             % extraxt T matrix from ms
%             TL = cell2mat(ms.Tmaxlayerstore);
%            
%             % iterate through layers
%             for i = layers
%                 % iterate through elements
%                 Tmaxl = TL(:,i);
%                 
%                 % plot
%                 plot(ms.tstore,Tmaxl,'Linewidth',self.linewidth,'Color',self.layC(i,:),'DisplayName',ms.layers{i});
%             end
%             
%             % finish up
%             axis tight
%             leg = legend('show','Location',self.tlegpos);
%             set(leg,'FontSize',self.legfs);
%             title(self.titlestr);
%             xlabel('Time [s]')
%             ylabel('Hotspot Temperature [K]')
%             
%         end
%     end
%     %% Plot avarage temperature for given coils
%     methods
%         function [ax,f] = plotTA(self,ms,coils)
%             % initialize
%             self.initialize(ms);
%             
%             % create figure
%             f = figure('Color','w','Position',self.pos,'Renderer',self.renderer);
%             ax = axes('Parent',f); hold(ax,'on'); grid(ax,'on');
%             if self.visible == false; set(f,'Visible','off'); end
%             
%             % extraxt T matrix from ms
%             TA = cell2mat(ms.Tavaragestore);
%            
%             % iterate through coils
%             for i = coils
%                 % iterate through elements, gather Tstore
%                 Tmaxa = TA(:,i);
%                 
%                 % plot
%                 plot(ms.tstore,Tmaxa,'Linewidth',self.linewidth,'Color',self.coilC(i,:),'DisplayName',ms.coils{i}.name);
%             end
%             
%             % finish up
%             axis tight
%             leg = legend('show','Location',self.tlegpos);
%             set(leg,'FontSize',self.legfs);
%             title(self.titlestr);
%             xlabel('Time [s]')
%             ylabel('Avarage Temperature [K]')
%             
%         end
%     end
%         %% Plot avarage temperature per layer
%     methods
%         function [ax,f] = plotTAL(self,ms,layers)
%             % initialize
%             self.initialize(ms);
%             
%             % create figure
%             f = figure('Color','w','Position',self.pos,'Renderer',self.renderer);
%             ax = axes('Parent',f); hold(ax,'on'); grid(ax,'on');
%             if self.visible == false; set(f,'Visible','off'); end
%             
%             % extraxt T matrix from ms
%             TAL = cell2mat(ms.Tavaragelayerstore);
%            
%             % iterate through coils
%             for i = layers
%                 % iterate through elements, gather Tstore
%                 Tmaxal = TAL(:,i);
%                 
%                 % plot
%                 plot(ms.tstore,Tmaxal,'Linewidth',self.linewidth,'Color',self.layC(i,:),'DisplayName',ms.layers{i});
%             end
%             
%             % finish up
%             axis tight
%             leg = legend('show','Location',self.tlegpos);
%             set(leg,'FontSize',self.legfs);
%             title(self.titlestr);
%             xlabel('Time [s]')
%             ylabel('Avarage Temperature [K]')
%             
%         end
%     end
%     
%     %%  end new new new new new new new Nikkie
    
    %% Plot max turn to turn voltage for given coils
    methods
        function [ax,f] = plotVt2t(self,ms,coils)
            % initialize
            self.initialize(ms);
            
            % create figure
            f = figure('Color','w','Position',self.pos,'Renderer',self.renderer);
            ax = axes('Parent',f); hold(ax,'on'); grid(ax,'on');
            if self.visible == false; set(f,'Visible','off'); end
            
            % iterate through coils
            for i = coils
                % extract Vt2tmax of i'th coil
                Vt2tmax = cell2mat( ms.Vt2tmaxstore(:,i) );
                
                % plot
                plot(ms.tstore,Vt2tmax,'Linewidth',self.linewidth,'Color',self.coilC(i,:),'DisplayName',ms.coils{i}.name);           
                
            end
            
            % finish up
            axis tight;
            leg = legend('show','Location',self.tlegpos);
            set(leg,'FontSize',self.legfs);
            title(self.titlestr);
            xlabel('Time [s]')
            ylabel('Max t2t Voltage [V]')
            
        end
    end
    
%     %% new new new new new new new Nikkie
%     
%     %% Plot max turn to turn voltage for given coils
%     methods
%         function [ax,f] = plotVt2tL(self,ms,layers)
%             % initialize
%             self.initialize(ms);
%             
%             % create figure
%             f = figure('Color','w','Position',self.pos,'Renderer',self.renderer);
%             ax = axes('Parent',f); hold(ax,'on'); grid(ax,'on');
%             if self.visible == false; set(f,'Visible','off'); end
%             
%             % iterate through coils
%             for i = layers
%                 % extract Vt2tmax of i'th coil
%                 Vt2tmaxL = cell2mat( ms.Vt2tmaxlayerstore(:,i) );
%                 
%                 % plot
%                 plot(ms.tstore,Vt2tmaxL,'Linewidth',self.linewidth,'Color',self.layC(i,:),'DisplayName',ms.layers{i});           
%                 
%             end
%             
%             % finish up
%             axis tight;
%             leg = legend('show','Location',self.tlegpos);
%             set(leg,'FontSize',self.legfs);
%             title(self.titlestr);
%             xlabel('Time [s]')
%             ylabel('Max t2t Voltage [V]')
%             
%         end
%     end
%     
%     %% Plot voltage at the end of each winding
%     methods
%         function [ax,f] = plotVtotL(self,ms,layers)
%             % initialize
%             self.initialize(ms);
%             
%             % create figure
%             f = figure('Color','w','Position',self.pos,'Renderer',self.renderer);
%             ax = axes('Parent',f); hold(ax,'on'); grid(ax,'on');
%             if self.visible == false; set(f,'Visible','off'); end
%             
%             % iterate through coils
%             for i = layers
%                 % extract Vt2tmax of i'th coil
%                 VtotL = cell2mat( ms.Vtotlayerstore(:,i) );
%                 
%                 % plot
%                 plot(ms.tstore,VtotL,'Linewidth',self.linewidth,'Color',self.layC(i,:),'DisplayName',ms.layers{i});           
%                 
%             end
%             
%             % finish up
%             axis tight;
%             leg = legend('show','Location',self.tlegpos);
%             set(leg,'FontSize',self.legfs);
%             title(self.titlestr);
%             xlabel('Time [s]')
%             ylabel('Voltage across a layer [V]')
%             
%         end
%     end
%     
%     %% Plot avarage turn to turn voltage for given coils
%     methods
%         function [ax,f] = plotVt2tA(self,ms,layers)
%             % initialize
%             self.initialize(ms);
%             
%             % create figure
%             f = figure('Color','w','Position',self.pos,'Renderer',self.renderer);
%             ax = axes('Parent',f); hold(ax,'on'); grid(ax,'on');
%             if self.visible == false; set(f,'Visible','off'); end
%             
%             % iterate through coils
%             for i = layers
%                 % extract Vt2tmax of i'th coil
%                 Vt2tmaxA = cell2mat( ms.Vt2tavaragestore(:,i) );
%                 
%                 % plot
%                 plot(ms.tstore,Vt2tmaxA,'Linewidth',self.linewidth,'Color',self.layC(i,:),'DisplayName',ms.layers{i});           
%                 
%             end
%             
%             % finish up
%             axis tight;
%             leg = legend('show','Location',self.tlegpos);
%             set(leg,'FontSize',self.legfs);
%             title(self.titlestr);
%             xlabel('Time [s]')
%             ylabel('Avarage t2t Voltage [V]')
%             
%         end
%     end
%     
%     %% Plot avarage turn to turn voltage for given layers
%     methods
%         function [ax,f] = plotVt2tAL(self,ms,layers)
%             % initialize
%             self.initialize(ms);
%             
%             % create figure
%             f = figure('Color','w','Position',self.pos,'Renderer',self.renderer);
%             ax = axes('Parent',f); hold(ax,'on'); grid(ax,'on');
%             if self.visible == false; set(f,'Visible','off'); end
%             
%             % iterate through coils
%             for i = layers
%                 % extract Vt2tmax of i'th coil
%                 Vt2tmaxAL = cell2mat( ms.Vt2tavaragelayerstore(:,i) );
%                 
%                 % plot
%                 plot(ms.tstore,Vt2tmaxAL,'Linewidth',self.linewidth,'Color',self.layC(i,:),'DisplayName',ms.layers{i});           
%                 
%             end
%             
%             % finish up
%             axis tight;
%             leg = legend('show','Location',self.tlegpos);
%             set(leg,'FontSize',self.legfs);
%             title(self.titlestr);
%             xlabel('Time [s]')
%             ylabel('Avarage t2t Voltage [V]')
%             
%         end
%     end
%     
%     
%     %% end new new new new new new new Nikkie
    
    %% Plot max layer to layer voltage for given coils
    methods
        function [ax,f] = plotVl2l(self,ms,coils)
            % initialize
            self.initialize(ms);
            
            % create figure
            f = figure('Color','w','Position',self.pos,'Renderer',self.renderer);
            ax = axes('Parent',f); hold(ax,'on'); grid(ax,'on');
            if self.visible == false; set(f,'Visible','off'); end
            
            % iterate through coils
            for i = coils
                % extract Vl2lmax of i'th coil
                Vl2lmax = cell2mat( ms.Vl2lmaxstore(:,i) );
                
                % plot
                plot(ms.tstore,Vl2lmax,'Linewidth',self.linewidth,'Color',self.coilC(i,:),'DisplayName',ms.coils{i}.name);
                
            end
            
            % finish up
            axis tight;
            leg = legend('show','Location',self.tlegpos);
            set(leg,'FontSize',self.legfs);
            title(self.titlestr);
            xlabel('Time [s]')
            ylabel('Max l2l Voltage [V]')
            
        end
    end
    
    %% Plot currents for all circuits in a subplot
    methods
        function [ax,f] = I(self,ms)
            % initialize
            self.initialize(ms);
            
            % create figure
            f = figure; set(f,'Color','w','Position',self.subpos,'Renderer',self.renderer);
            if self.visible == false; set(f,'Visible','off'); end
            
            % make subplot
            lcg = length(self.circgrp);
            for i = 1:lcg
                % make plot
                hTemp = subplot(lcg,1,i,'Parent',f); newPos = get(hTemp,'Position'); delete(hTemp);
                [ax,g] = self.plotI(ms,self.circgrp{i});
                set(ax,'Parent',f,'Position',newPos); close(g);
                
                % fix details
                if i ~= lcg; xlabel([]); end
                if i ~= 1; title([]); end
                leg = legend(ax,'show','Location',self.tlegpos);
                set(leg,'FontSize',self.legfs);
                
            end
            
        end
    end
    
    %% plot hotspot temperature in a subplot
    methods
        function [ax,f] = T(self,ms)
            % initialize
            self.initialize(ms);
            
            % create figure
            f = figure; set(f,'Color','w','Position',self.subpos,'Renderer',self.renderer);
            if self.visible == false; set(f,'Visible','off'); end
            
            % make subplot
            lcg = length(self.coilgrp);
            for i = 1:lcg
                % make plot
                hTemp = subplot(lcg,1,i,'Parent',f); newPos = get(hTemp,'Position'); delete(hTemp);
                [ax,g] = self.plotT(ms,self.coilgrp{i});
                set(ax,'Parent',f,'Position',newPos); close(g);
                
                % fix details
                if i ~= lcg; xlabel([]); end
                if i ~= 1; title([]); end
                leg = legend(ax,'show','Location',self.tlegpos);
                set(leg,'FontSize',self.legfs);
                
            end
            
        end
    end
    
%     %% new new new new new new new Nikkie
%     
%     %% plot hotspot temperature in a subplot per layer
%     methods
%         function [ax,f] = TL(self,ms)
%             % initialize
%             self.initialize(ms);
%             
%             % create figure
%             f = figure; set(f,'Color','w','Position',self.subpos,'Renderer',self.renderer);
%             if self.visible == false; set(f,'Visible','off'); end
%             
%             % make subplot
%             lcg = length(self.laygrp);
%             for i = 1:lcg
%                 % make plot
%                 hTemp = subplot(lcg,1,i,'Parent',f); newPos = get(hTemp,'Position'); delete(hTemp);
%                 [ax,g] = self.plotTL(ms,self.laygrp{i});
%                 set(ax,'Parent',f,'Position',newPos); close(g);
%                 
%                 % fix details
%                 if i ~= lcg; xlabel([]); end
%                 if i ~= 1; title([]); end
%                 leg = legend(ax,'show','Location',self.tlegpos);
%                 set(leg,'FontSize',self.legfs);
%                 
%             end
%             
%         end
%     end
%     
%     %% plot avarage temperature in a subplot
%     methods
%         function [ax,f] = TA(self,ms)
%             % initialize
%             self.initialize(ms);
%             
%             % create figure
%             f = figure; set(f,'Color','w','Position',self.subpos,'Renderer',self.renderer);
%             if self.visible == false; set(f,'Visible','off'); end
%             
%             % make subplot
%             lcg = length(self.coilgrp);
%             for i = 1:lcg
%                 % make plot
%                 hTemp = subplot(lcg,1,i,'Parent',f); newPos = get(hTemp,'Position'); delete(hTemp);
%                 [ax,g] = self.plotTA(ms,self.coilgrp{i});
%                 set(ax,'Parent',f,'Position',newPos); close(g);
%                 
%                 % fix details
%                 if i ~= lcg; xlabel([]); end
%                 if i ~= 1; title([]); end
%                 leg = legend(ax,'show','Location',self.tlegpos);
%                 set(leg,'FontSize',self.legfs);
%                 
%             end
%             
%         end
%     end
%     %% plot avarage temperature in a subplot per layer
%     methods
%         function [ax,f] = TAL(self,ms)
%             % initialize
%             self.initialize(ms);
%             
%             % create figure
%             f = figure; set(f,'Color','w','Position',self.subpos,'Renderer',self.renderer);
%             if self.visible == false; set(f,'Visible','off'); end
%             
%             % make subplot
%             lcg = length(self.laygrp);
%             for i = 1:lcg
%                 % make plot
%                 hTemp = subplot(lcg,1,i,'Parent',f); newPos = get(hTemp,'Position'); delete(hTemp);
%                 [ax,g] = self.plotTAL(ms,self.laygrp{i});
%                 set(ax,'Parent',f,'Position',newPos); close(g);
%                 
%                 % fix details
%                 if i ~= lcg; xlabel([]); end
%                 if i ~= 1; title([]); end
%                 leg = legend(ax,'show','Location',self.tlegpos);
%                 set(leg,'FontSize',self.legfs);
%                 
%             end
%             
%         end
%     end
%     
%     %% end new new new new new new new Nikkie
    
    %% Plot max turn to turn voltage in a subplot
    methods
        function [ax,f] = Vt2t(self,ms)
            % initialize
            self.initialize(ms);
            
            % create figure
            f = figure; set(f,'Color','w','Position',self.subpos,'Renderer',self.renderer);
            if self.visible == false; set(f,'Visible','off'); end
            
            % find out which coils are relevant
            lcg = length(self.coilgrp);
            relcoils = cell(1,0);
            for i = 1:lcg
                % coils
                rc = [];
                for j = self.coilgrp{i}
                    if ms.coils{j}.nax > 1
                        rc = [rc ms.coils{j}.coilnr];
                    end
                end
                
                % add if not empty
                if ~isempty(rc)
                    relcoils{i} = rc;
                end
                
            end
            
            % make lrc plots with the data in relcoils(i)
            lrc = length(relcoils);
            for i = 1:lrc
                
                % make plot
                hTemp = subplot(lrc,1,i,'Parent',f); newPos = get(hTemp,'Position'); delete(hTemp);
                [ax,g] = self.plotVt2t(ms,relcoils{i});
                set(ax,'Parent',f,'Position',newPos); close(g);
                
                % fix details
                if i ~= lrc; xlabel([]); end
                if i ~= 1; title([]); end
                leg = legend(ax,'show','Location',self.tlegpos);
                set(leg,'FontSize',self.legfs);
                
            end
        end
    end
    
%     %% new new new new new new new Nikkie
%     
%     %% Plot max turn to turn voltage in a subplot
%     methods
%         function [ax,f] = Vt2tL(self,ms)
%             % initialize
%             self.initialize(ms);
%             
%             % create figure
%             f = figure; set(f,'Color','w','Position',self.subpos,'Renderer',self.renderer);
%             if self.visible == false; set(f,'Visible','off'); end
%                   
%             % make lrc plots with the data in relcoils(i)
%             lcg = length(self.laygrp);
%             for i = 1:lcg
%                 
%                 % make plot
%                 hTemp = subplot(lcg,1,i,'Parent',f); newPos = get(hTemp,'Position'); delete(hTemp);
%                 [ax,g] = self.plotVt2tL(ms,self.laygrp{i});
%                 set(ax,'Parent',f,'Position',newPos); close(g);
%                 
%                 % fix details
%                 if i ~= lcg; xlabel([]); end
%                 if i ~= 1; title([]); end
%                 leg = legend(ax,'show','Location',self.tlegpos);
%                 set(leg,'FontSize',self.legfs);
%                 
%             end
%         end
%     end
%     
%     %% Plot voltage at the end of each winding
%     methods
%         function [ax,f] = VtotL(self,ms)
%             % initialize
%             self.initialize(ms);
%             
%             % create figure
%             f = figure; set(f,'Color','w','Position',self.subpos,'Renderer',self.renderer);
%             if self.visible == false; set(f,'Visible','off'); end
%                   
%             % make lrc plots with the data in relcoils(i)
%             lcg = length(self.laygrp);
%             for i = 1:lcg
%                 
%                 % make plot
%                 hTemp = subplot(lcg,1,i,'Parent',f); newPos = get(hTemp,'Position'); delete(hTemp);
%                 [ax,g] = self.plotVtotL(ms,self.laygrp{i});
%                 set(ax,'Parent',f,'Position',newPos); close(g);
%                 
%                 % fix details
%                 if i ~= lcg; xlabel([]); end
%                 if i ~= 1; title([]); end
%                 leg = legend(ax,'show','Location',self.tlegpos);
%                 set(leg,'FontSize',self.legfs);
%                 
%             end
%         end
%     end
%     
%     %% Plot avarage turn to turn voltage in a subplot
%     methods
%         function [ax,f] = Vt2tA(self,ms)
%             % initialize
%             self.initialize(ms);
%             
%             % create figure
%             f = figure; set(f,'Color','w','Position',self.subpos,'Renderer',self.renderer);
%             if self.visible == false; set(f,'Visible','off'); end
%                   
%             % make lrc plots with the data in relcoils(i)
%             lcg = length(self.coilgrp);
%             for i = 1:lcg
%                 
%                 % make plot
%                 hTemp = subplot(lcg,1,i,'Parent',f); newPos = get(hTemp,'Position'); delete(hTemp);
%                 [ax,g] = self.plotVt2tA(ms,self.coilgrp{i});
%                 set(ax,'Parent',f,'Position',newPos); close(g);
%                 
%                 % fix details
%                 if i ~= lcg; xlabel([]); end
%                 if i ~= 1; title([]); end
%                 leg = legend(ax,'show','Location',self.tlegpos);
%                 set(leg,'FontSize',self.legfs);
%                 
%             end
%         end
%     end
%     
%     %% Plot avarage turn to turn voltage per layer in a subplot
%     methods
%         function [ax,f] = Vt2tAL(self,ms)
%             % initialize
%             self.initialize(ms);
%             
%             % create figure
%             f = figure; set(f,'Color','w','Position',self.subpos,'Renderer',self.renderer);
%             if self.visible == false; set(f,'Visible','off'); end
%                   
%             % make lrc plots with the data in relcoils(i)
%             lcg = length(self.laygrp);
%             for i = 1:lcg
%                 
%                 % make plot
%                 hTemp = subplot(lcg,1,i,'Parent',f); newPos = get(hTemp,'Position'); delete(hTemp);
%                 [ax,g] = self.plotVt2tAL(ms,self.laygrp{i});
%                 set(ax,'Parent',f,'Position',newPos); close(g);
%                 
%                 % fix details
%                 if i ~= lcg; xlabel([]); end
%                 if i ~= 1; title([]); end
%                 leg = legend(ax,'show','Location',self.tlegpos);
%                 set(leg,'FontSize',self.legfs);
%                 
%             end
%         end
%     end
%     
%     %% end new new new new new new new Nikkie
    
    %% Plot max layer to layer voltage in a subplot
    methods
        function [ax,f] = Vl2l(self,ms)
            % initialize
            self.initialize(ms);
            
            % plot Vl2l in a subplot
            f = figure; set(f,'Color','w','Position',self.subpos,'Renderer',self.renderer);
            if self.visible == false; set(f,'Visible','off'); end
            
            % find out which coils are relevant
            lcg = length(self.coilgrp);
            relcoils = cell(1,0);
            for i = 1:lcg
                % coils
                rc = [];
                for j = self.coilgrp{i}
                    if ms.coils{j}.nrad > 1
                        rc = [rc ms.coils{j}.coilnr];
                    end
                end
                
                % add if not empty
                if ~isempty(rc)
                    relcoils{i} = rc;
                end
                
            end
            
            % make lrc plots with the data in relcoils(i)
            lrc = length(relcoils);
            for i = 1:lrc
                
                % make plot
                hTemp = subplot(lrc,1,i,'Parent',f); newPos = get(hTemp,'Position'); delete(hTemp);
                [ax,g] = self.plotVl2l(ms,relcoils{i});
                set(ax,'Parent',f,'Position',newPos); close(g);
                
                % fix details
                if i ~= lrc; xlabel([]); end
                if i ~= 1; title([]); end
                leg = legend(ax,'show','Location',self.tlegpos);
                set(leg,'FontSize',self.legfs);
                
            end
        end
    end
    
    %% Plot quench detection signal
    methods
        function [ax,f] = VQD(self,ms)
            % initialize
            self.initialize(ms);
            
            % denote some useful values and define colors
            nQD = size(ms.qcirc.QDconfig,1);
            lts = round(length(ms.tstore));
            C = HSV(nQD);
            
            % get quench detection signal
            VQD = cell2mat(ms.VQDstore);

            % setup figure
            f = figure('Color','w','Position',self.pos,'Renderer',self.renderer);
            ax = axes('Parent',f); hold(ax,'on'); grid(ax,'on');
            if self.visible == false; set(f,'Visible','off'); end
            
            % plot Vnz in quench circuit
            plot(ms.tstore(1:length(VQD)),VQD(:,1),'Color',[0.3 0.3 0.4],'Linewidth',self.linewidth,'DisplayName','Vnz')
            
            % plot quench detection signals
            for i = 1:nQD
                plot(ms.tstore(1:length(VQD)),VQD(:,i+1),'Color',C(i,:),'Linewidth',self.linewidth,'DisplayName',['QD' num2str(i)])
            end
            
            % set interesting range
            set(ax,'XLim',[ms.tstart,ms.tQD]);
            %ylim(ax,[min(VQD(:,i)) max(VQD(:,i))] );
            
            % make shorter title
            tQDstr = sprintf('tQD = %1.3f s',ms.tQD);
            shorttitle = [ms.qcoil.name ' Quench: ' tQDstr];
            
            % finish up
            legend(ax,'show','Location',self.tlegpos);
            title(shorttitle);
            xlabel('Time [s]')
            ylabel('Quench Detection Signal [V]')
            
        end
    end
    
    %% Plot the various voltage types of a circuit
    methods
        function [ax,f] = Vcheck(self,ms,circuit)
            % initialize
            self.initialize(ms);
            
            % prealocate
            Vnz = zeros(1,length(ms.tstore));
            Vind = zeros(1,length(ms.tstore));
            Vdr = zeros(1,length(ms.tstore));
            Vdiodes = zeros(1,length(ms.tstore));
            Vtot = zeros(1,length(ms.tstore));
            
            % go through times
            for i = 1:length(ms.tstore)
                % time
                ms.t = ms.tstore(i);
                
                % go through coils of given circuit and store Vnz and Vind
                for j = 1:length(circuit.coils);
                    coilnr = circuit.coils{j}.coilnr;
                    Vnz(i) = Vnz(i) + sum(sum( ms.Vnzstore{coilnr,i} ));
                    Vind(i) = Vind(i) + sum(sum( ms.Vindstore{coilnr,i} ));
                end
                
                % time dependent external dump resistor voltage
                Vdr(i) = ms.Istore{i}(circuit.circnr) * circuit.Rextern;
                
                % time dependent diode voltage
                circuit.I = ms.Istore{i}(circuit.circnr); Vdiodes(i) = circuit.Vdio;
%                 Vdiodes(i) = circuit.ndiodes.* Vdiode(ms.Istore{i}(circuit.circnr),circuit.diodetype);
               

                % total voltage
                Vtot(i) = Vnz(i) + Vind(i) + Vdr(i) +  Vdiodes(i);
                
            end
            
            % color
            C = HSV(5);
            
            % create figure
            f = figure('Color','w','Position',[100,100,1000,400],'Renderer',self.renderer);
            ax = axes('Parent',f); hold(ax,'on'); grid(ax,'on');
            if self.visible == false; set(f,'Visible','off'); end
            
            % plot five types of voltages
            plot(ms.tstore,Vnz,'Color',C(1,:),'LineWidth',self.linewidth,'DisplayName','NZ voltage')
            plot(ms.tstore,Vind,'Color',C(2,:),'LineWidth',self.linewidth,'DisplayName','Induced voltage')
            plot(ms.tstore,Vdr,'Color',C(3,:),'LineWidth',self.linewidth,'DisplayName','Voltage over dumpresistor')
            plot(ms.tstore,Vdiodes,'Color',C(4,:),'LineWidth',self.linewidth,'DisplayName','Voltage over diode(s)')
            plot(ms.tstore,Vtot,'Color',C(5,:),'LineWidth',self.linewidth,'DisplayName','Total voltage')
            
            % finish up
            axis tight;
            legend(ax,'show', 'Location',self.tlegpos);
            title(['Various voltage types over ' circuit.name]);
            xlabel('Time [s]')
            ylabel('Voltage [V]')
            
        end
    end
    
    %% Vtaps
    methods
        function [ax,f] = Vtaps(self,ms)
            % initialize
            self.initialize(ms);
            
            % create figure
            f = figure('Color','w','Position',[100,100,1000,400],'Renderer',self.renderer);
            ax = axes('Parent',f); hold(ax,'on'); grid(ax,'on');
            if self.visible == false; set(f,'Visible','off'); end
            
            % get voltage tap data
            Vtaps = cell2mat(ms.Vtapsstore); l = size(Vtaps,2)-1;
            %names = {'CompCoilUS', 'CompCoilDS', '1st-2nd layer', '3rd-8th layer', '9th-10th layer'};
            
            % color
            C = HSV(l);
            
            % plot HSV
            for i = 1:l
                plot(ms.tstore,Vtaps(:,i+1)-Vtaps(:,i),'Linewidth',self.linewidth,'Color',C(i,:),'DisplayName',['Vtap ' num2str(i+1) ' - Vtap' num2str(i)]);
            end
            
            % finish up
            axis tight
            legend(ax,'show','Location',self.tlegpos);
            title(self.titlestr);
            xlabel('Time [s]');
            ylabel('Voltage [V]');
            
        end
    end
    
    %% Plot distribution of energy types of system
    methods
        function [ax,f] = E(self,ms)
            % initialize
            self.initialize(ms);
            
            % color
            C = hsv(6);
            
            % make energy arrays
            % for energies per circuit
            Emag = sum(cell2mat(ms.Emagstore),2);
            Edisdr = sum(cell2mat(ms.Edisdrstore),2);
            Edisdio = sum(cell2mat(ms.Edisdiostore),2);
            
            % for energies per coil
            Eth = sum(cell2mat(ms.Ethstore),2);
            Edisqh = sum(cell2mat(ms.Edisqhstore),2);
            
            % convert dissipated energy to potential energy
            Epotqh = max(Edisqh) - Edisqh;
            
            % create figure
            f = figure('Color','w','Position',self.pos,'Renderer',self.renderer);
            ax = axes('Parent',f); hold(ax,'on'); grid(ax,'on');
            if self.visible == false; set(f,'Visible','off'); end
            
            % plot energy contributions
            plot(ax,ms.tstore,Emag,'-','Color',C(1,:),'Linewidth',self.linewidth,'DisplayName','Magnetic');
            plot(ax,ms.tstore,Eth,'-','Color',C(2,:),'Linewidth',self.linewidth,'DisplayName','Thermal');
            plot(ax,ms.tstore,Edisdr,'-','Color',C(3,:),'Linewidth',self.linewidth,'DisplayName','Dump Resistors');
            plot(ax,ms.tstore,Edisdio,'-','Color',C(4,:),'Linewidth',self.linewidth,'DisplayName','Diodes');
%             plot(ax,ms.tstore,Epotqh,'-','Color',C(5,:),'Linewidth',self.linewidth,'DisplayName','Quench Heaters');
            
            % total energy
            Etot = Emag + Eth + Edisdr + Edisdio + Epotqh;
            plot(ax,ms.tstore,Etot,'-','Color',C(6,:),'Linewidth',self.linewidth,'DisplayName','Total Energy');
            
            % finish up
            axis tight
            legend(ax,'show','Location',self.tlegpos);
            title(self.titlestr);
            xlabel('Time [s]');
            ylabel('Energy [J]');
            
        end
    end
    
    %% Final energy distribution amongst coils
    methods
        function [ax,f] = Ef(self,ms)
            % initialize
            self.initialize(ms);
            
            % create figure
            f = figure('Color','w','Position',self.subpos,'Renderer',self.renderer);
            ax = axes('Parent',f); hold(ax,'on'); grid(ax,'on');
            if self.visible == false; set(f,'Visible','off'); end
            
            % thermal energy
            hTemp = subplot(3,1,1,'Parent',f); newPos = get(hTemp,'Position'); delete(hTemp);
            [axh,g] = self.Eth(ms);
            set(axh,'Parent',f,'Position',newPos); close(g);
            
            % magnetic energy
            hTemp = subplot(3,1,2,'Parent',f); newPos = get(hTemp,'Position'); delete(hTemp);
            [axh,g] = self.Emag(ms); title(axh,[]);
            set(axh,'Parent',f,'Position',newPos); close(g);
            
            % externally dissipated energy
            hTemp = subplot(3,1,3,'Parent',f); newPos = get(hTemp,'Position'); delete(hTemp);
            [axh,g] = self.Edis(ms); title(axh,[]);
            set(axh,'Parent',f,'Position',newPos); close(g);
            
        end
    end
    
    %% Final thermal energy 
    methods
        function [ax,f] = Eth(self,ms)
            % initialize
            self.initialize(ms);    

            % create figure
            f = figure('Color','w','Position',self.pos,'Renderer',self.renderer);
            ax = axes('Parent',f); hold(ax,'on'); grid(ax,'on');
            if self.visible == false; set(f,'Visible','off'); end
            
            % barplot
            h = barh(ax,ms.Ethstore{end},'g');
            
            % find names
            names = cell(1,ms.ncoils);
            for i = 1:ms.ncoils
                names{i} = ms.coils{i}.name;
            end

            % set names
            set(ax,'YLim',[0.5 ms.ncoils+0.5],'Ytick',1:ms.ncoils,'YTickLabel',names);
            
            % finish up
            title(self.titlestr);
            xlabel('Final Thermal Energy [J]');
            
        end
    end
    
    %% Final magnetic energy 
    methods
        function [ax,f] = Emag(self,ms)
            % initialize
            self.initialize(ms);    

            % create figure
            f = figure('Color','w','Position',self.pos,'Renderer',self.renderer);
            ax = axes('Parent',f); hold(ax,'on'); grid(ax,'on');
            if self.visible == false; set(f,'Visible','off'); end
            
            % barplot
            h = barh(ax,ms.Emagstore{end},'g');
            
            % find names
            names = cell(1,ms.ncirc);
            for i = 1:ms.ncirc
                names{i} = ms.circ{i}.name;
            end

            % set names
            set(ax,'YLim',[0.5 ms.ncirc+0.5],'Ytick',1:ms.ncirc,'YTickLabel',names);
            
            % finish up
            title(self.titlestr);
            xlabel('Final Magnetic Energy [J]');
            
        end
    end
    
    %% Final externally dissipated energy 
    methods
        function [ax,f] = Edis(self,ms)
            % initialize
            self.initialize(ms);    

            % create figure
            f = figure('Color','w','Position',self.pos,'Renderer',self.renderer);
            ax = axes('Parent',f); hold(ax,'on'); grid(ax,'on');
            if self.visible == false; set(f,'Visible','off'); end
            
            % barplot
            h = barh(ax,ms.Edisdrstore{end}+ms.Edisdiostore{end},'g');
            
            % find names
            names = cell(1,ms.ncirc);
            for i = 1:ms.ncirc
                names{i} = ms.circ{i}.name;
            end

            % set names
            set(ax,'YLim',[0.5 ms.ncirc+0.5],'Ytick',1:ms.ncirc,'YTickLabel',names); % colormap(HSV);
            
            % finish up
            title(self.titlestr);
            xlabel('Final Externally Dissipated Energy [J]');
            
            
        end
    end
    
    
    
    
    %% Peak current
    methods
        function [ax,f] = Ipeak(self,mslist)
            % initialize
            self.initialize(mslist);
            
            % create figure
            f = figure('Color','w','Position',self.subpos,'Renderer',self.renderer);
            ax = axes('Parent',f); hold(ax,'on'); grid(ax,'on');
            if self.visible == false; set(f,'Visible','off'); end
            
            % iterate through mslist
            Iall = zeros(length(mslist),mslist{1}.ncirc);
            for nr = 1:length(mslist)
                % iterate through circuits
                Ipeak = zeros(1,mslist{nr}.ncirc);
                
                % extraxt I matrix from ms
                Imat = cell2mat(mslist{nr}.Istore);
               
                for i = 1:mslist{nr}.ncirc
                    % obtain I of i'th circuit
                    I = Imat(:,i);
                    
                    % calculate peak current in i'th coil
                    [val,idx] = max( abs(I) );
                    Ipeak(i) = sign(I(idx)) * val;
                end
                
                % store Ipeak in nr'th column of Iall
                Iall(nr,:) = Ipeak;
            end
            
            % cheat hacks for showing the right data
            if self.onlyone
                Iall = [zeros(1,length(Iall)), Iall];
            end
            
            % cut too big data
            Iini = mslist{1}.Istore{1};
            Iall = min(Iall,100+max(Iini)*(1+self.marg/100));
            
            % wtf barplot -.-
            Iall = rot90(Iall,2);
            
            % plot
            h = barh(ax,Iall,1);
            
            % set coilnames
            names = cell(1,mslist{1}.ncirc);
            for i = 1:mslist{1}.ncirc
                names{i} = mslist{1}.circ{i}.name;
            end
            names = fliplr(names);
            
            % make y labels
            if self.onlyone
                set(ax,'YLim',[0.5 1.5],'Ytick',1:1,'YTickLabel', self.cases{1}); colormap(HSV);
            else
                set(ax,'YLim',[0.5 length(self.cases)+0.5],'Ytick',1:length(self.cases),'YTickLabel',fliplr(self.cases)); colormap(HSV);
            end
            
            % set x limit
            xrange = [ min(Iini) max(Iini) ];
            dy = diff(xrange); xrange = [xrange(1), xrange(2)+dy*self.marg/100];
            xlim(ax,xrange);
            
            % finish up
            order = length(names):-1:1;
            leg = legend(h(order),names{order});
            set(leg,'FontSize',self.legfs,'Location','NorthEastOutside'); 
            title(self.titlestr);
            xlabel('Peak Circuit Current [A]');
            
        end
    end
    
    %% Peak temperature
    methods
        function [ax,f] = Tpeak(self,mslist)
            % initialize
            self.initialize(mslist);
            
            % create figure
            f = figure('Color','w','Position',self.subpos,'Renderer',self.renderer);
            ax = axes('Parent',f); hold(ax,'on'); grid(ax,'on');
            if self.visible == false; set(f,'Visible','off'); end
            
            % iterate through mslist
            Tall = zeros(length(mslist),mslist{1}.ncoils);
            for nr = 1:length(mslist)
                % iterate through circuits
                Tpeak = zeros(1,mslist{nr}.ncoils);
                
                % extraxt T matrix from ms
                Tmat = cell2mat(mslist{nr}.Tmaxstore);
               
                for i = 1:mslist{nr}.ncoils
                    % obtain max temperature as function of time of i'th coil
                    T = Tmat(:,i);
                    
                    % calculate peak temperature of i'th coil
                    Tpeak(i) = max(T);
                    
                end
                
                % store Tpeak in nr'th column of Tall
                Tall(nr,:) = Tpeak;
            end
            
            % cheat hacks for grouping of data
            if self.onlyone
                Tall = [zeros(1,length(Tall)) ; Tall];
            end
            
            % wtf barplot -.-
            Tall = rot90(Tall,2);
            
            % plot
            h = barh(ax,Tall,1);
            
            % set coilnames
            names = cell(1,mslist{1}.ncoils);
            for i = 1:mslist{1}.ncoils
                %set(h(i),'DisplayName',mslist{1}.coils{i}.name)
                names{i} = mslist{1}.coils{i}.name;
            end
            names = fliplr(names);
            
            % make y labels
            if self.onlyone
                set(ax,'YLim',[0.5 1.5],'Ytick',1:1,'YTickLabel', self.cases{1}); colormap(HSV);
            else
                set(ax,'YLim',[0.5 length(self.cases)+0.5],'Ytick',1:length(self.cases),'YTickLabel', fliplr(self.cases)); colormap(HSV);
            end
            
            % finish up
            order = length(names):-1:1;
            leg = legend(h(order),names{order});
            set(leg,'FontSize',self.legfs,'Location','NorthEastOutside','Box','off'); 
            title(self.titlestr);
            xlabel('Peak Temperature [K]');
            
        end
    end
    
    %% Peak turn to turn voltage
    methods
        function [ax,f] = Vt2tpeak(self,mslist)
            % initialize
            self.initialize(mslist);
            
            % create figure
            f = figure('Color','w','Position',self.subpos,'Renderer',self.renderer);
            ax = axes('Parent',f); hold(ax,'on'); grid(ax,'on');
            if self.visible == false; set(f,'Visible','off'); end
            
            % find out which coils are relevant
            relcoils = [];
            for i = 1:mslist{1}.ncoils
                if mslist{1}.coils{i}.nax > 1
                    relcoils = [relcoils mslist{1}.coils{i}.coilnr];
                end
            end
            
            % denote length of relevant coils
            lrc = length(relcoils);
            
            % iterate through msliststruct
            Vall = zeros(length(mslist),lrc);
            for nr = 1:length(mslist)
                % iterate through circuits
                Vpeak = [];
                
                % obtain Vt2t matrix
                Vt2tcell = mslist{nr}.Vt2tmaxstore;
                
                for i = relcoils
                    % find Vt2tmax of given coil
                    Vt2t = cell2mat( Vt2tcell(:,i) );
                    
                    % calculate peak current in i'th coil
                    Vpeak = [Vpeak max(Vt2t)];
                    
                end
                % store Vt2tpeak in nr'th column of Vall
                Vall(nr,:) = Vpeak;
                
            end
            
            % cheat hacks for grouping of data
            if self.onlyone
                Vall = [zeros(1,length(Vall)) ; Vall];
            end
            
            % wtf barplot -.-
            Vall = rot90(Vall,2);
            
            % plot
            h = barh(ax,Vall,1);
            
            % set coilnames
            names = cell(1,lrc);
            j = 1;
            for i = relcoils
                names{j} = mslist{1}.coils{i}.name;
                j = j+1;
            end
            names = fliplr(names);
            
            % make y labels
            if self.onlyone
                set(ax,'YLim',[0.5 1.5],'Ytick',1:1,'YTickLabel', self.cases{1}); colormap(HSV);
            else
                set(ax,'YLim',[0.5 length(self.cases)+0.5],'Ytick',1:length(self.cases),'YTickLabel',fliplr(self.cases)); colormap(HSV);
            end
            
            % finish up
            order = length(names):-1:1;
            leg = legend(h(order),names{order});
            set(leg,'FontSize',self.legfs,'Location','NorthEastOutside','Box','off'); 
            title(self.titlestr);
            xlabel('Peak t2t Voltage [V]');
                     
        end
    end
    
    %% Peak layer to layer voltage
    methods
        function [ax,f] = Vl2lpeak(self,mslist)
            % initialize
            self.initialize(mslist);
            
            % create figure
            f = figure('Color','w','Position',self.subpos,'Renderer',self.renderer);
            ax = axes('Parent',f); hold(ax,'on'); grid(ax,'on');
            if self.visible == false; set(f,'Visible','off'); end
            
            % find out which coils are relevant
            relcoils = [];
            for i = 1:mslist{1}.ncoils
                if mslist{1}.coils{i}.nrad > 1
                    relcoils = [relcoils mslist{1}.coils{i}.coilnr];
                end
            end
            
            % denote length of relevant coils array
            lrc = length(relcoils);
            
            % iterate through msliststruct
            Vall = zeros(length(mslist),lrc);
            for nr = 1:length(mslist)
                % iterate through circuits
                Vpeak = [];
                
                % obtain Vl2l matrix
                Vl2lcell = mslist{nr}.Vl2lmaxstore;
                
                for i = relcoils
                    % find Vt2tmax of given coil
                    Vl2l = cell2mat( Vl2lcell(:,i) );
                    
                    % calculate peak current in i'th coil
                    Vpeak = [Vpeak max(Vl2l)];
                    
                end
                
                % store Vl2lpeak in nr'th column of Vall
                Vall(nr,:) = Vpeak;
                
            end
            
            % cheat hacks for grouping of data
            if self.onlyone
                Vall = [zeros(1,length(Vall)) ; Vall];
            end
            
            % wtf barplot -.-
            Vall = rot90(Vall,2);
            
            % plot
            h = barh(ax,Vall,1);
            
           % set coilnames
            names = cell(1,lrc);
            j = 1;
            for i = relcoils
                names{j} = mslist{1}.coils{i}.name;
                j = j+1;
            end
            names = fliplr(names);
            
            % make y labels
            if self.onlyone
                set(ax,'YLim',[0.5 1.5],'Ytick',1:1,'YTickLabel', self.cases{1}); colormap(HSV);
            else
                set(ax,'YLim',[0.5 length(self.cases)+0.5],'Ytick',1:length(self.cases),'YTickLabel',fliplr(self.cases)); colormap(HSV);
            end
            
            % finish up
            order = length(names):-1:1;
            leg = legend(h(order),names{order});
            set(leg,'FontSize',self.legfs,'Location','NorthEastOutside','Box','off'); 
            title(self.titlestr);
            xlabel('Peak l2l Voltage [V]');
            
        end
    end
    
    %% IV curve of a diode
    methods
        function [ax,f] = IV_diode(self,type,varargin)
            % varargin
            I = [];
            if numel(varargin) == 1 && isvector(varargin{1});
                I = varargin{1};
            end
            
            % create figure
            f = figure('Color','w','Position',self.pos,'Renderer',self.renderer);
            ax = axes('Parent',f); hold(ax,'on'); grid(ax,'on');
            if self.visible == false; set(f,'Visible','off'); end
            
            % get data
            if isempty(I)
                [U,I] = Vdiode(type);
            else
                [U,~] = Vdiode(type,I);
            end
            
            % color
            fp = fireprint(8);
            
            % plot
            plot(I,U,'LineWidth',2,'Color',fp(3,:));
            
            % finish up
            axis tight
            xlabel('Current [A]')
            ylabel('Voltage [V]')
            title([type ' diode IV-curve'])
            
        end
    end
 
end




