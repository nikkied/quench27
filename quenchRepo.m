function [] = quenchRepo( ms,path,name )
%QUENCHREPORT creates a pdf report based on one quench
% it uses the stored data from the store arrays in ms, the quenchplot class
% ms.qpl and stores the tex and pdf files in folder 'path' as
% 'name.tex'/'name.pdf'

%% Post Process if still required
if length(ms.Tmaxstore) ~= length(ms.tstore)
    ms.postprocess(1);
end

%noob
fclose all;

% make directory
if ~exist(path,'dir'); mkdir(path); end;

%% Setup texwriter class
tex = texwriter;
tex.path = path;
tex.name = name;
tex.figmode = 'pdf';

%% Create Report
% header
tex.header;

% table of contents
tex.toc;

% input chapter
tex.head('Simulation Input',0);

% results chapter
tex.head('Simulation Results',0);

% quench detection signal
tex.head('Quench detection signal',3);
[~,f] = ms.qpl.VQD;
figname = [ms.qcoil.name 'Quench_VQD'];
tex.fig(path,f,figname,'usetitle');

% plot current
tex.head('Current through circuits',3);
[~,f] = ms.qpl.I(ms);
figname = [ms.qcoil.name 'Quench_I'];
tex.fig(path,f,figname,'usetitle');

% plot hotspot temperature
tex.head('Maximum temperature of coils',3);
[~,f] = ms.qpl.T(ms);
figname = [ms.qcoil.name 'Quench_T'];
tex.fig(path,f,figname,'usetitle');

% plot turn to turn voltage
tex.head('Maximum turn to turn voltage of coils',3);
[~,f] = ms.qpl.Vt2t(ms);
figname = [ms.qcoil.name 'Quench_Vt2t'];
tex.fig(path,f,figname,'usetitle');

% plot layer to layer voltage
tex.head('Maximum layer to layer voltage of coils',3);
[~,f] = ms.qpl.Vt2t(ms);
figname = [ms.qcoil.name 'Quench_Vt2t'];
tex.fig(path,f,figname,'usetitle');

% plot voltage tap voltages
tex.head('Voltages between voltage taps',3);
[~,f] = ms.qpl.Vtaps(ms);
figname = [ms.qcoil.name 'Quench_Vtaps'];
tex.fig(path,f,figname,'usetitle');

% plot energy as function of time
tex.head('Energy distribution of system',3);
[~,f] = ms.qpl.E(ms);
figname = [ms.qcoil.name 'Quench_E'];
tex.fig(path,f,figname,'usetitle');

% plot final energy distribution
tex.head('Final energy distribution of coils',3);
[~,f] = ms.qpl.Ef(ms);
figname = [ms.qcoil.name 'Quench_Ef'];
tex.fig(path,f,figname,'usetitle');

% finish up
tex.footer
tex.compile

end

