%% quench for simulating a system with 2 simple 10 layer coils
system = example_system();

%% detection time
% set threshold normal zone voltage
system.VQD_threshold = 0; 
% and threshold time. NZ voltage is over the threshold value for more than threshold time,
% the quench is detected (mysystem.t_QD is set to current time)
system.dt_QD = 0;                         

%% solver properties
system.tmax = 0.6;
% set timestep to STORE data every 5ms
% (solver decides which times to use for accurate calculations based on tolerances).
system.dt = 0.005; 
% set absolute tolerance of the solver
system.abstol = 1E-6;    
% set relative tolerance of the solver
system.reltol = 1E-4;                          

%% Setup type of quench
% give a description of this simulation in a short sentence. The goal is to be able
% to distinguish this name from other simulation you perform on the system with the same name.
system.scenario = 'quench in coil 1 element (1,1)';  
% acronym of scenario. This is used to create files and folders and possibly in latex, so choose carefully...
system.shortscenario = 'coil1(1,1)';
% Kelvin above critical temperature
system.Tquench = 5.0;                         

%% Initialize system
system.initialize;

% quench a coil
system.quench('Coil',[1,1])            % this command quenches 'Coil' at the bottom left (layer1, turn1)

% Solve
SolveQuench(system);

% Post process
system.postprocess;

%% Analyse data
system.plotgeo;
slider(system);

% Plotting

% plotting critical surface
system.coils{1}.cond.critsurf();

% setting preferences
qpl = quenchplot();
% This sets the size of the subplots (which contain only one plot in case of one coil and one circuit);
qpl.subpos = [1,1,1000,400];  
% use arrays in the cell elements to group coils into multiple groups for plotting
qpl.coilgrp = {[1,2]};
% use arrays in the cell elements to group circuits into multiple groups for plotting
qpl.circgrp = {[1]};                        

qpl.coilC = hsv(2);               % assign colors for each of the coils
qpl.circC = hsv(1);               % assign colors for each of the circuits

% plots
qpl.I(system);                    % plot I of all circuits (the current) as a function of time
qpl.T(system);                    % plot the peak temperature for the coils
qpl.E(system);                    % energy type distribution of the system as a whole as a function of time
qpl.Vt2t(system);                 % plot Vt2t for the coils
qpl.Vl2l(system);                 % plot Vl2l

qp2 = testplot();
% This sets the size of the subplots (which contain only one plot in case of one coil and one circuit)
qp2.subpos = [1,1,1000,400];                    

% use arrays in the cell elements to group coils into multiple groups for plotting
qp2.coilgrp = {[2]};
% use arrays in the cell elements to group circuits into multiple groups for plotting
qp2.circgrp = {[1]};             
% use arrays in the cell elements to group layers into multiple groups for plotting
qp2.laygrp = {[1,2,3,4,5,6,7,8,9,10],[11,12,13,14,15,16,17,18,19,20]};       

qp2.coilC = hsv(2);                             % assign colors for each of the coils
qp2.circC = hsv(1);                             % assign colors for each of the circuits
qp2.layC = hsv(20);                             % assign colors for each of the layers

% plots
qp2.Vl2l(system);
qp2.Vl2lind(system);
qp2.Vl2lnz(system);
qp2.Vl2lcontrole(system);

