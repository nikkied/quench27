%% System for simulating a system with 2 simple 10 layer coils

function [ system ] = example_system()

%% create system

%% ________________
% Coil conductor
% _________________

% create conductor class
conductor = Create_Conductor();

% set a name for conductor
conductor.name = 'Conductor';

% dimensions of conductor
conductor.axL = 1.22E-3;
conductor.radL = 0.75E-3;

% composition of the conducting part
% part of conductor that is superconducting on a scale of 0 to 1
conductor.ScFactor = 0.1923; 
% type of superconductor
conductor.ScMaterial = 'Niobium Titanium';
% part of conductor that is matrix material on a scale of 0 to 1
conductor.MatrixFactor = 0.8077;   
% type of matrix material
conductor.MatrixMaterial = 'OFHC Copper RRR112.3';  
% part of conductor that is stabilizer material on a scale of 0 to 1
conductor.StabFactor = 0;
% type of stabilizer
conductor.StabMaterial = 'None';
% part of conductor that is stabilizer material on a scale of 0 to 1
conductor.StabFactor2 = 0;   
% type of stabilizer
conductor.StabMaterial2 = 'None';         
conductor.VoidFactor = 0;

% insulating part of conductor
% thickness of insulation belonging to conductor in axial direction
conductor.axinsulationthickness = 0.05E-3;  
% thickness of insulation belonging to conductor in radial direction
conductor.radinsulationthickness = 0.25E-3;  
% type of material in axial direction (a string in a cell)
conductor.axinsulationmat = {'Fiberglass Epoxy G-10 CR normal'};   
% type of material in radial direction (a string in a cell)
conductor.radinsulationmat = {'Fiberglass Epoxy G-10 CR normal'};  



%% _____________________________
% creating superconducting coil1
% ______________________________
mycoil = Create_Coil();
mycoil.name = 'Coil';
mycoil.setcond(conductor); % set conductor
% set dimensions of the coil
mycoil.axrange = [-0.0995 0.0995];
mycoil.radrange = [0.1183 0.1283];
mycoil.nax = 157;
mycoil.nrad = 10;

% set non-conductor isolation
 % set material of radial insulation which does not belong to conductor, but to the coil
 %(typically fiberglass epoxy in radial direction for added electrical insulation in layer2layer direction).
mycoil.radinsulationmat = {};
mycoil.radinsulationthickness = 0; % set thickness of radial insulation
mycoil.axinsulationmat = {}; % similar for axial direction
mycoil.axinsulationthickness = 0;
mycoil.radcoilinsmat = {};
mycoil.radcoilinsthickness = 0;
mycoil.axcoilinsmat = {};
mycoil.axcoilinsthickness = 0;

%% _____________________________
% creating superconducting coil2
% ______________________________
mycoil2 = Create_Coil();
mycoil2.name = 'Coil2';
mycoil2.setcond(conductor); % set conductor
% set dimensions of the coil
mycoil2.axrange = [-0.0995 0.0995];
mycoil2.radrange = [0.1283 0.1383];
mycoil2.nax = 157;
mycoil2.nrad = 10;

% set non-conductor isolation
mycoil2.radinsulationmat = {}; 
mycoil2.radinsulationthickness = 0;
mycoil2.axinsulationmat = {};
mycoil2.axinsulationthickness = 0;
mycoil2.radcoilinsmat = {};
mycoil2.radcoilinsthickness = 0;
mycoil2.axcoilinsmat = {};
mycoil2.axcoilinsthickness = 0;

%% create quench heater class

nQH = 3;       % number of quench heaters
myQH = {};


for i = 1:nQH;
    myQH{i} = Create_QuenchHeater();
    % define location: location is a matrix the size of the elements of the coil with zeros for
    % elements which are not heated and ones for elements which are heated.
    myQH{i}.location = zeros(mycoil2.nrad,mycoil.nax);  
end

% define location: location is a matrix the size of the elements of the coil with zeros for elements
% which are not heated and ones for elements which are heated.
myQH{1}.location(1,5:9) = 1;
myQH{2}.location(1,77:81) = 1;
myQH{3}.location(1,143:152) = 1;


for i = 1:nQH;
    % other properties
    % heating will start 0.05s after the quench has been detected
    myQH{i}.response_t = 0.05; 
    % power of quench heater as a whole (heat is distributed evenly over elements).
    myQH{i}.P = 80; 
    % duration of quench heater fire.
    myQH{i}.duration = 0.1;         

    % add quench heater to coil
    mycoil.qh{i} = myQH{i};
end


%% _____________________________________________
% Creating circuit for the superconducting coil
% ______________________________________________
circ = Create_Circuit();
circ.name = 'Magnet';

circ.addcoils( mycoil );
circ.addcoils( mycoil2 );

% setup other circuit properties
circ.Iinitial = 500;
circ.Rdi = 0;
circ.Rdf = 0.1; % dump resistor of 0.1 ohm
circ.ndiodes = 1;
circ.diodetype = 'MDD26_one';
circ.dt_CB = 0.0;
circ.dt_Rd = 0.0;



%% ______________________
% Creating system
% _______________________

system = Create_System();
system.name = 'validation';

% add to system
system.addcirc(circ);


%% _______________________
% Creating thermal links
% ________________________

% Creating thermal link
mylink = Thermal_Link_n();
mylink.name = 'Coil to outer layers';

% Adding coils to thermal links
mylink.addcoils(mycoil, mycoil2);






end
