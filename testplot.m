classdef testplot< hgsetget
    %TESTPLOT Summary of this class goes here
    %   Detailed explanation goes here

    properties
        
        % system information
        cases = [];
        onlyone = false;
        
        % figure and axes handles (for interface)
        ax = [];
        f = [];
        
        % subplots
        circgrp = {};                   % grouping of circuits
        coilgrp = {};                   % grouping of coils
%         %new
        laygrp = {};                    % grouping of layers
%         %new
        
        % figure preferences
        visible = true;                 % boolian to toggle visibility of plots
        titlestr = [];                  % titlestring
        linewidth = 1.4;                % linewidth
        subpos = [1,1,800,960];
        pos = [100,100,1000,400];
        renderer = 'OpenGL';
        legfs =  9;                     % legend font size
        
        % 
        tlegpos = 'NorthEast';          % legend position of time dependent plots
        caselegpos = 'NorthEastOutside'; % legend position of time independent plots (usually barplots)

        % 
        marg = 2;                      % margin used (in percent) in barplots to distinguish out of bounds data from high inbound data
        %tQD = [];                       % quench protection times of length equeal to length(mslist)
        
        
        %         % export
        %         export = false;
        %         extension = []; %'-pdf' '-tikz' ?
        
        % colors
        circC = [];     % color of circuits (ms.ncirc x 3 matrix)
        coilC = [];     % color of coils (ms.ncoils x 3 matrix)
        
%         %new
        layC = [];      % color of layers ((ms.coils{i} x nrad) x 3 matrix)
%         %new
    end
    

    %% Initialize
    methods
        function initialize(self,input)
            % distinguish between tplots and caseplots
            if iscell(input)
                % define titlestring
                self.titlestr = [input{1}.qcoil.name ' Quench'];
                
                % define case-strings
                self.cases = cell(1,length(input));
                for i = 1:length(input)
                    self.cases{i} = input{i}.shortscenario;
                end
                
                % for showing peak values of only one quench --> put it in a cell
                if length(input) == 1
                    self.onlyone = true;
                else
                    self.onlyone = false;
                end
                
            else
                % define titlestring
                self.titlestr = [input.scenario];
                
            end
            
        end
    end
    
    %% Plot layer to layer voltage for given coils
    methods
        function [ax,f] = plotVl2l(self,ms,layers)
            % initialize
            self.initialize(ms);
            
            % create figure
            f = figure('Color','w','Position',self.pos,'Renderer',self.renderer);
            ax = axes('Parent',f); hold(ax,'on'); grid(ax,'on');
            if self.visible == false; set(f,'Visible','off'); end
            
            % iterate through coils
            for i = layers
                % extract Vl2lmax of i'th coil
                Vl2ldata = cell2mat( ms.Vl2lstore(:,i) );
                
                % plot
                plot(ms.tstore,Vl2ldata,'Linewidth',self.linewidth,'Color',self.layC(i,:),'DisplayName',num2str(i));
                
                
%                 for j = 1:length(ms.mapVl2lstore)
%                     disp(j)
%                     if i<ms.mapVl2lstore(j+1)
%                         plot(ms.tstore,Vl2lmax,'Linewidth',self.linewidth,'Color',self.coilC(j,:),...
%                             'DisplayName',i);
%                     end
%                 end
            end
            
            % finish up
            axis tight;
            leg = legend('show','Location',self.tlegpos);
            set(leg,'FontSize',self.legfs);
            title(self.titlestr);
            xlabel('Time [s]')
            ylabel('l2l Voltage [V]')
            
        end
    end
    
    %% Plot max layer to layer voltage in a subplot
    methods
        function [ax,f] = Vl2l(self,ms)
            % initialize
            self.initialize(ms);
            
            % plot Vl2l in a subplot
            f = figure; set(f,'Color','w','Position',self.subpos,'Renderer',self.renderer);
            if self.visible == false; set(f,'Visible','off'); end
            
            % find out which coils are relevant
            llg = length(self.laygrp);
            for i = 1:llg
              
                % make plot
                hTemp = subplot(llg,1,i,'Parent',f); newPos = get(hTemp,'Position'); delete(hTemp);
                [ax,g] = self.plotVl2l(ms,self.laygrp{i});
                set(ax,'Parent',f,'Position',newPos); close(g);
                
                % fix details
                if i ~= llg; xlabel([]); end
                if i ~= 1; title([]); end
                leg = legend(ax,'show','Location',self.tlegpos);
                set(leg,'FontSize',self.legfs);
                
            end
        end
    end
   
    %% @Rosalinde 07/09/2015
        %% Plot layer to layer voltage for given coils
    methods
        function [ax,f] = plotVl2lind(self,ms,layers)
            % initialize
            self.initialize(ms);
            
            % create figure
            f = figure('Color','w','Position',self.pos,'Renderer',self.renderer);
            ax = axes('Parent',f); hold(ax,'on'); grid(ax,'on');
            if self.visible == false; set(f,'Visible','off'); end
            
            % iterate through coils
            for i = layers
                % extract Vl2lmax of i'th coil
                Vl2linddata = cell2mat( ms.Vl2lindstore(:,i) );
                
                % plot
                plot(ms.tstore,Vl2linddata,'Linewidth',self.linewidth,'Color',self.layC(i,:),'DisplayName',num2str(i));
                
                
%                 for j = 1:length(ms.mapVl2lstore)
%                     disp(j)
%                     if i<ms.mapVl2lstore(j+1)
%                         plot(ms.tstore,Vl2lmax,'Linewidth',self.linewidth,'Color',self.coilC(j,:),...
%                             'DisplayName',i);
%                     end
%                 end
            end
            
            % finish up
            axis tight;
            leg = legend('show','Location',self.tlegpos);
            set(leg,'FontSize',self.legfs);
            title(self.titlestr);
            xlabel('Time [s]')
            ylabel('l2l inductive Voltage [V]')
            
        end
    end
    
    %% Plot max layer to layer voltage in a subplot
    methods
        function [ax,f] = Vl2lind(self,ms)
            % initialize
            self.initialize(ms);
            
            % plot Vl2l in a subplot
            f = figure; set(f,'Color','w','Position',self.subpos,'Renderer',self.renderer);
            if self.visible == false; set(f,'Visible','off'); end
            
            % find out which coils are relevant
            llg = length(self.laygrp);
            for i = 1:llg
              
                % make plot
                hTemp = subplot(llg,1,i,'Parent',f); newPos = get(hTemp,'Position'); delete(hTemp);
                [ax,g] = self.plotVl2lind(ms,self.laygrp{i});
                set(ax,'Parent',f,'Position',newPos); close(g);
                
                % fix details
                if i ~= llg; xlabel([]); end
                if i ~= 1; title([]); end
                leg = legend(ax,'show','Location',self.tlegpos);
                set(leg,'FontSize',self.legfs);
                
            end
        end
    end
  
        %% Plot layer to layer voltage for given coils
    methods
        function [ax,f] = plotVl2lnz(self,ms,layers)
            % initialize
            self.initialize(ms);
            
            % create figure
            f = figure('Color','w','Position',self.pos,'Renderer',self.renderer);
            ax = axes('Parent',f); hold(ax,'on'); grid(ax,'on');
            if self.visible == false; set(f,'Visible','off'); end
            
            % iterate through coils
            for i = layers
                % extract Vl2lmax of i'th coil
                Vl2lnzdata = cell2mat( ms.Vl2lnzstore(:,i) );
                
                % plot
                plot(ms.tstore,Vl2lnzdata,'Linewidth',self.linewidth,'Color',self.layC(i,:),'DisplayName',num2str(i));
                
                
%                 for j = 1:length(ms.mapVl2lstore)
%                     disp(j)
%                     if i<ms.mapVl2lstore(j+1)
%                         plot(ms.tstore,Vl2lmax,'Linewidth',self.linewidth,'Color',self.coilC(j,:),...
%                             'DisplayName',i);
%                     end
%                 end
            end
            
            % finish up
            axis tight;
            leg = legend('show','Location',self.tlegpos);
            set(leg,'FontSize',self.legfs);
            title(self.titlestr);
            xlabel('Time [s]')
            ylabel('l2l resistive Voltage [V]')
            
        end
    end
    
    %% Plot max layer to layer voltage in a subplot
    methods
        function [ax,f] = Vl2lnz(self,ms)
            % initialize
            self.initialize(ms);
            
            % plot Vl2l in a subplot
            f = figure; set(f,'Color','w','Position',self.subpos,'Renderer',self.renderer);
            if self.visible == false; set(f,'Visible','off'); end
            
            % find out which coils are relevant
            llg = length(self.laygrp);
            for i = 1:llg
              
                % make plot
                hTemp = subplot(llg,1,i,'Parent',f); newPos = get(hTemp,'Position'); delete(hTemp);
                [ax,g] = self.plotVl2lnz(ms,self.laygrp{i});
                set(ax,'Parent',f,'Position',newPos); close(g);
                
                % fix details
                if i ~= llg; xlabel([]); end
                if i ~= 1; title([]); end
                leg = legend(ax,'show','Location',self.tlegpos);
                set(leg,'FontSize',self.legfs);
                
            end
        end
    end
  
    
            %% Plot layer to layer voltage for given coils
    methods
        function [ax,f] = plotVl2lcontrole(self,ms,layers)
            % initialize
            self.initialize(ms);
            
            Vind = cell2mat(ms.Vl2lindstore);
            Vnz = cell2mat(ms.Vl2lnzstore);            
            allV = Vind+Vnz;            
            
            % create figure
            f = figure('Color','w','Position',self.pos,'Renderer',self.renderer);
            ax = axes('Parent',f); hold(ax,'on'); grid(ax,'on');
            if self.visible == false; set(f,'Visible','off'); end
            
            % iterate through coils
            for i = layers
                % extract Vl2lmax of i'th layer
                Vl2lcontroledata = allV(:,i);                
                % plot
                plot(ms.tstore,Vl2lcontroledata,'Linewidth',self.linewidth,'Color',self.layC(i,:),'DisplayName',num2str(i));                
            end
            
            % finish up
            axis tight;
            leg = legend('show','Location',self.tlegpos);
            set(leg,'FontSize',self.legfs);
            title(self.titlestr);
            xlabel('Time [s]')
            ylabel('l2l resistive+inductive Voltage [V]')
            
        end
    end
    
    %% Plot max layer to layer voltage in a subplot
    methods
        function [ax,f] = Vl2lcontrole(self,ms)
            % initialize
            self.initialize(ms);
            
            % plot Vl2l in a subplot
            f = figure; set(f,'Color','w','Position',self.subpos,'Renderer',self.renderer);
            if self.visible == false; set(f,'Visible','off'); end
            
            % find out which coils are relevant
            llg = length(self.laygrp);
            for i = 1:llg
              
                % make plot
                hTemp = subplot(llg,1,i,'Parent',f); newPos = get(hTemp,'Position'); delete(hTemp);
                [ax,g] = self.plotVl2lcontrole(ms,self.laygrp{i});
                set(ax,'Parent',f,'Position',newPos); close(g);
                
                % fix details
                if i ~= llg; xlabel([]); end
                if i ~= 1; title([]); end
                leg = legend(ax,'show','Location',self.tlegpos);
                set(leg,'FontSize',self.legfs);
                
            end
        end
    end
  
    %% end @Rosalinde 07/09/2015
    
        %% Plot layer to layer voltage for given coils method 2
    methods
        function [ax,f] = plotVl2l_alt(self,ms,layers)
            % initialize
            self.initialize(ms);
            
            % create figure
            f = figure('Color','w','Position',self.pos,'Renderer',self.renderer);
            ax = axes('Parent',f); hold(ax,'on'); grid(ax,'on');
            if self.visible == false; set(f,'Visible','off'); end
            
            % iterate through coils
            for i = layers
                % extract Vl2lmax of i'th coil
                Vl2ldata = cell2mat( ms.Vl2laltstore(:,i) );
                
                % plot
                plot(ms.tstore,Vl2ldata,'Linewidth',self.linewidth,'Color',self.layC(i,:),'DisplayName',num2str(i));
                
                
%                 for j = 1:length(ms.mapVl2lstore)
%                     disp(j)
%                     if i<ms.mapVl2lstore(j+1)
%                         plot(ms.tstore,Vl2lmax,'Linewidth',self.linewidth,'Color',self.coilC(j,:),...
%                             'DisplayName',i);
%                     end
%                 end
            end
            
            % finish up
            axis tight;
            leg = legend('show','Location',self.tlegpos);
            set(leg,'FontSize',self.legfs);
            title(self.titlestr);
            xlabel('Time [s]')
            ylabel('l2l Voltage [V]')
            
        end
    end
    
    %% Plot max layer to layer voltage in a subplot method 2
    methods
        function [ax,f] = Vl2l_alt(self,ms)
            % initialize
            self.initialize(ms);
            
            % plot Vl2l in a subplot
            f = figure; set(f,'Color','w','Position',self.subpos,'Renderer',self.renderer);
            if self.visible == false; set(f,'Visible','off'); end
            
            % find out which coils are relevant
            llg = length(self.laygrp);
            for i = 1:llg
              
                % make plot
                hTemp = subplot(llg,1,i,'Parent',f); newPos = get(hTemp,'Position'); delete(hTemp);
                [ax,g] = self.plotVl2l_alt(ms,self.laygrp{i});
                set(ax,'Parent',f,'Position',newPos); close(g);
                
                % fix details
                if i ~= ms.totallayers; xlabel([]); end
                if i ~= 1; title([]); end
                leg = legend(ax,'show','Location',self.tlegpos);
                set(leg,'FontSize',self.legfs);
                
            end
        end
    end
    
        %% Plot voltage of layers for given coils
    methods
        function [ax,f] = plotVlayer(self,ms,layers)
            % initialize
            self.initialize(ms);
            
            % create figure
            f = figure('Color','w','Position',self.pos,'Renderer',self.renderer);
            ax = axes('Parent',f); hold(ax,'on'); grid(ax,'on');
            if self.visible == false; set(f,'Visible','off'); end
            
            % iterate through coils
            for i = layers
                % extract Vl2lmax of i'th coil
                Vlayerdata = cell2mat( ms.Vlayerstore(:,i) );
                
                % plot
                plot(ms.tstore,Vlayerdata,'Linewidth',self.linewidth,'Color',self.layC(i,:),'DisplayName',num2str(i));
                
                
%                 for j = 1:length(ms.mapVl2lstore)
%                     disp(j)
%                     if i<ms.mapVl2lstore(j+1)
%                         plot(ms.tstore,Vl2lmax,'Linewidth',self.linewidth,'Color',self.coilC(j,:),...
%                             'DisplayName',i);
%                     end
%                 end
            end
            
            % finish up
            axis tight;
            leg = legend('show','Location',self.tlegpos);
            set(leg,'FontSize',self.legfs);
            title(self.titlestr);
            xlabel('Time [s]')
            ylabel('l2l Voltage [V]')
            
        end
    end
    
    %% Plot voltage per layer in a subplot
    methods
        function [ax,f] = Vlayer(self,ms)
            % initialize
            self.initialize(ms);
            
            % plot Vl2l in a subplot
            f = figure; set(f,'Color','w','Position',self.subpos,'Renderer',self.renderer);
            if self.visible == false; set(f,'Visible','off'); end
            
            % find out which coils are relevant
            llg = length(self.laygrp);
            for i = 1:llg
              
                % make plot
                hTemp = subplot(llg,1,i,'Parent',f); newPos = get(hTemp,'Position'); delete(hTemp);
                [ax,g] = self.plotVlayer(ms,self.laygrp{i});
                set(ax,'Parent',f,'Position',newPos); close(g);
                
                % fix details
                if i ~= ms.totallayers; xlabel([]); end
                if i ~= 1; title([]); end
                leg = legend(ax,'show','Location',self.tlegpos);
                set(leg,'FontSize',self.legfs);
                
            end
        end
    end
    
end

