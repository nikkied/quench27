% noob!
fclose all

% % Create plotclass
% qpl = quenchplot;
% qpl.visible = false;
% qpl.coilgrp = {1:3,4:11,12:19,20:37};
% qpl.circgrp = {1,2:9,10:17,18:35};
% fp = fireprint(4); fp = fp(1:3,:);
% jp = jet(8);
% op = oceanprint(9); op = op(1:8,:);
% qpl.coilC = [fp; jet(8); flipud(jet(8)); jp(1:2,:); op; flipud(op)];
% qpl.circC = qpl.coilC(3:end,:);

% Create plotclass
qpl = quenchplot;
qpl.visible = false;
qpl.coilgrp = {1:3,4:11,20:29};
qpl.circgrp = {1,2:9,18:27};
fp = fireprint(4); fp = fp(1:3,:);
jp = jet(8);
op = oceanprint(9); op = op(1:8,:);
qpl.coilC = [fp; jet(8); flipud(jet(8)); jp(1:2,:); op; flipud(op)];
qpl.circC = qpl.coilC(3:end,:);

% create quenchreport class
report = quenchreport;
report.qpl = qpl;

% get ms
[files,path,~] = uigetfile('.mat','Select solved Create_System-classes','multiselect','on');

% make sure filename is in a cell (even if only one ms was picked)
if ~iscell(files)
    files = {files};
end

for i = 1:length(files)
    % load 
    load([path,filesep,files{i}],'ms');
    
    % create report
    report.one(ms);
    
end

% pause(200);
% system('shutdown /s');