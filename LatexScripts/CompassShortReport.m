% noob!
fclose all

% Create plotclass
qpl = quenchplot;
qpl.visible = false;
qpl.coilgrp = {1:3,4:11,12:19};
qpl.circgrp = {1,2:9,10:17};
fp = fireprint(4); fp = fp(1:3,:);
jp = jet(8);
op = oceanprint(9); op = op(1:8,:);
qpl.coilC = [fp; jet(8); flipud(jet(8)); jp(1:2,:)];
qpl.circC = qpl.coilC(3:end,:);

% create quenchreport class
report = quenchreport;
report.qpl = qpl;

% get ms
[files,path,~] = uigetfile('.mat','Select solved Create_System-classes','multiselect','on');

% make sure filename is in a cell (even if only one ms was picked, which is not the intent of a peak report...)
if ~iscell(files)
    files = {files};
end
lf = length(files);

% preallocate
mslist = cell(1,lf);

% go through files
for i = 1:lf
    % load 
    load([path,filesep,files{i}],'ms');
    
    % add to list
    mslist{i} = ms;
end

% reorder prompt
prompt = cell(1,lf);
for i = 1:lf
    prompt{i} = ['Where to place ' mslist{i}.shortscenario ' ?'];
end

defanswer = num2cell(1:lf);
for i = 1:length(defanswer)
    defanswer{i} = num2str(defanswer{i});
end
check = false;

while check == false
    answers = inputdlg(prompt,'Reorder quenches',1,defanswer);
    if ~isempty(answers)
        check = true;
        idx = zeros(1,lf);
        for i=1:lf
            try
                idx(i) = eval(answers{i});
            catch
                check = false;
            end
        end
    else
        error('User cancelled this bullshit');
    end
end

%reorder
mslist = mslist(idx);

% create report
report.short(mslist);

% pause(200);
% system('shutdown /s');