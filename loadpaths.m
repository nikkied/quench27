function loadpaths()
   
    % add general 
    addpath(genpath([cd '/../GeneralML']));
    
    % add current directory
    addpath(cd);
    
    % add directories in current directory
    addpath([cd,'/Systems']);
    addpath([cd,'/Conductors']);
    addpath([cd,'/Superconductors']);
    addpath([cd,'/Interface']);
    %addpath([cd,'/CoreFcn']);
    addpath(genpath([cd,'/CoreFcn']))
    
    % save path for next time
    savepath;
    
end

