function [ ax,ylim ] = yfit( ax,varargin )
%YFIT sets YLim to fit the data in ax exactly.
%Varargin is used to enter a margin in percents

% analyse input
margin = 0;
if ~isempty(varargin)
    margin = varargin{:};
end

% get data
A = get(ax,'Children');
B = get(A,'YData');
C = horzcat(B{1:end});

% define y limits
ymin = min(C);
ymax = max(C);

% implement margin if requested
if margin~=0
    dy = ymax - ymin;
    ymax = ymax + margin*dy/100;
    ymin = ymin - margin*dy/100;
end

% set xlimits
ylim = [ymin,ymax];
set(ax,'YLim',ylim);

end