function [x] = nrfzero( f,lb,ub,abstol )

%% Description
% zero search routine based on Newton-Raphson method

%% Algorithm
% calculate upper and lower bounds
xlb = lb;
xub = ub;

ylb = f(xlb);
yub = f(xub);

% check bounds
if any(ylb>0)
    error('f(lb)>0');
end
if any(yub<0)
    error('f(ub)<0');
end

% start iterations untill tol is reached
yest = Inf(1,length(lb));
%niter = 0;
while any(abs(yest)>abstol)
    % naive method
    xest = (xub+xlb)/2;
    
    % newton-raphson
    %dxdy = (xub-xlb)./(yub-ylb);
    %xest = xlb - dxdy .* ylb;
    
    % calculate Y
    yest = f(xest);
    if length(yest)~=length(lb); 
        error('Output of supplied function has different length than lb'); 
    end
    
    idx_lb = yest<=0;
    idx_ub = yest>=0;
    
    xlb(idx_lb) = xest(idx_lb);
    ylb(idx_lb) = yest(idx_lb);
    
    xub(idx_ub) = xest(idx_ub);
    yub(idx_ub) = yest(idx_ub);
        
    %niter = niter + 1;
end
x = xest;

end

