function custom_legend(h,str,facecolor,edgecolor,varargin)

if ~iscell(str)
    h(1) = plot(nan,nan,'s','markeredgecolor',edgecolor,'markerfacecolor',facecolor);
    legend(h,str,varargin{:})
else
    for n = 1:length(str)
        h(n) = plot(nan,nan,'s','markeredgecolor',edgecolor{n},'markerfacecolor',facecolor{n});
    end
end

legend(h,str,varargin{:})
 
end
