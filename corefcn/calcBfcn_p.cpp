#include <mex.h>
#include <math.h>

#ifdef _WIN32 || _WIN64
#include <ppl.h>
#include <windows.h> 
using namespace Concurrency;
#else
#include <omp.h>
#endif

/* --- MATLAB Gateway Function --- */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    if(nrhs!=5)mexErrMsgTxt("5 inputs required");
    if(nlhs!=2)mexErrMsgTxt("2 outputs required");

    /* input arrays */
    double *Br = (double*)mxGetPr(prhs[0]);
    double *Bz= (double*)mxGetPr(prhs[1]);
    double *Bri = (double*)mxGetPr(prhs[2]);
    double *Bzi =(double*)mxGetPr(prhs[3]);
    double I = (double)mxGetScalar(prhs[4]);
    
    int NB = (int)mxGetN(prhs[0]);
    int MB = (int)mxGetM(prhs[0]);
    int Ntot = NB * MB;    
    
    /* Create output array */
    plhs[0] = mxCreateDoubleMatrix(NB, MB, mxREAL);
    plhs[1] = mxCreateDoubleMatrix(NB, MB, mxREAL);
    
    double *Brout = (double*)mxGetPr(plhs[0]);
    double *Bzout = (double*)mxGetPr(plhs[1]);
    
    #ifdef _WIN32 || _WIN64
    /* lower priorty prevents system from freezing during calculation */
    SetPriorityClass(GetCurrentProcess(),BELOW_NORMAL_PRIORITY_CLASS);
    #endif
    
    /* walk over Xi */
    #ifdef _WIN32 || _WIN64
    parallel_for(0,Ntot,[&](int i){
    #else
    #pragma omp parallel for schedule(dynamic,20) // chunksize 1
    for(int i=0;i<Ntot;i++){
    #endif
        /* Actual sumation */
        Brout[i] = Br[i] + I * Bri[i];
        Bzout[i] = Bz[i] + I * Bzi[i];

    #ifdef _WIN32 || _WIN64
    });
    #else
    }
    #endif
   
    
    #ifdef _WIN32 || _WIN64
    /* set priority back to normal (who knows what will happen if we don't) */
    SetPriorityClass(GetCurrentProcess(),NORMAL_PRIORITY_CLASS);
    #endif
    
}