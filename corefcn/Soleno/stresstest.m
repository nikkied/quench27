R = linspace(-0.4,0.4,1000);
Z = linspace(-0.4,0.4,1000);

[R2,Z2] = meshgrid(R,Z);

for i=1:1000
    disp(['Running - ',num2str(i)]);
    tic;
    [Br,Bz] = soleno_calcB(R2,Z2,0.13,0.17,-0.1,0.1,500*1570,5);
    disp(['Time - ',num2str(toc)]);
    drawnow;
end