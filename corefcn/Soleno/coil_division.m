function [solcoils,fmmcoils] = coil_division(ms,verbose,fieldtype,usesoleno)

% report
jprintf(verbose,'--- Dividing Coils ---\n','>>');

% check soleno enabled
soleno_compatible = false(1,length(ms));
if usesoleno == true && strcmp(fieldtype,'B');
    for i=1:length(ms)
        if isfield(ms{i},'soleno')
            if ms{i}.soleno.compatible == true
                soleno_compatible(i) = true;
            end
        end
    end
end

% divide coils into separate calculation routines
solcoils = find(soleno_compatible);
fmmcoils = find(~soleno_compatible);

% report coils found
jprintf(verbose,['SOL - ',num2str(length(solcoils)),' coils\n']);
jprintf(verbose,['FMM - ',num2str(length(fmmcoils)),' coils\n\n'],'<<');

end

