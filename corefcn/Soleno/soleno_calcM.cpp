// Description:
// Soleno rewrite to MATLAB mex file by Jeroen van Nugteren
// Uses microsofts paralel paterns library to attain maximum performance on multicore systems
// Requires VC-redistributable 2010 to be installed

// Soleno was originally developed at Twente University by Gert Mulder, and upgraded in 1999 by Erik Krooshoop

// This function calculates the field at (R,Z) using:
// M = soleno(Rin,Rout,Zmin,Zmax,I,Nlayers)

// Rin,Rout,Zmin,Zmax,I and Nlayers define the coils and must be equal in size
// output M is the inductance matrix

// Libraries
#include <mex.h>
#include <cmath>

using namespace std;

// constants
const double Pi = 3.1415926535897931;
const double Eps = 1e-10; // calculation accuracy
const int NumberOfLayers = 4;

// coil class
class SolType{
public:
    double Ri;
    double Ro;
    double Zl;
    double Zh;
    double Nw; // number of windings
    double Iw;
    int NL; // number of layers
    int NN;
};

// absolute value function
//double abs(double in){
//    double out;
//    if(in<0)out=-in;
//    if(in>=0)out=in;
//    return out;
//}

// Function for calculating elliptical integrals
double Sol_CI(double R1, double R2, double ZZ){
    int Imax = 25;
    int Itel = 0;
    
    double Ci = 0;
    double Rm = R1 - R2;
    double Rp = R1 + R2;
    double Zkw = ZZ * ZZ;
    double Rkw = 4 * R1 * R2;
    double Kkw = (Rm * Rm + Zkw) / (Rp * Rp + Zkw);
    if(Kkw < Eps){
        Ci = 1 / (6 * atan(1.0f));
    }else{
        double Kc = sqrt(Rkw / (Rp * Rp + Zkw));
        double Alfa1 = 1;
        double Beta1 = sqrt(Kkw);
        double Q1 = abs(Rm / Rp);
        bool R1eqR2 = (Q1 <= Eps);
        double A1 = 1 / (3 * Kc);
        double B1 = -Kkw * A1;
        double C1 = Kc * Zkw / Rkw;
        double D1 = 0;
        A1 = A1 - C1;
        if(R1eqR2){
            A1 = A1 + C1;
            B1 = -B1 - B1;
            C1 = 0;
        }
        while(abs(Alfa1 - Beta1) > Alfa1 * Eps && Itel < Imax){
            Itel = Itel + 1;
            double AlfBe1 = Alfa1 * Beta1;
            if(R1eqR2==false){
                double C2 = C1 + D1 / Q1;
                double D2 = AlfBe1 * C1 / Q1 + D1;
                D2 = D2 + D2;
                double Q2 = Q1 + AlfBe1 / Q1;
                C1 = C2;
                D1 = D2;
                Q1 = Q2;
            }
            double Alfa2 = Alfa1 + Beta1;
            double Beta2 = 2 * sqrt(AlfBe1);
            double A2 = B1 / Alfa1 + A1;
            double B2 = B1 + Beta1 * A1;
            B2 = B2 + B2;
            Alfa1 = Alfa2;
            Beta1 = Beta2;
            A1 = A2;
            B1 = B2;
        }
        Ci = (A1 + B1 / Alfa1) / (Alfa1 + Alfa1) + (C1 + D1 / Alfa1) / (Alfa1 + Q1);
        if(Itel >= Imax)mexErrMsgTxt("No Convergence");
    }
    return Ci;
}

// Main mutual inductance function returns mutual inductance between coil I and J
double CalcMutSub(SolType *SD, int I, int J, int NlI, int NlJ){
    double S1 = abs(SD[I].Zh - SD[J].Zl);
    double S2 = abs(SD[I].Zl - SD[J].Zl);
    double S3 = abs(SD[I].Zl - SD[J].Zh);
    double S4 = abs(SD[I].Zh - SD[J].Zh);
    bool S3neS1 = (S3 != S1);
    bool S4neS2 = (S4 != S2);
    bool RdivNE = (SD[I].Ri != SD[J].Ri) || (SD[I].Ro != SD[J].Ro) || (NlI != NlJ);
    double Factor = 0.0000008 * Pi * Pi * SD[I].Nw / NlI / (SD[I].Zh - SD[I].Zl) * SD[J].Nw / NlJ / (SD[J].Zh - SD[J].Zl);
    double DrI = (SD[I].Ro - SD[I].Ri) / NlI;
    double DrJ = (SD[J].Ro - SD[J].Ri) / NlJ;
    double Som = 0;
    
    for(int K=1;K<=NlI;K++){
        double R1 = SD[I].Ri + (K - 0.5) * DrI;
        for(int L=1;L<=NlJ;L++){
            double R2 = SD[J].Ri + (L - 0.5) * DrJ;
            if(RdivNE || (L <= K)){
                double C1 = Sol_CI(R1, R2, S1);
                double C2 = Sol_CI(R1, R2, S2);
                double C3 = C1;
                double C4 = C2;
                if(S3neS1)C3 = Sol_CI(R1, R2, S3);
                if(S4neS2)C4 = Sol_CI(R1, R2, S4);
                double HH = R1 * R2;
                HH = sqrt(HH * HH * HH);
                if((RdivNE==false) && (L < K))HH = 2 * HH;
                Som = Som + (C1 - C2 + C3 - C4) * HH;
            }
        }
    }
    return Som * Factor;
}



// Mutual inductance routine
double CalcMut(SolType *SD, double *Mut, int Ns, int N1, bool CalcE){
    double Msom = 0;
        
    int N2;
    if(N1 == Ns){
        N2 = 0;
    }else{
        N2 = N1;
    }
    
    for(int I=0; I<Ns; I++){
        for(int J=I; J<Ns; J++){
            double Hulp;
            if(J < N2 || I >= N1){
                Hulp = 0;
            }else{
                if(SD[I].NN > 0 || SD[J].NN > 0 || CalcE){
                    int NlI = SD[I].NL;
                    int NlJ = SD[J].NL;
                    double Hulp2 = CalcMutSub(SD,I, J, NlI, NlJ); //opgegeven aantal lagen
                    Hulp = CalcMutSub(SD,I, J, NlI * 2, NlJ * 2); //dubbel aantal lagen
                    Hulp = Hulp + (Hulp - Hulp2) / 3; //extrapolatie naar 1/NLI/NLJ=0
                    Mut[I+J*Ns] = Hulp;
                    Mut[J+I*Ns] = Hulp;
                }
                Hulp = Mut[I+J*Ns];
            }
            if(N1==Ns && I != J)Hulp = 2 * Hulp;
            if(CalcE)Hulp = Hulp * SD[I].Iw * SD[J].Iw;
            Msom = Msom + Hulp;
        }
    }
    
    if(N1 == Ns){
        for(int I=0; I<Ns; I++){
            SD[I].NN = 0;
        }
    }
    
    return Msom;
}

// Matlab GateWay Function
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    /* check number of inputs and outputs */
    if(nrhs!=6)mexErrMsgTxt("6 inputs required");
    if(nlhs!=1)mexErrMsgTxt("1 outputs required");
    
    /* check input */
    int RinN = (int)mxGetN(prhs[0]);
    int RinM = (int)mxGetM(prhs[0]);
    int RoutN = (int)mxGetN(prhs[1]);
    int RoutM = (int)mxGetM(prhs[1]);
    int ZlowN = (int)mxGetN(prhs[2]);
    int ZlowM = (int)mxGetM(prhs[2]);
    int ZhighN = (int)mxGetN(prhs[3]);
    int ZhighM = (int)mxGetM(prhs[3]);
    int nturnN = (int)mxGetN(prhs[4]);
    int nturnM = (int)mxGetM(prhs[4]);
    int nlayersN = (int)mxGetN(prhs[5]);
    int nlayersM = (int)mxGetM(prhs[5]);
    
    if(RinN!=RoutN || RinN!=ZlowN || RinN!=ZhighN || RinN!=nturnN || RinN!=nlayersN)
        mexErrMsgTxt("Rin, Rout, Zlow, Zhigh and I must be equal in size");
    if(RinM!=RoutM || RinM!=ZlowM || RinM!=ZhighM || RinM!=nturnM || RinM!=nlayersM)
        mexErrMsgTxt("Rin, Rout, Zlow, Zhigh and I must be equal in size");
    
    /* calculate number of coils */
    int Ns = RinN*RinM;
    
    /* get input */
    double *Rin = (double*)mxGetPr(prhs[0]);
    double *Rout = (double*)mxGetPr(prhs[1]);
    double *Zlow = (double*)mxGetPr(prhs[2]);
    double *Zhigh = (double*)mxGetPr(prhs[3]);
    double *Nturns = (double*)mxGetPr(prhs[4]);
    double *Nlayers = (double*)mxGetPr(prhs[5]);
    
    /* allocate memory for coil data */
    SolType *SD;
    SD = (SolType*)mxMalloc(sizeof(SolType)*Ns);
    
    /* fill SD with required coil data */
    for(int i=0; i<Ns; i++){
        SD[i].Ri = Rin[i];
        SD[i].Ro = Rout[i];
        SD[i].Zl = Zlow[i];
        SD[i].Zh = Zhigh[i];
        SD[i].Nw = Nturns[i];
        SD[i].NL = Nlayers[i];
        SD[i].NN = 1; // deze coil is nog niet berekend
    }
    
    /* allocate memory for output matrix and get pointer */
    plhs[0] = mxCreateDoubleMatrix(Ns, Ns, mxREAL);
    double *M = (double*)mxGetPr(plhs[0]);
    
    /* calculate */
    double Msom = CalcMut(SD,M,Ns,Ns,false);
    
    /* free memory */
    mxFree(SD);
}









