R = linspace(-0.4,0.4,1000);
Z = linspace(-0.4,0.4,1000);

[R2,Z2] = meshgrid(R,Z);


[Br,Bz] = soleno_calcB(R2,Z2,0.13,0.17,-0.1,0.1,500*1570,5);


contour(R2,Z2,sqrt(Br.^2+Bz.^2),30,'LineWidth',2);