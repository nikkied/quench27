function [field_sol] = calculate_field_soleno(Xt,ms,solcoils,verbose,waitbar,fieldtype)

% alocate
field_sol.A = 0; field_sol.B = 0;

% --- soleno ---
if ~isempty(solcoils)
    % soleno header
    jprintf(verbose,'Running SOLENO (utwente)\n','>>');
    
    % check field type again
    if ~strcmp(fieldtype,'B'); error('Soleno can only be used for B field'); end
    
    % waitbar
    if waitbar==true; multiWaitbar( 'Soleno',0,'Color',settings('MultiWaitbarSolenoColor')); end
    
    % allocate
    field_sol.B = zeros(3,size(Xt,2));
    
    % report
    jprintf(verbose,'--- Calculating ---\n','>>');
    
    % calculate field using soleno
    for i=1:length(solcoils)
        % report
        jprintf(verbose,['Coil - ',num2str(i),' / ',num2str(length(solcoils)),'\n']);
        
        % get coil id
        coilid = solcoils(i);
        
        % target point cylindrical coords
        [Th,R,Z] = cart2pol(Xt(1,:)-ms{coilid}.soleno.xoffset,Xt(2,:)-ms{coilid}.soleno.yoffset,Xt(3,:)-ms{coilid}.soleno.zoffset);
        
        % calculate B in polar coordinates
        [Br,Bz] = soleno_calcB( ...
            R,Z,ms{coilid}.soleno.Rin, ...
            ms{coilid}.soleno.Rout, ...
            ms{coilid}.soleno.Zmin, ...
            ms{coilid}.soleno.Zmax, ...
            ms{coilid}.soleno.Iall,5);
        
        % to cathesian coordinates
        Bx = cos(Th).*Br; By = sin(Th).*Br; %Bz=Bz
        
        % create output
        field_sol.B = field_sol.B + [Bx;By;Bz];
        
        % waitbar
        if waitbar==true; multiWaitbar( 'Soleno','Increment',1/length(solcoils)); end
    end
    
    % delete waitbar
    if waitbar==true; multiWaitbar( 'Soleno', 'Close' ); end
    
    % report
    jprintf(verbose,'\n','<<');
    jprintf(verbose,[],'<<');
end



end

