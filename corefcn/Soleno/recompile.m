
% report
disp('recompiling soleno');

% recompile
switch computer('arch');
    case 'win32'
        mex soleno_calcB.cpp -O -win32
        mex soleno_calcM.cpp -O -win32
    case 'win64'
        mex soleno_calcB.cpp -O -win64
        mex soleno_calcM.cpp -O -win64
    case 'maci64'
        %mex CXXFLAGS="\$CXXFLAGS -fopenmp" LDFLAGS="\$LDFLAGS -fopenmp" soleno_calcB.cpp -maci64 -O
        %mex CXXFLAGS="\$CXXFLAGS -fopenmp" LDFLAGS="\$LDFLAGS -fopenmp" soleno_calcM.cpp -maci64 -O
        mex soleno_calcB.cpp -maci64 -O
        mex soleno_calcM.cpp -maci64 -O
    case 'glnxa64'
        mex CXXFLAGS="\$CXXFLAGS -fopenmp" LDFLAGS="\$LDFLAGS -fopenmp" soleno_calcB.cpp -O
        mex CXXFLAGS="\$CXXFLAGS -fopenmp" LDFLAGS="\$LDFLAGS -fopenmp" soleno_calcM.cpp -O
    otherwise
        error('architecture not recognized.');
end