function [matrix] = line2mat(line,nrows)
% reshape
matrix = reshape(line,[],nrows)';

% create alternating false-true vector
alt = [false;true];
alt = repmat(alt,ceil(nrows/2),1);
alt = alt(1:nrows);

% flip even rows
matrix(alt,:) = fliplr(matrix(alt,:));

end