function [val] = matprops(mat,prop,T,varargin)

if ~isempty(varargin)
    B = varargin{1};
else
    B = [];
end

%% Material properties
% Lorentz number for wiedeman franz law
LorenzNumber = 2.44e-8; %[W Ohm / K^2]

% aluminum
Aluminum3003F_ThermalConductivity = @(T)10.^(0.63736 - 1.1437 * log10(T) + 7.4624 * log10(T).^2 - 12.6905 * log10(T).^3 + 11.9165 * log10(T).^4 - 6.18721 * log10(T).^5 + 1.63939 * log10(T).^6 - 0.172667 * log10(T).^7 + 0 * log10(T).^8);
Aluminum3003F_SpecificHeat = @(T)10.^(46.6467 - 314.292 * log10(T) + 866.662 * log10(T).^2 - 1298.3 * log10(T).^3 + 1162.27 * log10(T).^4 - 637.795 * log10(T).^5 + 210.351 * log10(T).^6 - 38.3094 * log10(T).^7 + 2.96344 * log10(T).^8);
Aluminum5083O_ThermalConductivity = @(T)10.^(-0.90933 + 5.751 * log10(T) - 11.112 * log10(T).^2 + 13.612 * log10(T).^3 - 9.3977 * log10(T).^4 + 3.6873 * log10(T).^5 - 0.77295 * log10(T).^6 + 0.067336 * log10(T).^7 + 0 * log10(T).^8);
Aluminum5083O_SpecificHeat = @(T)10.^(46.6467 - 314.292 * log10(T) + 866.662 * log10(T).^2 - 1298.3 * log10(T).^3 + 1162.27 * log10(T).^4 - 637.795 * log10(T).^5 + 210.351 * log10(T).^6 - 38.3094 * log10(T).^7 + 2.96344 * log10(T).^8);
Aluminum6061T6_ThermalConductivity = @(T)10.^(0.07981 + 1.0957 * log10(T) - 0.07277 * log10(T).^2 + 0.08084 * log10(T).^3 + 0.02803 * log10(T).^4 - 0.09464 * log10(T).^5 + 0.04179 * log10(T).^6 - 0.00571 * log10(T).^7 + 0 * log10(T).^8);
Aluminum6061T6_SpecificHeat = @(T)10.^(46.6467 - 314.292 * log10(T) + 866.662 * log10(T).^2 - 1298.3 * log10(T).^3 + 1162.27 * log10(T).^4 - 637.795 * log10(T).^5 + 210.351 * log10(T).^6 - 38.3094 * log10(T).^7 + 2.96344 * log10(T).^8);
Aluminum_Density = 2800; %[kg/m3]

% oxygen free copper
OFHCCopperRRR50_ThermalConductivity  = @(T)10.^((1.8743 - 0.6018*T.^0.5 + 0.26426*T - 0.051276*T.^1.5 + 0.003723*T.^2)./(1 - 0.41538*T.^0.5 + 0.13294*T - 0.0219*T.^1.5 + 0.0014871*T.^2));
OFHCCopperRRR100_ThermalConductivity = @(T)10.^((2.2154 - 0.88068*T.^0.5 + 0.29505*T - 0.04831*T.^1.5 + 0.003207*T.^2)./(1 - 0.47461*T.^0.5 + 0.13871*T - 0.02043*T.^1.5 + 0.001281*T.^2));
OFHCCopperRRR150_ThermalConductivity = @(T)10.^((2.3797 - 0.98615*T.^0.5 + 0.30475*T - 0.046897*T.^1.5 + 0.0029988*T.^2)./(1 - 0.4918*T.^0.5 + 0.13942*T - 0.019713*T.^1.5 + 0.0011969*T.^2));
OFHCCopperRRR300_ThermalConductivity = @(T)10.^((1.357 + 2.669*T.^0.5 - 0.6683*T + 0.05773*T.^1.5 + 0*T.^2)./(1 + 0.3981*T.^0.5 - 0.1346*T + 0.01342*T.^1.5 + 0.0002147*T.^2));
OFHCCopperRRR500_ThermalConductivity = @(T)10.^((2.8075 - 1.2777*T.^0.5 + 0.36444*T - 0.051727*T.^1.5 + 0.0030964*T.^2)./(1 - 0.54074*T.^0.5 + 0.15362*T - 0.02105*T.^1.5 + 0.0012226*T.^2));
OFHCCopper_SpecificHeat = @(T)10.^(-1.91844 - 0.15973 * log10(T) + 8.61013 * log10(T).^2 - 18.996 * log10(T).^3 + 21.9661 * log10(T).^4 - 12.7328 * log10(T).^5 + 3.54322 * log10(T).^6 - 0.3797 * log10(T).^7 + 0 * log10(T).^8);
OFHCCopper_Density = 8940; %[kg/m3]

% Fyberglass Epoxy G10
FyberglassEpoxyG10normal_ThermalConductivity = @(T)10.^(-4.1236 + 13.788 * log10(T) - 26.068 * log10(T).^2 + 26.272 * log10(T).^3 - 14.663 * log10(T).^4 + 4.4954 * log10(T).^5 - 0.6905 * log10(T).^6 + 0.0397 * log10(T).^7 + 0 * log10(T).^8);
FyberglassEpoxyG10warp_ThermalConductivity = @(T)10.^(polyval([-0.11701,1.48806,-7.95635,23.1778,-39.8754,41.1625,-24.8998,8.80228,-2.64827],log10(T)));
FyberglassEpoxyG10_SpecificHeat = @(T)10.^(-2.4083 + 7.6006 * log10(T) - 8.2982 * log10(T).^2 + 7.3301 * log10(T).^3 - 4.2386 * log10(T).^4 + 1.4294 * log10(T).^5 -0.24396 * log10(T).^6 + 0.015236 * log10(T).^7 + 0 * log10(T).^8);
FyberglassEpoxyG10_Density = 1850; %[kg/m3]

% niobium titanium
NbTi_Tinterp = [4:5:299,300];
NbTi_SpecificHeat = [0.5230,3.0000,8.6800,18.5000,33.7000,53.5000,78.5000,105.0000,126.0000,146.0000,170.0000,195.0000,217.0000,236.0000,253.0000,267.0000,283.0000,298.0000,305.0000,307.0000,311.0000,316.0000,322.0000,328.0000,334.0000,341.0000,348.0000,355.0000,362.0000,369.0000,373.0000,377.0000,381.0000,384.0000,387.0000,390.0000,393.0000,395.0000,397.0000,400.0000,402.0000,405.0000,407.0000,409.0000,411.0000,413.0000,415.0000,417.0000,418.0000,420.0000,421.0000,422.0000,423.0000,423.0000,424.0000,425.0000,425.0000,425.0000,426.0000,426.0000,426.0000];
NbTi_ElectricalResistivity = [0,497,698,628,605,607,608,610,612,614,616,618,619,621,623,625,626,628,630,632,634,635,637,639,641,643,645,646,648,650,651,653,655,656,658,660,662,663,665,667,668,670,672,674,676,677,679,681,683,685,686,688,690,692,693,695,697,699,701,703,703]*1e-9;
NbTi_Density = 6550; %[kg/m^3];

% tri-niobium tin
Nb3Sn_Tinterp = [4:5:299,300];
Nb3Sn_SpecificHeat = [0.1720,2.0700,5.5100,10.8000,18.9000,29.4000,41.7000,54.8000,67.6000,79.8000,91.6000,103.0000,113.0000,123.0000,132.0000,140.0000,147.0000,153.0000,159.0000,164.0000,169.0000,174.0000,179.0000,183.0000,187.0000,191.0000,195.0000,199.0000,202.0000,205.0000,208.0000,211.0000,214.0000,216.0000,218.0000,220.0000,222.0000,224.0000,225.0000,227.0000,228.0000,229.0000,230.0000,231.0000,232.0000,233.0000,234.0000,235.0000,235.0000,236.0000,237.0000,237.0000,238.0000,238.0000,239.0000,240.0000,241.0000,241.0000,242.0000,243.0000,243.0000];
Nb3Sn_ElectricalResistivity = [0,0,104,234,278,269,267,269,272,275,278,281,283,286,289,291,294,297,300,303,306,310,313,316,319,322,325,328,331,334,337,340,343,345,348,350,353,355,357,360,362,364,366,367,369,371,373,374,376,378,379,381,382,384,385,386,388,388,389,390,390]*1e-9;
Nb3Sn_Density = 8950; %[kg/m^3]

% Enamel
Enamel_ThermalConductivity = @(T)0.04919*T.^0.4147-0.07262;


%% calculation of material properties

switch mat
    case 'None'
        switch prop
            case 'TC'; val = 0;
            case 'ER'; val = inf;
            case 'SH'; val = 0;
            case 'Ro'; val = 0;
            otherwise; error('Property not available or recognized');
        end
        
    case 'Aluminum 3003-F'
        switch prop;
            case 'TC'; val = Aluminum3003F_ThermalConductivity(T);
            case 'ER'; val = (LorenzNumber * T)./Aluminum3003F_ThermalConductivity(T); %Wiederman franz Law
            case 'SH'; val = Aluminum3003F_SpecificHeat(T);
            case 'Ro'; val = Aluminum_Density;
            otherwise; error('Property not available or recognized');
        end
        
    case 'Aluminum 5083-O'
        switch prop;
            case 'TC'; val = Aluminum5083O_ThermalConductivity(T);
            case 'ER'; val = (LorenzNumber * T)./Aluminum5083O_ThermalConductivity(T); %Wiederman franz Law
            case 'SH'; val = Aluminum5083O_SpecificHeat(T);
            case 'Ro'; val = Aluminum_Density;
            otherwise; error('Property not available or recognized');
        end
        
    case 'Aluminum 6061-T6'
        switch prop;
            case 'TC'; val = Aluminum6061T6_ThermalConductivity(T);
            case 'ER'; val = (LorenzNumber * T)./Aluminum6061T6_ThermalConductivity(T); %Wiederman franz Law
            case 'SH'; val = Aluminum6061T6_SpecificHeat(T);
            case 'Ro'; val = Aluminum_Density;
            otherwise; error('Property not available or recognized');
        end
        
    case 'OFHC Copper RRR50'
        switch prop;
            case 'TC'; val = T*LorenzNumber ./ rho_cu(50,B,T); % [W/m.K] multiply with length conductor for magnetoresistant thermal conductivity
            case 'ER'; val = rho_cu(50,B,T );
            case 'SH'; val = OFHCCopper_SpecificHeat(T);
            case 'Ro'; val = OFHCCopper_Density;
            otherwise; error('Property not available or recognized');
        end
        
    case 'OFHC Copper RRR100'
        switch prop;
            case 'TC'; val = T*LorenzNumber ./ rho_cu(100,B,T); % [W/m.K] multiply with length conductor for magnetoresistant thermal conductivity
            case 'ER'; val = rho_cu(100,B,T );
            case 'SH'; val = OFHCCopper_SpecificHeat(T);
            case 'Ro'; val = OFHCCopper_Density;
            otherwise; error('Property not available or recognized');
        end
        
    case 'OFHC Copper RRR150'
        switch prop;
            case 'TC'; val = T*LorenzNumber ./ rho_cu(150,B,T); % [W/m.K] multiply with length conductor for magnetoresistant thermal conductivity
            case 'ER'; val = rho_cu(150,B,T );
            case 'SH'; val = OFHCCopper_SpecificHeat(T);
            case 'Ro'; val = OFHCCopper_Density;
            otherwise; error('Property not available or recognized');
        end
        
    case 'OFHC Copper RRR300'
        switch prop;
            case 'TC'; val = T*LorenzNumber ./ rho_cu(300,B,T); % [W/m.K] multiply with length conductor for magnetoresistant thermal conductivity
            case 'ER'; val = rho_cu(300,B,T );
            case 'SH'; val = OFHCCopper_SpecificHeat(T);
            case 'Ro'; val = OFHCCopper_Density;
            otherwise; error('Property not available or recognized');
        end
        
    case 'OFHC Copper RRR500'
        switch prop;
            case 'TC'; val = T*LorenzNumber ./ rho_cu(500,B,T); % [W/m.K] multiply with length conductor for magnetoresistant thermal conductivity
            case 'ER'; val = rho_cu(500,B,T );
            case 'SH'; val = OFHCCopper_SpecificHeat(T);
            case 'Ro'; val = OFHCCopper_Density;
            otherwise; error('Property not available or recognized');
        end
        
    case 'Enamel'
        switch prop
            case 'TC'; val = Enamel_ThermalConductivity(T);
                
                % cheat hacks
            case 'ER'; val = inf;
            case 'SH'; val = 0;
            case 'Ro'; val = 0;
                
            otherwise; error('Property not available or recognized');
        end
        
    case 'Fiberglass Epoxy G-10 CR normal'
        switch prop
            case 'TC'; val = FyberglassEpoxyG10normal_ThermalConductivity(T);
            case 'ER'; val = linspace(1e9,1e9,length(T));
            case 'SH'; val = FyberglassEpoxyG10_SpecificHeat(T);
            case 'Ro'; val = FyberglassEpoxyG10_Density;
            otherwise; error('Property not available or recognized');
        end
        
    case 'Fiberglass Epoxy G-10 CR warp'
        switch prop
            case 'TC'; val = FyberglassEpoxyG10warp_ThermalConductivity(T);
            case 'ER'; val = linspace(1e9,1e9,length(T));
            case 'SH'; val = FyberglassEpoxyG10_SpecificHeat(T);
            case 'Ro'; val = FyberglassEpoxyG10_Density;
            otherwise; error('Property not available or recognized');
        end
        
    case 'Niobium Titanium'
        switch prop
            case 'TC'; val = 0;
            case 'ER'; val = interp1(NbTi_Tinterp,NbTi_ElectricalResistivity,T,'linear','extrap');
            case 'SH'; val = interp1(NbTi_Tinterp,NbTi_SpecificHeat,T,'linear','extrap');
            case 'Ro'; val = NbTi_Density;
            case 'Tc'; val = 9.2;
            otherwise; error('Property not available or recognized');
        end
        
    case 'tri-Niobium Tin'
        switch prop
            case 'TC'; val = 0;
            case 'ER'; val = interp1(Nb3Sn_Tinterp,Nb3Sn_ElectricalResistivity,T,'linear','extrap');
            case 'SH'; val = interp1(Nb3Sn_Tinterp,Nb3Sn_SpecificHeat,T,'linear','extrap');
            case 'Ro'; val = Nb3Sn_Density;
            case 'Tc'; val = 18.2;
            otherwise; error('Property not available or recognized');
        end
        
    otherwise; error('Material not recognized');
end
end

 %% Electrical resistivity Copper
% Source:
% CUDI Manual (valid in RRR range 5-400 T range 1-500 K)
function [ rho ] = rho_cu( RRR,B,T )
% Constants
C0 = 1.7;
C1 = 2.33e9;
C2 = 9.57e5;
C3 = 163;

% Equation [Ohm/m]
rho = (C0/RRR + 1./(C1./T.^5 + C2./T.^3 + C3./T))*1e-8 + B*(0.37 + 0.0005*RRR)*1e-10;
 
end