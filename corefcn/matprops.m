function [val] = matprops(mat,prop,T,varargin)

% analyse input
B = 0;
if numel(varargin)==1
    B = varargin{1};
end

if length(mat)>15 && strcmp(mat(1:15),'OFHC Copper RRR')
    RRR = str2num(mat(15+1:end));
    mat = 'OFHC Copper RRR';
end

%% Material properties
% Lorentz number for wiedeman franz law
LorenzNumber = 2.44e-8; %[W Ohm / K^2]

% @Rosalinde
% Effective thermal conductivity of cable: Jacket: Al6061, Core: 84.4%
% AlRRR500; 7.8% NbTi; 7,8% CuRRR150. Cable: 89mm*89mm, radius core: 32mm
Cable_ThermalConductivity = @(T)10.^(-0.6632 * log10(T).^6 + 6.2672 * log10(T).^5 - 23.433 * log10(T).^4 + 43.933 * log10(T).^3 - 43.396 * log10(T).^2 + 22.312*log10(T) - 3.51);

% aluminum
Aluminum3003F_ThermalConductivity = @(T)10.^(0.63736 - 1.1437 * log10(T) + 7.4624 * log10(T).^2 - 12.6905 * log10(T).^3 + 11.9165 * log10(T).^4 - 6.18721 * log10(T).^5 + 1.63939 * log10(T).^6 - 0.172667 * log10(T).^7 + 0 * log10(T).^8);
Aluminum3003F_SpecificHeat = @(T)10.^(46.6467 - 314.292 * log10(T) + 866.662 * log10(T).^2 - 1298.3 * log10(T).^3 + 1162.27 * log10(T).^4 - 637.795 * log10(T).^5 + 210.351 * log10(T).^6 - 38.3094 * log10(T).^7 + 2.96344 * log10(T).^8);
Aluminum5083O_ThermalConductivity = @(T)10.^(-0.90933 + 5.751 * log10(T) - 11.112 * log10(T).^2 + 13.612 * log10(T).^3 - 9.3977 * log10(T).^4 + 3.6873 * log10(T).^5 - 0.77295 * log10(T).^6 + 0.067336 * log10(T).^7 + 0 * log10(T).^8);
Aluminum5083O_SpecificHeat = @(T)10.^(46.6467 - 314.292 * log10(T) + 866.662 * log10(T).^2 - 1298.3 * log10(T).^3 + 1162.27 * log10(T).^4 - 637.795 * log10(T).^5 + 210.351 * log10(T).^6 - 38.3094 * log10(T).^7 + 2.96344 * log10(T).^8);
Aluminum6061T6_ThermalConductivity = @(T)10.^(0.07981 + 1.0957 * log10(T) - 0.07277 * log10(T).^2 + 0.08084 * log10(T).^3 + 0.02803 * log10(T).^4 - 0.09464 * log10(T).^5 + 0.04179 * log10(T).^6 - 0.00571 * log10(T).^7 + 0 * log10(T).^8);
Aluminum6061T6_SpecificHeat = @(T)10.^(46.6467 - 314.292 * log10(T) + 866.662 * log10(T).^2 - 1298.3 * log10(T).^3 + 1162.27 * log10(T).^4 - 637.795 * log10(T).^5 + 210.351 * log10(T).^6 - 38.3094 * log10(T).^7 + 2.96344 * log10(T).^8);
Aluminum_Density = 2800; %[kg/m3]

% pure aluminun RRR1500
Pure_Al_Tinterp = [4.5:2:100,100:10:300];
Pure_Al_ThermalConductivity = [0.284,0.386,0.452,0.482,0.474,0.441,0.393,0.335,0.285,0.265,0.248,0.235,0.224,0.214,0.205,0.197,0.186,0.181,0.172,0.158,0.145,0.131,0.117,0.105,0.0961,0.0878,0.0805,0.0741,0.0686,0.0638,0.0596,0.0561,0.0530,0.0502,0.0477,0.0454,0.0434,0.0416,0.0400,0.0385,0.0371,0.0359,0.0348,0.0338,0.0329,0.0320,0.0312,0.0306,0.0301,0.0274,0.0256,0.0247,0.0244,0.0245,0.0242,0.0239,0.0238,0.0238,0.0238,0.0238,0.0237,0.0237,0.0237,0.0237,0.0236,0.0237,0.0237,0.0237,0.0237]*10000; % W/m/K
Pure_Al_Density = 2698; % [kg/m^3]
Pure_Al_SpecificHeat = [0.333,0.590,0.981,1.56,2.37,3.5,5.03,7.04,9.6,12.8,16.7,21.4,26.8,33.3,41,49.6,59,69.3,80.4,92.3,105,118,132,145,160,174,188,203,218,232,247,262,276,291,305,319,333,347,360,374,387,400,413,425,437,449,461,473,481,533,579,618,653,684,713,738,760,780,797,812,826,838,849,859,869,877,886,894,902];

% pure aluminum RRR1000
Pure_Al_RRR1000_Tinterp = [3:5:300,300];
Pure_Al_RRR1000_ThermalConductivity = [1280,3480,4050,3370,2750,2420,2130,1860,1530,1190,930,751,624,535,469,419,380,350,326,307,291,278,267,259,253,248,246,244,244,245,244,242,241,239,239,238,238,238,238,238,238,238,237,237,237,237,237,237,236,236,236,236,236,236,236,237,237,237,237,237,237];
Pure_Al_RRR1000_Density = 2698;
Pure_Al_RRR1000_SpecificHeat = [0.169,0.867,2.62,6.49,13.7,24.5,43.1,66.7,95.4,128,163,199,236,273,308,343,377,409,440,470,497,523,547,570,591,611,629,647,663,678,693,707,720,733,745,756,766,776,785,794,802,809,817,823,830,836,841,847,852,857,862,867,871,876,880,884,888,892,896,900,902];

% pure aluminun RRR500
Pure_Al_RRR500_Tinterp = [3:5:300,300];
Pure_Al_RRR500_ThermalConductivity = [815,2380,3150,3140,2890,2580,2220,1850,1480,1150,906,736,615,528,456,416,378,348,324,305,290,277,266,258,252,248,245,244,244,244,243,241,240,239,238,238,237,237,238,238,238,237,237,237,237,236,236,236,236,236,236,236,236,236,236,236,236,236,237,237,237];
Pure_Al_RRR500_Density = 2698; % [kg/m^3]
Pure_Al_RRR500_SpecificHeat = [0.169,0.867,2.26,6.49,13.7,25.4,43.1,66.7,95.4,128,163,199,236,273,308,343,377,409,440,470,497,523,547,570,591,611,629,647,663,678,693,707,720,733,745,756,766,776,785,794,802,809,817,823,830,836,841,847,852,857,862,867,871,876,880,884,888,892,896,900,902];

% pure aluminum RRR400
Pure_Al_RRR400_Tinterp = [2:5:300];
Pure_Al_RRR400_Density = 2698; % [kg/m^3]
Pure_AlRRR400_ThermalConductivity = [422,1860,2800,3030,2910,2650,2280,1900,1520,1190,936,758,631,540,474,423,383,352,328,308,292,279,268,259,253,248,245,244,243,244,243,242,240,239,238,238,237,237,237,237,238,237,237,237,236,236,236,236,236,236,236,236,236,236,236,236,236,236,236,237];
Pure_AlRRR400_SpecificHeat = [0.108,0.673,2.14,5.49,11.9,22.6,39,61.5,89.3,121,156,192,229,265,301,336,370,403,434,464,492,518,543,565,587,607,626,643,660,675,690,704,718,731,743,754,764,774,783,792,800,808,815,822,829,835,840,846,851,856,861,866,870,875,879,883,888,892,896,902];

% Aluminum RRR170 (Al+2%Ni)
Al_RRR170_Tinterp = [2:5:300];
Al_RRR170_Density = 2698; % [kg/m^3] %% Matthias, 10/11/15: I haven't confirmed the accuracy of this, but am rather assuming that it's about the same as all the other materials. Same goes for the specific heat
Al_RRR170_ThermalConductivity = 10.^(-7.861879E-01*log10(Al_RRR170_Tinterp).^6 + 6.889129E+00*log10(Al_RRR170_Tinterp).^5 - 2.314084E+01*log10(Al_RRR170_Tinterp).^4 + 3.756798E+01*log10(Al_RRR170_Tinterp).^3 - 3.147435E+01*log10(Al_RRR170_Tinterp).^2 + 1.408389E+01*log10(Al_RRR170_Tinterp) + 1.551833E-02);
Al_RRR170_SpecificHeat = [0.108,0.673,2.14,5.49,11.9,22.6,39,61.5,89.3,121,156,192,229,265,301,336,370,403,434,464,492,518,543,565,587,607,626,643,660,675,690,704,718,731,743,754,764,774,783,792,800,808,815,822,829,835,840,846,851,856,861,866,870,875,879,883,888,892,896,902];

% Aluminum RRR400
%% Matthias, 6/13/16, added this because the 'Pure aluminum RRR 400' doesn't actually seem to have a RRR of 400 :/
Al_RRR400_Tinterp = [2:5:500];
Al_RRR400_Density = 2698; % [kg/m^3] %% Matthias, 10/11/15: I haven't confirmed the accuracy of this, but am rather assuming that it's about the same as all the other materials. Same goes for the specific heat

Al_RRR400_ThermalConductivity = 10.^(1.635641E-01*log10(Al_RRR400_Tinterp).^6 - 2.445714E+00*log10(Al_RRR400_Tinterp).^5 + 1.349446E+01*log10(Al_RRR400_Tinterp).^4 - 3.517228E+01*log10(Al_RRR400_Tinterp).^3 + 4.470482E+01*log10(Al_RRR400_Tinterp).^2 - 2.563700E+01*log10(Al_RRR400_Tinterp) + 8.437016E+00);

%Al_RRR400_SpecificHeat = [0.108,0.673,2.14,5.49,11.9,22.6,39,61.5,89.3,121,156,192,229,265,301,336,370,403,434,464,492,518,543,565,587,607,626,643,660,675,690,704,718,731,743,754,764,774,783,792,800,808,815,822,829,835,840,846,851,856,861,866,870,875,879,883,888,892,896,902];
Al_RRR400_SpecificHeat = 10.^(-0.8476*log10(Al_RRR400_Tinterp).^6 + 9.2513*log10(Al_RRR400_Tinterp).^5 - 39.852*log10(Al_RRR400_Tinterp).^4 + 85.514*log10(Al_RRR400_Tinterp).^3 - 95.412*log10(Al_RRR400_Tinterp).^2 + 54.561*log10(Al_RRR400_Tinterp) - 13.028);

% pure aluminun RRR10
Pure_Al_RRR10_Tinterp = [3:5:300,300];
Pure_Al_RRR10_ThermalConductivity = [26.6,70.9,115,158,199,236,266,288,299,301,297,288,277,266,255,245,236,228,221,215,210,206,203,201,199,198,198,198,198,199,199,200,200,200,200,201,202,202,203,204,204,205,205,206,206,206,207,207,208,208,209,209,210,210,210,211,211,212,212,213,213];
Pure_Al_RRR10_Density = 2698; % [kg/m^3]
Pure_Al_RRR10_SpecificHeat = [0.169,0.867,2.62,6.49,13.7,24.5,43.1,66.7,95.4,128,163,199,236,273,308,343,377,409,440,470,497,523,547,570,591,611,629,647,663,678,693,707,720,733,745,756,766,776,785,794,802,809,817,823,830,836,841,847,852,857,862,867,871,876,880,884,888,892,896,900,902];

% Brass
Brass_Tinterp = [2.5,5,15,25,35,45,55,65,75,85,95,105,115,125,135,145,155,165,175,185,195,205,215,225,235,245,255,265,275,285,295,300];
Brass_ThermalConductivity = [2,4.3000,14.9000,24.2000,31.7000,37.7000,42.4000,45.9000,49.0000,53.0000,57.1000,61.0000,64.8000,68.5000,72.1000,75.6000,79.0000,82.3000,85.6000,88.8000,92.0000,95.1000,98.1000,101.0000,104.0000,107.0000,110.0000,113.0000,115.0000,118.0000,121.0000,122.0000];
Brass_ElectricalResistivity = [44.8000,44.8000,44.8000,45.0000,45.4000,46.1000,47.1000,48.1000,49.1000,50.1000,51.2000,52.2000,53.2000,54.2000,55.3000,56.3000,57.3000,58.3000,59.3000,60.4000,61.4000,62.40002,63.4000,64.5000,65.5000,66.5000,67.5000,68.5000,69.5000,70.5000,71.6000,72.1000]*1e-9; %[Ohm-m]

% Kevlar
Kevlar49_ThermalConductivity = @(T)10.^(63.69770859 - 426.993304 * log10(T) + 1188.658461 * log10(T).^2 - 1836.24689 * log10(T).^3 + 1725.583368 * log10(T).^4 - 1009.86649 * log10(T).^5 + 359.5950942 * log10(T).^6 - 71.3117819 * log10(T).^7 + 6.039246276 * log10(T).^8);

% Nylon
Polyamide_ThermalConductivity = @(T)10.^(-2.6135 + 2.3239 * log10(T) - 4.7586 * log10(T).^2 + 7.1602 * log10(T).^3 - 4.9155 * log10(T).^4 + 1.6324 * log10(T).^5 - 0.2507 * log10(T).^6 + 0.0131 * log10(T).^7 + 0 * log10(T).^8);
Polyimide_ThermalConductivity = @(T)10.^(5.73101 - 39.5199 * log10(T) + 79.9313 * log10(T).^2 - 83.8572 * log10(T).^3 + 50.9157 * log10(T).^4 -17.9835 * log10(T).^5 + 3.42413 * log10(T).^6 - 0.27133 * log10(T).^7 + 0 * log10(T).^8);

% Oxygen free copper
OFHCCopperRRR50_ThermalConductivity  = @(T)10.^((1.8743 - 0.6018*T.^0.5 + 0.26426*T - 0.051276*T.^1.5 + 0.003723*T.^2)./(1 - 0.41538*T.^0.5 + 0.13294*T - 0.0219*T.^1.5 + 0.0014871*T.^2));
OFHCCopperRRR100_ThermalConductivity = @(T)10.^((2.2154 - 0.88068*T.^0.5 + 0.29505*T - 0.04831*T.^1.5 + 0.003207*T.^2)./(1 - 0.47461*T.^0.5 + 0.13871*T - 0.02043*T.^1.5 + 0.001281*T.^2));
OFHCCopperRRR150_ThermalConductivity = @(T)10.^((2.3797 - 0.98615*T.^0.5 + 0.30475*T - 0.046897*T.^1.5 + 0.0029988*T.^2)./(1 - 0.4918*T.^0.5 + 0.13942*T - 0.019713*T.^1.5 + 0.0011969*T.^2));
OFHCCopperRRR300_ThermalConductivity = @(T)10.^((1.357 + 2.669*T.^0.5 - 0.6683*T + 0.05773*T.^1.5 + 0*T.^2)./(1 + 0.3981*T.^0.5 - 0.1346*T + 0.01342*T.^1.5 + 0.0002147*T.^2));
OFHCCopperRRR500_ThermalConductivity = @(T)10.^((2.8075 - 1.2777*T.^0.5 + 0.36444*T - 0.051727*T.^1.5 + 0.0030964*T.^2)./(1 - 0.54074*T.^0.5 + 0.15362*T - 0.02105*T.^1.5 + 0.0012226*T.^2));
OFHCCopper_SpecificHeat = @(T)10.^(-1.91844 - 0.15973 * log10(T) + 8.61013 * log10(T).^2 - 18.996 * log10(T).^3 + 21.9661 * log10(T).^4 - 12.7328 * log10(T).^5 + 3.54322 * log10(T).^6 - 0.3797 * log10(T).^7 + 0 * log10(T).^8);
OFHCCopper_Density = 8940; %[kg/m3]

% Stainless Steel 304
StainlessSteel_304_Tinterp = [2.5,5,15,25,35,45,55,65,75,85,95,105,115,125,135,145,155,165,175,185,195,205,215,225,235,245,255,265,275,285,295,300];
StainlessSteel_304_ElectricalResistivity = [490,490,490,493,497,503,507,515,525,534,542,549,559,570,582,594,604,613,621,629,636,644,652,660,668,676,684,693,702,710,717,720]*1e-9; %[Ohm-m]

% include steel 310
StainlessSteel_310_ThermalConductivity = @(T)10.^(-0.81907 + -2.1967 * log10(T) + 9.1059 * log10(T).^2 - 13.078 * log10(T).^3 + 10.853 * log10(T).^4 - 5.1269 * log10(T).^5 - 1.2583 * log10(T).^6 + 0 * log10(T).^7 - 0 * log10(T).^8);
StainlessSteel_310_SpecificHeat1 = @(T)10.^(20.694 - 171.007 * log10(T) + 218.743 * log10(T).^2 - 308.853 * log10(T).^3 + 239.5296 * log10(T).^4 - 89.9982 * log10(T).^5 + 3.15315 * log10(T).^6 + 8.44996 * log10(T).^7 - 1.91368 * log10(T).^8);
StainlessSteel_310_SpecificHeat2 = @(T)10.^(-1879.464 + 3643.198 * log10(T) + 76.70125 * log10(T).^2 - 6176.028 * log10(T).^3 + 7437.6247 * log10(T).^4 - 4305.7217 * log10(T).^5 + 1382.4627 * log10(T).^6 - 237.22704 * log10(T).^7 + 17.05262 * log10(T).^8);
StainlessSteel_310_Density = 7990; % kg / m^3

% Stainless Steel 316
StainlessSteel_316_ThermalConductivity = @(T)10.^(-1.4087 + 1.3982 * log10(T) + 0.2543 * log10(T).^2 - 0.626 * log10(T).^3 + 0.223 * log10(T).^4 + 0.4256 * log10(T).^5 - 0.4658 * log10(T).^6 + 0.1650 * log10(T).^7 - 0.0199 * log10(T).^8);
StainlessSteel_316_SpecificHeat1 = @(T)10.^(12.2486 - 80.6422 * log10(T) + 218.743 * log10(T).^2 - 308.853 * log10(T).^3 + 239.5296 * log10(T).^4 - 89.9982 * log10(T).^5 + 3.15315 * log10(T).^6 + 8.44996 * log10(T).^7 - 1.91368 * log10(T).^8);
StainlessSteel_316_SpecificHeat2 = @(T)10.^(-1879.464 + 3643.198 * log10(T) + 76.70125 * log10(T).^2 - 6176.028 * log10(T).^3 + 7437.6247 * log10(T).^4 - 4305.7217 * log10(T).^5 + 1382.4627 * log10(T).^6 - 237.22704 * log10(T).^7 + 17.05262 * log10(T).^8);
StainlessSteel_316_Density = 7990; % kg / m^3

% Fyberglass Epoxy G10
FyberglassEpoxyG10normal_ThermalConductivity = @(T)10.^(-4.1236 + 13.788 * log10(T) - 26.068 * log10(T).^2 + 26.272 * log10(T).^3 - 14.663 * log10(T).^4 + 4.4954 * log10(T).^5 - 0.6905 * log10(T).^6 + 0.0397 * log10(T).^7 + 0 * log10(T).^8);
FyberglassEpoxyG10warp_ThermalConductivity = @(T)10.^(polyval([-0.11701,1.48806,-7.95635,23.1778,-39.8754,41.1625,-24.8998,8.80228,-2.64827],log10(T)));
FyberglassEpoxyG10_SpecificHeat = @(T)10.^(-2.4083 + 7.6006 * log10(T) - 8.2982 * log10(T).^2 + 7.3301 * log10(T).^3 - 4.2386 * log10(T).^4 + 1.4294 * log10(T).^5 -0.24396 * log10(T).^6 + 0.015236 * log10(T).^7 + 0 * log10(T).^8);
FyberglassEpoxyG10_Density = 1850; %[kg/m3]

% % niobium titanium Jeroen
% NbTi_Tinterp = [4:5:299,300];
% NbTi_SpecificHeat = [0.5230,3.0000,8.6800,18.5000,33.7000,53.5000,78.5000,105.0000,126.0000,146.0000,170.0000,195.0000,217.0000,236.0000,253.0000,267.0000,283.0000,298.0000,305.0000,307.0000,311.0000,316.0000,322.0000,328.0000,334.0000,341.0000,348.0000,355.0000,362.0000,369.0000,373.0000,377.0000,381.0000,384.0000,387.0000,390.0000,393.0000,395.0000,397.0000,400.0000,402.0000,405.0000,407.0000,409.0000,411.0000,413.0000,415.0000,417.0000,418.0000,420.0000,421.0000,422.0000,423.0000,423.0000,424.0000,425.0000,425.0000,425.0000,426.0000,426.0000,426.0000];
% NbTi_ElectricalResistivity = [0,497,698,628,605,607,608,610,612,614,616,618,619,621,623,625,626,628,630,632,634,635,637,639,641,643,645,646,648,650,651,653,655,656,658,660,662,663,665,667,668,670,672,674,676,677,679,681,683,685,686,688,690,692,693,695,697,699,701,703,703]*1e-9;
% NbTi_Density = 6550; %[kg/m^3];

% % niobium titanium Bas 1
% NbTi_Tinterp = [-1:5:299,300];
% NbTi_SpecificHeat = [0 0.5230,3.0000,8.6800,18.5000,33.7000,53.5000,78.5000,105.0000,126.0000,146.0000,170.0000,195.0000,217.0000,236.0000,253.0000,267.0000,283.0000,298.0000,305.0000,307.0000,311.0000,316.0000,322.0000,328.0000,334.0000,341.0000,348.0000,355.0000,362.0000,369.0000,373.0000,377.0000,381.0000,384.0000,387.0000,390.0000,393.0000,395.0000,397.0000,400.0000,402.0000,405.0000,407.0000,409.0000,411.0000,413.0000,415.0000,417.0000,418.0000,420.0000,421.0000,422.0000,423.0000,423.0000,424.0000,425.0000,425.0000,425.0000,426.0000,426.0000,426.0000];
% NbTi_ElectricalResistivity = [0,0,497,698,628,605,607,608,610,612,614,616,618,619,621,623,625,626,628,630,632,634,635,637,639,641,643,645,646,648,650,651,653,655,656,658,660,662,663,665,667,668,670,672,674,676,677,679,681,683,685,686,688,690,692,693,695,697,699,701,703,703]*1e-9;
% NbTi_Density = 6550; %[kg/m^3];

% niobium titanium Bas 2
NbTi_Tinterp = [4:5:299,300];
NbTi_SpecificHeat = [0.5230,3.0000,8.6800,18.5000,33.7000,53.5000,78.5000,105.0000,126.0000,146.0000,170.0000,195.0000,217.0000,236.0000,253.0000,267.0000,283.0000,298.0000,305.0000,307.0000,311.0000,316.0000,322.0000,328.0000,334.0000,341.0000,348.0000,355.0000,362.0000,369.0000,373.0000,377.0000,381.0000,384.0000,387.0000,390.0000,393.0000,395.0000,397.0000,400.0000,402.0000,405.0000,407.0000,409.0000,411.0000,413.0000,415.0000,417.0000,418.0000,420.0000,421.0000,422.0000,423.0000,423.0000,424.0000,425.0000,425.0000,425.0000,426.0000,426.0000,426.0000];
NbTi_ElectricalResistivity = [497,497,698,628,605,607,608,610,612,614,616,618,619,621,623,625,626,628,630,632,634,635,637,639,641,643,645,646,648,650,651,653,655,656,658,660,662,663,665,667,668,670,672,674,676,677,679,681,683,685,686,688,690,692,693,695,697,699,701,703,703]*1e-9;
NbTi_Density = 6550; %[kg/m^3];

% tri-niobium tin
Nb3Sn_Tinterp = [4:5:299,300];
Nb3Sn_SpecificHeat = [0.1720,2.0700,5.5100,10.8000,18.9000,29.4000,41.7000,54.8000,67.6000,79.8000,91.6000,103.0000,113.0000,123.0000,132.0000,140.0000,147.0000,153.0000,159.0000,164.0000,169.0000,174.0000,179.0000,183.0000,187.0000,191.0000,195.0000,199.0000,202.0000,205.0000,208.0000,211.0000,214.0000,216.0000,218.0000,220.0000,222.0000,224.0000,225.0000,227.0000,228.0000,229.0000,230.0000,231.0000,232.0000,233.0000,234.0000,235.0000,235.0000,236.0000,237.0000,237.0000,238.0000,238.0000,239.0000,240.0000,241.0000,241.0000,242.0000,243.0000,243.0000];
Nb3Sn_ElectricalResistivity = [0,0,104,234,278,269,267,269,272,275,278,281,283,286,289,291,294,297,300,303,306,310,313,316,319,322,325,328,331,334,337,340,343,345,348,350,353,355,357,360,362,364,366,367,369,371,373,374,376,378,379,381,382,384,385,386,388,388,389,390,390]*1e-9;
Nb3Sn_Density = 8950; %[kg/m^3]

% Enamel
Enamel_ThermalConductivity = @(T)0.04919*T.^0.4147-0.07262;

% @Rosalinde 13/09/2015
% Test Fyberglass Epoxy G10 changed!!!!
test_ThermalConductivity = @(T) 2.0 * 10.^(-4.1236 + 13.788 * log10(T) - 26.068 * log10(T).^2 + 26.272 * log10(T).^3 - 14.663 * log10(T).^4 + 4.4954 * log10(T).^5 - 0.6905 * log10(T).^6 + 0.0397 * log10(T).^7 + 0 * log10(T).^8);
test_SpecificHeat = @(T)10.^(-2.4083 + 7.6006 * log10(T) - 8.2982 * log10(T).^2 + 7.3301 * log10(T).^3 - 4.2386 * log10(T).^4 + 1.4294 * log10(T).^5 -0.24396 * log10(T).^6 + 0.015236 * log10(T).^7 + 0 * log10(T).^8);
test_Density = 1850; %[kg/m3]

% @Rosalinde 29/09/2015
% Heliumbath (including film boiling)
helium_ThermalConductivity = @(T) 0.2;



%% calculation of material properties

    switch mat
        case 'None'
            switch prop
                case 'TC'; val = 0;
                case 'ER'; val = inf;
                case 'SH'; val = 0;
                case 'Ro'; val = 0;
                otherwise; error('Property not available or recognized');
            end
            
        case 'Aluminum 3003-F'
            switch prop;
                case 'TC'; val = Aluminum3003F_ThermalConductivity(T);
                case 'ER'; val = (LorenzNumber * T)./Aluminum3003F_ThermalConductivity(T); % Wiedemann-Franz Law
                case 'SH'; val = Aluminum3003F_SpecificHeat(T);
                case 'Ro'; val = Aluminum_Density;
                otherwise; error('Property not available or recognized');
            end
            
        case 'Aluminum 5083-O'
            switch prop;
                case 'TC'; val = Aluminum5083O_ThermalConductivity(T);
                case 'ER'; val = (LorenzNumber * T)./Aluminum5083O_ThermalConductivity(T); % Wiedemann-Franz Law
                case 'SH'; val = Aluminum5083O_SpecificHeat(T);
                case 'Ro'; val = Aluminum_Density;
                otherwise; error('Property not available or recognized');
            end
            
        case 'Aluminum 6061-T6'
            switch prop;
                % @Rosalinde
                case 'TC'; val = Aluminum6061T6_ThermalConductivity(T);
%                 case 'TC'; val = Cable_ThermalConductivity(T);
                % end @Rosalinde
                case 'ER'; val = (LorenzNumber * T)./Aluminum6061T6_ThermalConductivity(T); % Wiedemann-Franz Law
                case 'SH'; val = Aluminum6061T6_SpecificHeat(T);
                case 'Ro'; val = Aluminum_Density;
                otherwise; error('Property not available or recognized');
            end
        case 'Aluminum_RRR_170' %% Added by Matthias, 10/11/15
            switch prop
                case 'TC'; val = interp1(Al_RRR170_Tinterp,Al_RRR170_ThermalConductivity,T,'linear','extrap');
                case 'ER'; val = (LorenzNumber * T)./(interp1(Al_RRR170_Tinterp,Al_RRR170_ThermalConductivity,T,'linear','extrap')); % Wiedemann-Franz Law
                case 'SH'; val = interp1(Al_RRR170_Tinterp,Al_RRR170_SpecificHeat,T,'linear','extrap');
                case 'Ro'; val = Al_RRR170_Density;
                otherwise; error('Property not available or recognized');
            end  
        case 'Aluminum_RRR_400' %% Added by Matthias, 6/13/16
            switch prop
                case 'TC'; val = interp1(Al_RRR400_Tinterp,Al_RRR400_ThermalConductivity,T,'linear','extrap');
                case 'ER'; val = (LorenzNumber * T)./(interp1(Al_RRR400_Tinterp,Al_RRR400_ThermalConductivity,T,'linear','extrap')); % Wiedemann-Franz Law
                case 'SH'; val = interp1(Al_RRR400_Tinterp,Al_RRR400_SpecificHeat,T,'linear','extrap');
                case 'Ro'; val = Al_RRR400_Density;
                otherwise; error('Property not available or recognized');
            end              
        case 'OFHC Copper RRR'
            switch prop;
                case 'TC';
                    if RRR < 100
                        f = (RRR-50)/50;
                        val = (1-f)*OFHCCopperRRR50_ThermalConductivity(T) + f*OFHCCopperRRR100_ThermalConductivity(T);
                    elseif 100 <= RRR && RRR < 150
                        f = (RRR-100)/50;
                        val = (1-f)*OFHCCopperRRR100_ThermalConductivity(T) + f*OFHCCopperRRR150_ThermalConductivity(T);
                    elseif 150 <= RRR && RRR < 300
                        f = (RRR-150)/150;
                        val = (1-f)*OFHCCopperRRR150_ThermalConductivity(T) + f*OFHCCopperRRR300_ThermalConductivity(T);
                        % @Rosalinde
%                         val = Cable_ThermalConductivity(T);
                        % end @Rosalinde
                    elseif 300 <= RRR
                        f = (RRR-300)/200;
                        val = (1-f)*OFHCCopperRRR300_ThermalConductivity(T) + f*OFHCCopperRRR500_ThermalConductivity(T);
                    end
                    
                case 'ER';
                    C0 = 1.7; C1 = 2.33e9; C2 = 9.57e5; C3 = 163;
                    val = (C0/RRR + 1./(C1./T.^5+C2./T.^3+C3./T))*1e-8 + B*(0.37 + 0.0005 * RRR)*1e-10;
                case 'SH'; val = OFHCCopper_SpecificHeat(T);
                case 'Ro'; val = OFHCCopper_Density;
                otherwise; error('Property not available or recognized');
            end
            
            % @Rosalinde 28/08/2015
            % material properties of conductor of coil Tim+Alexey, !with
            % really high electrical resistivity!
            % under construction ;)
        case 'NbTiCu';
            switch prop;
                case 'TC'; val = NbTiCu_ThermalConductivity(T);
                case 'ER'; val = linspace(1E3,1E3,length(T));
                case 'SH'; val = NbTiCu_SpecificHeat(T);
                case 'Ro'; val = NbTiCu_Density(T);
                otherwise; error('Property not available or recognized');
            end
            
            % @Rosalinde 05/09/2015
            % Thermal conductivity of cable for TWIN Solenoid
        case 'Cablerad';
            switch prop;
                case 'TC'; val = Cable_ThermalConductivity(T);
                otherwise; error('Only thermal conductivity of cable as a whole is calculated');
            end
            
        case 'Cableax';
            switch prop;
                case 'TC'; val = Cable_ThermalConductivity(T);
                otherwise; error('Only thermal conductivity of cable as a whole is calculated');
            end
            % end @Rosalinde 05/09/2015
            
            
            % @Rosalinde 13/09/2015
        case 'test'
            switch prop
                case 'TC'; val = test_ThermalConductivity(T);
                case 'ER'; val = linspace(1e9,1e9,length(T));
                case 'SH'; val = test_SpecificHeat(T);
                case 'Ro'; val = test_Density;
                otherwise; error('Property not available or recognized');
            end
            
            % @Rosalinde 29/09/2015
        case 'helium'
            switch prop
                case 'TC'; val = helium_ThermalConductivity(T);
                otherwise; error('Property not available or recognized');
            end
            
            
            
%         case 'OFHC Copper RRR50'
%             
%             switch prop;
%                 case 'TC'; val = OFHCCopperRRR50_ThermalConductivity(T);
%                 case 'ER';
%                     C0 = 1.7; C1 = 2.33e9; C2 = 9.57e5; C3 = 163; RRR = 50;
%                     val = (C0/RRR + 1./(C1./T.^5+C2./T.^3+C3./T))*1e-8 + B*(0.37 + 0.0005 * RRR)*1e-10;
%                 case 'SH'; val = OFHCCopper_SpecificHeat(T);
%                 case 'Ro'; val = OFHCCopper_Density;
%                 otherwise; error('Property not available or recognized');
%             end
             
             
%             
%         case 'OFHC Copper RRR100'
%             switch prop;
%                 case 'TC'; val = OFHCCopperRRR100_ThermalConductivity(T);
%                 case 'ER';
%                     C0 = 1.7; C1 = 2.33e9; C2 = 9.57e5; C3 = 163; RRR = 100;
%                     val = (C0/RRR + 1./(C1./T.^5+C2./T.^3+C3./T))*1e-8 + B*(0.37 + 0.0005 * RRR)*1e-10;
%                 case 'SH'; val = OFHCCopper_SpecificHeat(T);
%                 case 'Ro'; val = OFHCCopper_Density;
%                 otherwise; error('Property not available or recognized');
%             end
%             
%         case 'OFHC Copper RRR150'
%             switch prop;
%                 case 'TC'; val = OFHCCopperRRR150_ThermalConductivity(T);
%                 case 'ER';
%                     C0 = 1.7; C1 = 2.33e9; C2 = 9.57e5; C3 = 163; RRR = 150;
%                     val = (C0/RRR + 1./(C1./T.^5+C2./T.^3+C3./T))*1e-8 + B*(0.37 + 0.0005 * RRR)*1e-10;
%                 case 'SH'; val = OFHCCopper_SpecificHeat(T);
%                 case 'Ro'; val = OFHCCopper_Density;
%                 otherwise; error('Property not available or recognized');
%             end
%             
%         case 'OFHC Copper RRR300'
%             switch prop;
%                 case 'TC'; val = OFHCCopperRRR300_ThermalConductivity(T);
%                 case 'ER';
%                     C0 = 1.7; C1 = 2.33e9; C2 = 9.57e5; C3 = 163; RRR = 300;
%                     val = (C0/RRR + 1./(C1./T.^5+C2./T.^3+C3./T))*1e-8 + B*(0.37 + 0.0005 * RRR)*1e-10;
%                 case 'SH'; val = OFHCCopper_SpecificHeat(T);
%                 case 'Ro'; val = OFHCCopper_Density;
%                 otherwise; error('Property not available or recognized');
%             end
%             
%         case 'OFHC Copper RRR500'
%             switch prop;
%                 case 'TC'; val = OFHCCopperRRR500_ThermalConductivity(T);
%                 case 'ER';
%                     C0 = 1.7; C1 = 2.33e9; C2 = 9.57e5; C3 = 163; RRR = 500;
%                     val = (C0/RRR + 1./(C1./T.^5+C2./T.^3+C3./T))*1e-8 + B*(0.37 + 0.0005 * RRR)*1e-10;
%                 case 'SH'; val = OFHCCopper_SpecificHeat(T);
%                 case 'Ro'; val = OFHCCopper_Density;
%                 otherwise; error('Property not available or recognized');
%             end
            
       % case 'Stainless Steel 316'
       %     switch prop;
       %         case 'TC'; val = StainlessSteel_316_ThermalConductivity(T);
       %         case 'ER'; val = (LorenzNumber * T)./StainlessSteel_316_ThermalConductivity(T); %Wiederman franz Law
       %         case 'SH'; if T < 50; val = StainlessSteel_316_SpecificHeat1(T); else val = StainlessSteel_316_SpecificHeat2(T); end
       %         case 'Ro'; val = StainlessSteel_316_Density;
       %         otherwise; error('Property not available or recognized');
       %     end
            
        case 'Stainless Steel 316'
            switch prop;
                case 'TC'; val = (10.^(3.656242E-01.*log10(T).^6 - 3.336049E+00.*log10(T).^5 + 1.248699E+01.*log10(T).^4 - 2.472119E+01.*log10(T).^3 + 2.701899E+01.*log10(T).^2 - 1.389461E+01.*log10(T).^1 + 2.036036E+00));
                case 'ER'; val = (LorenzNumber * T)./(10.^(3.656242E-01.*log10(T).^6 - 3.336049E+00.*log10(T).^5 + 1.248699E+01.*log10(T).^4 - 2.472119E+01.*log10(T).^3 + 2.701899E+01.*log10(T).^2 - 1.389461E+01.*log10(T).^1 + 2.036036E+00)); %Wiederman franz Law
                case 'SH'; val = 10.^(4.441927E-01.*log10(T).^6 - 3.279031E+00.*log10(T).^5 + 8.788655E+00.*log10(T).^4 - 1.048562E+01.*log10(T).^3 + 5.785497E+00.*log10(T).^2 - 3.814614E-01.*log10(T).^1 - 1.732867E-01);
                case 'Ro'; val = StainlessSteel_316_Density;
                otherwise; error('Property not available or recognized');
            end
            
        case 'Enamel'
            switch prop
                case 'TC'; val = Enamel_ThermalConductivity(T);
                    
                    % cheat hacks
                case 'ER'; val = inf;
                case 'SH'; val = 0;
                case 'Ro'; val = 0;
                    
                otherwise; error('Property not available or recognized');
            end
            
        case 'Fiberglass Epoxy G-10 CR normal'
            switch prop
                case 'TC'; val = FyberglassEpoxyG10normal_ThermalConductivity(T);
                case 'ER'; val = linspace(1e9,1e9,length(T));
                case 'SH'; val = FyberglassEpoxyG10_SpecificHeat(T);
                case 'Ro'; val = FyberglassEpoxyG10_Density;
                otherwise; error('Property not available or recognized');
            end
            
        case 'Fiberglass Epoxy G-10 CR warp'
            switch prop
                case 'TC'; val = FyberglassEpoxyG10warp_ThermalConductivity(T);
                case 'ER'; val = linspace(1e9,1e9,length(T));
                case 'SH'; val = FyberglassEpoxyG10_SpecificHeat(T);
                case 'Ro'; val = FyberglassEpoxyG10_Density;
                otherwise; error('Property not available or recognized');
            end
            
        case 'Niobium Titanium'
            switch prop
                case 'Tc'; val = 9.2;
                case 'TC'; val = 0;
                % @Rosalinde
%                 case 'TC'; val = Cable_ThermalConductivity(T);
                % end @Rosalinde
                case 'ER'; val = interp1(NbTi_Tinterp,NbTi_ElectricalResistivity,T,'linear','extrap');
                case 'SH'; val = interp1(NbTi_Tinterp,NbTi_SpecificHeat,T,'linear','extrap');
                case 'Ro'; val = NbTi_Density;
                otherwise; error('Property not available or recognized');
            end
            
        case 'tri-Niobium Tin'
            switch prop
                case 'TC'; val = 0;
                case 'ER'; val = interp1(Nb3Sn_Tinterp,Nb3Sn_ElectricalResistivity,T,'linear','extrap');
                case 'SH'; val = interp1(Nb3Sn_Tinterp,Nb3Sn_SpecificHeat,T,'linear','extrap');
                case 'Ro'; val = Nb3Sn_Density;
                case 'Tc'; val = 18.2;
                otherwise; error('Property not available or recognized');
            end
            
        case 'Pure Aluminum'
            switch prop
                case 'TC'; val = interp1(Pure_Al_Tinterp,Pure_Al_ThermalConductivity,T,'linear','extrap');
                case 'ER'; val = (LorenzNumber * T)./(interp1(Pure_Al_Tinterp,Pure_Al_ThermalConductivity,T,'linear','extrap')); % Wiedemann-Franz Law
                case 'SH'; val = interp1(Pure_Al_Tinterp,Pure_Al_SpecificHeat,T,'linear','extrap');
                case 'Ro'; val = Pure_Al_Density;
                otherwise; error('Property not available or recognized');
            end
            
        case 'Pure Aluminum RRR400'
            switch prop
                case 'TC'; val = interp1(Pure_Al_RRR400_Tinterp,Pure_AlRRR400_ThermalConductivity,T,'linear','extrap');
                case 'ER'; val = (LorenzNumber * T)./(interp1(Pure_Al_RRR400_Tinterp,Pure_AlRRR400_ThermalConductivity,T,'linear','extrap')); % Wiedemann-Franz Law
                case 'SH'; val = interp1(Pure_Al_RRR400_Tinterp,Pure_AlRRR400_SpecificHeat,T,'linear','extrap');
                case 'Ro'; val = Pure_Al_RRR400_Density;
                otherwise; error('Property not available or recognized');
            end
            
        case 'Pure_Al_RRR1000'
            switch prop
                case 'TC'; val = interp1(Pure_Al_RRR1000_Tinterp,Pure_Al_RRR1000_ThermalConductivity,T,'linear','extrap');
                case 'ER'; val = (LorenzNumber * T)./(interp1(Pure_Al_RRR1000_Tinterp,Pure_Al_RRR1000_ThermalConductivity,T,'linear','extrap')); % Wiedemann-Franz Law
                case 'SH'; val = interp1(Pure_Al_RRR1000_Tinterp,Pure_Al_RRR1000_SpecificHeat,T,'linear','extrap');
                case 'Ro'; val = Pure_Al_RRR1000_Density;
                otherwise; error('Property not available or recognized');
            end
            
        case 'Pure_Al_RRR500'
            switch prop
                case 'TC'; val = interp1(Pure_Al_RRR500_Tinterp,Pure_Al_RRR500_ThermalConductivity,T,'linear','extrap');
                % @Rosalinde
%                 case 'TC'; val = Cable_ThermalConductivity(T);
                % end @Rosalinde
                case 'ER'; val = (LorenzNumber * T)./(interp1(Pure_Al_RRR500_Tinterp,Pure_Al_RRR500_ThermalConductivity,T,'linear','extrap')); % Wiedemann-Franz Law
                case 'SH'; val = interp1(Pure_Al_RRR500_Tinterp,Pure_Al_RRR500_SpecificHeat,T,'linear','extrap');
                case 'Ro'; val = Pure_Al_RRR500_Density;
                otherwise; error('Property not available or recognized');
            end
            
        case 'Pure_Al_RRR10'
            switch prop
                case 'TC'; val = interp1(Pure_Al_RRR10_Tinterp,Pure_Al_RRR10_ThermalConductivity,T,'linear','extrap');
                case 'ER'; val = (LorenzNumber * T)./(interp1(Pure_Al_RRR10_Tinterp,Pure_Al_RRR10_ThermalConductivity,T,'linear','extrap')); % Wiedemann-Franz Law
                case 'SH'; val = interp1(Pure_Al_RRR10_Tinterp,Pure_Al_RRR10_SpecificHeat,T,'linear','extrap');
                case 'Ro'; val = Pure_Al_RRR10_Density;
                otherwise; error('Property not available or recognized');
            end
            
        otherwise; error('Material not recognized');
    end

end
