function [ ax,xlim ] = xfit( ax,varargin )
%XFIT sets XLim to fit the data in ax exactly.
%Varargin is used to enter a margin in percents

% analyse input
margin = 0;
if ~isempty(varargin)
    margin = varargin{:};
end

% get data
A = get(ax,'Children');
B = get(A,'XData');
C = horzcat(B{1:end});

% define x limits
xmin = min(C);
xmax = max(C);

% implement margin if requested
if margin~=0
    dx = xmax - xmin;
    xmax = xmax + margin*dx/100;
    xmin = xmin - margin*dx/100;
end

% set xlimits
xlim = [xmin,xmax];
set(ax,'XLim',xlim);

end

