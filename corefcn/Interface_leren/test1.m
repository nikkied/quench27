classdef test1 < hgsetget
    
    
    properties
        % interface properties
        
        % figure
        f = [];
        ax = [];
        
        % boxes
        vbox = [];
        hbox = [];
        
        % 'practical' properties
        madness = 0;
        
        % callback functions
        cb_clickme = [];
        cb_umod = [];
        cb_troll = [];
        
    end
    
    methods
        function obj = test1()
            
            obj.initialize;
        end
        
        %% Spawn interface
        function initialize(self)
            
            self.f = figure('Color','w','Position',[100,100,400,400],'Renderer','OpenGL','Toolbar','none','MenuBar','none','NumberTitle','off','Name','lalalalala');
            
            % split tussen plaatje en knoppen
            self.vbox = uiextras.VBox( 'Parent',self.f );
            
            % plaatje
            self.ax = axes( 'Parent',self.vbox );
            
            % knoppen
            self.hbox = uiextras.HButtonBox('Parent',self.vbox);
            
            % define callback functi
            setappdata(self.f,'trolltool',self);
            
            % individuele knoppen
            h1 = uicontrol( 'String', 'ClickMe', 'Parent',self.hbox , 'Callback',@self.clickme);
            h2 = uicontrol( 'String', 'Umod?', 'Parent',self.hbox , 'Callback',@self.umod);
            h3 = uicontrol( 'String', 'Troll', 'Parent',self.hbox , 'Callback',@trollback);
            setappdata(self.f,'buttoons',[h1,h2,h3]);
            
            
            % resize
            set(self.vbox, 'Sizes' , [-1 35]);
            
            
            
        end
        
        %% Callback function of 'clickme' button
        function clickme(self,src,~)
            disp('test')
            disp( num2str(src) )
            buttoons = getappdata(gcbf,'buttoons');
            n = ceil(rand(1)*length(buttoons));
            a = false(length(buttoons),1); a(n) = true;
            set(buttoons(a),'Enable','off');
            set(buttoons(~a),'Enable','on');
        end
        
        %% Callback function of 'umod?' button
        function umod(self,src,~)
            % increase madness level
            self.madness = self.madness + 1;
            disp(num2str(self.madness));
            buttoons = getappdata(gcbf,'buttoons');
            n = ceil(rand(1)*length(buttoons));
            a = false(length(buttoons),1); a(n) = true;
            set(buttoons(a),'Enable','off');
            set(buttoons(~a),'Enable','on');
        end
        
        %% Callback function of 'troll' button
        function troll(self,src,~)
            disp('trollolololol')
            
            
        end
    end
    
end

function trollback(src,event)
myclass = getappdata(gcbf,'trolltool');
buttoons = getappdata(gcbf,'buttoons');
n = ceil(rand(1)*length(buttoons));
a = false(length(buttoons),1); a(n) = true;
set(buttoons(a),'Enable','off');
set(buttoons(~a),'Enable','on');
end

