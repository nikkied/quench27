classdef test2 < hgsetget
    
    
    properties
        % interface properties
        
        % figure
        f = [];
        ax = [];
        
        % boxes
        TabPanel
        
        % tabs
        tab1 = [];
        tab2 = [];
        
        
        % 'practical' properties
        madness = 0;
        
        % callback functions
        cb_clickme = [];
        cb_umod = [];
        cb_troll = [];
        
    end
    
    methods
        function obj = test2()
            
            obj.initialize;
        end
        
        %% Spawn interface
        function initialize(self)
            
            % make figure for interface
            self.f = figure('Color','w','Position',[100,100,400,400],'Renderer','OpenGL','Toolbar','none','MenuBar','none','NumberTitle','off','Name','lalalalala');
            
            % define tab structure
            self.TabPanel = uiextras.TabPanel();
            
            self.tab1 = test1; self.tab2 = test1;
            
%             set(self.tab1.f,'Parent',self.TabPanel);
%             set(self.tab2.f,'Parent',self.TabPanel);

            self.f1 = 
            
            uicontrol( 'Parent',self.TabPanel, 'Background', 'b')
            uicontrol( 'Parent',self.TabPanel, 'Background', 'r')
            
            self.TabPanel.TabNames  = {'clickme','umod'};
            
            
        end
        
        %% Callback function of 'clickme' button
        function clickme(self,src,~)
            disp('test')
            disp( num2str(src) )
            buttoons = getappdata(gcbf,'buttoons');
            n = ceil(rand(1)*length(buttoons));
            a = false(length(buttoons),1); a(n) = true;
            set(buttoons(a),'Enable','off');
            set(buttoons(~a),'Enable','on');
        end
        
        %% Callback function of 'umod?' button
        function umod(self,src,~)
            % increase madness level
            self.madness = self.madness + 1;
            disp(num2str(self.madness));
            buttoons = getappdata(gcbf,'buttoons');
            n = ceil(rand(1)*length(buttoons));
            a = false(length(buttoons),1); a(n) = true;
            set(buttoons(a),'Enable','off');
            set(buttoons(~a),'Enable','on');
        end
        
        %% Callback function of 'troll' button
        function troll(self,src,~)
            disp('trollolololol')
            
            
        end
    end
    
end




function trollback(src,event)
myclass = getappdata(gcbf,'trolltool');
buttoons = getappdata(gcbf,'buttoons');
n = ceil(rand(1)*length(buttoons));
a = false(length(buttoons),1); a(n) = true;
set(buttoons(a),'Enable','off');
set(buttoons(~a),'Enable','on');
end

