legend_str = [];
C = jet(10);
figure();
for i=1:10,
    x = 1:i:(10*i);
    plot(x,'Color',C(i,:)); hold on;
    legend_str = [legend_str; {num2str(i)}];
end
columnlegend(3, legend_str, 'Location', 'SouthEast','boxon');