%make plot
x = (1:100)/100*2*pi;
y = sin(x);

%configure texwriter
write = texwriter;
write.path = cd;
write.name = 'latextest';
write.figmode = 'pdf';

%enter plots
write.header

for i = 1:17
    f = figure;
    plot(x,y,'Visible',false);
    write.fig(f,'sine','one period of a sine function')
end
write.floatbar

%finish up
write.footer
write.compile


