function [fid] = mat2tex( fid, matrix, varargin)
% THIS IS A COMPLETELY EDITED MATRIX2LATEX FROM FILE-EXCHANGE --> IT WAS TEMPERED WITH
% TO WORK WITH TEXWRITER CLASS

% function: matrix2latex(...)
% Author:   M. Koehler
% Contact:  koehler@in.tum.de
% Version:  1.1
% Date:     May 09, 2004

% This software is published under the GNU GPL, by the free software
% foundation. For further reading see: http://www.gnu.org/licenses/licenses.html#GPL

% Usage:
% matrix2late(matrix, filename, varargs)
% where
%   - matrix is a 2 dimensional numerical or cell array
%   - filename is a valid filename, in which the resulting latex code will
%   be stored
%   - varargs is one ore more of the following (denominator, value) combinations
%      + 'rowLabels', array -> Can be used to label the rows of the
%      resulting latex table
%      + 'columnLabels', array -> Can be used to label the columns of the
%      resulting latex table
%      + 'alignment', 'value' -> Can be used to specify the alginment of
%      the table within the latex document. Valid arguments are: 'l', 'c',
%      and 'r' for left, center, and right, respectively
%      + 'format', 'value' -> Can be used to format the input data. 'value'
%      has to be a valid format string, similar to the ones used in
%      fprintf('format', value);
%      + 'size', 'value' -> One of latex' recognized font-sizes, e.g. tiny,
%      HUGE, Large, large, LARGE, etc.
%
% Example input:
%   matrix = [1.5 1.764; 3.523 0.2];
%   rowLabels = {'row 1', 'row 2'};
%   columnLabels = {'col 1', 'col 2'};
%   matrix2latex(matrix, 'out.tex', 'rowLabels', rowLabels, 'columnLabels', columnLabels, 'alignment', 'c', 'format', '%-6.2f', 'size', 'tiny');
%
% The resulting latex file can be included into any latex document by:
% /input{out.tex}
%
% Enjoy life!!!

    alignment = 'l';
    format = [];
    textsize = [];
    rotate = [];
    bold = [];
    italic = [];
    hline = [];
    thinhline = [];
    vline = [];
    standalone = false;
    rottable = false;
    caption = [];
    if (rem(nargin,2) == 1 || nargin < 2)
        error(['matrix2latex: ', 'Incorrect number of arguments to %s.'], mfilename);
    end

    okargs = {'alignment', 'format', 'size', 'rotate','bold','italic','rottable','caption','hline','thinhline','vline','standalone'};
    for j=1:2:(nargin-2)
        pname = varargin{j};
        pval = varargin{j+1};
        k = strmatch(lower(pname), okargs);
        if isempty(k)
            error('matrix2latex: ', 'Unknown parameter name: %s.', pname);
        elseif length(k)>1
            error('matrix2latex: ', 'Ambiguous parameter name: %s.', pname);
        else
            switch(k)
                case 1  % alignment
                    alignment = lower(pval);
                    if alignment == 'right'
                        alignment = 'r';
                    end
                    if alignment == 'left'
                        alignment = 'l';
                    end
                    if alignment == 'center'
                        alignment = 'c';
                    end
                    if alignment ~= 'l' && alignment ~= 'c' && alignment ~= 'r'
                        alignment = 'l';
                        warning('matrix2latex: ', 'Unkown alignment. (Set it to \''left\''.)');
                    end
                case 2  % format
                    format = lower(pval);
                case 3  % format
                    textsize = pval;
                case 4  
                    rotate = pval;  % matrix of size equal to data with values equal to rotation required
                case 5 
                    bold = pval;    % matrix of size equal to data with boolians for bold / nonbold
                case 6  %
                    italic = [];    % matrix of size equal to data with boolians for italic / nonitalic
                case 7
                    rottable = pval;    % use landscape
                case 8
                    caption = pval; % caption...
                case 9
                    hline = pval;   % array of length equal to number of rows of data + 1: value is number of hlines to use
                case 10
                    thinhline = pval;                
                case 11
                    vline = pval;   % array of length equal to number of columns of data + 1: value is number of vlines
                case 12
                    standalone = pval;  % adjust table to work in a stand alone file / write header here ?
                    
            end
        end
    end

    % denote width and height
    width = size(matrix, 2);
    height = size(matrix, 1);

    if isnumeric(matrix)
        matrix = num2cell(matrix);
    end
    
    for h=1:height
        for w=1:width
            if isnumeric(matrix{h,w})
                if(~isempty(format))
                    matrix{h, w} = num2str(matrix{h, w}, format);
                else
                    matrix{h, w} = num2str(matrix{h, w});
                end
            end
        end
    end

    
    if rottable == true;
        fprintf(fid, '\\begin{landscape}\r\n');
    end
     
    if(~isempty(textsize))
        fprintf(fid, '\\begin{%s}\r\n', textsize);
    end
    
    if standalone
        fprintf(fid, '\\begin{tabular}{');
    else
        fprintf(fid, '\\begin{longtable}{');
    end
   
    for i = 1:width
        if isempty(vline)
            fprintf(fid, '%c|', alignment);
        else
            fprintf(fid, '%c', alignment);
            for j = 1:vline(i)
                fprintf(fid, '|');
            end
        end
    end
    fprintf(fid, '}\r\n');
    
    % go through data
    for i = 1:height
        for j = 1:width
            
            % apply stuff
            if ~isempty(rotate) && rotate(i,j)~=0
                fprintf(fid, ['\\begin{turn}{' num2str(rotate(i,j)) '}']);
            end
            if ~isempty(bold) && bold(i,j)
                fprintf(fid, '\\textbf{');
            end
            if ~isempty(italic) && italic(i,j)
                fprintf(fid, '\\textit{');
            end
            
            % data
            fprintf(fid, '%s', matrix{i,j});
            
            % stop with stuff
            if ~isempty(italic) && italic(i,j)
                fprintf(fid, '}');
            end
            if ~isempty(bold) && bold(i,j)
                fprintf(fid, '}');
            end
            if ~isempty(rotate) && rotate(i,j)~=0
                fprintf(fid, '\\end{turn}');
            end
            
            % and signs
            if j~=width
                fprintf(fid, '&');
            end
            
        end
       
        fprintf(fid, '\\\\\r\n');
        
        if isempty(hline) && isempty(thinhline)
            fprintf(fid, '\\hline\r\n');
        end
        if ~isempty(hline)
            for j = 1:hline(i)
                fprintf(fid, '\\hline\r\n');
            end
        end
        if ~isempty(thinhline)
            for j = 1:thinhline(i)
                fprintf(fid, '\\arrayrulecolor{lightgray}\\hline\\arrayrulecolor{black}\r\n');
            end
        end
        
    end
    
    % finish up
    if standalone
        fprintf(fid, '\\end{tabular}\r\n');
    else
        fprintf(fid, ['\\caption{' caption '}\r\n']);
        fprintf(fid, '\\end{longtable}\r\n');
    end
    
    if(~isempty(textsize))
        fprintf(fid, '\\end{%s}\r\n', textsize);
    end
    
    if rottable == true;
        fprintf(fid, '\\end{landscape}');
    end
    
    

    %fclose(fid);