function [fid] = mat2tex( fid, matrix, varargin)
% THIS IS A COMPLETELY EDITED MATRIX2LATEX FROM FILE-EXCHANGE --> IT WAS TEMPERED WITH
% TO WORK WITH TEXWRITER CLASS

% function: matrix2latex(...)
% Author:   M. Koehler
% Contact:  koehler@in.tum.de
% Version:  1.1
% Date:     May 09, 2004

% This software is published under the GNU GPL, by the free software
% foundation. For further reading see: http://www.gnu.org/licenses/licenses.html#GPL

% Usage:
% matrix2late(matrix, filename, varargs)
% where
%   - matrix is a 2 dimensional numerical or cell array
%   - filename is a valid filename, in which the resulting latex code will
%   be stored
%   - varargs is one ore more of the following (denominator, value) combinations
%      + 'rowLabels', array -> Can be used to label the rows of the
%      resulting latex table
%      + 'columnLabels', array -> Can be used to label the columns of the
%      resulting latex table
%      + 'alignment', 'value' -> Can be used to specify the alginment of
%      the table within the latex document. Valid arguments are: 'l', 'c',
%      and 'r' for left, center, and right, respectively
%      + 'format', 'value' -> Can be used to format the input data. 'value'
%      has to be a valid format string, similar to the ones used in
%      fprintf('format', value);
%      + 'size', 'value' -> One of latex' recognized font-sizes, e.g. tiny,
%      HUGE, Large, large, LARGE, etc.
%
% Example input:
%   matrix = [1.5 1.764; 3.523 0.2];
%   rowLabels = {'row 1', 'row 2'};
%   columnLabels = {'col 1', 'col 2'};
%   matrix2latex(matrix, 'out.tex', 'rowLabels', rowLabels, 'columnLabels', columnLabels, 'alignment', 'c', 'format', '%-6.2f', 'size', 'tiny');
%
% The resulting latex file can be included into any latex document by:
% /input{out.tex}
%
% Enjoy life!!!

    rowLabels = [];
    colLabels = [];
    alignment = 'l';
    format = [];
    textsize = [];
    hline = [];
    vline = [];
    standalone = []; 
    rottable = false;
    rotlabels = false;
    caption = [];
    if (rem(nargin,2) == 1 || nargin < 2)
        error(['matrix2latex: ', 'Incorrect number of arguments to %s.'], mfilename);
    end

    okargs = {'rowlabels', 'columnlabels', 'alignment', 'format', 'size', 'rotlabels', 'rottable','caption','hline','vline','standalone'};
    for j=1:2:(nargin-2)
        pname = varargin{j};
        pval = varargin{j+1};
        k = strmatch(lower(pname), okargs);
        if isempty(k)
            error('matrix2latex: ', 'Unknown parameter name: %s.', pname);
        elseif length(k)>1
            error('matrix2latex: ', 'Ambiguous parameter name: %s.', pname);
        else
            switch(k)
                case 1  % rowlabels
                    rowLabels = pval;
                    if isnumeric(rowLabels)
                        rowLabels = cellstr(num2str(rowLabels(:)));
                    end
                case 2  % column labels
                    colLabels = pval;
                    if isnumeric(colLabels)
                        colLabels = cellstr(num2str(colLabels(:)));
                    end
                case 3  % alignment
                    alignment = lower(pval);
                    if alignment == 'right'
                        alignment = 'r';
                    end
                    if alignment == 'left'
                        alignment = 'l';
                    end
                    if alignment == 'center'
                        alignment = 'c';
                    end
                    if alignment ~= 'l' && alignment ~= 'c' && alignment ~= 'r'
                        alignment = 'l';
                        warning('matrix2latex: ', 'Unkown alignment. (Set it to \''left\''.)');
                    end
                case 4  % format
                    format = lower(pval);
                case 5  % format
                    textsize = pval;
                case 6 % rotate labels
                    rotlabels = pval;
                case 7 
                    rottable = pval;
                case 8
                    caption = pval;
                case 9
                    hline = pval;
                case 10
                    vline = pval;
                case 11
                    standalone = pval;
                    
            end
        end
    end

    % denote width and height
    width = size(matrix, 2);
    height = size(matrix, 1);

    if isnumeric(matrix)
        matrix = num2cell(matrix);
    end
    
    for h=1:height
        for w=1:width
            if isnumeric(matrix{h,w})
                if(~isempty(format))
                    matrix{h, w} = num2str(matrix{h, w}, format);
                else
                    matrix{h, w} = num2str(matrix{h, w});
                end
            end
        end
    end

    
    %fprintf(fid, '\\fontsize{4pt}{10pt}\selectfont\r\n');
    if rottable == true;
        fprintf(fid, '\\begin{landscape}\r\n');
    end
     
    if(~isempty(textsize))
        fprintf(fid, '\\begin{%s}\r\n', textsize);
    end
    
    if standalone
        fprintf(fid, '\\begin{tabular}{');
    else
        fprintf(fid, '\\begin{longtable}{');
    end
   

    if(~isempty(rowLabels))
        fprintf(fid, 'l|');
    end
    for i=1:width
        if isempty(vline)
            fprintf(fid, '%c|', alignment);
        else
            fprintf(fid, '%c', alignment);
            for j = 1:vline(i)
                fprintf(fid, '|');
            end
        end
    end
    fprintf(fid, '}\r\n');
    
%     if(~isempty(colLabels))
%         if(~isempty(rowLabels))
%             fprintf(fid, '&');
%         end
%         for w=1:width-1
%             if rotlabels == true; 
%                 fprintf(fid, '\\begin{turn}{90}');
%                 fprintf(fid, '\\textbf{%s}', colLabels{w});
%                 fprintf(fid, '\\end{turn}&');
%             else
%                 fprintf(fid, '\\textbf{%s}&', colLabels{w});
%             end
%         end
%         if rotlabels == true;
%             fprintf(fid, '\\begin{turn}{90}');
%             fprintf(fid, '\\textbf{%s}\\end{turn}\\\\\\hline\r\n', colLabels{width}); 
%         else
%             fprintf(fid, '\\textbf{%s}\\\\\\hline\r\n', colLabels{width}); 
%         end
%     end
%     
%     for h=1:height
%         if(~isempty(rowLabels))
%             fprintf(fid, '\\textbf{%s}&', rowLabels{h});
%         end
%         for w=1:width-1
%             fprintf(fid, '%s&', matrix{h, w});
%         end
%         fprintf(fid, '%s\\\\\r\n', matrix{h, width});
%         
%         if isempty(hline)
%             fprintf(fid, '\\hline\r\n');
%         else
%             for j = 1:hline(i)
%                 fprintf(fid, '\\hline\r\n');
%             end
%         end
%         
%     end
    
    if standalone
        fprintf(fid, '\\end{tabular}\r\n');
    else
        fprintf(fid, ['\\caption{' caption '}\r\n']);
        fprintf(fid, '\\end{longtable}\r\n');
    end
    
    if(~isempty(textsize))
        fprintf(fid, '\\end{%s}\r\n', textsize);
    end
    
    if rottable == true;
        fprintf(fid, '\\end{landscape}');
    end
    
    

    %fclose(fid);