classdef texwriter < hgsetget
    % keuzes:
    % - fid in class opslaan (en dan geen < hgsetget, opdat kopieren van de
    % class (en dus je instellingen) makkelijk is en iedere file die
    % je potentieel paralel aan het maken bent z'n eigen instellingen
    % heeft). Of fid steeds meegeven? --> nu met gestoorde fid (gestoorde
    % fid....)
    
    % - chapter / section / subsection variabel per documentsoort...
    % Doel: je kunt zonder moeilijk te doen van document type switchen
    % --> iets van een 'tussenkop(level)' die een kop schrijft van niveau 1
    % (chapter), 2 (section) etc schrijft?
    
    % add bunch of boolians too choose packages?
    
    properties
        % directories
        path = [];      % path of .tex file (and the pdf-report)
        name = [];      % name of .tex file (and the pdf-report)
        fid = [];       % file identifier
        
        % style
        documentclass = 'report';
        fontsize = 10;
        font = []       % still need to implement
        
        % packages?
        
        % chapters/sections etc
        
        % floats
        floatpos = 'hp';
        
        % figures
        figmode = [];   % pdf or tikz
        
        % pdf
        figwidth = 1.0; % or maybe floatwidth?
        dpi = 300;
        
        % tables
        
        % compilation
        ncompile = 1;
        ncompiled = 0;
        
        
        
    end
    
    
    methods
        
        %% Basic write function
        function write( self,varargin )
            % stitch input together
            str = [];
            for i=1:length(varargin)
                str = [str,varargin{i}]; %#ok
            end
            
            % write to file
            fwrite(self.fid,str); fprintf(self.fid,'\n');
            
        end
        
        %% Create document and write header (split?)
        
        function [fid] = header(self)
            
            % create directory
            if ~exist(self.path,'dir')
                mkdir(self.path);
            end
            
            % create file (with writing rights)
            self.fid = fopen([self.path,filesep,self.name,'.tex'],'w');
            
            % header
            self.write('\documentclass[',num2str(self.fontsize),'pt,a4paper,onecolumn]{',self.documentclass,'}');
            self.write('');
            
            % graphicx package
            self.write('\usepackage{graphicx}');
            self.write('');
            
            % language settings
            self.write('\usepackage[latin1]{inputenc}');
            self.write('\usepackage[english]{babel}');
            self.write('');
            
            % font cmbright
            self.write('\usepackage[T1]{fontenc}');
            self.write('\usepackage{amssymb,amsfonts}');
            self.write('\usepackage{cmbright}');
            self.write('');
            
            % checkmarks
            self.write('\usepackage{pifont}');
            self.write('\newcommand{\cmark}{\ding{51}}');
            self.write('\newcommand{\xmark}{\ding{55}}');
            self.write('');
            
            % margins 1.5 cm
            if ~strcmp(self.documentclass,'standalone')
            self.write('\usepackage[left=1.2cm,right=1.2cm,top=1.2cm,bottom=1.2cm]{geometry}');
            self.write('');
            end
            
            % title formatting
            self.write('\usepackage{titlesec}');
            self.write('\titleformat{\chapter}{\LARGE\bfseries}{Chapter \thechapter}{1em}{}');
            self.write('\titleformat{\section}{\large\bfseries}{\thesection}{1em}{}');
            self.write('\titleformat{\subsection}{\normalsize\bfseries}{\thesubsection}{1em}{}');
            self.write('');
            
            % bookmarks
            self.write('\usepackage[bookmarks=true,bookmarksnumbered=true]{hyperref}');
            self.write('');
            
            % tikz plot settings
            self.write('\usepackage{tikz,pgfplots}');
            self.write('\pgfplotsset{compat=newest}');
            self.write('\pgfplotsset{x tick label style={/pgf/number format/fixed}}');
            self.write('\pgfplotsset{y tick label style={/pgf/number format/fixed}}');
            self.write('\usetikzlibrary{plotmarks}');
            self.write('\usepackage[maxfloats=40]{morefloats}');
            self.write('');
            
            % table environment
            self.write('\usepackage{longtable}');
            self.write('\usepackage{pdflscape}');
            self.write('\usepackage{array, xcolor}');
            self.write('\definecolor{lightgray}{gray}{0.8}');
            self.write('\newcolumntype{L}{>{\raggedright}p{0.15\textwidth}}');
            self.write('\newcolumntype{R}{p{0.8\textwidth}}');
            self.write('\newcommand{\ThinVRule}{\color{lightgray}\vrule width 0.5pt}');
            self.write('\newcommand{\ThinHRule}{\color{lightgray}\hrule height 0.5pt}');
            self.write('\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}');
            self.write('\usepackage{rotating}');
            self.write('\usepackage{colortbl}');
            self.write('');
            
            % enable \floatbarrier command for clearwrite
            self.write('\usepackage{placeins}');
            self.write('');

            % begin document
            self.write('\begin{document}');
            self.write('');
          
            % give output
            fid = self.fid;
            
        end
        
        %% Table of content
        function toc(self)
            
            % table of contents
            self.write('\addcontentsline{toc}{chapter}{Table of Contents}');
            self.write('\tableofcontents');
            self.write('');
            
            % toggle dubblecompile
            self.ncompile = 2;
            
        end
        
        %% Variable heading (check. if ok --> simplify levels 1-5)
        function head(self,headname,level)
            % choose set of headings dependent on self.documentclass
            if strcmp(self.documentclass,'report') || strcmp(self.documentclass,'book')
                % choose correct heading dependent on given level
                switch level
                    case {-1}
                        % part
                        self.write('\part{',headname,'}');
                        self.write('');
                    case 0
                        % chapter
                        self.write('\chapter{',headname,'}');
                        self.write('');
                    case 1
                        % section
                        self.write('\section{',headname,'}');
                        self.write('');
                    case 2
                        % subsection
                        self.write('\subsection{',headname,'}');
                        self.write('');
                    case 3
                        % subsubsection
                        self.write('\subsubsection{',headname,'}');
                        self.write('');
                    case 4
                        % paragraph
                        self.write('\paragraph{',headname,'}');
                        self.write('');
                    case 5
                        % subparagraph
                        self.write('\subparagraph{',headname,'}');
                        self.write('');
                        
                end
            end
            
            if strcmp(self.documentclass,'article')
                % choose correct heading dependent on given level
                switch level
                    case -1
                        % part
                        self.write('\part{',headname,'}');
                        self.write('');
                    case 0
                        % section
                        self.write('\section{',headname,'}');
                        self.write('');
                    case 1
                        % subsection
                        self.write('\subsection{',headname,'}');
                        self.write('');
                    case 2
                        % subsubsection
                        self.write('\subsubsection{',headname,'}');
                        self.write('');
                    case 3
                        % paragraph
                        self.write('\paragraph{',headname,'}');
                        self.write('');
                    case 4
                        % subparagraph
                        self.write('\subparagraph{',headname,'}');
                        self.write(''); 
                        
                end
                
            end
        end
        
        %% Figure
        function fig( self,fh,figname,capstr )
            % call figure writer depending on self.figmode
            switch self.figmode
                case {'tikz'}
                self.tikzfig(fh,figname,capstr);
            
                case {'pdf'}
                self.pdffig(fh,figname,capstr);
                
                case {'jpg'}
                self.jpgfig(fh,figname,capstr);
                
                % more?
 
            end
            
        end
        
        %% Tikz figure
        function tikzfig( self,fh,figname,capstr )
            
            % setup
            foldername = 'tikzfigs';
            tffolder = [self.path,filesep,foldername];
            if ~exist(tffolder,'dir'); mkdir(tffolder); end;
            
            % get caption from titlestring
            if strcmp(capstr,'usetitle')
                ha = findobj(fh,'Type','axes');
                for i=1:length(ha)
                    ht = get(ha(i),'Title'); str = get(ht,'String');
                    if ~isempty(str);
                        capstr = str;
                        title(ha(i),[]);
                        break;
                    end
                end
            end 
            
            % export figure
            matlab2tikz([tffolder filesep figname '.tex'],'figurehandle',fh,'Standalone',false,'width','14cm','showInfo',false);
            
            % figure
            self.write('\begin{figure}[',self.floatpos,']');
            self.write('  \centering');
            self.write('  \input{./',foldername,'/',figname,'.tex}');
            self.write('  \caption{',capstr,'}');
            self.write('  \label{fig:',figname,'}');
            self.write('\end{figure}');
            self.write('');
            
            % close figure
            close(fh);
            
        end
        
        %% Pdf figure
        function pdffig( self,fh,figname,capstr )
            
            % setup
            foldername = 'pdffigs';
            tffolder = [self.path,filesep,foldername];
            if ~exist(tffolder,'dir'); mkdir(tffolder); end;
            
            % get caption from titlestring
            if strcmp(capstr,'usetitle')
                ha = findobj(fh,'Type','axes');
                for i=1:length(ha)
                    ht = get(ha(i),'Title'); str = get(ht,'String');
                    if ~isempty(str);
                        capstr = str;
                        title(ha(i),[]);
                        break;
                    end
                end
            end
            
            
            % export figure
            export_fig(fh,[tffolder filesep figname '.pdf'],'-pdf',['-r',num2str(self.dpi)]);
            
            % figure
            self.write('\begin{figure}[',self.floatpos,']');
            self.write('  \centering');
            self.write('  \includegraphics[width=',num2str(self.figwidth),'\textwidth]{./',foldername,'/',figname,'.pdf}');
            self.write('  \caption{',capstr,'}');
            self.write('  \label{fig:',figname,'}');
            self.write('\end{figure}');
            self.write('');
            
            % close figure
            close(fh);
            
        end
        
        %% Pdf figure
        function jpgfig( self,fh,figname,capstr )
            
            % setup
            foldername = 'jpgfigs';
            tffolder = [self.path,filesep,foldername];
            if ~exist(tffolder,'dir'); mkdir(tffolder); end;
            
            % get caption from titlestring
            if strcmp(capstr,'usetitle')
                ha = findobj(fh,'Type','axes');
                for i=1:length(ha)
                    ht = get(ha(i),'Title'); str = get(ht,'String');
                    if ~isempty(str);
                        capstr = str;
                        title(ha(i),[]);
                        break;
                    end
                end
            end
            
            
            % export figure
            export_fig(fh,[tffolder filesep figname '.jpg'],'-jpg',['-r',num2str(self.dpi)]);
            
            % figure
            self.write('\begin{figure}[',self.floatpos,']');
            self.write('  \centering');
            self.write('  \includegraphics[width=',num2str(self.figwidth),'\textwidth]{./',foldername,'/',figname,'.jpg}');
            self.write('  \caption{',capstr,'}');
            self.write('  \label{fig:',figname,'}');
            self.write('\end{figure}');
            self.write('');
            
            % close figure
            close(fh);
            
        end
        
        %% Table 
        function table( self,matrix,varargin )
        
        % use matlab2latex from file exchange (modified version)
        mat2tex(self.fid, matrix,varargin{:});
        
        end       
        
        %% Place floats
        function floatbar( self,varargin )
            
            % if varargin is supplied, then also clearpage
            clearpage = true;
            if numel(varargin) == 1 && varargin{1} == false
                clearpage = false;
            end
                
            % write in file
            self.write('\FloatBarrier');
            if clearpage; self.write('\clearpage'); end
            self.write('');
            
        end
        
        %% Footer
        function footer( self )
            
            % end document
            self.write('\end{document}');
            self.write('');
            
            % close
            fclose(self.fid);
  
        end
        
        %% Compile
        function compile( self )
            % compile base on operating system
            switch computer('arch')
                case {'win32','win64'}
                    disp(['Compiling ',self.name]);
                    drive = self.path(1);
                    
                    ch = system(['chdir[/',drive,'] & cd "',self.path,'" & pdflatex "',self.path,filesep,self.name,'.tex"']);
                    if ch~=0; error(['pdflatex exited with following error code',ch]); end
                    
                otherwise; error('Operating system not supported yet');
            end
            
            % add 1 to self.ncompiled
            self.ncompiled = self.ncompiled + 1;
            
            % compile again if requested
            if self.ncompiled < self.ncompile
                self.compile
            end
            
        end
         
        %% Include text from .tex / .txt file ? (maybe useful for titlepage / longer parts you'd prefer to write in latex)
        function includetext( self, stuff   )
            
            
            
        end
        
        %% Include image file (png, jpg, tif, etc) ?
        function includefig( self, stuff   )
        
        
            
        end
        
        %% Open/Display in MATLAB (maybe truly display in command prompt... is this even useful?)
        function open( self )
            
            open([self.path,self.name]);  
            
        end
        
        
    end
    
end

