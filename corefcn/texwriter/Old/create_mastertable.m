function create_mastertable(results,path,name)

report = true;

%% Create file
% create directory
if exist(path,'dir')~=7
    mkdir(path);
end

% create file (with writing rights)
fid = fopen([path,'/',name,'.tex'], 'w');

%% header
if report==true; disp('Building Header ...'); end
fwrite(fid,'\documentclass[border=1em]{standalone}'); fprintf(fid,'\n');

% table packages
fwrite(fid,'\usepackage{multirow}'); fprintf(fid,'\n');
fwrite(fid,'\usepackage{hhline}'); fprintf(fid,'\n');

% tikz plot settings
fwrite(fid,'\usepackage{tikz,pgfplots}'); fprintf(fid,'\n');
fwrite(fid,'\pgfplotsset{compat=newest}'); fprintf(fid,'\n');
fwrite(fid,'\pgfplotsset{x tick label style={/pgf/number format/fixed}}'); fprintf(fid,'\n');
fwrite(fid,'\pgfplotsset{y tick label style={/pgf/number format/fixed}}'); fprintf(fid,'\n');
fwrite(fid,'\usetikzlibrary{plotmarks}'); fprintf(fid,'\n');

% tikz externalization
fwrite(fid,'\usepgfplotslibrary{external}'); fprintf(fid,'\n');
fwrite(fid,'\tikzexternalize'); fprintf(fid,'\n');
fwrite(fid,'% please add flag -shell-escape to pdflatex'); fprintf(fid,'\n');

% table stuff
fwrite(fid,'\newcommand\PBS[1]{\let\temp=\\%'); fprintf(fid,'\n');
fwrite(fid,'#1%'); fprintf(fid,'\n');
fwrite(fid,'\let\\=\temp'); fprintf(fid,'\n');
fwrite(fid,'}'); fprintf(fid,'\n');

%% begin document
fwrite(fid,'\begin{document}'); fprintf(fid,'\n');

% table header
fwrite(fid,'\begin{tabular}{ r | ');
for i=1:length(results); fwrite(fid,'| p{4.0cm} |'); end
fwrite(fid,'}'); fprintf(fid,'\n');

%% coil name
fwrite(fid,'name ');
for i=1:length(results); fwrite(fid,['& \PBS\centering \bf{',results{i}.name,'} ']); end
fwrite(fid,'\\'); fprintf(fid,'\n');

%% Production Date
fwrite(fid,'date ');
for i=1:length(results); fwrite(fid,['& \PBS\centering ',results{i}.date_str,' ']); end
fwrite(fid,'\\'); fprintf(fid,'\n');
fwrite(fid,'\hline'); fprintf(fid,'\n');
fwrite(fid,'\hline'); fprintf(fid,'\n');

%% layout
if report==true; disp('Creating layouts ...'); end
fwrite(fid,'layout '); fprintf(fid,'\n');
for i=1:length(results)
    if results{i}.dual_aperture==false
        results{i}.symmetry = 1:4;
    else
        results{i}.symmetry = 1:8;
    end
    ax = results{i}.plot('large','surfd'); 
    axis(ax,'off'); axis(ax,'tight'); set(findobj(ax,'Type','line'),'LineWidth',0.5); f = get(ax,'Parent');
    %xlim(ax,[-0.6,0.6]); ylim(ax,3*[-0.14,0.14]);
    matlab2tikz([path,'/',results{i}.name,'.tex'],'figurehandle',f,'width','\4cm','relativePngPath','.','showInfo',false);
    fwrite(fid,'&'); fprintf(fid,'\n');
    fwrite(fid,'\begin{minipage}{4.0cm}'); fprintf(fid,'\n');
    fwrite(fid,['\include{',results{i}.name,'}']); fprintf(fid,'\n');
    fwrite(fid,'\end{minipage}'); fprintf(fid,'\n');
    delete(f);
end
fwrite(fid,'\\'); fprintf(fid,'\n');
fwrite(fid,'\hline'); fprintf(fid,'\n');
fwrite(fid,'\hline'); fprintf(fid,'\n');

%% superconductor info
if report==true; disp('Gathering information per superconductor ...'); end
sc = {'YBCO','BSCCO','Nb3Sn','NbTi','NbTiSh'};
totalcosts = zeros(1,length(results)); 
for k=1:length(sc)
    if report==true; disp(['- ',sc{k},' ...']); end
    
    % calculate values
    Breq = zeros(1,length(results)); A = zeros(1,length(results)); 
    sigmapeak = zeros(1,length(results)); costs = zeros(1,length(results)); 

    %Fx = zeros(1,length(results)); Fy = zeros(1,length(results));
    for i=1:length(results)
        for j=1:results{i}.Nl
            for l=1:results{i}.layers{j}.Nb
                if (strcmp(results{i}.layers{j}.blocks{l}.sc.material,sc{k}) && results{i}.layers{j}.isshield==false) || (strcmp(sc{k},'NbTiSh') && results{i}.layers{j}.isshield==true)
                    [~,By,~] = results{i}.layers{j}.blocks{l}.calc_B(0,0);
                    % dual aperture
                    if results{i}.dual_aperture == true && results{i}.layers{j}.isshield==false
                        % calculate dual separation
                        dual_separation = results{i}.calc_dualsep();
                        
                        % calculate field of other aperture
                        [~,By2,~] = results{i}.layers{j}.blocks{l}.calc_B(dual_separation,0);
                        By = By - By2;
                    end
                    Breq(i) = Breq(i) + By;
                    
                    A(i)= A(i) + results{i}.layers{j}.blocks{l}.calc_A;
                    costs(i) = costs(i) + results{i}.layers{j}.blocks{l}.calc_costs();

                    for u=1:length(results{i}.symmetry)
                        Q = results{i}.symmetry(u);
                        
                        if (Q<=4 && results{i}.layers{j}.isshield==true) || results{i}.layers{j}.isshield==false
                            [sigmap,~,~,~] = results{i}.layers{j}.blocks{l}.calc_sigmap(Q);
                            [sigmapl,~,~,~] = results{i}.layers{j}.blocks{l}.calc_sigmap(Q);
                            sigma = max([max(max(sigmap)),max(max(sigmapl))]);
                            if sigma>sigmapeak(i); sigmapeak(i) = sigma; end
                        end
                    end

                end
            end
        end
        totalcosts(i) = totalcosts(i) + costs(i);
    end

    % write values to tex
    fwrite(fid,['\multirow{5}{*}{',sc{k},'} ']);
    for i=1:length(results)
        fwrite(fid,['& ',num2str(Breq(i),'%6.2f'),' $[T]$ ']);
    end
    fwrite(fid,'\\'); fprintf(fid,'\n');
      
%     fwrite(fid,' ');
%     for i=1:length(results)
%         fwrite(fid,['& ',num2str(dhi(i)*1e3,'%6.2f'),' $[mm]$ ']);
%     end
%     fwrite(fid,'\\'); fprintf(fid,'\n');

    fwrite(fid,' ');
    for i=1:length(results)
        fwrite(fid,['& ',num2str(A(i)*1e6,'%6.1f'),' $[mm^2]$ ']);
    end
    fwrite(fid,'\\'); fprintf(fid,'\n');
    
    fwrite(fid,' ');
    for i=1:length(results)
        fwrite(fid,['& ',num2str(sigmapeak(i)*1e-6,'%6.2f'),' $[MPa]$ ']);
    end
    fwrite(fid,'\\'); fprintf(fid,'\n');
    
    fwrite(fid,' ');
    for i=1:length(results)
        fwrite(fid,['& ',num2str(costs(i)/1000,'%6.2f'),' $[k\$ / m]$ ']);
    end
    fwrite(fid,'\\'); fprintf(fid,'\n');
    
%     fwrite(fid,' ');
%     for i=1:length(results)
%         fwrite(fid,['& ',num2str(Fx(i)/1000,'%6.1f'),' $[kN/m]$ ']);
%     end
%     fwrite(fid,'\\'); fprintf(fid,'\n');
    
%     fwrite(fid,' ');
%     for i=1:length(results)
%         fwrite(fid,['& ',num2str(Fy(i)/1000,'%6.1f'),' $[kN/m]$ ']);
%     end
%     fwrite(fid,'\\'); fprintf(fid,'\n');
    fwrite(fid,'\hline'); fprintf(fid,'\n');
end
fwrite(fid,'\hline'); fprintf(fid,'\n');

%% Total costs
if report==true; disp('Calculating total costs ...'); end
% write
fwrite(fid,'C. Costs '); 
for i=1:length(results)
    fwrite(fid,['& ',num2str(totalcosts(i)/1000,'%6.2f'),' [$k\$ / m$]']);
end
fwrite(fid,'\\'); fprintf(fid,'\n');
fwrite(fid,'\hline'); fprintf(fid,'\n');
fwrite(fid,'\hline'); fprintf(fid,'\n');

%% important sizes
if report==true; disp('Calculating coil sizes ...'); end
% calculate
raperture = zeros(1,length(results)); 
rshield = zeros(1,length(results)); 
dualsep = zeros(1,length(results));
rpack = zeros(1,length(results));
hc = zeros(1,length(results));
for i=1:length(results)
    raperture(i) = results{i}.daperture/2;
    if results{i}.isshielded==true
        rshield(i) = results{i}.layers{end}.r + results{i}.layers{end}.dhi;
        rpack(i) = results{i}.layers{end-1}.r + results{i}.layers{end-1}.dhi;
    else
        rpack(i) = results{i}.layers{end}.r + results{i}.layers{end}.dhi;
    end
    if results{i}.dual_aperture==true
        dualsep(i) = results{i}.calc_dualsep;
    end
    for j=1:results{i}.Nl
        if results{i}.layers{j}.isshield==false
            for k=1:results{i}.layers{j}.Nb
                [~,y] = results{i}.layers{j}.blocks{k}.create_surface(1);
                h = 2*max(y);
                if h>hc(i); hc(i)=h; end
            end
        end
    end
end

% write
fwrite(fid,'$R_{aperture}$ '); 
for i=1:length(results)
    fwrite(fid,['& ',num2str(raperture(i)*1000,'%6.2f'),' [$mm$]']);
end
fwrite(fid,'\\'); fprintf(fid,'\n');

% write
fwrite(fid,'$R_{shield}$ '); 
for i=1:length(results)
    fwrite(fid,['& ',num2str(rshield(i)*1000,'%6.2f'),' [$mm$]']);
end
fwrite(fid,'\\'); fprintf(fid,'\n');

% write
fwrite(fid,'$\ell_{dualsep}$ '); 
for i=1:length(results)
    fwrite(fid,['& ',num2str(dualsep(i)*1000,'%6.2f'),' [$mm$]']);
end
fwrite(fid,'\\'); fprintf(fid,'\n');

% write
fwrite(fid,'$w_{coils}$ '); 
for i=1:length(results)
    fwrite(fid,['& ',num2str(1000*(dualsep(i)+2*rpack(i)),'%6.2f'),' [$mm$]']);
end
fwrite(fid,'\\'); fprintf(fid,'\n');

% write
fwrite(fid,'$h_{coils}$ '); 
for i=1:length(results)
    fwrite(fid,['& ',num2str(1000*hc(i),'%6.2f'),' [$mm$]']);
end
fwrite(fid,'\\'); fprintf(fid,'\n');
fwrite(fid,'\hline'); fprintf(fid,'\n');
fwrite(fid,'\hline'); fprintf(fid,'\n');

%% Stored energy
if report==true; disp('Calculating stored energy ...'); end
% calculate
Estored = zeros(1,length(results));
for i=1:length(results)
    Estored(i) = results{i}.calc_E;
end

% write
fwrite(fid,'Estored '); 
for i=1:length(results)
    fwrite(fid,['& ',num2str(Estored(i)*1e-6,'%6.2f'),' [$MJ/m$]']);
end
fwrite(fid,'\\'); fprintf(fid,'\n');
fwrite(fid,'\hline'); fprintf(fid,'\n');
fwrite(fid,'\hline'); fprintf(fid,'\n');

%% end document
fwrite(fid,'\end{tabular}'); fprintf(fid,'\n');

% end document
fwrite(fid,'\end{document}'); fprintf(fid,'\n');

% done and close close
disp('Done');
fclose(fid);

end