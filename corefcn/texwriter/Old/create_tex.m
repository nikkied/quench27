function create_tex(mymodel,path,name)
% display
if mymodel.report == true; disp('Generating LaTeX Report ...'); end

% create directory
if exist(path,'dir')~=7
    mkdir(path);
end

% create file (with writing rights)
fid = fopen([path,'/',name,'.tex'], 'w');

% header
fwrite(fid,'\documentclass[10pt,a4paper,onecolumn]{article}'); fprintf(fid,'\n');

% language settings
fwrite(fid,'\usepackage[latin1]{inputenc}'); fprintf(fid,'\n');
fwrite(fid,'\usepackage[english]{babel}'); fprintf(fid,'\n');

% font cmbright
fwrite(fid,'\usepackage[T1]{fontenc}'); fprintf(fid,'\n');
fwrite(fid,'\usepackage{amssymb,amsfonts}'); fprintf(fid,'\n');
fwrite(fid,'\usepackage{cmbright}'); fprintf(fid,'\n');

% checkmarks
fwrite(fid,'\usepackage{pifont}'); fprintf(fid,'\n');
fwrite(fid,'\newcommand{\cmark}{\ding{51}}'); fprintf(fid,'\n');
fwrite(fid,'\newcommand{\xmark}{\ding{55}}'); fprintf(fid,'\n');

% margins 1.5 cm
fwrite(fid,'\usepackage[left=1.2cm,right=1.2cm,top=1.2cm,bottom=1.2cm]{geometry}'); fprintf(fid,'\n');

% tikz plot settings
fwrite(fid,'\usepackage{tikz,pgfplots}'); fprintf(fid,'\n');
fwrite(fid,'\pgfplotsset{compat=newest}'); fprintf(fid,'\n');
fwrite(fid,'\pgfplotsset{x tick label style={/pgf/number format/fixed}}'); fprintf(fid,'\n');
fwrite(fid,'\pgfplotsset{y tick label style={/pgf/number format/fixed}}'); fprintf(fid,'\n');
fwrite(fid,'\usetikzlibrary{plotmarks}'); fprintf(fid,'\n');

% begin document
fwrite(fid,'\begin{document}'); fprintf(fid,'\n');

% title
fwrite(fid,['\title{',mymodel.name,'}']); fprintf(fid,'\n');
fwrite(fid,'\date{Generated on \today}'); fprintf(fid,'\n');
fwrite(fid,'\author{J. van Nugteren, G. de Rijk\\ CERN, TE-department, EUCARD II}'); fprintf(fid,'\n');
fwrite(fid,'\maketitle'); fprintf(fid,'\n');

% introduction
%fwrite(fid,'\textit{Results from the optimization ...}'); fprintf(fid,'\n');

% create plot of magnetic field
f = figure('Position',[100,100,1337,667],'Visible','off');
ax = axes('Parent',f);
mymodel.plot(ax,'tight','beampipel','axisd','centerr','surfd');
matlab2tikz([path,'/FieldMag.tex'],'figurehandle',f,'width','\14cm','relativePngPath','.','showInfo',false);
fwrite(fid,'\begin{figure}[htb]'); fprintf(fid,'\n');
fwrite(fid,'\centering'); fprintf(fid,'\n');
fwrite(fid,'\include{FieldMag}'); fprintf(fid,'\n');
fwrite(fid,'\caption{The magnitude of the magnetic field in and around the coil in Tesla.}'); fprintf(fid,'\n');
fwrite(fid,'\end{figure}'); fprintf(fid,'\n');
delete(f);

% Field harmonics
tBncen = mymodel.table('Bncen');
tBnrem = mymodel.table('Bnrem');
fwrite(fid,'\begin{table}[htb]'); fprintf(fid,'\n');
fwrite(fid,'\centering'); fprintf(fid,'\n');
fwrite(fid,'\caption{Contribution to the central normal source constants.}'); fprintf(fid,'\n');
tBncen.toLatex(fid,'tight');
fwrite(fid,'\end{table}'); fprintf(fid,'\n');

%fwrite(fid,'\begin{table}[htb]'); fprintf(fid,'\n');
%fwrite(fid,'\centering'); fprintf(fid,'\n');
%fwrite(fid,'\caption{Central skew harmonics per coil block.}'); fprintf(fid,'\n');
%tAncen.toLatex(fid);
%fwrite(fid,'\end{table}'); fprintf(fid,'\n');

fwrite(fid,'\begin{table}[htb]'); fprintf(fid,'\n');
fwrite(fid,'\centering'); fprintf(fid,'\n');
fwrite(fid,'\caption{Contribution to the remote normal source constants calculated at the center of the coil system.}'); fprintf(fid,'\n');
tBnrem.toLatex(fid,'tight');
fwrite(fid,'\end{table}'); fprintf(fid,'\n');

%fwrite(fid,'\begin{table}[htb]'); fprintf(fid,'\n');
%fwrite(fid,'\centering'); fprintf(fid,'\n');
%fwrite(fid,'\caption{Remote skew harmonics per coil block.}'); fprintf(fid,'\n');
%tAnrem.toLatex(fid);
%fwrite(fid,'\end{table}'); fprintf(fid,'\n');

% Geometrical data on block coils
[table] = mymodel.table('geob');
if size(table,1)>1
    fwrite(fid,'\begin{table}[htb]'); fprintf(fid,'\n');
    fwrite(fid,'\centering'); fprintf(fid,'\n');
    fwrite(fid,'\caption{Positioning of the \emph{block} coils (note that the cross sectional area A takes the symmetry into account).}'); fprintf(fid,'\n');
    table.toLatex(fid,'tight');
    fwrite(fid,'\end{table}'); fprintf(fid,'\n');
end

% geometrical data on sector coils
[table] = mymodel.table('geoc');
if size(table,1)>1
    fwrite(fid,'\begin{table}[htb]'); fprintf(fid,'\n');
    fwrite(fid,'\centering'); fprintf(fid,'\n');
    fwrite(fid,'\caption{Positioning of the \emph{sector} coils (note that the cross sectional area A takes the symmetry into account).}'); fprintf(fid,'\n');
    table.toLatex(fid,'tight');
    fwrite(fid,'\end{table}'); fprintf(fid,'\n');
end

% create plot of loadlines
f = figure('Position',[100,100,800,500],'Visible','off');
ax = axes('Parent',f);
mymodel.plot(ax,'loadlines');
matlab2tikz([path,'/LoadLines.tex'],'figurehandle',f,'width','\14cm','height','\6cm','relativePngPath','.','showInfo',false);
fwrite(fid,'\begin{figure}[htb]'); fprintf(fid,'\n');
fwrite(fid,'\centering'); fprintf(fid,'\n');
fwrite(fid,'\include{LoadLines}'); fprintf(fid,'\n');
fwrite(fid,'\caption{Plot showing the loadlines with respect to the critical surfaces of the superconductor(s).}'); fprintf(fid,'\n');
fwrite(fid,'\end{figure}'); fprintf(fid,'\n');
delete(f);

% conductor data
table = mymodel.table('current');
fwrite(fid,'\begin{table}[htb]'); fprintf(fid,'\n');
fwrite(fid,'\centering'); fprintf(fid,'\n');
fwrite(fid,'\caption{Current densities and operating points of blocks.}'); fprintf(fid,'\n');
table.toLatex(fid,'tight');
fwrite(fid,'\end{table}'); fprintf(fid,'\n');

% superconductor settings
table = mymodel.table('sc');
fwrite(fid,'\begin{table}[htb]'); fprintf(fid,'\n');
fwrite(fid,'\centering'); fprintf(fid,'\n');
fwrite(fid,'\caption{Superconductor settings per layer.}'); fprintf(fid,'\n');
table.toLatex(fid,'tight');
fwrite(fid,'\end{table}'); fprintf(fid,'\n');

% force table
table = mymodel.table('force');
fwrite(fid,'\begin{table}[htb]'); fprintf(fid,'\n');
fwrite(fid,'\centering'); fprintf(fid,'\n');
fwrite(fid,'\caption{Forces on the coil on the second quadrant.}'); fprintf(fid,'\n');
table.toLatex(fid,'tight');
fwrite(fid,'\end{table}'); fprintf(fid,'\n');

% end document
fwrite(fid,'\end{document}'); fprintf(fid,'\n');

% close
fclose(fid);

% display
if mymodel.report == true; disp('Done'); end
end

