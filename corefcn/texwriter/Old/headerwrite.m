function fid = headerwrite(path,name)

% create directory
if exist(path,'dir')~=7
    mkdir(path);
end

% create file (with writing rights)
fid = fopen([path,filesep,name,'.tex'],'w');

% header
texwrite(fid,'\documentclass[10pt,a4paper,onecolumn]{report}');
texwrite(fid,'');

% graphicx package
texwrite(fid,'\usepackage{graphicx}');

% language settings
texwrite(fid,'\usepackage[latin1]{inputenc}');
texwrite(fid,'\usepackage[english]{babel}'); 
texwrite(fid,'');

% font cmbright
texwrite(fid,'\usepackage[T1]{fontenc}');
texwrite(fid,'\usepackage{amssymb,amsfonts}');
texwrite(fid,'\usepackage{cmbright}');
texwrite(fid,'');

% checkmarks
texwrite(fid,'\usepackage{pifont}');
texwrite(fid,'\newcommand{\cmark}{\ding{51}}');
texwrite(fid,'\newcommand{\xmark}{\ding{55}}');
texwrite(fid,'');

% margins 1.5 cm
texwrite(fid,'\usepackage[left=1.2cm,right=1.2cm,top=1.2cm,bottom=1.2cm]{geometry}');
texwrite(fid,'');

% title formatting
texwrite(fid,'\usepackage{titlesec}');
texwrite(fid,'\titleformat{\chapter}{\LARGE\bfseries}{Chapter \thechapter}{1em}{}');
texwrite(fid,'\titleformat{\section}{\large\bfseries}{\thesection}{1em}{}');
texwrite(fid,'\titleformat{\subsection}{\normalsize\bfseries}{\thesubsection}{1em}{}');
texwrite(fid,'');

% bookmarks
texwrite(fid,'\usepackage[bookmarks=true,bookmarksnumbered=true]{hyperref}');
texwrite(fid,'');

% tikz plot settings
texwrite(fid,'\usepackage{tikz,pgfplots}');
texwrite(fid,'\pgfplotsset{compat=newest}');
texwrite(fid,'\pgfplotsset{x tick label style={/pgf/number format/fixed}}');
texwrite(fid,'\pgfplotsset{y tick label style={/pgf/number format/fixed}}');
texwrite(fid,'\usetikzlibrary{plotmarks}');
texwrite(fid,'\usepackage[maxfloats=40]{morefloats}');
texwrite(fid,'');

% enable \floatbarrier command for clearwrite
texwrite(fid,'\usepackage{placeins}');

% begin document
texwrite(fid,'\begin{document}');
texwrite(fid,'');



end

