function figurewrite( fid,fpath,figname,mode,fh,capstr )

% setup
foldername = [mode,'figs'];
tffolder = [fpath,filesep,foldername];
if ~exist(tffolder,'dir'); mkdir(tffolder); end;

% get caption from titlestring
if strcmp(capstr,'usetitle')
    ha = findobj(fh,'Type','axes');
    for i=1:length(ha)
        ht = get(ha(i),'Title'); str = get(ht,'String');
        if ~isempty(str); 
            capstr = str; 
            title(ha(i),[]);
            break; 
        end
    end
end

% check mode
if strcmp(mode,'tikz')
    % export figure
    matlab2tikz([tffolder filesep figname '.tex'],'figurehandle',fh,'Standalone',false,'width','14cm','showInfo',false);
    
    % figure
    texwrite(fid,'\begin{figure}[hp]');
    texwrite(fid,'  \centering');
    texwrite(fid,'  \input{./',foldername,'/',figname,'.tex}');
    texwrite(fid,'  \caption{',capstr,'}');
    texwrite(fid,'  \label{fig:',figname,'}');
    texwrite(fid,'\end{figure}');
    texwrite(fid,'');
    
end

% check mode
if strcmp(mode,'pdf')
    % export figure
    export_fig(fh,[tffolder filesep figname '.pdf'],'-pdf','-r300');
    
    % figure
    texwrite(fid,'\begin{figure}[hp]');
    texwrite(fid,'  \centering');
    texwrite(fid,'  \includegraphics[width=1.0\textwidth]{./',foldername,'/',figname,'.pdf}');
    texwrite(fid,'  \caption{',capstr,'}');
    texwrite(fid,'  \label{fig:',figname,'}');
    texwrite(fid,'\end{figure}');
    texwrite(fid,'');
    
end

% close figure
close(fh);

end

