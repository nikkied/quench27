function [ fid ] = texwrite( fid,varargin )

str = [];
for i=1:length(varargin)
    str = [str,varargin{i}]; %#ok
end

fwrite(fid,str); fprintf(fid,'\n');
end

