function texcompile( path,name )

switch computer('arch')
    case {'win32','win64'}
        disp(['Compiling ',name]);
        drive = path(1);
        
        ch = system(['chdir[/',drive,'] & cd "',path,'" & pdflatex "',path,filesep,name,'.tex"']);
        if ch~=0; error(['pdflatex exited with following error code',ch]); end
    
    otherwise; error('Operating not supported yet');
end

end

