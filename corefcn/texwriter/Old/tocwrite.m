function tocwrite( fid )

% table of contents
texwrite(fid,'\addcontentsline{toc}{chapter}{Table of Contents}');
texwrite(fid,'\tableofcontents');
texwrite(fid,'');

end

