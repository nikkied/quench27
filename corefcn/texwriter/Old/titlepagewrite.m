function titlepagewrite(fid,path)

figurepath = [path,filesep,'figures'];

% create directory
if exist(figurepath,'dir')~=7
    mkdir(figurepath);
end

% copy logos
% location of m-file
fname = mfilename('fullpath');

% reduce to path
[mainpath,~,~] = fileparts(fname);
logodir = [mainpath,filesep,'figures'];

% copy
copyfile([logodir,filesep,'utlogo.jpg'],[figurepath,filesep,'utlogo.jpg'])
copyfile([logodir,filesep,'cernlogo.png'],[figurepath,filesep,'cernlogo.png'])
copyfile([logodir,filesep,'titlepage.jpg'],[figurepath,filesep,'titlepage.jpg'])

% write
texwrite(fid,'\begin{titlepage}');
texwrite(fid,'\begin{center}');

texwrite(fid,'\begin{Huge}');
texwrite(fid,'\textbf{Technical Report -- CERN\\[0.4cm]}');
texwrite(fid,'\end{Huge}');

texwrite(fid,'\begin{LARGE}');
texwrite(fid,'Quench Scenarios for the Compass Magnet System \\[0.4cm]');
texwrite(fid,'\end{LARGE}');

texwrite(fid,'\vspace{0.4cm}');

texwrite(fid,'\textit{Studies performed in the context of an internship at CERN, Physics Department, Atlas Magnet Team.\\}');

texwrite(fid,'\vspace{0.4cm}');

texwrite(fid,'\textit{\today\\}');

texwrite(fid,'\vspace{0.4cm}');

texwrite(fid,'\textit{Author -- B. van Nugteren}');

texwrite(fid,'\vspace{0.4cm}');

texwrite(fid,'\textit{Supervised by -- H.H.J. ten Kate and A. Dudarev}');

texwrite(fid,'\vspace{0.4cm}');

texwrite(fid,'\begin{figure}[h!]');
texwrite(fid,'	\centering');
texwrite(fid,'	\includegraphics[width=0.8\textwidth]{./figures/titlepage}');
texwrite(fid,'\end{figure}');

texwrite(fid,'\end{center}');

% CERN and twente Logo
texwrite(fid,'\begin{figure}[b!]');
texwrite(fid,'	\flushright	');
texwrite(fid,'	\includegraphics[width=0.5\textwidth]{./figures/utlogo}');
texwrite(fid,'	\hspace{6.0cm}	');
texwrite(fid,'	\includegraphics[width=0.15\textwidth]{./figures/cernlogo}');
texwrite(fid,'\end{figure}');

texwrite(fid,'\end{titlepage}');


end

