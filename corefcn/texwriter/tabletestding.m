fclose all;

tex = texwriter;
tex.path = [cd filesep 'tabletest'];
tex.name = 'tabletest';
tex.documentclass = 'standalone';

tex.header;
% tex.head('Dit is H1',0);

matrix = 10* rand(35,35); matrix = round( matrix*10 ) /10;
matrix = num2cell(matrix); 
for i = 1:size(matrix,1) 
    matrix(i,i) = {'test'}; 
end

lab = {'row 1', 'row 2', 'row 3', 'row 4', 'row 5','row 1', 'row 2', 'row 3', 'row 4', 'row 5','row 1', 'row 2', 'row 3', 'row 4', 'row 5','row 1', 'row 2', 'row 3', 'row 4', 'row 5','row 1', 'row 2', 'row 3', 'row 4', 'row 5','row 1', 'row 2', 'row 3', 'row 4', 'row 5','row 1', 'row 2', 'row 3', 'row 4', 'row 5'}; %columnLabels = {'col 1', 'col 2', 'col 3', 'col 4','col 5'};

matrix = [{''},lab;lab',matrix];

m = ones(size(matrix)) * false;
m(1,:) = true; n = m*90;
m(:,1) = true;

tex.table(matrix,'standalone','true','size','small','bold',m,'rotate',n);


% matrix = [1.5 1.764; 3.523 0.2];
% rowLabels = {'row 1', 'row 2'};
% columnLabels = {'col 1', 'col 2'};
% input = {matrix,tex.fid,'rowLabels', rowLabels, 'columnLabels', columnLabels};
% tex.fid = matrix2latex(input{:});
  

tex.footer;
tex.compile;
