function rect_legend(h,str,varargin)

for n=1:length(h)
h(n)=plot(nan,nan,'s','markeredgecolor',get(h(n),'edgecolor'),'markerfacecolor',get(h(n),'facecolor'));
end
legend(h,str,varargin{:})

end