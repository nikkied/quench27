#include <mex.h>
#include <math.h>

using namespace std;

/* --- MATLAB Gateway Function --- */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    if(nrhs!=4)mexErrMsgTxt("4 inputs required");
    
    double *X = (double*)mxGetPr(prhs[0]);
    double *Y = (double*)mxGetPr(prhs[1]);
    double *Xi = (double*)mxGetPr(prhs[2]);
    double Yextrap = (double)mxGetScalar(prhs[3]);
    
    int nX = (int)mxGetN(prhs[0]) * (int)mxGetM(prhs[0]);
    int nY = (int)mxGetN(prhs[1]) * (int)mxGetM(prhs[1]);
    if(nX!=nY)mexErrMsgTxt("length(X) ~= length(Y)");
        
    int nXi = (int)mxGetN(prhs[2]);
    int mXi = (int)mxGetM(prhs[2]);
    int totXi = nXi * mXi;
    
    double Xmin = X[0];
    double Xmax = X[nX-1];
    double Xrange = Xmax - Xmin;
        
    /* Create output array */
    plhs[0] = mxCreateDoubleMatrix(mXi, nXi, mxREAL);
    double *Yi = (double*)mxGetPr(plhs[0]);
    
    /* walk over Xi */
    for(int idx = 0; idx<totXi; idx++){
        /* middle */
        if(Xi[idx]>Xmin && Xi[idx]<Xmax){
            double Xrel = (Xi[idx] - Xmin)/Xrange;
            int Xidx = int(Xrel * double(nX-1));
            
            double X1 = X[Xidx];
            double X2 = X[Xidx+1];
            
            double Y1 = Y[Xidx];
            double Y2 = Y[Xidx+1];
            
            Yi[idx] = (Xi[idx]-X1) * ((Y2-Y1)/(X2-X1)) + Y1; 
        }
        
        /* edges */
        if(Xi[idx]==Xmin)Yi[idx]=Y[0];
        if(Xi[idx]==Xmax)Yi[idx]=Y[nY-1];
        
        /* outside (no real extrapolation for now) */
        if(Xi[idx]<Xmin || Xi[idx]>Xmax)Yi[idx]=Yextrap;
    }
    
}