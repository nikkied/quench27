#include <mex.h>
#include <math.h>

#ifdef _WIN32 || _WIN64
#include <ppl.h>
#include <windows.h> 
using namespace Concurrency;
#else
#include <omp.h>
#endif

/* --- MATLAB Gateway Function --- */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    if(nrhs!=4)mexErrMsgTxt("4 inputs required");
    
    double *X = (double*)mxGetPr(prhs[0]);
    double *Y = (double*)mxGetPr(prhs[1]);
    double *Xi = (double*)mxGetPr(prhs[2]);
    double Yextrap = (double)mxGetScalar(prhs[3]);
    
    int nX = (int)mxGetN(prhs[0]) * (int)mxGetM(prhs[0]);
    int nY = (int)mxGetN(prhs[1]) * (int)mxGetM(prhs[1]);
    if(nX!=nY)mexErrMsgTxt("length(X) ~= length(Y)");
        
    int nXi = (int)mxGetN(prhs[2]);
    int mXi = (int)mxGetM(prhs[2]);
    int totXi = nXi * mXi;
    
    double Xmin = X[0];
    double Xmax = X[nX-1];
    double Xrange = Xmax - Xmin;
        
    /* Create output array */
    plhs[0] = mxCreateDoubleMatrix(mXi, nXi, mxREAL);
    double *Yi = (double*)mxGetPr(plhs[0]);
    
    #ifdef _WIN32 || _WIN64
    /* lower priorty prevents system from freezing during calculation*/
    SetPriorityClass(GetCurrentProcess(),BELOW_NORMAL_PRIORITY_CLASS);
    #endif
    
    /* walk over Xi */
    #ifdef _WIN32 || _WIN64
    parallel_for(0,totXi,[&](int idx){
    #else
    #pragma omp parallel for schedule(dynamic, 2000)
    for(int idx=0;idx<totXi;idx++){
    #endif
        /* middle */
        if(Xi[idx]>Xmin && Xi[idx]<Xmax){
            double Xrel = (Xi[idx] - Xmin)/Xrange;
            int Xidx = int(Xrel * double(nX-1));
            
            double X1 = X[Xidx];
            double X2 = X[Xidx+1];
            
            double Y1 = Y[Xidx];
            double Y2 = Y[Xidx+1];
            
            Yi[idx] = (Xi[idx]-X1) * ((Y2-Y1)/(X2-X1)) + Y1; 
        }
        
        /* extrapolate higher using linear extrapolation */
        if(Xi[idx]>Xmax && Yextrap==1){
            double X1 = X[nX-2];
            double X2 = X[nX-1];
            
            double Y1 = Y[nX-2];
            double Y2 = Y[nX-1];
            
            double dX = X2-X1;
            double dY = Y2-Y1;
            
            double b = dY/dX;
            
            Yi[idx] = Y2 + b*(Xi[idx]-X2);
        }
        
        /* extrapolate lower using linear extrapolation */
        if(Xi[idx]<Xmin && Yextrap==1){
            double X1 = X[0];
            double X2 = X[1];
            
            double Y1 = Y[0];
            double Y2 = Y[1];
            
            double dX = X2-X1;
            double dY = Y2-Y1;
            
            double b = dY/dX;
            
            Yi[idx] = Y1 - b*(X1-Xi[idx]);
        }
        
        /* edges */
        if(Xi[idx]==Xmin)Yi[idx]=Y[0];
        if(Xi[idx]==Xmax)Yi[idx]=Y[nY-1];
        
        /* outside (no real extrapolation for now) */
        if((Xi[idx]<Xmin || Xi[idx]>Xmax) && Yextrap==0)Yi[idx]=0;
    #ifdef _WIN32 || _WIN64
    });
    #else
    }
    #endif
   
    
    #ifdef _WIN32 || _WIN64
    /* set priority back to normal (who knows what will happen if we don't) */
    SetPriorityClass(GetCurrentProcess(),NORMAL_PRIORITY_CLASS);
    #endif
    
}