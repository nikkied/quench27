function hrtn = jprintf(dev,str,varargin)

% mlock;
persistent JavaBoxPos
persistent JavaBoxIncr

% initialise
if isempty(JavaBoxIncr); JavaBoxIncr=0; end

% reinitialise
if ~isempty(varargin)
    if strcmp(varargin{1},'0') || strcmp(varargin,'O') || strcmp(varargin,'o') || strcmp(varargin,'Reset') || strcmp(varargin,'reset'); JavaBoxIncr=0; end
    if strcmp(varargin{1},'0>>') || strcmp(varargin{1},'O>>') || strcmp(varargin{1},'o>>'); JavaBoxIncr=0; end
end

% add spaces to string
if ~isempty(str)
    % add increment
    space = char(32*ones(1,JavaBoxIncr));
    str = [space,str];
    str = sprintf(str);
    
    % If device specifier is positive number,
    % then do a regular built-in fprintf:
    if dev > 0
        count = fprintf(dev,str);
        
        % Return an optional count of characters.
        if nargout
            hrtn = count;
        end
        
        % Else if its a negative number, then
        % we must create or update a java window:
    elseif dev < 0
        
        % Get the name of this java window:
        dev = abs(dev);
        namestr = ['JavaBox',num2str(dev)];
        
        % See if the java window already exists:
        text = getappdata(0,namestr);
        if isempty(text)
            
            % The window doesn't exist, so we need to create it:
            import java.awt.*
            import javax.swing.*
            
            % Create frame object:
            frame = javax.swing.JFrame(['Quench Output: ',num2str(dev)]);
            frame.setSize(500,900)
            
            % Stagger position so frames don't overlap.  Restart
            % if the new frame will be off the screen.
            ScreenDim = get(0,'ScreenSize');
            if isempty(JavaBoxPos)
                JavaBoxPos = [0 0];
            else
                JavaBoxPos = JavaBoxPos + [30 30];
                if any(JavaBoxPos+[600 600] > ScreenDim(3:4))
                    JavaBoxPos = [0 0];
                end
            end
            frame.setLocation(JavaBoxPos(1),JavaBoxPos(2));
            
            % Create text object and set some default properties:
            text = javax.swing.JTextArea;
            c = listfonts;
            if ismember('Bitstream Vera Sans Mono',c)
                f = java.awt.Font('Bitstream Vera Sans Mono', 0, 12);
            else
                f = java.awt.Font('DialogInput', 0, 12);
            end
            text.setFont(f);
            set(text,'Background',[1,1,1]);
            text.setCursor(java.awt.Cursor(2));  %text cursor
            
            % Create scroller object with text:
            scroller = javax.swing.JScrollPane(text);
                
            % Add the widgets to the frame and make visible:
            frame.getContentPane.add(BorderLayout.CENTER,scroller);
            %frame.getContentPane.add(BorderLayout.NORTH,mymenu);
            frame.show
            
            % Add the input text to the java TextArea and position
            % the cursor (caret) at the bottom of the window::
            text.append(str);
            text.setCaretPosition(text.getDocument.getLength);
            
            % Enable some handy features:
            h = handle(text, 'CallbackProperties');
            set(h,'dragEnabled',true);
            
            % Store the text object in appdata so we can find it later.
            % Also define a callback to remove appdata when the box is closed.
            set(text,'name',namestr);
            set(h,'AncestorRemovedCallback','rmappdata(0,get(gcbo,''name''))');
            setappdata(0,namestr,text);
            
        else
            
            % Add the input text to the java TextArea and position
            % the cursor (caret) at the bottom of the window:
            text.append(str);
            text.setCaretPosition(text.getDocument.getLength);
            
        end
        
        % Return an optional java TextArea object.
        if nargout
            hrtn = text;
        end
        
        % draw
        %drawnow; 
        %pause(0.001);
    end
    
end

% increment for next time
if ~isempty(varargin)
    if strcmp(varargin{1},'>>'); JavaBoxIncr=JavaBoxIncr+2; end
    if strcmp(varargin{1},'>>>>'); JavaBoxIncr=JavaBoxIncr+4; end
    if strcmp(varargin{1},'>>>>>>'); JavaBoxIncr=JavaBoxIncr+6; end
    if strcmp(varargin{1},'<<'); JavaBoxIncr=JavaBoxIncr-2; end
    if strcmp(varargin{1},'<<<<'); JavaBoxIncr=JavaBoxIncr-4; end
    if strcmp(varargin{1},'<<<<<<'); JavaBoxIncr=JavaBoxIncr-6; end
    if strcmp(varargin{1},'0>>') || strcmp(varargin{1},'O>>') || strcmp(varargin{1},'o>>'); JavaBoxIncr=JavaBoxIncr+2; end
    if JavaBoxIncr<0; JavaBoxIncr=0; end
end

end
