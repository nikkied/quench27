function [line] = mat2line(matrix)
% create alternating false-true vector
nrows = size(matrix,1);
alt = [false;true];
alt = repmat(alt,ceil(nrows/2),1);
alt = alt(1:nrows);

% flip even rows
matrix(alt,:) = fliplr(matrix(alt,:));

% reshape
line = reshape(matrix',1,[]);
end
