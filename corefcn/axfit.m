function [ ax,xlim,ylim ] = axfit( ax,varargin )
%AXFIT sets XLim and YLim to fit the data in ax exactly.
%Varargin is used to enter their two respective margins in percents

% analyse input
margin = [0,0];
if ~isempty(varargin)
    if numel(varargin{:}) == 2
        margin = varargin{:};
    else
        error('numel(varagin) ~= 2')
    end
end

[ax,xlim] = xfit(ax, margin(1) );
[ax,ylim] = yfit(ax, margin(2) );

end

