function [ mycond ] = OI_WiC_thick()
% This is a modified Wire-in-Channel conductor by Oxford Instruments, which is used in the HMC Compass' Solenoid. The original is listed on their site:
% http://www.oxford-instruments.com/products/superconducting-magnets-and-wire/superconducting-wire/nb-ti-wire-in-channel-superconducting-wire
% They added copper to it up to these dimensions.

% create conductor
mycond = Create_Conductor();
set(mycond,'name','OI WiC thick');

% dimensions
set(mycond,'axL',3.05E-3,'radL',1.79E-3);
set(mycond,'radinsulationthickness',2E-4);
set(mycond,'axinsulationthickness',5E-5); 

% composition non-insulation
set(mycond,'ScFactor',1/12.3,'ScMaterial','Niobium Titanium');
set(mycond,'MatrixFactor',11.3/12.3,'MatrixMaterial','OFHC Copper RRR150');
set(mycond,'StabFactor',0,'StabMaterial','None');
set(mycond,'VoidFactor',0);

% insulation
set(mycond,'radinsulationmat',{'Enamel'});
set(mycond,'axinsulationmat',{'Enamel'});

% factory data
set(mycond,'critsurf_data',[2 3 4; 2000 1550 1210]);