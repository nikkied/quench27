function [mycond] = OI_WiC_thin()
% OI_WiC_thin is a Wire-in-Channel conductor by Oxford Instruments, which is used in the HMC Compass' compensation coils. It is listed on their site:
% http://www.oxford-instruments.com/products/superconducting-magnets-and-wire/superconducting-wire/nb-ti-wire-in-channel-superconducting-wire

% create conductor
mycond = Create_Conductor();
set(mycond,'name','OI WiC thin');

% dimensions
set(mycond,'axL',2.286E-3,'radL',1.524E-3);
set(mycond,'radinsulationthickness',2.56E-4);
set(mycond,'axinsulationthickness',2.74E-4); 

% composition non-insulation
set(mycond,'ScFactor',1/9,'ScMaterial','Niobium Titanium');
set(mycond,'MatrixFactor',8/9,'MatrixMaterial','OFHC Copper RRR150');
set(mycond,'StabFactor',0,'StabMaterial','None');
set(mycond,'VoidFactor',0);

% insulation
set(mycond,'radinsulationmat',{'Enamel'});
set(mycond,'axinsulationmat',{'Enamel'});

% factory data
set(mycond,'critsurf_data',[2 3 4; 2000 1550 1210]);