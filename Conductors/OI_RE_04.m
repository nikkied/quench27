function [ mycond ] = OI_RE_04()
% This is a 0.4mm thick round enamel wire by Oxford Instruments. It is used
% for the ShimCoils in Compass. It is modeled as a squared wire with a void
% fraction of pi/4 and insulation thickness equal to the real insulation
% thickness (not entirely realistic --> could be a bit thicker)

% create conductor
mycond = Create_Conductor();
set(mycond,'name','OI RE 04');

% dimensions
set(mycond,'axL',(0.4 - 0.036)*1E-3,'radL',(0.4 - 0.036)*1E-3);
set(mycond,'radinsulationthickness',0.036E-3);
set(mycond,'axinsulationthickness',0.036E-3);

% conductor settings
set(mycond,'MatrixFactor',1.4/2.4 * pi/4,'MatrixMaterial','OFHC Copper RRR100');
set(mycond,'ScFactor',1/2.4 * pi/4,'ScMaterial','Niobium Titanium');
set(mycond,'StabFactor',(1 - mycond.MatrixFactor - mycond.ScFactor),'StabMaterial','None'); %simulate nothingness around round wire in square unit cell
%set(mycond,'VoidFactor',(1 - mycond.MatrixFactor - mycond.ScFactor));

%insulation material
set(mycond,'radinsulationmat',{'Enamel'});
set(mycond,'axinsulationmat',{'Enamel'});

% factory data
set(mycond,'critsurf_data',[3 4 5 6 7; 194	167	140	113	86]);