function [ mycond ] = aluminium()
% This conductor is used to simulate the aluminium cilinders that are
% usually around / inside solenoids. Note that the dimensions must still be
% set.

% create conductor
mycond = Create_Conductor();
set(mycond,'name','Aluminium');

% conductor settings
set(mycond,'MatrixFactor',1,'MatrixMaterial','Aluminum 3003-F');
set(mycond,'ScFactor',0,'ScMaterial','None');
set(mycond,'StabFactor',0,'StabMaterial','None');
set(mycond,'VoidFactor',0);

end

