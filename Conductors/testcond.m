function [mycond] = testcond()
% This is a conductor used for testsimulations

% create conductor
mycond = Create_Conductor();
set(mycond,'name','testcond');

% dimensions
set(mycond,'axL',2.75E-3,'radL',1.5E-3);
set(mycond,'radinsulationthickness',5E-5);
set(mycond,'axinsulationthickness',2.5e-4); 

% composition
set(mycond,'ScFactor',1/11,'ScMaterial','Niobium Titanium');
set(mycond,'MatrixFactor',10/11,'MatrixMaterial','OFHC Copper RRR150');
set(mycond,'StabFactor',0,'StabMaterial','None');
set(mycond,'VoidFactor',0);

% insulation settings
set(mycond,'radinsulationmat',{'Enamel'});
set(mycond,'axinsulationmat',{'Enamel'});
