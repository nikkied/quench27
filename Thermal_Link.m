classdef Thermal_Link < hgsetget
    %% Thermal_Link Summary of this class goes here
    
    %% This Thermal link class works only if the coils that are linked have the same number of turns!
    
    %%
    
    %  This class thermally links two coils together. This can be used for
    %  coil formers.
    %  This class works only for cillidrical coils which are linked in the
    %  radial direction!
    %  Further more, the number of axial elements of one of the
    %  coils divided by the number of axial elements of the other most but
    %  an integer value!!!
    %  A THERMAL LINK CAN ONLY BE ADDED AFTER THE SYSTEM IS INITIALIZED!!!
    
    %% Properties of the class
    properties
        
        name = [];              % name of the thermal link
        linknr = [];            % number of the link
        
        coil_1 = [];            % dependency with coil 1 (for instance the SC coil)
        coil_2 = [];            % dependency with coil 2 (for instance the Coil Former)
        system = [];            % dependency with system
        
        LinkType = [];          % a link can either be radial of axial
        
        Tinterp = [];
        Tlink = [];             % 2 * links array with temperatures of the elements that are linked [K]
        Alink = [];             % 1 * links array with the areas that are connected [m^2]
        
        % thermal properties of a link
        krad = [];              % [W/m^2.K]
        kax = [];               % [W/m^2.K]
        Krad = [];              % [W/m]
        Kax = [];               % [W/m]
        Qlink= [];              % link heat transfer
                
%         radcoilinsthickness = [];% insulation surrounding the coil in rad direction 
%         radcoilinsmat = [];     % material of rad insulation
%         axcoilinsthickness = [];% insulation surrounding the coil in ax direction
%         axcoilinsmat = [];      % material of ax insulation
    
    end % end properties
    
    
    %% Methods of the class
    
    %% Add coils links
    methods
        function addcoils(self,coil1,coil2)
            % analyse input
            if isempty(coil1) || isempty(coil2)
                error(['an attempt was made to add a coil to the link ' self.name ', but no coil was supplied']);
            end
            
            % add coils to the links
            self.coil_1 = coil1;
            self.coil_2 = coil2;
            
            % add the link to all the coils.  {This works!!!}
            self.coil_1.tlinks = {self.coil_1.tlinks{:} self};
            self.coil_2.tlinks = {self.coil_2.tlinks{:} self};
            
            % add the link to the system.
            self.coil_1.system.tlinks = {self.coil_1.system.tlinks{:} self};
            
        end
    end
    
    %% Get the Relative Position, the Type and the Area of the linked coils
    
    methods
        function getStatics(self)
            
            % determine which coil has the biggest radius
            delta = self.coil_1.Rinner(1,1) - self.coil_2.Rinner(1,1);
            if delta > 1e-7 % Coil 1 is bigger than coil 2, so coil 1 is on the outside
                self.Alink = zeros(1,self.coil_1.nax);
                self.Alink(:) = ones(1,self.coil_1.nax) * 2 * pi * self.coil_1.Rinner(1,1) * self.coil_1.cond.axL;
                self.LinkType = 'Radial_12';
                
            elseif delta < -1e-7 % Coil 2 is biggen than coil 1, so coil 2 is on the outside
                self.Alink = zeros(1,self.coil_2.nax);
                self.Alink(:) = ones(1,self.coil_2.nax) * 2*pi*self.coil_2.Rinner(1,1)*self.coil_2.cond.axL;
                self.LinkType = 'Radial_21';
                
            end
            
            % determine which side of the coil is linked (in Z direction)
            if abs( self.coil_1.axrange(2) - self.coil_2.axrange(1) ) < 1e-4 % coil 1 is at the left side of coil 2
                for i = 1:self.coil_1.nrad
                    self.Alink(i,1) = pi * (self.coil_1.Router(i,self.coil_1.nax)^2 - self.coil_1.Rinner(i,self.coil_1.nax)^2) ;
                end
                self.LinkType = 'Axial_lr';
                
            elseif abs ( self.coil_1.axrange(1) - self.coil_2.axrange(2) ) < 1e-4 % coil 1 is at the right side of coil 2
                for i = 1:self.coil_1.nrad
                    self.Alink(i,1) = pi * (self.coil_1.Router(i,1)^2 - self.coil_1.Rinner(i,1)^2) ;
                end
                self.LinkType = 'Axial_rl';
                
            end
            
        end
    end
    
    %% Get temperature of the coil
    
    methods
        function getTemp(self)
            switch self.LinkType
                case 'Radial_12'
                    self.Tlink = zeros(2,self.coil_1.nax);
                    self.Tlink(1,:) = self.coil_1.T(1,:);
                    self.Tlink(2,:) = self.coil_2.T(end,:);
                    
                case 'Radial_21'
                    self.Tlink = zeros(2,self.coil_1.nax);
                    self.Tlink(1,:) = self.coil_1.T(end,:);
                    self.Tlink(2,:) = self.coil_2.T(1,:);
                    
                case 'Axial_lr'
                    self.Tlink = zeros(self.coil_1.nrad,2);
                    self.Tlink(:,1) = self.coil_1.T(:,end);
                    self.Tlink(:,2) = self.coil_2.T(:,1);
                                   
                case 'Axial_rl'
                    self.Tlink = zeros(self.coil_1.nrad,2);
                    self.Tlink(:,1) = self.coil_1.T(:,1);
                    self.Tlink(:,2) = self.coil_2.T(:,end);
                    
                otherwise; error('Linktype unknown');
            end
        end
    end
    
    %% Calculating interpolation array for k of the coil
    methods
        function calck(self)
            % create interpolation array
            self.Tinterp = 3:300;
            
            % check linktype
            switch self.LinkType
                % calculate k between adjacent coils in the radial direction
                case {'Radial_12','Radial_21'}
                    
                    rrad = zeros(1,length(self.Tinterp));
                    
                    % calculate and add contribution of coil 1
                    % 1/2 in front of all the lengths!
                    rrad = rrad + (1/2 .* self.coil_1.cond.radL) ./ self.coil_1.kazi; % half because the thermal link is in the middle of the element.
                    
                    % calculate and add contribution of conductor insulation
                    for i = 1:length(self.coil_1.cond.radinsulationmat);
                        if matprops(self.coil_1.cond.radinsulationmat{i},'TC',self.Tinterp) ~= 0
                            rrad = rrad + (1/2 .* self.coil_1.cond.radinsulationthickness(i)) ./ matprops(self.coil_1.cond.radinsulationmat{i},'TC',self.Tinterp);
                        end % end for
                    end
                    
                    % calculate and add contribution of coil insulation
                    for i = 1:length(self.coil_1.radinsulationmat);
                        if matprops(self.coil_1.radinsulationmat{i},'TC',self.Tinterp) ~= 0
                            rrad = rrad + (1/2 .* self.coil_1.radinsulationthickness(i)) ./ matprops(self.coil_1.radinsulationmat{i},'TC',self.Tinterp);
                        end % end for
                    end
                    
                    % calculate and add contribution of insulation surrounding the coil
                    for i = 1:length(self.coil_1.radcoilinsmat);
                        if matprops(self.coil_1.radcoilinsmat{i},'TC',self.Tinterp) ~= 0
                            rrad = rrad + (1/2 .* self.coil_1.radcoilinsthickness(i)) ./ matprops(self.coil_1.radcoilinsmat{i},'TC',self.Tinterp);
                        end % end for
                    end                    
                    
                    % calculate and add contribution of the coil 2
                    % 1/2 in front of all the lengths!
                    rrad = rrad + (1/2 .* self.coil_2.cond.radL) ./ self.coil_2.kazi; % half because of the thermal link is in the middel of the element
                    
                    % calculate and add contribution of conductor insulation
                    for i = 1:length(self.coil_2.cond.radinsulationmat);
                        if matprops(self.coil_2.cond.radinsulationmat{i},'TC',self.Tinterp) ~= 0
                            rrad = rrad + (1/2 .* self.coil_2.cond.radinsulationthickness(i)) ./ matprops(self.coil_2.cond.radinsulationmat{i},'TC',self.Tinterp);
                        end % end for
                    end
                    
                    % calculate and add contribution of coil insulation
                    for i = 1:length(self.coil_2.radinsulationmat);
                        if matprops(self.coil_2.radinsulationmat{i},'TC',self.Tinterp) ~= 0
                            rrad = rrad + (1/2 .* self.coil_2.radinsulationthickness(i)) ./ matprops(self.coil_2.radinsulationmat{i},'TC',self.Tinterp);
                        end % end for
                    end
                    
                    % calculate and add contribution of insulation surrounding the coil
                    for i = 1:length(self.coil_2.radcoilinsmat);
                        if matprops(self.coil_2.radcoilinsmat{i},'TC',self.Tinterp) ~= 0
                            rrad = rrad + (1/2 .* self.coil_2.radcoilinsthickness(i)) ./ matprops(self.coil_2.radcoilinsmat{i},'TC',self.Tinterp);
                        end % end for
                    end 
                    
                    % invert to obtain thermal conductivity, rather than thermal resistivity
                    self.krad =  1 ./ rrad; %[W/m^2.K]
                    
                    
                % calculate k between adjacent coils in the axial direction
                case {'Axial_lr','Axial_rl'}
                    rax = zeros(1,length(self.Tinterp));
                    
                    % calculate and add contribution of coil 1
                    % 1/2 in front of all the lengths!
                    rax = rax + (1/2 .* self.coil_1.cond.axL) ./ self.coil_1.kazi;
                    
                    % calculate and add contribution of conductor insulation
                    for i = 1:length(self.coil_1.cond.axinsulationmat);
                        if matprops(self.coil_1.cond.axinsulationmat{i},'TC',self.Tinterp) ~= 0
                            rax = rax + (1/2 .* self.coil_1.cond.axinsulationthickness(i)) ./ matprops(self.coil_1.cond.axinsulationmat{i},'TC',self.Tinterp);
                        end
                    end
                    
                    % calculate and add contribution of coil insulation
                    for i = 1:length(self.coil_1.axcoilinsmat);
                        if matprops(self.coil_1.axcoilinsmat{i},'TC',self.Tinterp)
                            rax = rax + (1/2 .* self.coil_1.axinsulationthickness(i)) ./ matprops(self.coil_1.axcoilinsmat{i},'TC',self.Tinterp);
                        end
                    end
                    
                    % calculate and add contribution of insulation surrounding the coil
                    for i = 1:length(self.coil_1.axcoilinsmat);
                        if matprops(self.coil_1.axcoilinsmat{i},'TC',self.Tinterp) ~= 0
                            rax = rax + (1/2 .* self.coil_1.axcoilinsthickness(i)) ./ matprops(self.coil_1.axcoilinsmat{i},'TC',self.Tinterp);
                        end % end for
                    end 
                    
                    % calculate and add contribution of coil 2
                    % 1/2 in front of all the lengths!
                    rax = rax + (1/2 .* self.coil_2.cond.axL) ./ self.coil_2.kazi;
                    
                    % calculate and add contribution of conductor insulation
                    for i = 1:length(self.coil_2.cond.axinsulationmat);
                        if matprops(self.coil_2.cond.axinsulationmat{i},'TC',self.Tinterp) ~= 0
                            rax = rax + (1/2 .* self.coil_2.cond.axinsulationthickness(i)) ./ matprops(self.coil_2.cond.axinsulationmat{i},'TC',self.Tinterp);
                        end
                    end
                    
                    % calculate and add contribution of coil insulation
                    for i = 1:length(self.coil_2.axinsulationmat);
                        if matprops(self.coil_2.axinsulationmat{i},'TC',self.Tinterp)
                            rax = rax + (1/2 .* self.coil_2.axinsulationthickness(i)) ./ matprops(self.coil_2.axinsulationmat{i},'TC',self.Tinterp);
                        end
                    end
                    
                    % calculate and add contribution of insulation surrounding the coil
                    for i = 1:length(self.coil_2.axcoilinsmat);
                        if matprops(self.coil_2.axcoilinsmat{i},'TC',self.Tinterp)
                            rax = rax + (1/2 .* self.coil_2.axinsulationthickness(i)) ./ matprops(self.coil_2.axcoilinsmat{i},'TC',self.Tinterp);
                        end
                    end
                    
                    % invert to obtain thermal conductivity, rather than thermal resistivity
                    self.kax =  1 ./ rax ; %[W/m^2.K]
                    
                otherwise; error('Linktype unknown');
            end % end if axial

        end % end function
    end % end methods
    
    %% Calculating K
    methods
        function calcK(self)
            % for radial link
            switch self.LinkType
                case {'Radial_12','Radial_21'}
                    TmeanR = self.Tlink(1:end-1,:) + diff(self.Tlink,1,1)/2; % [K]
                    self.Krad = lininterp1f_p(self.Tinterp,self.coil_1.krad,TmeanR,true) .* self.Alink; % [W/K]
                    
                    % for axial link
                case {'Axial_lr','Axial_rl'}
                    TmeanA = self.Tlink(:,1:end-1) + diff(self.Tlink,1,2)/2; % [K]
                    self.Kax = lininterp1f_p(self.Tinterp,self.coil_1.krad,TmeanA,true) .* self.Alink; % [W/K]
                    
                otherwise; error('Unknown linktype');
            end
            
        end % end function
    end % end methods
    
    %% Calculating Q
    methods
        function calcQ(self)
            % for radial link
            switch self.LinkType
                case {'Radial_12','Radial_21'}
                    self.Qlink = self.Krad .* diff(self.Tlink,1,1);
                    
                case {'Axial_lr','Axial_rl'}
                    self.Qlink = self.Kax .* diff(self.Tlink,1,2);
                    
                otherwise; error('Unknown linktype');
            end
            
        end % end function
    end % end methods
    
    %% add Q to coils
    methods
        function addQ(self)
            % add heat transfer coil 1
            switch self.LinkType
                case 'Radial_12'
                    self.coil_1.Qlink(1,:) = self.coil_1.Qlink(1,:) + self.Qlink;
                    self.coil_2.Qlink(end,:) = self.coil_2.Qlink(end,:) - self.Qlink;
                    
                case 'Radial_21'
                    self.coil_1.Qlink(end,:) = self.coil_1.Qlink(end,:) + self.Qlink;
                    self.coil_2.Qlink(1,:) = self.coil_2.Qlink(1,:) - self.Qlink;
                    
                case 'Axial_lr'
                    self.coil_1.Qlink(:,end) = self.coil_1.Qlink(:,end) + self.Qlink;
                    self.coil_2.Qlink(:,1) = self.coil_2.Qlink(:,1) - self.Qlink;
                    
                case 'Axial_rl'
                    self.coil_1.Qlink(:,1) = self.coil_1.Qlink(:,1) + self.Qlink;
                    self.coil_2.Qlink(:,end) = self.coil_2.Qlink(:,end) - self.Qlink;
                    
                otherwise; error('What kind of linktype is this ... ? D:');
            end
            
        end
    end
    
end % end class

